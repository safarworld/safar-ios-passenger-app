//
//  AppConstants.m
//  privMD
//
//  Created by Surender Rathore on 22/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AppConstants.h"

NSString *const kPMDPubNubPublisherKey      = @"pub-c-2733909f-bdbe-4ae6-85be-cbdbd232a738";
NSString *const kPMDPubNubSubcriptionKey    = @"sub-c-f3ac73b8-78d7-11e6-9195-02ee2ddab7fe";
NSString *const kPMDGoogleMapsAPIKey        = @"AIzaSyC993XnLVeydilXvZW_8LlBV4af1amyD6M"; // @"AIzaSyALANQycl0vxo9c-vsCA6tsP_d1Fe7yQ4g";
NSString *const kPMDServerGoogleMapsAPIKey  = @"AIzaSyCJQbB9brWUZ_HPk56l5zPz3-tCb46-BcI";
NSString *const kPMDFlurryId                = @"24VV3H2V2RPM6ZXG4G99";
NSString *const kPMDTestDeviceidKey         = @"C2A33350-D9CF-4A7E-8751-A36016838381";
NSString *const kPMDDeviceIdKey             = @"deviceid";
NSString *const kPMDStripeTestKey           = @"pk_test_xUUiN7SbzZXHct3wDrjTfC0l";
NSString *const kPMDStripeLiveKey           = @"pk_live_23DpWMrC1wXYjtP1hCk0cMb7";

#pragma mark - mark URLsn

#define BASE_IP  @"http://188.166.48.202/safar/"

NSString *BASE_URL                            = BASE_IP @"services_v1.php/";
NSString *BASE_URL_SUPPORT                    = BASE_IP;
NSString *BASE_URL_UPLOADIMAGE                = BASE_IP @"services_v1.php/uploadImage";
NSString *const   baseUrlForXXHDPIImage       = BASE_IP @"pics/xxhdpi/";
NSString *const   baseUrlForOriginalImage     = BASE_IP @"pics/";


NSString *const termsAndCondition             = @"http://188.166.48.202/safar/term&condition.html";//BASE_IP @"terms_of_use.php";
NSString *const privacyPolicy                 = @"http://188.166.48.202/safar/privacy&policy.html";//BASE_IP @"privacy_policy.php";
NSString *const itunesURL                     = @"itunes.apple.com/us/app/roadyo-on-demand-taxi-booking/id918849109?ls=1&mt=8";
NSString *const facebookURL                   = @"https://www.facebook.com/safarworld";
NSString *const websiteLabel                  = @"'www.safar.in'";
NSString *const websiteURL                    = @"http://www.safar.world";
NSString *const imgLinkForSharing             = @"http://188.166.48.202/safar/appimages/imgpsh_fullsize.png";


#pragma mark - Distance Km Or Miles
double   const  kPMDDistanceMetric            = 1608.34;
NSString *const kPMDDistanceParameter         = @"KM";
NSString *const kPMDMPHorKMPH                 = @"MPH";

#pragma  mark - Langauge Support 
BOOL const kLanguageSupport                   = false;
NSString *const DEFAULTS_KEY_LANGUAGE_CODE    = @"selected_langauge_key";
NSString *const kLangaugeCodeOtherThanEnglish = @"ar";

BOOL const kRTLSupport                        = false;

// Payment Type = true then Card and cash both
// Payment Type = false then Card or cash
BOOL const kPMDPaymentType                    = true;


// CardOrCash = true then Card
// CardOrCash = false then Cash
BOOL const kPMDCardOrCash                     = true;

// BookLater = true then module present
// BookLater = false then module not present
BOOL const kPMDBookLater                      = true;

// WithDispatch = true then Later booking with dispatch
// WithDispatch = false then Later booking without dispatch
BOOL const kPMDWithDispatch                   = true;


#pragma mark - ServiceMethods

NSString *const kSMLiveBooking                = @"liveBooking";
NSString *const kSMGetAppointmentDetial       = @"getAppointmentDetails";
NSString *const kSMUpdateSlaveReview          = @"updateSlaveReview";
NSString *const kSMGetMasters                 = @"getMasters";
NSString *const kSMCancelAppointment          = @"cancelAppointment";
NSString *const kSMCancelOngoingAppointment   = @"cancelAppointmentRequest";

//Methods

NSString *MethodPatientSignUp                = @"slaveSignup";
NSString *MethodPatientLogin                 = @"slaveLogin";
NSString *MethodDoctorUploadImage            = @"uploadImage";
NSString *MethodPassengerLogout              = @"logout";
NSString *MethodFareCalculator               = @"fareCalculator";


//SignUp

NSString *KDASignUpFirstName                  = @"ent_first_name";
NSString *KDASignUpLastName                   = @"ent_last_name";
NSString *KDASignUpMobile                     = @"ent_mobile";
NSString *KDASignUpEmail                      = @"ent_email";
NSString *KDASignUpPassword                   = @"ent_password";
NSString *KDASignUpAddLine1                   = @"ent_address_line1";
NSString *KDASignUpAddLine2                   = @"ent_address_line2";
NSString *KDASignUpAccessToken                = @"ent_token";
NSString *KDASignUpDateTime                   = @"ent_date_time";
NSString *KDASignUpCountry                    = @"ent_country";
NSString *KDASignUpCity                       = @"ent_city";
NSString *KDASignUpLanguage                   = @"ent_lang";
NSString *KDASignUpDeviceType                 = @"ent_device_type";
NSString *KDASignUpDeviceId                   = @"ent_dev_id";
NSString *KDASignUpPushToken                  = @"ent_push_token";
NSString *KDASignUpZipCode                    = @"ent_zipcode";
NSString *KDASignUpCreditCardNo               = @"ent_cc_num";
NSString *KDASignUpCreditCardCVV              = @"ent_cc_cvv";
NSString *KDASignUpCreditCardExpiry           = @"ent_cc_exp";
NSString *KDASignUpTandC                      = @"ent_terms_cond";
NSString *KDASignUpPricing                    = @"ent_pricing_cond";
NSString *KDASignUpReferralCode               = @"ent_referral_code";
NSString *KDASignUpLatitude                   = @"ent_latitude";
NSString *KDASignUpLongitude                  = @"ent_longitude";

// Login

NSString *KDALoginEmail                       = @"ent_email";
NSString *KDALoginPassword                    = @"ent_password";
NSString *KDALoginDeviceType                  = @"ent_device_type";
NSString *KDALoginDevideId                    = @"ent_dev_id";
NSString *KDALoginPushToken                   = @"ent_push_token";
NSString *KDALoginUpDateTime                  = @"ent_date_time";


//Upload
NSString *KDAUploadDeviceId                    = @"ent_dev_id";
NSString *KDAUploadSessionToken                = @"ent_sess_token";
NSString *KDAUploadImageName                   = @"ent_snap_name";
NSString *KDAUploadImageChunck                 = @"ent_snap_chunk";
NSString *KDAUploadfrom                        = @"ent_upld_from";
NSString *KDAUploadtype                        = @"ent_snap_type";
NSString *KDAUploadDateTime                    = @"ent_date_time";
NSString *KDAUploadOffset                      = @"ent_offset";

// Logout the user

NSString *KDALogoutSessionToken                = @"user_session_token";
NSString *KDALogoutUserId                      = @"logout_user_id";



//Parsms for checking user loged out or not

NSString *KDAcheckUserId                        = @"user_id";
NSString *KDAcheckUserSessionToken              = @"ent_sess_token";
NSString *KDAgetPushToken                       = @"ent_push_token";

//Params to store the Country & City.

NSString *KDACountry                            = @"country";
NSString *KDACity                               = @"city";
NSString *KDALatitude                           = @"latitudeQR";
NSString *KDALongitude                          = @"longitudeQR";

//params for Firstname
NSString *KDAFirstName                          = @"ent_first_name";
NSString *KDALastName                           = @"ent_last_name";
NSString *KDAEmail                              = @"ent_email";
NSString *KDAPhoneNo                            = @"ent_mobile";
NSString *KDAPassword                           = @"ent_password";


#pragma mark - NSUserDeafults Keys

NSString *const KNSUPubnubPublishKey                  = @"pubnubPublish";
NSString *const kNSUPubnubSubscribeKey                = @"pubnubSubscribe";

NSString *const kNSUUserPubnubChannel                 = @"userChannel";
NSString *const kNSUServerPubnubChannel               = @"serverChhannel";
NSString *const kNSUPresensePubnubChannel             = @"presenseChannel";
NSString *const kNSUDriverPubnubChannel               = @"driverChannel";

NSString *const kNSUUserEmailID                       = @"userEmailId";
NSString *const kNSUUserReferralCode                  = @"referralCode";
NSString *const kNSUUserShareMessage                  = @"shareMessage";


NSString *const kNSUPlaceAPIKey                       = @"placeAPIKey";
NSString *const kNSUStripeKey                         = @"stripeKey";

NSString *const kNSUMongoDataBaseAPIKey               = @"mongoDBapi";
NSString *const kNSUIsPassengerBookedKey              = @"passengerBooked";
NSString *const kNSUPassengerBookingStatusKey         = @"STATUSKEY";
NSString *const KUBERCarArrayKey                      = @"carTypeArray";
NSString *const kNSUPatientCouponkey                  = @"coupon";
NSString *const kNSUPatientPaypalkey                  = @"paypal";

#pragma mark - PushNotification Payload Keys

 NSString *const kPNPayloadDoctorNameKey            = @"n";
 NSString *const kPNPayloadAppoinmentTimeKey        = @"dt";
 NSString *const kPNPayloadDistanceKey              = @"dis";
 NSString *const kPNPayloadEstimatedTimeKey         = @"eta";
 NSString *const kPNPayloadDoctorEmailKey           = @"e";
 NSString *const kPNPayloadDoctorContactNumberKey   = @"ph";
 NSString *const kPNPayloadProfilePictureUrlKey     = @"pic";
NSString *const  kPNPayloadStatusIDKey              = @"nt";
NSString *const  kPNPayloadAppointmentIDKey         = @"id";

#pragma mark - Car DescriptionKeys
NSString *const KUBERCarTypeID               = @"type_id";
NSString *const KUBERCarTypeName             = @"type_name";
NSString *const KUBERCarMaxSize              = @"max_size";
NSString *const KUBERCarBaseFare             = @"basefare";
NSString *const KUBERCarMinFare              = @"min_fare";
NSString *const KUBERCarPricePerMin          = @"price_per_min";
NSString *const KUBERCarPricePerKM           = @"price_per_km";
NSString *const KUBERCarTypeDescription      = @"type_desc";
NSString *const KUBERDriverStatus            = @"status";


#pragma mark - Notification Name keys
NSString *const kNotificationNewCardAddedNameKey   = @"cardAdded";
NSString *const kNotificationCardDeletedNameKey   = @"cardDeleted";
NSString *const kNotificationLocationServicesChangedNameKey = @"CLChanged";
NSString *const kNotificationBookingConfirmationNameKey = @"bookingConfirmed";

#pragma mark - Network Error
NSString *const kNetworkErrormessage          = @"No network connection";

NSString *const KNUCurrentLat          = @"latitude";
NSString *const KNUCurrentLong         = @"longitude";
NSString *const KNUserCurrentCity      = @"usercity";
NSString *const KNUserCurrentState     = @"userstate";
NSString *const KNUserCurrentCountry   = @"userCountry";

NSString *const KUDriverEmail           = @"DriverEmail";
NSString *const KUBookingDate           = @"BookingDate";
NSString *const KUBookingID             = @"BookingID";
