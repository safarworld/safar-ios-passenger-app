//
//  PaymentViewController.m
//  privMD
//
//  Created by Rahul Sharma on 02/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PaymentViewController.h"
#import "CardLoginViewController.h"
#import "XDKAirMenuController.h"
#import "PaymentDetailsViewController.h"
#import "PaymentCell.h"
#import "Entity.h"
#import "Database.h"
#import "CustomNavigationBar.h"
#import "PatientViewController.h"

@interface PaymentViewController ()<CustomNavigationBarDelegate>
@property(strong,nonatomic) UIButton *addPaymentbutton;
@end

@implementation PaymentViewController
@synthesize addPaymentbutton;
@synthesize paymentTable;
@synthesize callback;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
#pragma mark - ViewLifeCycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.00];
    
    [self addCustomNavigationBar];
    
    //Table VIew
    [self addCoustomTableView];
    
    /**
     *  get all the cards for current user
     */
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    if (context!=nil)
    {
        arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
    }
    
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    if ( [reachability isNetworkAvailable]) {
        
        [self sendServicegetCardDetail];
    }
    else {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi showMessage:kNetworkErrormessage On:self.view];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cardIsDeleted:) name:kNotificationCardDeletedNameKey object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cardIsAdded:) name:kNotificationNewCardAddedNameKey object:nil];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) addCoustomTableView
{
    CGFloat x = 10;
    CGFloat y = 74;
    CGFloat width = self.view.frame.size.width-20;
    CGFloat height = self.view.frame.size.height;
    
    CGRect tableFrame;
    tableFrame = CGRectMake(x, y, width, height - 74);
    paymentTable = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    paymentTable.sectionFooterHeight = 11;
    paymentTable.sectionHeaderHeight = 2;
    paymentTable.scrollEnabled = YES;
    paymentTable.showsVerticalScrollIndicator = NO;
    paymentTable.userInteractionEnabled = YES;
    paymentTable.backgroundColor = [UIColor clearColor];
    paymentTable.delegate = self;
    paymentTable.dataSource = self;
    paymentTable.tag =1;
    paymentTable.separatorColor = [UIColor clearColor];
    
    [self.view addSubview:paymentTable];
    
}

-(void)cardDetailsButtonClicked:(id)sender
{
    UIButton *mBtn = (UIButton *)sender;
    isGoingDelete = YES;
    _dict =   _arrayContainingCardInfo[mBtn.tag];
    // [Helper showAlertWithTitle:@"Message" Message:@"Not done now"];
    [self performSegueWithIdentifier:@"goToPaymentDetail" sender:self];
}

-(void)cardDetailsClicked:(NSDictionary *)getDict
{
    _dict =  [getDict mutableCopy];
    // [Helper showAlertWithTitle:@"Message" Message:@"Not done now"];
    [self performSegueWithIdentifier:@"goToPaymentDetail" sender:self];
}

-(void)addPayment
{
    [self performSegueWithIdentifier:@"goTocardScanController" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"goToPaymentDetail"])
    {
        PaymentDetailsViewController *PDVC = [segue destinationViewController];
        PDVC.containingDetailsOfCard = _dict;
        PDVC.callback = ^()
        {
            //isGoingDelete = NO;
            [self sendServicegetdeleteCard];
        };
    }
    else
    {
        CardLoginViewController *CLVC = [segue destinationViewController];
        CLVC.isComingFromPayment = 1;
    }
    
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

-(void)cancelButtonPressedAccount
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void) addCustomNavigationBar
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"PAYMENT", @"PAYMENT")];
    if(_isComingFromSummary == YES)
    {
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView hideLeftMenuButton:YES];
        [customNavigationBarView setleftBarButtonImage:[UIImage imageNamed:@"payment_cancel_btn_on.png"] :[UIImage imageNamed:@"payment_cancel_btn_off.png"]];
    }
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    if(_isComingFromSummary == YES)
    {
        [self cancelButtonPressedAccount];
    }
    else{
        [self menuButtonPressedAccount];
    }
}
-(void)rightBarButtonClicked:(UIButton *)sender
{
    
}

#pragma mark - WebService call

-(void)sendServicegetCardDetail
{
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    NSString *sesstionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"";
    }
    
    NSDictionary *params = @{@"ent_sess_token": sesstionToken,
                             @"ent_dev_id": deviceId,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getCards"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self getCardDetails:response];
                                   }
                               }];
    
}

-(void)getCardDetails:(NSDictionary *)response
{
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        
        if ([dictResponse[@"errFlag"] intValue] == 1 && ([dictResponse[@"errNum"] intValue] == 6 || [dictResponse[@"errNum"] intValue] == 7)) { //session Expired
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [XDKAirMenuController relese];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
        }
        else  if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            
            _arrayContainingCardInfo = dictResponse[@"cards"];
            if (arrDBResult.count != _arrayContainingCardInfo.count) {
                
                [Database DeleteAllCard];
                [arrDBResult removeAllObjects];
                [self addInDataBase];
                arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
                [paymentTable reloadData];
            }
        }
        else
        {
            
            
            
        }
    }
}

-(void)sendServicegetdeleteCard
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [[ProgressIndicator sharedInstance]showPIOnView:window withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *parameters = [NSString stringWithFormat:@"ent_sess_token=%@&ent_dev_id=%@&ent_cc_id=%@&ent_date_time=%@",[[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken],deviceId,_dict[@"id"],[Helper getCurrentDateTime]];
    
    NSString *removeSpaceFromParameter=[Helper removeWhiteSpaceFromURL:parameters];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@removeCard",BASE_URL]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody:[[removeSpaceFromParameter stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding]
                             dataUsingEncoding:NSUTF8StringEncoding
                             allowLossyConversion:YES]];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(getdeleteCardResponse:)];
    
}

-(void)getdeleteCardResponse:(NSDictionary *)response
{
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        
        NSDictionary *dictResponse=[response objectForKey:@"ItemsList"];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [_arrayContainingCardInfo removeAllObjects];
            [self sendServicegetCardDetail];
            [self.navigationController popViewControllerAnimated:YES];
            
        }
    }
}


-(void)sendServicegetForMakingCArdDefault:(NSString *)cardId
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    NSString *sesstionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"";
    }
    
    NSDictionary *params = @{@"ent_sess_token": sesstionToken,
                             @"ent_dev_id": deviceId,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             @"ent_cc_id":cardId
                             
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"makeCardDefault"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success) { //handle success response
                                       [self defaultCardResponse:response];
                                   }
                               }];
    
}

-(void)defaultCardResponse:(NSDictionary*)response {
    
    //check if response is not null
    if(response == nil)
    {
        //hide progress indicator
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }
    
    //check for network error
    if (response[@"Error"])
    {
        //hide progress indicator
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"Error"]];
    }
    else {
        //hide progress indicator
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        //handle response here
        NSDictionary *itemList = [response mutableCopy];
        
        NSMutableArray *cardArr = [itemList objectForKey:@"cards"];
        
        NSDictionary *cardDetails = [cardArr objectAtIndex:0];
        NSString *cardId = cardDetails[@"last4"];
        NSString *type   = cardDetails[@"type"];
        
        if (callback) {
            callback(cardId,type);
        }
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


- (void)setPlaceholderToCardType:(NSString *)mycardType :(NSInteger)mytag :(NSInteger)viewTag
{
    int k = 0;
    UIView *my = [self.view viewWithTag:viewTag];
    UIImageView *image = (UIImageView *) [my viewWithTag:mytag];
    
    NSString* cardTypeName   = @"placeholder";
    if([mycardType isEqualToString:@"amex"])
        k=1;
    else if([mycardType isEqualToString:@"diners"])
        k=2;
    else if([mycardType isEqualToString:@"discover"])
        k=3;
    else if([mycardType isEqualToString:@"jcb"])
        k=4;
    else if([mycardType isEqualToString:@"MasterCard"])
        k=5;
    else if([mycardType isEqualToString:@"Visa"])
        k=6;
    
    switch (k )
    {
        case 1:
            cardTypeName = NSLocalizedString(@"amex", @"amex");
            break;
        case 2:
            cardTypeName = NSLocalizedString(@"diners", @"diners");
            break;
        case 3:
            cardTypeName = NSLocalizedString(@"discover", @"discover");
            break;
        case 4:
            cardTypeName = NSLocalizedString(@"jcb", @"jcb");
            break;
        case 5:
            cardTypeName = NSLocalizedString(@"mastercard", @"mastercard");
            break;
        case 6:
            cardTypeName = NSLocalizedString(@"visa.png", @"visa.png");  //PaymentKit/PaymentKit/Resources/Cards/amex.png
            break;
        default:
            break;
    }
    
    
    [image setImage:[UIImage imageNamed:cardTypeName]];
}


#pragma mark -UITABLEVIEW DELEGATE

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrDBResult.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"Cell";
    PaymentCell *cell = nil;
    if(cell==nil)
    {
        cell =[[PaymentCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellAccessoryNone;
        
        cell.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:0.5];
        tableView.backgroundColor=[UIColor clearColor];
    }
    if(indexPath.row == arrDBResult.count)
    {
        UIImageView *addpaymentIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 17, 17, 17)];
        addpaymentIcon.image = [UIImage imageNamed:@"payment_add.png"];
        [cell.contentView addSubview:addpaymentIcon];
        
        UILabel *labelAddpayment = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, [UIScreen mainScreen].bounds.size.width-50, 50)];
        if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
        {
            labelAddpayment.textAlignment = NSTextAlignmentRight;
        }
        else
        {
            labelAddpayment.textAlignment = NSTextAlignmentLeft;
        }
        [Helper setToLabel:labelAddpayment Text:NSLocalizedString(@"Add Card", @"Add Card") WithFont:Roboto_Regular FSize:15 Color:[UIColor blackColor]];
        [cell.contentView addSubview:labelAddpayment];
    }
    else
    {
        Entity *fav = arrDBResult[indexPath.row];
        NSString *str = @"****";
        str = [str stringByAppendingString:fav.last4];
        cell.cardLast4Number.text = flStrForObj(str);
        cell.cardPersonal.text = NSLocalizedString(@"PERSONAL", @"PERSONAL");
        [cell setPlaceholderToCardType:fav.cardtype];
    }
    
    if(indexPath.row == arrDBResult.count)
    {
        if (arrDBResult.count == 0)
        {
            cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_top.png"];
        }
        else
        {
            cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_bottom.png"];
        }
    }
    else if(indexPath.row == 0)
    {
        cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_top.png"];
    }
    else
    {
        cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_middle.png"];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 50;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 40)];
    footerView.backgroundColor = [UIColor clearColor];
    UILabel *poweredByLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 14, 100, 21)];
    [Helper setToLabel:poweredByLabel Text:NSLocalizedString(@"Powered by", @"Powered by") WithFont:ZURICH_Roman_CONDENSED FSize:12 Color:UIColorFromRGB(0x999999)];
    poweredByLabel.textAlignment = NSTextAlignmentLeft;
    [footerView addSubview:poweredByLabel];
    
    UIImageView *stripeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(poweredByLabel.frame.size.width+poweredByLabel.frame.origin.x+5, 15, 51, 21)];
    stripeImageView.image = [UIImage imageNamed:@"selectpayment_stripe_icn.png"];
    [footerView addSubview:stripeImageView];
    
    return footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(_isComingFromSummary == NO)
    {
        if(arrDBResult.count == indexPath.row)
        {
            [self addPayment];
        }
        else
        {
            Entity *fav = arrDBResult[indexPath.row];
            NSMutableDictionary *getDetailsfromDB = [[NSMutableDictionary alloc]init];
            [getDetailsfromDB setObject:fav.expMonth forKey:@"exp_month"];
            [getDetailsfromDB setObject:fav.expYear forKey:@"exp_year"];
            [getDetailsfromDB setObject:fav.cardtype forKey:@"type"];
            [getDetailsfromDB setObject:fav.last4 forKey:@"last4"];
            [getDetailsfromDB setObject:fav.idCard forKey:@"id"];
            [self cardDetailsClicked:getDetailsfromDB];
        }
    }
    else
    {
        if(arrDBResult.count == indexPath.row)
        {
            CardLoginViewController *vc = (CardLoginViewController*)[self.storyboard instantiateViewControllerWithIdentifier:@"cardLogin"];
            vc.isComingFromPayment = 2;
            UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
            [self presentViewController:navBar animated:YES completion:nil];
        }
        else
        {
            Entity *event = [arrDBResult objectAtIndex:indexPath.row];
            [[NSUserDefaults standardUserDefaults] setObject:[event idCard] forKey:@"idOfSelectedCard"];
            [self sendServicegetForMakingCArdDefault:event.idCard];
        }
    }
}


//DataBase

-(void)addInDataBase
{
    Database *db = [[Database alloc] init];
    [self checkCampaignIdAddedOrNot];
    if(isPresentInDBalready != 1)
    {
        for (int i =0; i<_arrayContainingCardInfo.count; i++)
        {
            [db makeDataBaseEntry:_arrayContainingCardInfo[i]];
        }
    }
}

- (void)checkCampaignIdAddedOrNot
{
    isPresentInDBalready = 0;
    NSArray *array = [Database getCardDetails];
    if ([array count])
    {
        for(int i=0 ; i<[array count];i++)
        {
            Entity *fav = [array objectAtIndex:i];
            if ([fav.idCard isEqualToString:_arrayContainingCardInfo[i][@"id"]])
            {
                isPresentInDBalready = 1;
                break;
            }
        }
    }
}


-(void)cardIsAdded:(NSNotification *)notification
{
    if (arrDBResult)
    {
        [arrDBResult removeAllObjects];
    }
    [arrDBResult addObjectsFromArray:[Database getCardDetails]];
    [paymentTable reloadData];
}

-(void)cardIsDeleted:(NSNotification *)notification
{
    if (arrDBResult)
    {
        [arrDBResult removeAllObjects];
    }
    
    [arrDBResult addObjectsFromArray:[Database getCardDetails]];
    [paymentTable reloadData];
}


@end
