//
//  PatientAppDelegate.h
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WatchConnectivity/WatchConnectivity.h>

@interface PatientAppDelegate : UIResponder <UIApplicationDelegate, WCSessionDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
//-(void)presentLoginViewController;
//-(void)goToHomeController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void)noPushForceChangingController:(NSDictionary *)userInfo :(int)type;
@property (assign, nonatomic) CancelBookingReasons cancelReason;

@end
