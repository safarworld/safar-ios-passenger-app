//
//  ExtensionDelegate.h
//  watch Extension
//
//  Created by 3Embed on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface ExtensionDelegate : NSObject <WKExtensionDelegate>

@end
