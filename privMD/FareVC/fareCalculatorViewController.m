//
//  fareCalculatorViewController.m
//  UBER
//
//  Created by Rahul Sharma on 12/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "fareCalculatorViewController.h"
#import "PMDReachabilityWrapper.h"
#import "NetworkHandler.h"
#import "PickUpViewController.h"
#import "PatientGetLocalCurrency.h"

@interface fareCalculatorViewController ()

@end

@implementation fareCalculatorViewController
@synthesize changeLocationButton;
@synthesize locationDetails;
@synthesize onCompletion;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

#pragma mark - WEB SERVICES

-(void)sendAServiceForFareCalculator{
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    NSString *currentLatitude = [locationDetails objectForKey:@"cLat"];
    NSString *currentLongitude = [locationDetails objectForKey:@"cLoc"];
    NSString *pickupLatitude = [locationDetails objectForKey:@"pLat"];
    NSString *pickupLongitude = [locationDetails objectForKey:@"pLoc"];
    NSString *dropLatitude = [locationDetails objectForKey:@"dLat"];
    NSString *dropLongitude = [locationDetails objectForKey:@"dLon"];
    NSString *dateTime = [Helper getCurrentDateTime];
    NSString *type = [locationDetails objectForKey:@"typeid"];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_type_id":[NSString stringWithFormat:@"%@",type],
                             @"ent_curr_lat":currentLatitude,
                             @"ent_curr_long":currentLongitude,
                             @"ent_from_lat":pickupLatitude,
                             @"ent_from_long":pickupLongitude,
                             @"ent_to_lat":dropLatitude,
                             @"ent_to_long":dropLongitude,
                             @"ent_date_time":dateTime
                             };
    NSLog(@"Fare calculator params = %@", params);
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodFareCalculator
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   NSLog(@"Fare calculator response = %@", response);
                                   if (success)
                                   {
                                       [self parsegetBookingAppointment:response];
                                   }
                                   else
                                   {
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
}

#pragma mark - WEB SERVICE RESPONSE

-(void)parsegetBookingAppointment:(NSDictionary *)responseDictionary
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[responseDictionary objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDictionary objectForKey:@"Error"]];
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper setToLabel:_sourceLoactionLabel Text:[locationDetails objectForKey:@"pAddr"] WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x000000)];
            NSString *fareValue =  [PatientGetLocalCurrency getCurrencyLocal:[responseDictionary[@"fare"] floatValue]];
            
            [[NSUserDefaults standardUserDefaults] setObject:fareValue forKey:@"FareEstimate"];
            [Helper setToLabel:_sourceDistanceLabel Text:[NSString stringWithFormat:@" %@",responseDictionary[@"curDis"]] WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            [Helper setToLabel:_pickupLAbel Text:NSLocalizedString(@"Pick Up Location", @"Pick Up Location") WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            [Helper setToLabel:_destinationLocationLabel Text:[locationDetails objectForKey:@"dAddr"] WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x000000)];
            [Helper setToLabel:_destinationDistanceLabel Text:[NSString stringWithFormat:@" %@",responseDictionary[@"dis"]]  WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            [Helper setToLabel:_dropoffLabel Text:NSLocalizedString(@"Drop Off Location", @"Drop Off Location") WithFont:Roboto_Light FSize:11 Color:UIColorFromRGB(0x969797)];
            [Helper setToLabel:_paymentLabel Text:fareValue WithFont:Roboto_Bold FSize:40 Color:UIColorFromRGB(0x000000)];
            NSString *stri = NSLocalizedString(@"Fares may vary due to traffic,weather and other factors.Estimate does not include discount or promotions.", @"Fares may vary due to traffic,weather and other factors.Estimate does not include discount or promotions.");
            [Helper setToLabel:_messageLabel Text:stri WithFont:Roboto_Regular FSize:12 Color:UIColorFromRGB(0x9e9c9c)];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        }
    }
}

#pragma mark - CustomNavigation Button

-(void)cancelButtonClicked
{
    if (onCompletion)
    {
        NSString *dropLatitude = [locationDetails objectForKey:@"dLat"];
        NSString *dropLongitude = [locationDetails objectForKey:@"dLon"];
        NSString *add1 = [locationDetails objectForKey:@"dAddr"];
        NSString *add2 = [locationDetails objectForKey:@"dAddr2"];

        NSDictionary  *dict = @{@"address1":add1,
                                @"address2":add2,
                                @"lat":dropLatitude,
                                @"lng":dropLongitude,
                                };
        
        onCompletion(dict);
    }
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}
-(void) createNavLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,60,buttonImage.size.height)];

    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BOOK NOW", @"BOOK NOW") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BOOK NOW", @"BOOK NOW") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createNavLeftButton];
    [_centerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"fc_price_bg.png"]]];
    [self sendAServiceForFareCalculator];
}

-(void)viewDidAppear:(BOOL)animated
{
    self.navigationItem.title = NSLocalizedString(@"FARE CALCULATOR", @"FARE CALCULATOR");
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor], NSFontAttributeName: [UIFont fontWithName:Roboto_Regular size:15]}];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (IBAction)changeLocationButtonClicked:(id)sender
{
    if (_isComingFromMapVC == YES)
    {
        PickUpViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"pick"];
        pickController.locationType = kDestinationAddress;
        pickController.isComingFromMapVCFareButton = YES;
        NSString *type = [locationDetails objectForKey:@"typeid"];
        pickController.typeID = [type integerValue];
        [self.navigationController setViewControllers:@[pickController]];
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void) pushController: (UIViewController*) controller withTransition: (UIViewAnimationTransition) transition
{
    [UIView beginAnimations:nil context:NULL];
    [self.navigationController pushViewController:controller animated:NO];
    [UIView setAnimationDuration:.5];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationTransition:transition forView:controller.view cache:YES];
    [UIView commitAnimations];
}


@end
