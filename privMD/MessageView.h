//
//  MessageView.h
//  RoadyoDispatch
//
//  Created by 3Embed on 26/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageView : UIView<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIView *headingView;
@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UIView *textViewReferenceView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
@property (strong, nonatomic) NSString *textViewPlaceHolder;
@property (strong, nonatomic) NSString *headingText;
@property (nonatomic) BOOL isDispute;
@property (strong, nonatomic) NSString *appoinmentDate;

- (IBAction)submitBtnAction:(id)sender;
-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (nonatomic, copy)   void (^onCompletion)(NSInteger subimtClicked, NSString *text);
- (IBAction)cancelBtnAction:(id)sender;


@end
