//
//  InviteViewController.m
//  UBER
//
//  Created by Rahul Sharma on 06/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "InviteViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <FacebookSDK/FacebookSDK.h>
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"



@interface InviteViewController ()<MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,CustomNavigationBarDelegate>
{
    MFMailComposeViewController *mailer;
    NSString *shareText;
}
@property (strong, nonatomic) IBOutlet UILabel *shareLable;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeMsgShowingLabel;
@property (strong, nonatomic) IBOutlet UILabel *promoCodeLablel;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UILabel *fbShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *twitterButton;
@property (strong, nonatomic) IBOutlet UILabel *twitterShareLAble;
@property (strong, nonatomic) IBOutlet UIButton *smsButton;
@property (strong, nonatomic) IBOutlet UILabel *smsShareLabel;
@property (strong, nonatomic) IBOutlet UIButton *emailButton;
@property (strong, nonatomic) IBOutlet UILabel *emailShareLable;
@property (strong, nonatomic) IBOutlet UILabel *shareyourcodeLabel;

- (IBAction)facebookButtonClicked:(id)sender;
- (IBAction)twitterButtonClicked:(id)sender;
- (IBAction)smsButtonClicked:(id)sender;
- (IBAction)emailButtonClicked:(id)sender;

@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self addCustomNavigationBar];
    NSString *pCode = flStrForStr([[NSUserDefaults standardUserDefaults]objectForKey:kNSUUserReferralCode]);
    [Helper setToLabel:_shareLable Text:NSLocalizedString(@"SHARE SAFAR", @"SHARE SAFAR") WithFont:Roboto_Bold FSize:12 Color:UIColorFromRGB(0x656565)];
    NSString *str1 = NSLocalizedString(@"Get a discount if your contacts sign up using your invite code", @"Get a discount if your contacts sign up using your invite code");
    
    shareText = [NSString stringWithFormat:@"%@\n",str1];
    [Helper setToLabel:_promoCodeMsgShowingLabel Text:shareText WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0x333333)];

    shareText = nil;
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserShareMessage] length])
    {
        shareText = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserShareMessage];
    }
    else
    {
        shareText = NSLocalizedString(@"Use my Safar code, ", @"Use my Safar code, ");
        NSString *str2 = NSLocalizedString(@"and get discount on your first Safar ride", @"and get discount on your first Safar ride.");
        NSString *str3 = NSLocalizedString(@"To download go to", @"To download go to");
        NSString *link = [NSString stringWithFormat:@"https://%@", itunesURL];
        shareText = [shareText stringByAppendingString:pCode];
        shareText = [NSString stringWithFormat:@"%@ , %@ %@ %@", shareText,str2,str3,link];
    }
    
    [Helper setToLabel:_promoCodeLablel Text:pCode WithFont:Roboto_Light FSize:33 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_shareyourcodeLabel Text:NSLocalizedString(@"SHARE YOUR CODE", @"SHARE YOUR CODE") WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];

    [Helper setToLabel:_fbShareLabel Text:NSLocalizedString(@"FACEBOOK", @"FACEBOOK") WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_twitterShareLAble Text:NSLocalizedString(@"TWITTER", @"TWITTER") WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_smsShareLabel Text:NSLocalizedString(@"MESSAGE", @"MESSAGE") WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:_emailShareLable Text:NSLocalizedString(@"EMAIL", @"EMAIL") WithFont:Roboto_Light FSize:12 Color:UIColorFromRGB(0x333333)];
}

#pragma mark- Custom Methods

- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"SHARE", @"SHARE")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonclicked];
}

- (void)menuButtonclicked {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)facebookButtonClicked:(id)sender {
    
    [self shareOnFacebook];
}

- (IBAction)twitterButtonClicked:(id)sender {
    [self shareOnTwitter];
}

- (IBAction)smsButtonClicked:(id)sender {
    
    [self shareViaSMS];
}

- (IBAction)emailButtonClicked:(id)sender {
    [self EmaiSharing];
}



-(void)shareViaSMS
{    
    if(![MFMessageComposeViewController canSendText])
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT", @"ALERT") message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"%@",shareText];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [[mailer navigationBar] setTintColor: [UIColor blackColor]];
    mailer.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];

    //[messageController setRecipients:recipents];
    [messageController setBody:message];
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:nil];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Failed to send !", @"Failed to send !") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
        [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)shareOnTwitter
{
    
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController
                                               composeViewControllerForServiceType:SLServiceTypeTwitter];
           NSString *message = [NSString stringWithFormat:@"%@",shareText];
        
        [tweetSheet setInitialText:message];
        
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgLinkForSharing]]];
        [tweetSheet addImage:image];
        
        [self presentViewController:tweetSheet animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"Message", @"Message")
                                  message:NSLocalizedString(@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup", @"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup")
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                  otherButtonTitles:nil];
        [alertView show];
    }
    
}


-(void)shareOnFacebook
{
    [self publishWithWebDialog];
}

/*
 * Share using the Web Dialog
 */
- (void) publishWithWebDialog {
    // Put together the dialog parameters
    
        NSString *message = [NSString stringWithFormat:@"%@",shareText];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:NSLocalizedString(@"Safar", @"Safar"),@"name",
                                   message,@"description",imgLinkForSharing,@"picture",nil];
    
    
    [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                           parameters:params
                                              handler:
     ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
         if (error) {
             // Error launching the dialog or publishing a story.
             [self showAlert:[self checkErrorMessage:error]];
         } else {
             if (result == FBWebDialogResultDialogNotCompleted) {
             } else {
                 // Handle the publish feed callback
                 NSDictionary *urlParams = [self parseURLParams:[resultURL query]];
                 if (![urlParams valueForKey:@"post_id"]) {

                 } else {
                     // User clicked the Share button
                     [self showAlert:[self checkPostId:urlParams]];
                 }
             }
         }
     }];
}

/*
 * Helper method to parse URL parameters.
 */
- (NSDictionary*)parseURLParams:(NSString *)query
{
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs) {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val =
        [[kv objectAtIndex:1]
         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [params setObject:val forKey:[kv objectAtIndex:0]];
    }
    return params;
}


#pragma mark - Helper methods

- (NSString *)checkErrorMessage:(NSError *)error
{
    NSString *errorMessage = @"";
    errorMessage = NSLocalizedString(@"Operation failed due to a connection problem, retry later.", @"Operation failed due to a connection problem, retry later.");
    return errorMessage;
}

- (NSString *) checkPostId:(NSDictionary *)results {
   
    NSString *message = NSLocalizedString(@"Posted successfully.", @"Posted successfully.");
   
    return message;
}

- (void)showAlert:(NSString *) alertMsg {
    if (![alertMsg isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Result", @"Result")
                                    message:alertMsg
                                   delegate:nil
                          cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                          otherButtonTitles:nil] show];
    }
}

-(void)EmaiSharing
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:Roboto_Bold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil];
        
        [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [[mailer navigationBar] setTintColor: [UIColor blackColor]];
        mailer.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];

     
        NSString *shareUrl =  [NSString stringWithFormat:@"%@",shareText];
        [mailer setSubject:NSLocalizedString(@"Safar", @"Safar")];
        NSArray *toRecipents;
        NSMutableString* message =[[NSMutableString alloc] init];
        [message appendString:shareUrl];
        toRecipents = [NSArray arrayWithObject:@""];
        [mailer setMessageBody:message isHTML:NO];
        [mailer setToRecipients:toRecipents];
        NSString *strURL = [NSString stringWithFormat:@"%@",imgLinkForSharing];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]]];
        
        NSData *photoData = UIImageJPEGRepresentation(image,1);
        [mailer addAttachmentData:photoData mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"photo.png"]];
        
        [self presentViewController:mailer animated:YES completion:^{
            [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failure", @"Failure")
                                                        message:NSLocalizedString(@"Your device doesn't support the composer sheet", @"Your device doesn't support the composer sheet")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - MFMailComposeViewController Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

@end
