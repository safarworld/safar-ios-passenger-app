//
//  Entity.m
//  privMD
//
//  Created by Rahul Sharma on 20/03/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "Entity.h"


@implementation Entity

@dynamic expMonth;
@dynamic expYear;
@dynamic idCard;
@dynamic last4;
@dynamic cardtype;

@end
