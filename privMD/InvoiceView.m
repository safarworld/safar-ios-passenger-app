//
//  InvoiceView.m
//  RoadyoDispatch
//
//  Created by 3Embed on 24/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "InvoiceView.h"
#import "ReceiptView.h"
#import "MessageView.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import <AXRatingView/AXRatingView.h>

@implementation InvoiceView
@synthesize driverEmail;
@synthesize apptDate;

@synthesize yourLastRideLabe, dayDateLabel, needHelpBtn;
@synthesize pickUpWithDateLabel, pickUpAddressLabel, dropOffWithDateLabel, dropOffAddressLabel;
@synthesize greenDotLabel, redDotLabel, verticalLineLabel, horizontalLineLabel;
@synthesize driverImageView, driverNameLabel, totalAmountLabel, receiptBtn;
@synthesize rateYourDriverLabel, ratingView;
@synthesize writeCommentBtn;
@synthesize submitBtn;
@synthesize isComingFromBookingHistory;
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"InvoiceView" owner:self options:nil] firstObject];
    return self;
}

- (IBAction)receiptBtnAction:(id)sender
{
    ReceiptView* receiptView = [[ReceiptView alloc]init];
    [receiptView showPopUpWithDetailedDict:invoiceData Onwindow:window];
    receiptView.onCompletion = ^(NSInteger closeClicked)
    {
        [self setHidden:NO];
    };
    [self setHidden:YES];
}

- (IBAction)writeCommentBtnAction:(id)sender
{
    MessageView* messageView = [[MessageView alloc]init];
    messageView.headingText = NSLocalizedString(@"Write a review", @"Write a review");
    if (textWritenByUser.length)
    {
        messageView.textViewPlaceHolder = textWritenByUser;
    }
    else
    {
        messageView.textViewPlaceHolder = @"Let us know your experience on this last trip and we will follow up with our team to improve your experience in the next time you take a ride";
    }
    messageView.isDispute = NO;
    [messageView showPopUpWithDetailedDict:nil Onwindow:window];
    messageView.onCompletion = ^(NSInteger closeClicked, NSString *reportMsg)
    {
        if (closeClicked == 1)
        {
            textWritenByUser = reportMsg;
            if (textWritenByUser.length)
            {
                [Helper setButton:writeCommentBtn Text:NSLocalizedString(@"Edit review", @"Edit review")  WithFont:Lato_Regular FSize:13 TitleColor:UIColorFromRGB(0xd0d0d0) ShadowColor:nil];
            }
        }
        else
        {
            textWritenByUser = @"";
        }

        [self setHidden:NO];
    };
    [self setHidden:YES];
}

- (IBAction)needHelpBtnAction:(id)sender
{
    MessageView* messageView = [[MessageView alloc]init];
    messageView.headingText = NSLocalizedString(@"Need help?", @"Need help?");
    messageView.textViewPlaceHolder = NSLocalizedString(@"Please let us know if you faced any issue on this ride ?", @"Please let us know if you faced any issue on this ride ?");
    messageView.isDispute = YES;
    messageView.appoinmentDate = invoiceData[@"apptDt"];
    [messageView showPopUpWithDetailedDict:nil Onwindow:window];
    messageView.onCompletion = ^(NSInteger closeClicked, NSString *reportMsg)
    {
        [self setHidden:NO];
    };
    [self setHidden:YES];
}

- (IBAction)submitBtnAction:(id)sender
{
    if (isComingFromBookingHistory)
    {
        [self hidePOPup];
    }
    else
    {
        [self sendRequestForReviewSubmit];
    }
}


-(void)showPopUpWithDetailedDict:(NSDictionary *)dict
{
    window = [[UIApplication sharedApplication] keyWindow];
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;
    
    greenDotLabel.layer.cornerRadius = greenDotLabel.frame.size.width/2;
    greenDotLabel.layer.masksToBounds = YES;
    
    redDotLabel.layer.cornerRadius = redDotLabel.frame.size.width/2;
    redDotLabel.layer.masksToBounds = YES;

    
    needHelpBtn.layer.cornerRadius = 4.0f;
    needHelpBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    needHelpBtn.layer.borderWidth = 1.0f;
    
    receiptBtn.layer.cornerRadius = 4.0f;
    receiptBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    receiptBtn.layer.borderWidth = 1.0f;

    
    self.writeCommentBtn.layer.cornerRadius = 5.0f;
    self.writeCommentBtn.layer.masksToBounds = YES;
    
    self.submitBtn.layer.cornerRadius = 5.0f;
    self.submitBtn.layer.masksToBounds = YES;
    
    [self setRatingViewInInvoice];
    
    if (isComingFromBookingHistory)
    {
        self.needHelpBtn.hidden = YES;
        self.commentBtnHeight.constant = 0;
        [self layoutIfNeeded];
    }
    else
    {
        self.needHelpBtn.hidden = NO;
        self.commentBtnHeight.constant = 40;
        [self layoutIfNeeded];
    }
    
    [self sendRequestForAppointmentInvoice];
    
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;

                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 0.5;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}

-(void)sendRequestForAppointmentInvoice
{
    NSLog(@"Appionment Details service called");
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self withMessage:NSLocalizedString(@"Please wait..", @"Please wait..")];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSInteger tip = [[NSUserDefaults standardUserDefaults]integerForKey:@"drivertip"];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *docEmail = driverEmail;
    NSString *appointmntDate = apptDate;
    
    NSString *currentDate = [Helper getCurrentDateTime];
    @try
    {
        NSDictionary *params = @{@"ent_sess_token":sessionToken,
                                 @"ent_dev_id":deviceID,
                                 @"ent_email":docEmail,
                                 @"ent_user_type":@"2",
                                 @"ent_tip":[NSNumber numberWithInteger:tip],
                                 @"ent_appnt_dt":appointmntDate,
                                 @"ent_date_time":currentDate,
                                 };
        NSLog(@"Invoice Params = %@", params);
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       NSLog(@"Invoice Response = %@", response);
                                       if (success)
                                       {
                                           [self parseAppointmentDetailResponse:response];
                                       }
                                       else
                                       {
                                           [self  hidePOPup];
                                       }
                                   }];
    }
    @catch (NSException *exception)
    {
    }
}

#pragma mark - WebService Response
-(void)parseAppointmentDetailResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
        [self  hidePOPup];
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        [self  hidePOPup];
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 ))
    {
        //session Expired
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            invoiceData = [response mutableCopy];
            [self updateUI];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"errmsg", @"errmsg")];
            [self hidePOPup];
        }
    }
}

-(void)updateUI
{
    [Helper setToLabel:yourLastRideLabe Text:NSLocalizedString(@"YOUR RIDE DETAILS", @"YOUR RIDE DETAILS") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    
    NSString *rideDate = [self getDateAndTime:flStrForObj(invoiceData[@"dropDt"])];
    [Helper setToLabel:dayDateLabel Text:rideDate WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x919191)];
    
    NSString *pickUp = NSLocalizedString(@"PICK UP", @"PICK UP");
    NSString *pTime = [self getTime:flStrForObj(invoiceData[@"pickupDt"])];
    [Helper setToLabel:pickUpWithDateLabel Text:[NSString stringWithFormat:@"%@ %@", pickUp,pTime] WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:pickUpAddressLabel Text:flStrForObj(invoiceData[@"addr1"]) WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x6f6f6f)];
    
    NSString *dropOff = NSLocalizedString(@"DROP OFF", @"DROP OFF");
    NSString *dTime = [self getTime:flStrForObj(invoiceData[@"dropDt"])];
    [Helper setToLabel:dropOffWithDateLabel Text:[NSString stringWithFormat:@"%@ %@", dropOff,dTime] WithFont:Lato_Regular FSize:11 Color:UIColorFromRGB(0x333333)];
    [Helper setToLabel:dropOffAddressLabel Text:flStrForObj(invoiceData[@"dropAddr1"]) WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x6f6f6f)];
    
    driverImageView.layer.cornerRadius = driverImageView.frame.size.width/2;
    driverImageView.layer.masksToBounds = YES;
    
    NSString *strImageURL = flStrForStr(invoiceData[@"pPic"]);
    if (strImageURL.length)
    {
        strImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,invoiceData[@"pPic"]];
        [driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageURL]
                           placeholderImage:[UIImage imageNamed:@"driverImage"]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                      
                                      if (error || !image)
                                      {
                                          driverImageView.image = [UIImage imageNamed:@"driverImage"];
                                      }
                                      else
                                      {
                                          driverImageView.image = image;
                                      }
                                  }];
    }
    else
    {
        driverImageView.image = [UIImage imageNamed:@"driverImage"];
    }
    [Helper setToLabel:driverNameLabel Text:[[NSString stringWithFormat:@"%@ %@",flStrForObj(invoiceData[@"fName"]),flStrForObj(invoiceData[@"lName"])] uppercaseString] WithFont:Lato_Regular FSize:15 Color:UIColorFromRGB(0x707070)];
    
    NSString *totalAmount =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
    [Helper setToLabel:totalAmountLabel Text:totalAmount WithFont:Lato_Regular FSize:15 Color:UIColorFromRGB(0x333333)];
    
    [Helper setButton:needHelpBtn Text:NSLocalizedString(@"NEED HELP?", @"NEED HELP?") WithFont:Lato_Regular FSize:11 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [Helper setButton:receiptBtn Text:NSLocalizedString(@"RECEIPT", @"RECEIPT") WithFont:Lato_Regular FSize:11 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    
    if (isComingFromBookingHistory)
    {
        [Helper setButton:submitBtn Text:NSLocalizedString(@"OK", @"OK") WithFont:Lato_Regular FSize:14 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
        self.needHelpBtn.hidden = YES;
        [Helper setToLabel:rateYourDriverLabel Text:NSLocalizedString(@"DRIVER RATING", @"DRIVER RATING") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
        ratingView.value = [invoiceData[@"r"] floatValue];
        ratingView.userInteractionEnabled = NO;
        [self layoutIfNeeded];
    }
    else
    {
        [Helper setToLabel:rateYourDriverLabel Text:NSLocalizedString(@"RATE YOUR DRIVER", @"RATE YOUR DRIVER") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
        [ratingView addTarget:self action:@selector(ratingChanged:) forControlEvents:UIControlEventValueChanged];
        ratingView.value = 0.0;
        ratingView.stepInterval = 1.0;
        ratingView.userInteractionEnabled = YES;
        [Helper setButton:writeCommentBtn Text:NSLocalizedString(@"Leave a comment", @"Leave a comment")  WithFont:Lato_Regular FSize:13 TitleColor:UIColorFromRGB(0xd0d0d0) ShadowColor:nil];
        [Helper setButton:submitBtn Text:NSLocalizedString(@"SUBMIT", @"SUBMIT") WithFont:Lato_Regular FSize:14 TitleColor:UIColorFromRGB(0xFFFFFF) ShadowColor:nil];
    }
}


-(void)sendRequestForReviewSubmit
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self withMessage:NSLocalizedString(@"Submitting...", @"Submitting...")];
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *docEmail = invoiceData[@"email"];
    NSString *appointmntDate = invoiceData[@"apptDt"];
    NSString *currentDate = [Helper getCurrentDateTime];

    NSString *review;
    if (textWritenByUser)
    {
        review = textWritenByUser;
    }
    else
    {
        review = @"";
    }
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":docEmail,
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             @"ent_rating_num":[NSNumber numberWithInteger:ratingValue],
                             @"ent_review_msg":review,
                             };
    
    NSLog(@"Submit Params = %@", params);
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMUpdateSlaveReview
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   NSLog(@"Submit Response = %@", response);
                                   if (success)
                                   {
                                       [self parseSubmitReviewResponse:response];
                                   }
                                   else
                                   {
                                       NSLog(@"Submit Response1 = %@", response);
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
}

-(void)parseSubmitReviewResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 ))
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self hidePOPup];
        [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
            [self hidePOPup];
        }
        else
        {
            [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"errMsg"]];
        }
    }
}


-(void) setRatingViewInInvoice
{
    ratingView.stepInterval = 1.0;
    ratingView.markFont = [UIFont systemFontOfSize:30];
    ratingView.baseColor = UIColorFromRGB(0xA9A9A9);
    ratingView.highlightColor = UIColorFromRGB(0xEDC714);
}

- (void)ratingChanged:(AXRatingView *)sender
{
    ratingValue = sender.value;
}


-(NSString*)getTime:(NSString *)aDatefromServer{
    
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
    
}

-(NSString*)getDateAndTime:(NSString *)aDatefromServer{
    
    NSString *mGetting = [NSString stringWithFormat:@"%@",aDatefromServer];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date  = [dateFormatter dateFromString:mGetting];
    dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy hh:mm a"];
    NSString *retTime = [dateFormatter stringFromDate:date];
    return retTime;
    
}

@end
