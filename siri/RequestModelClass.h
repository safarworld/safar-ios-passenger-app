//
//  RequestModelClass.h
//  Safar
//
//  Created by 3Embed on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IntentHandler.h"

@interface RequestModelClass : NSObject<NSURLSessionDelegate>
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, copy) void(^onCompletion)(NSDictionary *dataFromServer);
@property (nonatomic, copy) void(^onCompletionForLiveBooking)(NSDictionary *dataForLiveBooking);

+ (instancetype) getSharedInstance;
-(void)sendServiceToGetCurrentVehiclesAndDriver:(NSString *)latitude longitude:(NSString *)longitude;
-(void)sendServiceToBookDriver:(NSDictionary *)bookingRequiredData andDriverEmail:(NSString *) driverEmail;
@end
