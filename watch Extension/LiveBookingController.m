//
//  LiveBookingController.m
//  SimpleWeather
//
//  Created by 3Embed on 09/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import "LiveBookingController.h"

@implementation LiveBookingController

-(void)session:(WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(NSError *)error {
    NSLog(@"New Session activationState: %ld", (long)activationState);
}
-(void)session:(WCSession *)session didReceiveApplicationContext:(NSDictionary<NSString *,id> *)applicationContext {
    NSLog(@"New Session Context: %@", applicationContext);
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    for (NSString *key in applicationContext.allKeys) {
        [defaults setObject:[applicationContext objectForKey:key] forKey:key];
    }
    [defaults synchronize];
}


- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    NSMutableArray *drivers = [context[@"masters"] mutableCopy];
    WCSession* session = [WCSession defaultSession];
    session.delegate = self;
    [session activateSession];
    if ([drivers  count]) {
        NSDictionary *firstDriver = drivers[0];
        [self sendServiceToBookDriverEmail:firstDriver[@"e"] withData:(NSDictionary *)context];
    }
}

-(void)sendServiceToBookDriverEmail:(NSString *) driverEmail withData:(NSDictionary *)selectedType {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"http://188.166.48.202/safar/services_v2.php/liveBooking"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:45.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    
    //setup parameters
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    NSString *deviceID = [shared objectForKey:@"deviceidSiri"];
    NSString *pickupLatitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"];
    NSString *pickupLongitude = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"];
    NSString *pickupAddress = selectedType[@"pickupAddress"];
    NSString *carTypeID = selectedType[@"type_id"];
    if (!pickupLongitude.length) {
        pickupLongitude = @"";
    }
    if (!pickupLongitude.length) {
        pickupLongitude = @"";
    }
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [dateFormatter stringFromDate:now];
    
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_wrk_type":carTypeID,
                             @"ent_lat":pickupLatitude,
                             @"ent_long":pickupLongitude,
                             @"ent_addr_line1":pickupAddress,
                             @"ent_addr_line2":@"",
                             @"ent_drop_lat":@"",
                             @"ent_drop_long":@"",
                             @"ent_drop_addr_line1":@"",
                             @"ent_drop_addr_line2":@"",
                             @"ent_zipcode":@"560024",
                             @"ent_extra_notes":@"",
                             @"ent_date_time":dateTime,
                             @"ent_later_dt":@"",
                             @"ent_payment_type":[NSNumber numberWithInteger:1],
                             @"ent_card_id":@"",
                             @"ent_dri_email":driverEmail,
                             @"ent_coupon":@"",
                             @"ent_surge":[NSNumber numberWithDouble:0.0]
                             };
    NSLog(@"Live Booking Params = %@", params);
    NSError *error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Live Booking response = %@", response);
        if(error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil) {
                NSLog(@"error = %@",error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Live Booking Output = %@", json);
                if (json) {
                    NSLog(@"Response = %@", json);
                    if ([json[@"errFlag"] integerValue] == 1 && [json[@"errNum"] integerValue] == 71) {
                        NSMutableArray* controllerNames = [NSMutableArray new];
                        [controllerNames addObject:@"FirstController"];
                        [WKInterfaceController reloadRootControllersWithNames:controllerNames contexts:[NSArray arrayWithObject:json]];
                    } else if ([json[@"errFlag"] integerValue] == 0) {
                        NSMutableArray* controllerNames = [NSMutableArray new];
                        [controllerNames addObject:@"BookedController"];
                        [WKInterfaceController reloadRootControllersWithNames:controllerNames contexts:nil];
                    }
                } else {
                    NSMutableArray* controllerNames = [NSMutableArray new];
                    [controllerNames addObject:@"FirstController"];
                    [WKInterfaceController reloadRootControllersWithNames:controllerNames contexts:nil];
                }
            });
        }
        else{
            NSLog(@"Error : %@",error.description);
        }
    }];
    [postDataTask resume];
}

@end
