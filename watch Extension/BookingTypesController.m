//
//  BookingTypesController.m
//  SimpleWeather
//
//  Created by 3Embed on 07/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import "BookingTypesController.h"
#import "TypeDetails.h"

@implementation BookingTypesController
@synthesize callService;
@synthesize tableView;
@synthesize vehicleNDriverDetails;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    NSLog(@"Context data = %@", context);
    vehicleNDriverDetails = [[NSMutableArray alloc] initWithArray:context];
    [self configureTableWithData:vehicleNDriverDetails];
    callService = NO;
}

- (void)willActivate {
    [super willActivate];
    if (callService && [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"] != 0&& [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"] != 0) {
        NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"];
        NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"];
        callService = NO;
        [self sendServiceToGetCurrentVehiclesAndDriver:lat longitude:log];
    }
}

- (void)didDeactivate {
    [super didDeactivate];
    NSLog(@"Did Deactivate");
}

- (void)configureTableWithData:(NSArray*) dataObjects {
    [self.tableView setNumberOfRows:[dataObjects count] withRowType:@"TypeDetails"];
    for (NSInteger i = 0; i < self.tableView.numberOfRows; i++) {
        TypeDetails* row = [self.tableView rowControllerAtIndex:i];
        NSDictionary* dataObj = [dataObjects objectAtIndex:i];
        [row.typeName setText:dataObj[@"type_name"]];
        NSString *imgURL= [NSString stringWithFormat:@"http://188.166.48.202/safar/pics/%@", dataObjects[i][@"vehicle_img_off"]];
        [self loadImage:imgURL forImageView:row.typeImage];
    }
}
-(void)loadImage:(NSString *)imageURL forImageView:(WKInterfaceImage *) imageView {
    NSURL *url = [NSURL URLWithString:imageURL];
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              UIImage *downloadedImage = [UIImage imageWithData:data];
                                              // Do what you like with your image
                                              [imageView setImage:downloadedImage];
                                          }];
    
    [downloadTask resume];

}

- (void)table:(WKInterfaceTable *)table didSelectRowAtIndex:(NSInteger)rowIndex {
    if ([vehicleNDriverDetails[rowIndex][@"masters"] count]) {
        [self pushControllerWithName:@"OnlineDriver" context:vehicleNDriverDetails[rowIndex]];
    } else {
        [self pushControllerWithName:@"NoDrivers" context:nil];
    }
    callService = YES;
}


#pragma mark - WebServiceCall -

-(void)sendServiceToGetCurrentVehiclesAndDriver:(NSString *)latitude longitude:(NSString *)longitude {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"http://188.166.48.202/safar/services_v2.php/getVehilesAndDriversTest"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:45.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    if(!sessionToken.length) {
        sessionToken = @"";
    }
    NSString *deviceID = [shared objectForKey:@"deviceidSiri"];
    if(!deviceID.length) {
        deviceID = @"";
    }
    NSDictionary *params = @{
                             @"lt":latitude,
                             @"lg":longitude,
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID
                             };
    NSError *error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil) {
                NSLog(@"error = %@",error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sortVehiclesAndDriversData:json];
            });
        }
        else{
            NSLog(@"Error : %@",error.description);
        }
    }];
    [postDataTask resume];
}


-(void)sortVehiclesAndDriversData:(NSDictionary *)response {
    if ([response[@"flag"] integerValue] == 1) {
        
    } else if([response[@"flag"] integerValue] == 0) {
        NSMutableArray *types = [response[@"types"] mutableCopy];
        [self configureTableWithData:types];
    }
}

@end
