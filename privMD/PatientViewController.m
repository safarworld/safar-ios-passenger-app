//
//  PatientViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PatientViewController.h"
#import "HelpViewController.h"
#import "ViewController.h"
#import "NetworkHandler.h"
#import "AppointedDoctor.h"
#import "User.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "LocationServicesMessageVC.h"
#import <Canvas/CSAnimationView.h>
#import <AVFoundation/AVFoundation.h>

static NSBundle *bundle = nil;

@interface PatientViewController () <UserDelegate>

@property (nonatomic, strong) AVPlayer *avplayer;
@property (strong, nonatomic) IBOutlet UIView *movieView;
@property (strong, nonatomic) IBOutlet UIView *gradientView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@end

@implementation PatientViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    if ([UIScreen mainScreen].bounds.size.height < 568)
    {
        imageview.image = [UIImage imageNamed:@"Default"];
    }
    else
    {
        imageview.image = [UIImage imageNamed:@"Default-568h"];
    }
    [self.view addSubview:imageview];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getDirection];
//    if (self.avplayer)
//    {
//        [self.avplayer pause];
//    }
}

-(void)viewDidAppear:(BOOL)animated
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
    {
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(removeSplash) userInfo:Nil repeats:NO];
    }
    else
    {
        [self removeSplash];
    }
}
-(void) viewDidDisappear:(BOOL)animated
{
//    if (self.avplayer)
//    {
//        [self.avplayer pause];
//    }
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

-(void)removeSplash
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    }
    else
    {
        HelpViewController *help = [self.storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
        [[self navigationController ] pushViewController:help animated:NO];
//        if (!self.avplayer)
//        {
//            [self createMoviePlayer];
//        }
        [self.view bringSubviewToFront:self.movieView];
    }
}

- (void)createMoviePlayer
{
    NSURL *movieURL = [[NSBundle mainBundle] URLForResource:@"Roadyo" withExtension:@"mov"];
    //movieView
    self.movieView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height)];
    [self.view addSubview:self.movieView];
    
    //grediant view
    self.gradientView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,[[UIScreen mainScreen] bounds].size.height)];
    [self.view addSubview:self.gradientView];
    [self.view sendSubviewToBack:self.movieView];
    [self.view sendSubviewToBack:self.gradientView];
    
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    
    AVAsset *avAsset = [AVAsset assetWithURL:movieURL];
    AVPlayerItem *avPlayerItem =[[AVPlayerItem alloc]initWithAsset:avAsset];
    self.avplayer = [[AVPlayer alloc]initWithPlayerItem:avPlayerItem];
    AVPlayerLayer *avPlayerLayer =[AVPlayerLayer playerLayerWithPlayer:self.avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[[UIScreen mainScreen] bounds]];
    [self.movieView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [self.avplayer seekToTime:kCMTimeZero];
    [self.avplayer setVolume:0.0f];
    [self.avplayer setActionAtItemEnd:AVPlayerActionAtItemEndPause];
    
    [self playerStartPlaying];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[self.avplayer currentItem]];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)playerItemDidReachEnd:(NSNotification *)notification
{
    [self.avplayer pause];
    HelpViewController *help = [self.storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
    [[self navigationController ] pushViewController:help animated:NO];
}

- (void)playerStartPlaying
{
    if (self.avplayer)
    {
        [self.avplayer seekToTime:kCMTimeZero];
    }
    [self.avplayer play];
}

/**
 *  All Directions Method
 */
#pragma mark-CLLocation Delegate Method
- (void) getDirection
{
    clmanager = [[CLLocationManager alloc] init];
    clmanager.delegate = self;
    clmanager.distanceFilter = kCLDistanceFilterNone;
    clmanager.desiredAccuracy = kCLLocationAccuracyBest;
    if  ([clmanager respondsToSelector:@selector(requestWhenInUseAuthorization)])//requestAlwaysAuthorization
    {
        [clmanager requestWhenInUseAuthorization];//requestAlwaysAuthorization
    }
    [clmanager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



/*
 To Get Updated lattitude & longitude
 @return nil.
 */
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSString *lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString * log = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    [[NSUserDefaults standardUserDefaults]setObject:lat forKey:KNUCurrentLat];
    [[NSUserDefaults standardUserDefaults]setObject:log forKey:KNUCurrentLong];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat] && [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong] == 0)
    {
        [self getDirection];
    }
    else
    {
        [self requestForGoogleGeocoding :lat:log];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [clmanager stopUpdatingLocation];
    }
}

/*
 To print error msg of location manager
 @param error msg.
 @return nil.
 */

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
    {
        // The user denied your app access to location information.
    }
    else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorNetwork)
    {
        
    }
    else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
    {
        
    }
    [self gotoLocationServicesMessageViewController];
}

-(void)locationServicesChanged:(NSNotification*)notification
{
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController
{
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}

/*
 Get GoogleGeocoding
 @Params lattitude, longitude
 @Return nil.
 */

-(void)requestForGoogleGeocoding :(NSString*)lattitude :(NSString*)longitude
{
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    [handler setRequestType:eLatLongparser];
    NSString *string = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&sensor=true",lattitude,longitude ];
    
    NSURL *url = [NSURL URLWithString:string];
    
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(googleReverseGeocodingResponse:)];
    
}

-(void)googleReverseGeocodingResponse:(NSDictionary*)_response
{
    //hide Progress indcator
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    NSDictionary *dict  = [_response objectForKey:@"ItemsList"];
    
    if(!_response)
    {
        return;
    }
    else
    {
        NSString *country = [dict objectForKey:@"country"];
        NSString *state = [dict objectForKey:@"administrative_area_level_1"];
        NSString *city = [dict objectForKey:@"locality"];
        
        [[NSUserDefaults standardUserDefaults]setObject:country forKey:KNUserCurrentCountry];
        [[NSUserDefaults standardUserDefaults]setObject:state forKey:KNUserCurrentState];
        [[NSUserDefaults standardUserDefaults]setObject:city forKey:KNUserCurrentCity];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
}

-(void)userDidUpdateSessionSucessfully:(BOOL)sucess {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    
    ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    
}
-(void)userDidUpdateSessionUnSucessfully:(BOOL)sucess {
    
    HelpViewController *help = [self.storyboard instantiateViewControllerWithIdentifier:@"helpVC"];
    
    [[self navigationController ] pushViewController:help animated:NO];
}




@end
