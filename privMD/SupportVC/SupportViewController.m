//
//  SupportViewController.m
//  DallasPoshPassenger
//
//  Created by Rahul Sharma on 14/05/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "SupportViewController.h"
#import "SupportDetailTableViewController.h"
#import "SupportWebViewController.h"
#import "CustomNavigationBar.h"
#import "XDKAirMenuController.h"
#import "SupportTableViewCell.h"

@interface SupportViewController ()<CustomNavigationBarDelegate>
{
    NSMutableArray *listOfItemsArray;
    NSMutableArray *detailsOfListArray;
    NSString *navTitle;
    NSString *webUrlLink;
}
@property (weak, nonatomic) IBOutlet UITableView *supportTable;

@end

@implementation SupportViewController

- (void) addCustomNavigationBar
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"SUPPORT", @"SUPPORT")];
    [self.view addSubview:customNavigationBarView];
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    listOfItemsArray = [[NSMutableArray alloc]initWithObjects:@"Request a ride",@"Perfecting your pickup",@"Paying for your trip",@"Understanding your fare",@"Using the app",@"What cities is Dallash-Posh available in?",@"How old i must be to use Dallash-Posh",@"Code of Conduct", nil];
    
    listOfItemsArray = [NSMutableArray array];
    detailsOfListArray = [NSMutableArray array];
    [self addCustomNavigationBar];
    [self sendRequestToGetSupportDetails];
    [_supportTable reloadData];
    
}
-(void)sendRequestToGetSupportDetails {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    //setup parameter
    
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"support"
                                    paramas:[NSDictionary dictionary]
                                    onComplition:^(BOOL success, NSDictionary *response) {
                                   
                                   if (success)
                                   {
                                       listOfItemsArray = [response[@"support"]mutableCopy];
                                       [_supportTable reloadData];
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                       
                                   }
                                   else
                                   {
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
 
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return listOfItemsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *cellIdentifier=@"Cell";
    SupportTableViewCell *cell = nil; //[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if(cell==nil)
    {
        cell =[[SupportTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        cell.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:0.5];
        tableView.backgroundColor=[UIColor clearColor];
        
    }
    
    cell.titleLabel.text = listOfItemsArray[indexPath.row][@"tag"];
    cell.accessoryImage.image = [UIImage imageNamed:@"cell_arrow_icon"];
    
    if(indexPath.row == 0)
    {
        cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_top.png"];
    }
    else
    {
        cell.cellBgImage.image = [UIImage imageNamed:@"selectpayment_textlayout_middle.png"];
    }

    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 20;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 20)];
    view.backgroundColor = [[UIColor whiteColor]colorWithAlphaComponent:0.5];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 49;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    detailsOfListArray = listOfItemsArray[indexPath.row][@"childs"];
    
    if (detailsOfListArray.count == 0) {
        
        webUrlLink = [NSString stringWithFormat:@"%@%@",BASE_URL_SUPPORT,listOfItemsArray[indexPath.row][@"link"]];
        [self performSegueWithIdentifier:@"supportWebViewVC" sender:indexPath];
    }
    else {
      
        [self performSegueWithIdentifier:@"supportDetailsVC" sender:indexPath];
        
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *path = (NSIndexPath *)sender;

    
    if ([[segue identifier] isEqualToString:@"supportDetailsVC"])
    {
        SupportDetailTableViewController *CLVC = (SupportDetailTableViewController*)[segue destinationViewController];
        CLVC.detailsArray = [detailsOfListArray mutableCopy];
        CLVC.title = navTitle;
        
    }
    else if ([[segue identifier] isEqualToString:@"supportWebViewVC"])
    {
        SupportWebViewController *webView = (SupportWebViewController*)[segue destinationViewController];
        
        webUrlLink = [NSString stringWithFormat:@"%@%@",BASE_URL_SUPPORT,listOfItemsArray[path.row][@"link"]];
        webView.title = NSLocalizedString(@"LEARN MORE", @"LEARN MORE");
        webView.weburl = webUrlLink;

        
    }
    
}


@end
