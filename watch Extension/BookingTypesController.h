//
//  BookingTypesController.h
//  SimpleWeather
//
//  Created by 3Embed on 07/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface BookingTypesController : WKInterfaceController
@property (nonatomic) BOOL callService;

@property (weak, nonatomic) IBOutlet WKInterfaceTable *tableView;
@property (strong, nonatomic) NSMutableArray *vehicleNDriverDetails;
@end
