//
//  TypeDetails.h
//  SimpleWeather
//
//  Created by 3Embed on 07/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <WatchKit/WatchKit.h>

@interface TypeDetails : NSObject
@property (weak, nonatomic) IBOutlet WKInterfaceImage *typeImage;
@property (weak, nonatomic) IBOutlet WKInterfaceLabel *typeName;

@end
