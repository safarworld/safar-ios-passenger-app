//
//  WalletViewController.h
//  RoadyoDispatch
//
//  Created by 3Embed on 20/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WalletViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;

@property (weak, nonatomic) IBOutlet UIView *balanceView;
@property (weak, nonatomic) IBOutlet UILabel *balanceStatusLabel;
@property (weak, nonatomic) IBOutlet UIButton *currentBalanceBtn;

@property (weak, nonatomic) IBOutlet UIView *balanceAddView;
@property (weak, nonatomic) IBOutlet UILabel *balanceAdd1Label;
@property (weak, nonatomic) IBOutlet UILabel *balanceAdd2Label;
@property (weak, nonatomic) IBOutlet UIImageView *AddBalanceImageView;

@property (weak, nonatomic) IBOutlet UITextField *addMoneyTextField;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn1;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn2;
@property (weak, nonatomic) IBOutlet UIButton *addMoneyBtn3;


@property (weak, nonatomic) IBOutlet UIView *promoCodeView;
@property (weak, nonatomic) IBOutlet UITextField *promoCodeTextField;
@property (weak, nonatomic) IBOutlet UIButton *promoCodeApplyBtn;


- (IBAction)currentBalanceBtnAction:(id)sender;
- (IBAction)addMoneyBtn1Action:(id)sender;
- (IBAction)addMoneyBtn2Action:(id)sender;
- (IBAction)addMoneyBtn3Action:(id)sender;
- (IBAction)promoCodeApplyBtnAction:(id)sender;

- (IBAction)addMoneyBtnAction:(id)sender;

@end
