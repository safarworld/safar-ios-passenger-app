//
//  IntentViewController.h
//  siriUI
//
//  Created by 3Embed on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import <IntentsUI/IntentsUI.h>

@interface IntentViewController : UIViewController <INUIHostedViewControlling>

@end
