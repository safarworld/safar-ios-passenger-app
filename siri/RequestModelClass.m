//
//  RequestModelClass.m
//  Safar
//
//  Created by 3Embed on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "RequestModelClass.h"

static RequestModelClass *sharedInstance = nil;

@implementation RequestModelClass
@synthesize onCompletion, onCompletionForLiveBooking;
@synthesize location;

+ (instancetype) getSharedInstance {
    if (!sharedInstance)
    {
        sharedInstance  = [[self alloc] init];
    }
    return sharedInstance;
}


-(void)sendServiceToGetCurrentVehiclesAndDriver:(NSString *)latitude longitude:(NSString *)longitude {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"http://188.166.48.202/safar/services_v2.php/getVehilesAndDrivers"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:45.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    if(!sessionToken.length) {
        sessionToken = @"";
    }
    NSString *deviceID = [shared objectForKey:@"deviceidSiri"];
    if(!deviceID.length) {
        deviceID = @"";
    }
    
    NSDictionary *params = @{
                             @"lt":latitude,
                             @"lg":longitude,
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID
                             };
    NSError *error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil) {
                NSLog(@"error = %@",error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Output = %@", json);
                [self sortVehiclesAndDriversData:json];
            });
        }
        else{
            NSLog(@"Error : %@",error.description);
        }
    }];
    [postDataTask resume];
}


-(void)sortVehiclesAndDriversData:(NSDictionary *)response {
    if ([response[@"errFlag"] integerValue] == 1) {
        onCompletion(nil);
    } else if([response[@"errFlag"] integerValue] == 0) {
        if([response[@"types"] count]){
            onCompletion(response[@"types"]);
        }
    }
}

-(void)sendServiceToBookDriver:(NSDictionary *)bookingRequiredData andDriverEmail:(NSString *) driverEmail {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"http://188.166.48.202/safar/services_v2.php/liveBooking"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:45.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    //setup parameters
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    NSString *deviceID = [shared objectForKey:@"deviceidSiri"];
    NSString *zipcode = @"90210";
    NSString *carTypeID = bookingRequiredData[@"carTypeId"];
    NSString *pickupLatitude = bookingRequiredData[@"pickUpLat"];
    NSString *pickupLongitude = bookingRequiredData[@"pickUpLong"];
    NSString *pickAddress = bookingRequiredData[@"pickUpAddress"];
    NSString *dropLatitude = bookingRequiredData[@"dropOffLat"];
    NSString *dropLongitude = bookingRequiredData[@"dropOffLong"];
    NSString *dropAddress = bookingRequiredData[@"dropOffAddress"];
    NSString *cardId = @"";
    NSString *promo = @"";
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateTime = [dateFormatter stringFromDate:now];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_wrk_type":carTypeID,
                             @"ent_lat":pickupLatitude,
                             @"ent_long":pickupLongitude,
                             @"ent_addr_line1":pickAddress,
                             @"ent_addr_line2":@"",
                             @"ent_drop_lat":dropLatitude,
                             @"ent_drop_long":dropLongitude,
                             @"ent_drop_addr_line1":dropAddress,
                             @"ent_drop_addr_line2":@"",
                             @"ent_zipcode":zipcode,
                             @"ent_extra_notes":@"",
                             @"ent_date_time":dateTime,
                             @"ent_later_dt":@"",
                             @"ent_payment_type":[NSNumber numberWithInteger:1],
                             @"ent_card_id":cardId,
                             @"ent_dri_email":driverEmail,
                             @"ent_coupon":promo,
                             @"ent_surge":[NSNumber numberWithDouble:0.0]
                             };
    NSLog(@"Live Booking Params = %@", params);
    NSError *error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@"Live Booking response = %@", response);
        if(error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil) {
                NSLog(@"error = %@",error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Live Booking Output = %@", json);
                if (json) {
                    onCompletionForLiveBooking(json);
                } else {
                    onCompletionForLiveBooking(nil);
                }
            });
        }
        else{
            NSLog(@"Error : %@",error.description);
        }
    }];
    [postDataTask resume];
}





@end
