//
//  SignInViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignInViewController.h"
#import "ViewController.h"
#import "Database.h"

@interface SignInViewController ()
@property (nonatomic,strong)  NSMutableArray *arrayContainingCardInfo;

@end

@implementation SignInViewController

@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize navDoneButton;
@synthesize emailImageView;
@synthesize signinButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg-568h"]];
    
    self.navigationItem.title = NSLocalizedString(@"SIGN IN", @"SIGN IN");
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = NO;
    [self createNavLeftButton];
    _fNameLabel.backgroundColor = WHITE_COLOR;
    _passwordLabel.backgroundColor = WHITE_COLOR;
    [Helper setButton:signinButton Text:NSLocalizedString(@"SIGN IN", @"SIGN IN") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [signinButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [signinButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];

    [Helper setButton:_forgotPasswordButton Text:NSLocalizedString(@"Forgot password?", @"Forgot password?") WithFont:Roboto_Regular FSize:12 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [_forgotPasswordButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];

    [emailTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    
    emailTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    emailTextField.textColor = UIColorFromRGB(0x333333);
    passwordTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    passwordTextField.textColor = UIColorFromRGB(0x333333);

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
}

-(void) createNavLeftButton
{
     UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)createnavRightButton
{
    navDoneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navDoneButton addTarget:self action:@selector(DoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    [navDoneButton setFrame:CGRectMake(0.0f,0.0f, 60,30)];
    
    [Helper setButton:navDoneButton Text:NSLocalizedString(@"Done", @"Done") WithFont:@"HelveticaNeue" FSize:17 TitleColor:[UIColor blueColor] ShadowColor:nil];
    
    
    [navDoneButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    // Create a container bar button
    UIBarButtonItem *containingnextButton = [[UIBarButtonItem alloc] initWithCustomView:navDoneButton];
    
    self.navigationItem.rightBarButtonItem = containingnextButton;
    
}
-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    
}

-(void)viewDidAppear:(BOOL)animated
{
  [emailTextField becomeFirstResponder];
    
}

-(void)dismissKeyboard
{
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    
}

-(void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
    //[self callPop];
}


-(void)callPop
{
    [UIView  beginAnimations:nil context:NULL];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.75];
    [UIView setAnimationTransition:UIViewAnimationTransitionNone forView:self.navigationController.view cache:NO];
    [UIView commitAnimations];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDelay:0.375];
    [self.navigationController popViewControllerAnimated:NO];
    [UIView commitAnimations];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)signInUser
{
    NSString *email = emailTextField.text;
    
    NSString *password = passwordTextField.text;
    
    if((unsigned long)email.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Email ID", @"Enter Email ID")];
        [emailTextField becomeFirstResponder];
    }
    else if([self emailValidationCheck:email] == 0)
    {
        //email is not valid
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email ID", @"Invalid Email ID")];
        emailTextField.text = @"";
        [emailTextField becomeFirstResponder];
        
    }
    else if((unsigned long)password.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Password", @"Enter Password")];
        [passwordTextField becomeFirstResponder];
    }
    else
    {
        checkLoginCredentials = YES;
        [self sendServiceForLogin];
    }
}

-(void)DoneButtonClicked
{
    
    [self signInUser];
}

-(void)sendServiceForLogin
{
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"garbagevalue";
    }
    NSString *city = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity])
        city = @"";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity];
    
    NSDictionary *params = @{
                             KDALoginEmail:emailTextField.text,
                             KDALoginPassword:passwordTextField.text,
                             KDALoginDevideId:deviceId,
                             KDALoginPushToken:pToken,
                             KDASignUpLatitude:lat,
                             KDASignUpLongitude:lon,
                             KDASignUpCity:city,
                             KDALoginDeviceType:@"1",
                             KDASignUpLanguage:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"],
                             KDALoginUpDateTime:[Helper getCurrentDateTime],
                             };
    NSLog(@"Params = %@", params);
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientLogin
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self loginResponse:response];
                                   }
                               }];
    
    // Activating progress Indicator.
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Signing In...", @"Signing In...")];
}

-(void)loginResponse:(NSDictionary *)dictionary
{
    [[ProgressIndicator sharedInstance]hideProgressIndicator];
    itemList = [dictionary mutableCopy];
    
    if(!dictionary)
    {
        return;
    }
    if ([[dictionary objectForKey:@"Error"] length] != 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[dictionary objectForKey:@"Error"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    } else {
        if ([[itemList objectForKey:@"errFlag"]integerValue] == 0 || ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115)) {
            NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
            [shared setObject:flStrForStr(itemList[@"token"]) forKey:@"ent_sess_tokenSiri"];
            NSString *deviceId;
            if (IS_SIMULATOR) {
                deviceId = kPMDTestDeviceidKey;
            }
            else {
                deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
            }
            [shared setObject:flStrForStr(deviceId) forKey:@"deviceidSiri"];
            [shared setObject:flStrForStr(itemList[@"email"]) forKey:@"userEmailIdSiri"];
            [shared synchronize];
            
            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            [ud setObject:flStrForStr(itemList[@"token"]) forKey:KDAcheckUserSessionToken];
            [ud setObject:flStrForStr(itemList[@"apiKey"]) forKey:kNSUMongoDataBaseAPIKey];
            
            [ud setObject:flStrForStr(itemList[@"pub"]) forKey:KNSUPubnubPublishKey];
            [ud setObject:flStrForStr(itemList[@"sub"]) forKey:kNSUPubnubSubscribeKey];
            
            [ud setObject:flStrForStr(itemList[@"chn"]) forKey:kNSUUserPubnubChannel];
            [ud setObject:flStrForStr(itemList[@"serverChn"]) forKey:kNSUServerPubnubChannel];
            [ud setObject:flStrForStr(itemList[@"presenseChn"]) forKey:kNSUPresensePubnubChannel];
            
            [ud setObject:flStrForStr(itemList[@"ClientmapKey"]) forKey:kNSUPlaceAPIKey];
            [ud setObject:flStrForStr(itemList[@"stipeKey"]) forKey:kNSUStripeKey];
            
            [ud setObject:flStrForStr(itemList[@"email"]) forKey:kNSUUserEmailID];
            [ud setObject:flStrForStr(itemList[@"coupon"]) forKey:kNSUUserReferralCode];
            [ud setObject:flStrForStr(itemList[@"shareMessage"]) forKey:kNSUUserShareMessage];

            [ud synchronize];
            
            NSMutableArray *carTypes = [[NSMutableArray alloc]initWithArray:itemList[@"types"]];
            
            if (!carTypes || !carTypes.count){
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
            ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
            self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            
            if ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message:[itemList objectForKey: @"errMsg"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                [alertView show];
            }
        }
        else if ([[itemList objectForKey:@"errFlag"]integerValue] == 1)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message:[itemList objectForKey:@"errMsg"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alertView show];
            checkLoginCredentials = NO;
            passwordTextField.text = @"";
            [passwordTextField becomeFirstResponder];
        }
    }
}

- (void)shakeView:(UIView *)viewToShake
{
    CGFloat t = 15.0;
    CGAffineTransform translateRight  = CGAffineTransformTranslate(CGAffineTransformIdentity, t, 0.0);
    CGAffineTransform translateLeft = CGAffineTransformTranslate(CGAffineTransformIdentity, -t, 0.0);
    
    viewToShake.transform = translateLeft;
    
    [UIView animateWithDuration:0.07 delay:0.0 options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat animations:^{
        [UIView setAnimationRepeatCount:2.0];
        viewToShake.transform = translateRight;
    } completion:^(BOOL finished) {
        if (finished) {
            [UIView animateWithDuration:0.05 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
                viewToShake.transform = CGAffineTransformIdentity;
            } completion:NULL];
        }
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
   
           
}

#pragma mark - TextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField isEqual:emailTextField])
    {
        if (textField.text.length > 0)
        {
            BOOL isValid = [Helper emailValidationCheck:emailTextField.text];
            if (!isValid)
            {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your Email-Id is not valid.Please enter a valid email.", @"Your Email-Id is not valid.Please enter a valid email.")];
                emailTextField.text = @"";
                [emailTextField becomeFirstResponder];
            }
        }
    }
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if(textField == emailTextField)
    {
        if (textField.text.length > 0)
        {
            BOOL isValid = [Helper emailValidationCheck:emailTextField.text];
            if (!isValid)
            {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Your Email-Id is not valid.Please enter a valid email.", @"Your Email-Id is not valid.Please enter a valid email.")];
                emailTextField.text = @"";
                [emailTextField becomeFirstResponder];
            }
            else
            {
                [passwordTextField becomeFirstResponder];
            }
        }
    }
    else
    {
        [passwordTextField resignFirstResponder];
        [self DoneButtonClicked];
    }
    return YES;
}



- (IBAction)forgotPasswordButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Forgot Password?", @"Forgot Password?")
                                        message:NSLocalizedString(@"Enter your email id with which you registered. (An email will be sent to you with details.)", @"Enter your email id with which you registered. (An email will be sent to you with details.)")
                                        delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                        otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    forgotPasswordAlert.tag = 1;
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *txtfld = [forgotPasswordAlert textFieldAtIndex:0];
    txtfld.keyboardType = UIKeyboardTypeEmailAddress;
    [forgotPasswordAlert show];
}

- (IBAction)signInButtonClicked:(id)sender {
    [self.view endEditing:YES];
     [self signInUser];
}

#pragma mark - UIAlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1)
    {
        UITextField *forgotEmailtext = [alertView textFieldAtIndex:0];
        if (((unsigned long)forgotEmailtext.text.length ==0))
        {
            [self forgotPasswordButtonClicked:self];
        }
        else if ([Helper emailValidationCheck:forgotEmailtext.text] == 0)
        {
            [self forgotPaswordAlertviewTextField:NSLocalizedString(@"Invalid Email ID. Reenter your email ID", @"Invalid Email ID. Reenter your email ID")];
        }
        else
        {
            [self retrievePassword:forgotEmailtext.text];
        }
    }
    else
    {
        self.passwordTextField.text = @"";
    }
}

#pragma mark - Custom Methods -

- (void) forgotPaswordAlertviewTextField:(NSString *)message
{
    UIAlertView *forgotPasswordAlert = [[UIAlertView alloc]
                                        initWithTitle:NSLocalizedString(@"Message", @"Message")
                                        message:message
                                        delegate:self
                                        cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                        otherButtonTitles:NSLocalizedString(@"Submit", @"Submit"), nil];
    forgotPasswordAlert.tag = 1;
    forgotPasswordAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *txtfld = [forgotPasswordAlert textFieldAtIndex:0];
    txtfld.keyboardType = UIKeyboardTypeEmailAddress;
    [forgotPasswordAlert show];
}

# pragma mark - WebServices -
- (void)retrievePassword:(NSString *)text
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    NSDictionary *params = @{
                             @"ent_email":text,
                             @"ent_user_type":@"2",
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"forgotPassword"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       [self retrievePasswordResponse:response];
                                   }
                               }];
}


-(void)retrievePasswordResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];

    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey: @"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        NSDictionary *dictResponse = [response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
    }
}

- (BOOL) emailValidationCheck: (NSString *) emailToValidate
{
    NSString *regexForEmailAddress = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailValidation = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",regexForEmailAddress];
    return [emailValidation evaluateWithObject:emailToValidate];
}

-(void)addInDataBase
{
    Database *db = [[Database alloc] init];
       {
        for (int i =0; i<_arrayContainingCardInfo.count; i++)
        {
            [db makeDataBaseEntry:_arrayContainingCardInfo[i]];
        }
    }
    
}


@end
