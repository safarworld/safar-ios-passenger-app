//
//  BookingDetailsController.m
//  SimpleWeather
//
//  Created by 3Embed on 02/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import "BookingDetailsController.h"

@implementation BookingDetailsController
@synthesize selectedType, pickupAddress;
@synthesize bookingNotAcceptedLabel;
@synthesize carImage;
@synthesize timeLabel;
@synthesize getRideButton;
@synthesize firstDriver;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    NSLog(@"Context data = %@", context);
    selectedType = [context mutableCopy];
    NSMutableArray *drivers = [context[@"masters"] mutableCopy];
    NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"];
    NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"];
    CLLocation *location = [[CLLocation alloc] initWithLatitude:[lat doubleValue] longitude:[log doubleValue]];
    [self requestForGoogleGeocoding:location];
    if ([drivers  count]) {
        firstDriver = [drivers[0] mutableCopy];
        [self calculateTimeToDisplayOnMarker:[firstDriver[@"lt"] floatValue] Olong:[firstDriver[@"lg"] floatValue] Dlat:[lat floatValue] Dlong:[log floatValue]];
        NSString *imageURL = [NSString stringWithFormat:@"http://188.166.48.202/safar/pics/%@", context[@"vehicle_img_off"]];
        [self loadImage:imageURL forImageView:self.carImage];
    }
    [bookingNotAcceptedLabel setHidden:YES];
    [carImage setHidden:NO];
    [timeLabel setHidden:NO];
    [getRideButton setHidden:YES];

}

-(void)loadImage:(NSString *)imageURL forImageView:(WKInterfaceImage *) imageView {
    NSURL *url = [NSURL URLWithString:imageURL];
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              UIImage *downloadedImage = [UIImage imageWithData:data];
                                              // Do what you like with your image
                                              [imageView setImage:downloadedImage];
                                          }];
    
    [downloadTask resume];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    NSLog(@"Will activate");
}


- (void)didDeactivate {
    [super didDeactivate];
    NSLog(@"Did Deactivate");
}

/*
 Get GoogleGeocoding
 @Params lattitude, longitude
 @Return nil.
 */

-(void)requestForGoogleGeocoding:(CLLocation *)location {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           pickupAddress = @"";
                           return;
                       }
                       NSLog(@"Place Mark = %@", placemarks);
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       NSArray *addrArray = placemark.addressDictionary[@"FormattedAddressLines"];
                       pickupAddress = [addrArray componentsJoinedByString:@", "];
                       [getRideButton setHidden:NO];
                   }];
}


- (IBAction)getRideButtonAction {
    NSLog(@"Call live Booking Service");
    NSMutableArray* controllerNames = [NSMutableArray new];
    [controllerNames addObject:@"LiveBookingController"];
    [selectedType setValue:pickupAddress forKey:@"pickupAddress"];
    [WKInterfaceController reloadRootControllersWithNames:controllerNames contexts:[NSArray arrayWithObject:selectedType]];
}

- (void) calculateTimeToDisplayOnMarker:(double)originLat Olong:(double)originLong Dlat:(double)destinationLat Dlong:(double)destinationLong
{
    if (originLat == 0 || originLong == 0 || destinationLat == 0 || destinationLong == 0){
        return;
    }
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
//    NSURL *url = [NSURL URLWithString:@"http://188.166.48.202/safar/services_v2.php/getVehilesAndDriversTest"];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f",originLat,originLong,destinationLat,destinationLong];
    NSURL *url = [NSURL URLWithString:[self removeWhiteSpaceFromURL:strUrl]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:45.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    if(!sessionToken.length) {
        sessionToken = @"";
    }
    NSString *deviceID = [shared objectForKey:@"deviceidSiri"];
    if(!deviceID.length) {
        deviceID = @"";
    }
    NSDictionary *params = @{
                             @"lt":@"",
                             @"lg":@"",
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID
                             };

    NSError *error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil) {
                NSLog(@"error = %@",error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self calculatedTime:json];
            });
        }
        else{
            NSLog(@"Error : %@",error.description);
        }
    }];
    [postDataTask resume];
}

#pragma mark Webservice Handler(Response) -

- (void)calculatedTime:(NSDictionary *)response
{
    if (!response) {
    } else {
        BOOL checkStatus = [self containsKey:@"status" dictionary:response];
        if (!checkStatus) {
            NSString *str = @"Limit Exceed";
            int pickupTime = [firstDriver[@"d"] floatValue]*60/(1000*40);
            if (pickupTime < 1) {
                pickupTime = 1;
            }
            self.timeLabel.text = [NSString stringWithFormat:@"%d MIN", pickupTime];

        } else {
//            NSString *distance = response[@"ItemsList"][@"rows"][0][@"elements"][0][@"distance"][@"value"];
//            distanceOfClosetCar = [NSString stringWithFormat:@"%.2f %@",[distance doubleValue]/kPMDDistanceMetric, kPMDDistanceParameter];
            int timeInSec =  [response[@"rows"][0][@"elements"][0][@"duration"][@"value"] intValue];
            int timeInMin = [self convertTimeInMin:timeInSec];
            NSString *time = NSLocalizedString(@"MIN", @"MIN");
            time = [NSString stringWithFormat:@"%d %@", timeInMin, time];
            self.timeLabel.text = [NSString stringWithFormat:@"%@", time];
        }
    }
}
- (NSString *)removeWhiteSpaceFromURL:(NSString *)url
{
    NSMutableString *string = [[NSMutableString alloc] initWithString:url] ;
    [string replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
    return string;
}


- (BOOL)containsKey: (NSString *)key dictionary:(NSDictionary *) dict {
    BOOL retVal = 0;
    NSArray *allKeys = [dict allKeys];
    retVal = [allKeys containsObject:key];
    if (retVal) {
        if ([dict[@"status"] isEqualToString:@"OK"]) {
            retVal = YES;
        } else {
            retVal = NO;
        }
    }
    return retVal;
}

-(int) convertTimeInMin:(int) timeInSec {
    int min;
    if (timeInSec < 60 && timeInSec <= 0) {
        min = 1;
    } else if (timeInSec > 60) {
        min = timeInSec/60;
        int remainingTime = timeInSec%60;
        if (remainingTime < 60 && remainingTime >= 30) {
            min++;
        }
    } else {
        min = 1;
    }
    return min;
}


@end
