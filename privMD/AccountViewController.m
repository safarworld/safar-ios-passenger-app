//
//  AccountViewController.m
//  privMD
//
//  Created by Rahul Sharma on 19/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AccountViewController.h"
#import "XDKAirMenuController.h"
#import "PatientViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "CustomNavigationBar.h"
#import "Database.h"
#import "LanguageManager.h"
#import "Locale.h"

@interface AccountViewController () <CustomNavigationBarDelegate, UITextFieldDelegate>
@end

@implementation AccountViewController
@synthesize accFirstNameTextField;
@synthesize accLastNameTextField;
@synthesize accEmailTextField;
@synthesize accPhoneNoTextField;
@synthesize accProfilePic;
@synthesize accProfileButton;
@synthesize activityIndicator;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

#pragma mark - WebService call

-(void)sendServiceForUpdateProfile
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Saving...", @"Saving...")];
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *fName = accFirstNameTextField.text;
    NSString *lName = accLastNameTextField.text;
    NSString *eMail = accEmailTextField.text;
    NSString *phone = accPhoneNoTextField.text;
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_first_name":fName,
                               @"ent_last_name":lName,
                               @"ent_email":eMail,
                               @"ent_phone":phone,
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    NSLog(@"Update Profile Params = %@", params);
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"updateProfile" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             NSLog(@"Update Profile Response = %@", response);
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success)
                             {
                                 [self updateProfileResponse:response];
                             }
                         }];

}

-(void)updateProfileResponse:(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [[NSUserDefaults standardUserDefaults] setObject:accFirstNameTextField.text forKey:KDAFirstName];
            [[NSUserDefaults standardUserDefaults] setObject:accLastNameTextField.text forKey:KDALastName];
            [[NSUserDefaults standardUserDefaults] setObject:accEmailTextField.text forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:accPhoneNoTextField.text forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults] synchronize];            
        }
    }
}

-(void)sendServiceForegetProfileData
{
    CGRect fra = accProfilePic.frame;
    fra.origin.x = accProfilePic.frame.size.width/2-10;
    fra.origin.y = accProfilePic.frame.size.height/2-10;
    fra.size.width = 20;
    fra.size.height = 20;
    activityIndicator = [[UIActivityIndicatorView alloc]init];
    activityIndicator.frame = fra;
    [self.accProfilePic addSubview:activityIndicator];
    activityIndicator.backgroundColor=[UIColor clearColor];
    activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    [activityIndicator startAnimating];

    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_date_time":[Helper getCurrentDateTime]
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getProfile"
                              paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success)
                             {
                                 [self getProfileResponse:response];
                             }
                    }];
}

-(void)getProfileResponse:(NSDictionary *)response
{
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
        
    }
    
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 96 || [response[@"errNum"] intValue] == 94 )) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            NSString *strImageUrl = flStrForStr(dictResponse[@"pPic"]);
            if (strImageUrl.length)
            {
                strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,dictResponse[@"pPic"]];
                __weak typeof(self) weakSelf = self;
                [accProfilePic sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                 placeholderImage:nil
                                        completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                            [weakSelf.activityIndicator stopAnimating];
                                            
                                        }];
            }
            if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
            {
                accFirstNameTextField.textAlignment = NSTextAlignmentRight;
                accLastNameTextField.textAlignment = NSTextAlignmentRight;
                accEmailTextField.textAlignment = NSTextAlignmentRight;
                accPhoneNoTextField.textAlignment = NSTextAlignmentRight;
                self.chooseLanLabel.textAlignment = NSTextAlignmentRight;
            }
            else
            {
                accFirstNameTextField.textAlignment = NSTextAlignmentLeft;
                accLastNameTextField.textAlignment = NSTextAlignmentLeft;
                accEmailTextField.textAlignment = NSTextAlignmentLeft;
                accPhoneNoTextField.textAlignment = NSTextAlignmentLeft;
                self.chooseLanLabel.textAlignment = NSTextAlignmentLeft;
            }
          
            accFirstNameTextField.text = dictResponse[@"fName"];
            accLastNameTextField.text = dictResponse[@"lName"];
            accPhoneNoTextField.text = [NSString stringWithFormat:@"+%@",dictResponse[@"phone"]];
            accEmailTextField.text = dictResponse[@"email"];

            [[NSUserDefaults standardUserDefaults] setObject:accFirstNameTextField.text forKey:KDAFirstName];
            [[NSUserDefaults standardUserDefaults] setObject:accLastNameTextField.text forKey:KDALastName];
            [[NSUserDefaults standardUserDefaults] setObject:accEmailTextField.text forKey:KDAEmail];
            [[NSUserDefaults standardUserDefaults] setObject:accPhoneNoTextField.text forKey:KDAPhoneNo];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
    }
}
- (void) updateSelectedLanguage:(NSString *)selectedLanguage
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Updating Language...", @"Updating Laguage...")];
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    NSDictionary *params = @{
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceId,
                             @"ent_lang":selectedLanguage,
                             @"ent_user_type":@"2",
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateLanguage"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self updateSelectedLanguageResponse:response];
                                   }
                               }];
}

-(void)updateSelectedLanguageResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"] isEqualToString:@"1"])
            {
                [self.englsihLanBtn setSelected:YES];
                [self.arabicLanBtn setSelected:NO];
                LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
                Locale *localeObj = languageManager.availableLocales[0];
                [languageManager setLanguageWithLocale:localeObj];
            }
            else  if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"] isEqualToString:@"2"])
            {
                [self.englsihLanBtn setSelected:NO];
                [self.arabicLanBtn setSelected:YES];
                LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
                Locale *localeObj = languageManager.availableLocales[1];
                [languageManager setLanguageWithLocale:localeObj];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHomeVC" object:nil userInfo:nil];
            [self updateUI];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg"]];
    
    accFirstNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDAFirstName];
    accFirstNameTextField.textColor = UIColorFromRGB(0x000000);
    accFirstNameTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    
    accLastNameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDALastName];
    accLastNameTextField.textColor = UIColorFromRGB(0x000000);
    accLastNameTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tapGesture];
    
    accEmailTextField.userInteractionEnabled = NO;
    accFirstNameTextField.userInteractionEnabled = NO;
    accLastNameTextField.userInteractionEnabled = NO;
    accPhoneNoTextField.userInteractionEnabled = NO;
    accProfileButton.userInteractionEnabled = NO;
    
    accEmailTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:KDAEmail];
    accEmailTextField.textColor = UIColorFromRGB(0x000000);
    accEmailTextField.font = [UIFont fontWithName:Lato_Regular size:13];
    
    accPhoneNoTextField.text = [[NSUserDefaults standardUserDefaults]objectForKey:KDAPhoneNo];
    accPhoneNoTextField.textColor = UIColorFromRGB(0x000000);
    accPhoneNoTextField.font = [UIFont fontWithName:Lato_Regular size:13];
    
    
    [self.englsihLanBtn setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [self.englsihLanBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateSelected];
    [self.arabicLanBtn setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [self.arabicLanBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateSelected];
    
    if (kLanguageSupport)
    {
        self.languageView.hidden = NO;
    }
    else
    {
        self.languageView.hidden = YES;
    }
    [self updateUI];
    [self sendServiceForegetProfileData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [accPhoneNoTextField resignFirstResponder];
    [accFirstNameTextField resignFirstResponder];
    [accLastNameTextField resignFirstResponder];
    [accEmailTextField resignFirstResponder];
}

#pragma mark ButtonAction Methods -

- (void)menuButtonPressedAccount
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (void)saveUserDetails:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    mBut.userInteractionEnabled = YES;
    {
        if(mBut.isSelected)
        {
            mBut.selected =NO;
            [mBut setTitle:NSLocalizedString(@"EDIT", @"EDIT") forState:UIControlStateNormal];
            accEmailTextField.userInteractionEnabled = NO;
            accFirstNameTextField.userInteractionEnabled = NO;
            accLastNameTextField.userInteractionEnabled = NO;
            accPhoneNoTextField.userInteractionEnabled = NO;
            accProfileButton.userInteractionEnabled = NO;
            if(textFieldEditedFlag == 1)
            {
                if(!accFirstNameTextField.text.length)
                {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your name", @"Please enter your name")];
                }
                else if(!accPhoneNoTextField.text.length)
                {
                    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your mobile number", @"Please enter your mobile number")];
                }
                else
                {
                    [self sendServiceForUpdateProfile];
                    textFieldEditedFlag = 0;
                }
            }
        }
        else
        {
            mBut.selected = YES;
            [mBut setTitle:NSLocalizedString(@"SAVE", @"SAVE") forState:UIControlStateSelected];
            accEmailTextField.userInteractionEnabled = NO;
            accProfileButton.userInteractionEnabled = YES;
            accFirstNameTextField.userInteractionEnabled = YES;
            accLastNameTextField.userInteractionEnabled = YES;
            accPhoneNoTextField.userInteractionEnabled = YES;
        }
    }
}




#pragma mark Custom Methods -

- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle: NSLocalizedString(@"PROFILE", @"PROFILE")];
    [customNavigationBarView createRightBarButton];
    [customNavigationBarView setRightBarButtonTitle:NSLocalizedString(@"EDIT", @"EDIT")];
    [self.view addSubview:customNavigationBarView];
    
}

-(void)rightBarButtonClicked:(UIButton *)sender
{
    if(!accFirstNameTextField.text.length)
    {
        [accFirstNameTextField becomeFirstResponder];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your name", @"Please enter your name")];
    }
    else if(!accPhoneNoTextField.text.length)
    {
        [accPhoneNoTextField becomeFirstResponder];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter your mobile number", @"Please enter your mobile number")];
    }
    else
    {
        [self saveUserDetails:sender];
    }
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self menuButtonPressedAccount];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

- (IBAction)profilePicButtonClicked:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Edit Picture", @"Edit Picture")
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                               destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),
                                                                                            NSLocalizedString(@"Choose From Library", @"Choose From Library"), nil];
    actionSheet.tag = 1;
    [actionSheet showInView:self.view];
}

- (IBAction)passwordButtonClicked:(id)sender
{
    [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please visit our website safar.net to change your password.", @"Please visit our website safar.net to change your password.")];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 1)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked:nil];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked:nil];
                break;
            }
            default:
                break;
        }
    }
}


-(void)cameraButtonClicked:(id)sender
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message: NSLocalizedString(@"Camera is not available", @"Camera is not available") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked:(id)sender
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
    } else
    {
        [self presentViewController:picker animated:YES completion:nil];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Uploading Profile Pic...", @"Uploading Profile Pic...")];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    accProfilePic.image = _pickedImage;
    _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
    
    UploadFile * upload = [[UploadFile alloc]init];
    upload.delegate = self;
    [upload uploadImageFile:_pickedImage];
    
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
   
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
#pragma UploadFileDelegate

-(void)uploadFile:(UploadFile *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
}
-(void)uploadFile:(UploadFile *)uploadfile didFailedWithError:(NSError *)error
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Oops!", @"Oops!") Message:NSLocalizedString(@"Your profile photo has not been updated try again.", @"Your profile photo has not been updated try again.")];
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    textFieldEditedFlag = 1;
    if(!accFirstNameTextField.text.length)
    {
        [accFirstNameTextField becomeFirstResponder];
    }
    else if(!accPhoneNoTextField.text.length)
    {
        [accPhoneNoTextField becomeFirstResponder];
    }
}
- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(accFirstNameTextField)
    {
        if(!accFirstNameTextField.text.length)
        {
            [accFirstNameTextField becomeFirstResponder];
        }
        else
        {
            [accFirstNameTextField resignFirstResponder];
        }
    }
    else if(accPhoneNoTextField)
    {
        if(!accPhoneNoTextField.text.length)
        {
            [accPhoneNoTextField becomeFirstResponder];
        }
        else
        {
            [accPhoneNoTextField resignFirstResponder];
        }
    }
    return YES;
}

-(void)moveViewUpToPoint:(int)point
{
    CGRect rect = self.view.frame;
    rect.origin.y = point;
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         self.view.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
    
    
    
}

-(void)moveViewDown
{
    CGRect rect = self.view.frame;
    rect.origin.y = 0;
    
    
    [UIView animateWithDuration:0.4
                     animations:^(void){
                         self.view.frame = rect;
                     }
                     completion:^(BOOL finished){
                     }];
    
}

- (IBAction)englishLanSelected:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"selectedLanguage"];
    [self updateSelectedLanguage:@"1"];
}


- (IBAction)arabicLanSelected:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"selectedLanguage"];
    [self updateSelectedLanguage:@"2"];
}

-(void)updateUI
{
    [self addCustomNavigationBar];
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
    {
        accFirstNameTextField.textAlignment = NSTextAlignmentRight;
        accLastNameTextField.textAlignment = NSTextAlignmentRight;
        accEmailTextField.textAlignment = NSTextAlignmentRight;
        accPhoneNoTextField.textAlignment = NSTextAlignmentRight;
        self.chooseLanLabel.textAlignment = NSTextAlignmentRight;
        self.emailLbl.textAlignment = NSTextAlignmentRight;
        self.phoneNoLbl.textAlignment = NSTextAlignmentRight;
        self.passwordLbl.textAlignment = NSTextAlignmentRight;
    }
    else
    {
        accFirstNameTextField.textAlignment = NSTextAlignmentLeft;
        accLastNameTextField.textAlignment = NSTextAlignmentLeft;
        accEmailTextField.textAlignment = NSTextAlignmentLeft;
        accPhoneNoTextField.textAlignment = NSTextAlignmentLeft;
        self.emailLbl.textAlignment = NSTextAlignmentLeft;
        self.phoneNoLbl.textAlignment = NSTextAlignmentLeft;
        self.passwordLbl.textAlignment = NSTextAlignmentLeft;
    }
    
    
    [Helper setToLabel:self.chooseLanLabel Text:NSLocalizedString(@"Choose your language/اختر لغتك", @"Choose your language/اختر لغتك") WithFont:Roboto_Regular FSize:14 Color:UIColorFromRGB(0x333333)];
    self.chooseLanLabel.textAlignment = NSTextAlignmentLeft;
    
    [self.englsihLanBtn setTitle:@"ENGLISH" forState:UIControlStateNormal];
    self.englsihLanBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.englsihLanBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [self.englsihLanBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [self.arabicLanBtn setTitle:@"عربي" forState:UIControlStateNormal];
    self.arabicLanBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.arabicLanBtn setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateNormal];
    [self.arabicLanBtn setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
}


@end
