//
//  PatientAppDelegate.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PatientAppDelegate.h"
#import "HelpViewController.h"
#import "PatientViewController.h"
#import "MapViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "AppointedDoctor.h"
#import "PMDReachabilityWrapper.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <AudioToolbox/AudioToolbox.h>
#import "XDKAirMenuController.h"
#import "Flurry.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>
#import "NetworkStatusShowingView.h"
#import "Locale.h"
#import "LanguageManager.h"
#import "InvoiceView.h"
#import "ViewController.h"


@interface PatientAppDelegate()

@property (nonatomic,strong) HelpViewController *helpViewController;
@property (nonatomic,strong) PatientViewController *splashViewController;
@property (nonatomic,strong) MapViewController *mapViewContoller;

@end

@implementation PatientAppDelegate
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static UIView *mySharedView;

+ (void) initialize {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    mySharedView = [[UIView alloc]initWithFrame:CGRectMake(0, 430, screenWidth, 50)];
    mySharedView.backgroundColor = [[UIColor redColor]colorWithAlphaComponent:0.5];
}
void uncaughtExceptionHandler(NSException *exception) {
    
//    TELogInfo(@"The app has encountered an unhandled exception: %@", [exception debugDescription]);
//    TELogInfo(@"Stack trace: %@", [exception callStackSymbols]);
//    TELogInfo(@"desc: %@", [exception description]);
//    TELogInfo(@"name: %@", [exception name]);
//    TELogInfo(@"user info: %@", [exception userInfo]);
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [GMSServices provideAPIKey:kPMDGoogleMapsAPIKey];
    [self setupAppearance];
    
    //save device id
    NSString *uuidSting =  [[[UIDevice currentDevice] identifierForVendor] UUIDString];
    [[NSUserDefaults standardUserDefaults] setObject:uuidSting forKey:kPMDDeviceIdKey];
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    [shared setObject:flStrForStr(uuidSting) forKey:@"deviceidSiri"];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken] length]) {
        [shared setObject:[[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken] forKey:@"ent_sess_tokenSiri"];
    }
    [shared synchronize];
    [[NSUserDefaults standardUserDefaults] synchronize];
   
    if ([WCSession isSupported]) {
        WCSession* session = [WCSession defaultSession];
        session.delegate = self;
        [session activateSession];
    }
    [[WCSession defaultSession] updateApplicationContext:[[[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"] dictionaryRepresentation] error:nil];
    [Flurry startSession:kPMDFlurryId];

    [self handlePushNotificationWithLaunchOption:launchOptions];
    [Fabric with:@[[Crashlytics class]]];

    //Network Check
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager.reachabilityManager startMonitoring];
    [manager.reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
         switch (status) {
             case AFNetworkReachabilityStatusReachableViaWWAN:
             case AFNetworkReachabilityStatusReachableViaWiFi: {
                 [NetworkStatusShowingView removeViewShowingNetworkStatus];
             }
             break;
             case AFNetworkReachabilityStatusNotReachable:
             default: {
                 [NetworkStatusShowingView sharedInstance];
             }
                 break;
         }
     }];
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    NSString *dt = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    dt = [dt stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"Push token = %@", dt);
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:KDAgetPushToken];
    } else {
        [[NSUserDefaults standardUserDefaults]setObject:dt forKey:KDAgetPushToken];
        //for testing
    }
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"Push Token Coming with data =%@",userInfo);
    [self handleNotificationForUserInfo:userInfo];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
    NSString *model = [[UIDevice currentDevice] model];
    if ([model isEqualToString:@"iPhone Simulator"]) {
        //device is simulator
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:KDAgetPushToken];
    }
    NSLog(@"Push Notification Error = %@", error);
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLocationServicesChangedNameKey object:nil userInfo:nil];

    // Use Reachability to monitor connectivity
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    if ([reachablity isNetworkAvailable]) {
        XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
        if ([menu.currentViewController isKindOfClass:[MapViewController class]]) {
            PubNubWrapper *pubnub = [PubNubWrapper sharedInstance];
            [pubnub subscribeToUserChannel];
            NSString *driverChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel]);
            if (driverChn.length) {
                [pubnub subscribeToChannel:driverChn];
            }
            [[MapViewController getSharedInstance] checkBookingStatus];
        }
    }       
}

-(void)applicationDidEnterBackground:(UIApplication *)application {
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    if ([reachablity isNetworkAvailable]) {
        PubNubWrapper *pubnub = [PubNubWrapper sharedInstance];
        [pubnub unsubscribeFromMyChannel];
        NSString *driverChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel]);
        if (driverChn.length) {
            [pubnub subscribeToChannel:driverChn];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kNSUPassengerBookingStatusKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    PMDReachabilityWrapper *reachablity = [PMDReachabilityWrapper sharedInstance];
    [reachablity monitorReachability];
    if ([reachablity isNetworkAvailable]) {
        PubNubWrapper *pubnub = [PubNubWrapper sharedInstance];
        [pubnub unsubscribeFromMyChannel];
        NSString *driverChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel]);
        if (driverChn.length) {
            [pubnub unsubscribeFromChannel:driverChn];
        }
    }
    [self saveContext];
    [self stopBackgroundTask];
}

-(void)stopBackgroundTask {
    UIBackgroundTaskIdentifier bgTask = 0;
    UIApplication *app = [UIApplication sharedApplication];
    [app endBackgroundTask:bgTask];
}


#pragma mark - HandlePushNotification
/**
 *  handle push if app is opened by clicking push
 *
 *  @param launchOptions pushpayload dictionary
 */
-(void)handlePushNotificationWithLaunchOption:(NSDictionary*)launchOptions
{
    NSDictionary *remoteNotificationPayload = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (remoteNotificationPayload) {
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken]) {
            [self handleNotificationForUserInfo:remoteNotificationPayload];
        }
    }
}

-(void)noPushForceChangingController:(NSDictionary *)userInfo :(int)type
{
    NSDictionary *dictionary = [userInfo mutableCopy];
    if(type == kNotificationTypeBookingOnMyWay)
    {
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"email"] forKey:KUDriverEmail];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"apptDt"] forKey:KUBookingDate];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"bid"] forKey:@"BOOKINGID"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"fName"] forKey:@"drivername"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"pPic"] forKey:@"driverpic"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"mobile"] forKey:@"DriverTelNo"];
        [[NSUserDefaults standardUserDefaults] setObject:dictionary[@"chn"] forKey:kNSUDriverPubnubChannel];
        
        //Status Key For Changing content on HomeViewController
        [[NSUserDefaults standardUserDefaults] setInteger:kNotificationTypeBookingOnMyWay forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //Local Notification to notifyi HomeViewController
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DRIVERONTHEWAY" object:nil userInfo:nil];
    }
    else if(type == kNotificationTypeBookingReachedLocation)
    {
        //Status Key For Changing content on HomeViewController
        [[NSUserDefaults standardUserDefaults] setInteger:kNotificationTypeBookingReachedLocation forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //Local Notification to notifyi HomeViewController
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DRIVERREACHED" object:nil userInfo:nil];
    }
    else if(type == kNotificationTypeBookingStarted)
    {
        //Status Key For Changing content on HomeViewController
        [[NSUserDefaults standardUserDefaults] setInteger:kNotificationTypeBookingStarted forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //Local Notification to notifyi HomeViewController
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBSTARTED" object:nil userInfo:nil];
    }
    else if (type == kNotificationTypeBookingComplete)
    {
        for (UIView *view in [UIApplication sharedApplication].keyWindow.subviews)
        {
            if([view isKindOfClass:[InvoiceView class]])
            {
                return;
            }
        }
        [[NSUserDefaults standardUserDefaults] setInteger:kNotificationTypeBookingComplete forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        InvoiceView* invoiceView = [[InvoiceView alloc]init];
        invoiceView.driverEmail = [[NSUserDefaults standardUserDefaults] objectForKey:@"DriverEmail"];
        invoiceView.apptDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"BookingDate"];
        invoiceView.isComingFromBookingHistory = NO;
        [invoiceView showPopUpWithDetailedDict:nil];
    } //completed
    else if (type == kNotificationTypeBookingReject)
    {
        _cancelReason  = [dictionary[@"r"] integerValue];
        
        NSString *cancelStatus = [self getCancelStatus:_cancelReason];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Cancel Report", @"Cancel Report") message:[NSString stringWithFormat:@"%@",cancelStatus] delegate:Nil cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok") otherButtonTitles:Nil, nil];
        [alert show];
        
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
    }
}

-(NSString*)getCancelStatus:(CancelBookingReasons)statusCancel
{
    NSString *cancelStatus;
    switch (statusCancel)
    {
        case kCAPassengerDoNotShow:
            cancelStatus = NSLocalizedString(@"Passenger not came.", @"Passenger not came.");
            break;
        case kCAWrongAddressShown:
            cancelStatus = NSLocalizedString(@"Address wrong.", @"Address wrong.");
            break;
        case kCAPassengerRequestedCancel:
            cancelStatus = NSLocalizedString(@"Passenger asked to cancel.", @"Passenger asked to cancel.");
            break;
        case kCADoNotChargeClient:
            cancelStatus = NSLocalizedString(@"Donot charge customer.", @"Donot charge customer.");
            break;
        case kCAOhterReasons:
            cancelStatus = NSLocalizedString(@"Others.", @"Others.");
            break;
        default:
            cancelStatus = NSLocalizedString(@"Others.", @"Others.");
            break;
    }
    
    return cancelStatus;
}
-(void)playNotificationSound
{
    //play sound
    SystemSoundID	pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"sms-received" ofType:@"wav"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
}

-(void)handleNotificationForUserInfo:(NSDictionary*)userInfo
{
    NSLog(@"Push Message %@ " ,userInfo);
    [self playNotificationSound];
    int type = [userInfo[@"aps"][@"nt"] intValue];
    if (type == kNotificationTypeBookingAccept)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Message", @"Message") message:[NSString stringWithFormat:@" :\n%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    }
    else if(type == kNotificationTypeBookingOnMyWay)
    {
        BOOL isCameFrom = [[NSUserDefaults standardUserDefaults]boolForKey:kNSUIsPassengerBookedKey];
        if (!isCameFrom)
        {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:kNSUIsPassengerBookedKey];
            [[NSUserDefaults standardUserDefaults]synchronize];
        }
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingOnMyWay];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked)
        {
            //do nothing
        }
        else if (!isAlreadyBooked)
        {
            //already completed
        }
        else
        {
            NSDictionary *dictionary = userInfo[@"aps"];
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate)
            {
                //this will call when App was killed
            }
            else if (![menu.currentViewController isKindOfClass:[MapViewController class]])
            {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];

                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    
                    [menu openViewControllerAtIndexPath:indexpath];
                    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBookingConfirmationNameKey object:nil userInfo:dictionary];
                });
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationBookingConfirmationNameKey object:nil userInfo:dictionary];
            }
        }
    }
    else if(type == kNotificationTypeBookingReachedLocation)
    {
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingReachedLocation];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked)
        {
            //do nothing
        }
        else if (!isAlreadyBooked)
        {
            //already completed
        }
        else
        {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate)
            {
                //this will call when App was killed
            }
            else if (![menu.currentViewController isKindOfClass:[MapViewController class]])
            {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    
                    [menu openViewControllerAtIndexPath:indexpath];
                    [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingReachedLocation];
                });
            }
            else
            {
                [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingReachedLocation];
            }
        }
    }
    else if (type == kNotificationTypeBookingStarted)
    {
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingStarted];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked)
        {
            //do nothing
        }
        else if (!isAlreadyBooked)
        {
            //already completed
        }
        else
        {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate)
            {
                //this will call when App was killed
            }
            else if (![menu.currentViewController isKindOfClass:[MapViewController class]])
            {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    
                    [menu openViewControllerAtIndexPath:indexpath];
                    [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingStarted];
                });
            }
            else
            {
                [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingStarted];
            }
        }
    }
    else if (type == kNotificationTypeBookingComplete)
    {
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingComplete];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        if (isAlreadyCame && isAlreadyBooked)
        {
            //do nothing
        }
        else if (!isAlreadyBooked)
        {
            //already completed
        }
        else
        {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            if (!menu.airDelegate)
            {
                //this will call when App was killed
            }
            else if (![menu.currentViewController isKindOfClass:[MapViewController class]])
            {
                NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
                [menu openMenuAnimated];
                dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                    
                    [menu openViewControllerAtIndexPath:indexpath];
                    [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingComplete];
                });
            }
            else
            {
                [self noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingComplete];
            }
        }
    }
    else if(type == kNotificationTypeBookingReject)
    {
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        BOOL isDriverCancel = [[NSUserDefaults standardUserDefaults]boolForKey:@"isDriverCanceledBookingOnce"];
        if (!isDriverCancel)
        {
            [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isDriverCanceledBookingOnce"];
        }
        if (isDriverCancel && isAlreadyBooked)
        {
            NSDictionary *dictionary = userInfo[@"aps"];
            [self noPushForceChangingController:dictionary :kNotificationTypeBookingReject];
        }
    }
    else if (type == kNotificationTypePassengerRejected)
    {
        [[XDKAirMenuController sharedMenu] openViewControllerAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@" :\n%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LOGOUT" object:nil userInfo:nil];
    }
    else if (type == 11)
    {
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"STATUSKEY"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
    }
    else if (type == kNotificationTypePassengerRecieveMessage)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message",@"Message") Message:[NSString stringWithFormat:@"%@",[[userInfo objectForKey:@"aps"] objectForKey:@"alert"]]];
    }
}

-(BOOL)checkCurrentStatus:(NSInteger)Status
{
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    NSInteger newBookingStatus =   Status;
    if(bookingStatus != newBookingStatus && bookingStatus < newBookingStatus)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(BOOL)checkIfBokkedOrNot
{
    BOOL isBooked = [[NSUserDefaults standardUserDefaults] boolForKey:kNSUIsPassengerBookedKey];
    if(isBooked)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            abort();
        }
    }
}

#pragma mark -
#pragma mark Core Data stack



// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Model" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Model.sqlite"];
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSDictionary *options = @{
                              NSMigratePersistentStoresAutomaticallyOption : @YES,
                              NSInferMappingModelAutomaticallyOption : @YES
                              };
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)setupAppearance {
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"Navigationbar.png"] forBarMetrics:UIBarMetricsDefault];
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:Roboto_Regular size:16], NSFontAttributeName,
                                UIColorFromRGB(0xFFFFFF), NSForegroundColorAttributeName, nil];
    
    [[UINavigationBar appearance] setTitleTextAttributes:attributes];
    
    
}
/**
 *  Handle Localization
 */
- (void)handleLocalization
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    
    /*
     * Check the user defaults to find whether a localisation has been set before.
     * If it hasn't been set, (i.e. first run of the app), select the locale based
     * on the device locale setting.
     */
    
    // Check whether the language code has already been set.
    if (![userDefaults stringForKey:DEFAULTS_KEY_LANGUAGE_CODE])
    {
        NSLocale *currentLocale = [NSLocale currentLocale];
        
        // Iterate through available localisations to find the matching one for the device locale.
        for (Locale *localisation in languageManager.availableLocales)
        {
            if ([localisation.languageCode caseInsensitiveCompare:[currentLocale objectForKey:NSLocaleLanguageCode]] == NSOrderedSame)
            {
                [languageManager setLanguageWithLocale:localisation];
                break;
            }
        }
        // If the device locale doesn't match any of the available ones, just pick the first one.
        if (![userDefaults stringForKey:DEFAULTS_KEY_LANGUAGE_CODE])
        {
            [languageManager setLanguageWithLocale:languageManager.availableLocales[0]];
        }
    }
}

#pragma mark - 3D touch delegates

- (void)application:(UIApplication *)application performActionForShortcutItem:(UIApplicationShortcutItem *)shortcutItem completionHandler:(void (^)(BOOL))completionHandler
{
    if ([shortcutItem.type isEqualToString:@"com.roadyo.home"])
    {
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:0 inSection:0];
            [menu openMenuAnimated];
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
            });
        }
    }
    if ([shortcutItem.type isEqualToString:@"com.roadyo.bookings"])
    {
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:1 inSection:0];
            [menu openMenuAnimated];
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
            });
        }
    }
    if ([shortcutItem.type isEqualToString:@"com.roadyo.share"])
    {
        if([[NSUserDefaults standardUserDefaults] objectForKey:KDAcheckUserSessionToken])
        {
            XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
            NSIndexPath *indexpath = [NSIndexPath indexPathForRow:5 inSection:0];
            [menu openMenuAnimated];
            dispatch_after(0.2, dispatch_get_main_queue(), ^(void){
                [menu openViewControllerAtIndexPath:indexpath];
            });
        }
    }
}


-(void)session:(WCSession *)session didReceiveApplicationContext:(NSDictionary<NSString *,id> *)applicationContext {
    NSLog(@"New Session Context: %@", applicationContext);
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    for (NSString *key in applicationContext.allKeys) {
        [defaults setObject:[applicationContext objectForKey:key] forKey:key];
    }
    [defaults synchronize];
}




@end
