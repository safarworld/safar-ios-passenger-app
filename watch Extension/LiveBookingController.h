//
//  LiveBookingController.h
//  SimpleWeather
//
//  Created by 3Embed on 09/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <WatchConnectivity/WatchConnectivity.h>

@interface LiveBookingController : WKInterfaceController <WCSessionDelegate>

@end
