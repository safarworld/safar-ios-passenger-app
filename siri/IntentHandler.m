//
//  IntentHandler.m
//  siri
//
//  Created by 3Embed on 10/03/17.
//  Copyright © 2017 Rahul Sharma. All rights reserved.
//

#import "IntentHandler.h"
#import "RequestModelClass.h"

// As an example, this class is set up to handle Message intents.
// You will want to replace this or add other intents as appropriate.
// The intents you wish to handle must be declared in the extension's Info.plist.

// You can test your example integration by saying things to Siri like:
// "Send a message using <myApp>"
// "<myApp> John saying hello"
// "Search for messages in <myApp>"
@interface IntentHandler () <INSendMessageIntentHandling, INSearchForMessagesIntentHandling, INSetMessageAttributeIntentHandling, INListRideOptionsIntentHandling, INRequestRideIntentHandling, INGetRideStatusIntentHandling, NSURLSessionDelegate> {
    NSMutableArray *pubnubData;
    NSArray *selectedCarTypeWithData;
    int pickupTime;
}

@end

@implementation IntentHandler

- (id)handlerForIntent:(INIntent *)intent {
    // This is the default implementation.  If you want different objects to handle different intents,
    // you can override this and return the handler you want for that particular intent.
    NSLog(@"Intent = %@", intent);
    return self;
}


#pragma mark - INListRideOptionsIntentHandling

- (void)handleListRideOptions:(INListRideOptionsIntent *)intent
                   completion:(void (^)(INListRideOptionsIntentResponse *response))completion NS_SWIFT_NAME(handle(listRideOptions:completion:)){
    INListRideOptionsIntentResponse *response = [[INListRideOptionsIntentResponse alloc] initWithCode:INListRideOptionsIntentResponseCodeSuccess userActivity:nil];
    completion(response);
}

- (void)confirmListRideOptions:(INListRideOptionsIntent *)intent
                    completion:(void (^)(INListRideOptionsIntentResponse *response))completion NS_SWIFT_NAME(confirm(listRideOptions:completion:)) {
    INRideOption *rideOption1 = [[INRideOption alloc] initWithName:@"Safar1" estimatedPickupDate:[[NSDate date] dateByAddingTimeInterval:60]];
    rideOption1.priceRange = [[INPriceRange alloc] initWithPrice:[[NSDecimalNumber alloc] initWithString:@"5.66"] currencyCode:@"EGP"];
    rideOption1.disclaimerMessage = @"This is a very small car, tall passengers may not fit.";
    
    rideOption1.availablePartySizeOptions = @[[[INRidePartySizeOption alloc] initWithPartySizeRange:NSMakeRange(0, 1) sizeDescription:@"One Person" priceRange:nil], [[INRidePartySizeOption alloc] initWithPartySizeRange:NSMakeRange(0, 1) sizeDescription:@"Two people" priceRange:[[INPriceRange alloc] initWithRangeBetweenPrice:[[NSDecimalNumber alloc] initWithString:@"5.66"] andPrice:[[NSDecimalNumber alloc] initWithString:@"7.66"] currencyCode:@"EGP"]]];
    
    rideOption1.availablePartySizeOptionsSelectionPrompt = @"Choose a party size";
    rideOption1.specialPricing = @"High demand. 50% extra will be added to your fare.";
    rideOption1.specialPricingBadgeImage = [INImage imageNamed:@"card_icon"];
    
    INRideFareLineItem *baseFare = [[INRideFareLineItem alloc] initWithTitle:@"Base fare" price:[[NSDecimalNumber alloc] initWithString:@"5.66"] currencyCode:@"EGP"];
    INRideFareLineItem *airport = [[INRideFareLineItem alloc] initWithTitle:@"Airport fee" price:[[NSDecimalNumber alloc] initWithString:@"3.00"] currencyCode:@"EGP"];
    INRideFareLineItem *discount = [[INRideFareLineItem alloc] initWithTitle:@"Promo code (3fs8sdx)" price:[[NSDecimalNumber alloc] initWithString:@"-4.00"] currencyCode:@"EGP"];
    rideOption1.fareLineItems = @[baseFare, airport, discount];
    
    INRideOption *rideOption2 = [[INRideOption alloc] initWithName:@"Safar2" estimatedPickupDate:[[NSDate date] dateByAddingTimeInterval:120]];
    INRideOption *rideOption3 = [[INRideOption alloc] initWithName:@"Safar3" estimatedPickupDate:[[NSDate date] dateByAddingTimeInterval:180]];
    INRideOption *rideOption4 = [[INRideOption alloc] initWithName:@"Safar4" estimatedPickupDate:[[NSDate date] dateByAddingTimeInterval:240]];
    
    INListRideOptionsIntentResponse *response = [[INListRideOptionsIntentResponse alloc] initWithCode:INListRideOptionsIntentResponseCodeSuccess userActivity:nil];
    response.rideOptions = @[rideOption1, rideOption2, rideOption3, rideOption4];
    completion(response);
}

//- (void)resolvePickupLocationForListRideOptions:(INListRideOptionsIntent *)intent
//                                 withCompletion:(void (^)(INPlacemarkResolutionResult *resolutionResult))completion {
//    INPlacemarkResolutionResult *response;
//    if(intent.pickupLocation != nil) {
//        response = [INPlacemarkResolutionResult successWithResolvedPlacemark:intent.pickupLocation];
//    } else {
//        response = [INPlacemarkResolutionResult needsValue];
//    }
//    completion(response);
//}
//
//- (void)resolveDropOffLocationForListRideOptions:(INListRideOptionsIntent *)intent
//                                  withCompletion:(void (^)(INPlacemarkResolutionResult *resolutionResult))completion {
//    INPlacemarkResolutionResult *response;
//    if(intent.dropOffLocation != nil) {
//        response = [INPlacemarkResolutionResult confirmationRequiredWithPlacemarkToConfirm:intent.dropOffLocation];
//    } else {
//        response = [INPlacemarkResolutionResult needsValue];
//    }
//    completion(response);
//}

#pragma mark - INRequestRideIntentHandling

- (void)resolvePickupLocationForRequestRide:(INRequestRideIntent *)intent
                             withCompletion:(void (^)(INPlacemarkResolutionResult *resolutionResult))completion NS_SWIFT_NAME(resolvePickupLocation(forRequestRide:with:)) {
    INPlacemarkResolutionResult *response;
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    if(!sessionToken.length) {
        response = [INPlacemarkResolutionResult unsupported];
    } else if(intent.pickupLocation != nil) {
        response = [INPlacemarkResolutionResult successWithResolvedPlacemark:intent.pickupLocation];
    } else {
        response = [INPlacemarkResolutionResult needsValue];
    }
    completion(response);
}

- (void)resolveDropOffLocationForRequestRide:(INRequestRideIntent *)intent
                              withCompletion:(void (^)(INPlacemarkResolutionResult *resolutionResult))completion NS_SWIFT_NAME(resolveDropOffLocation(forRequestRide:with:)){
    INPlacemarkResolutionResult *response;
    response = [INPlacemarkResolutionResult successWithResolvedPlacemark:intent.dropOffLocation];
    completion(response);
    //    if(intent.dropOffLocation != nil) {
    //        response = [INPlacemarkResolutionResult successWithResolvedPlacemark:intent.dropOffLocation];
    //    } else {
    //        response = [INPlacemarkResolutionResult needsValue];
    //    }
    //    completion(response);
}

- (void)resolveRideOptionNameForRequestRide:(INRequestRideIntent *)intent
                             withCompletion:(void (^)(INSpeakableStringResolutionResult *resolutionResult))completion NS_SWIFT_NAME(resolveRideOptionName(forRequestRide:with:)) {
    if(intent.rideOptionName == nil) {
        NSString *lt = [NSString stringWithFormat:@"%f", intent.pickupLocation.location.coordinate.latitude];
        NSString *lg = [NSString stringWithFormat:@"%f", intent.pickupLocation.location.coordinate.longitude];
        RequestModelClass *requestObj = [RequestModelClass getSharedInstance];
        [requestObj sendServiceToGetCurrentVehiclesAndDriver:lt longitude:lg];
        requestObj.onCompletion = ^(NSDictionary *data) {
            pubnubData = [data mutableCopy];
            NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
            [shared setObject:pubnubData forKey:@"pubnubData"];
            [shared synchronize];
            NSArray *carTypeNames = [pubnubData valueForKeyPath:@"type_name"];
            INSpeakableStringResolutionResult *response;
            if(carTypeNames.count) {
                response = [INSpeakableStringResolutionResult disambiguationWithStringsToDisambiguate:carTypeNames];
            } else {
                response = [INSpeakableStringResolutionResult unsupported];
            }
            completion(response);
        };
    } else {
        INSpeakableStringResolutionResult *response;
        response = [INSpeakableStringResolutionResult confirmationRequiredWithStringToConfirm:intent.rideOptionName];
        completion(response);
    }
}

- (void)confirmRequestRide:(INRequestRideIntent *)intent
                completion:(void (^)(INRequestRideIntentResponse *response))completion NS_SWIFT_NAME(confirm(requestRide:completion:)) {
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    pubnubData = [shared objectForKey:@"pubnubData"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type_name == [c] %@", intent.rideOptionName.description];// CONTAINS[cd]
    selectedCarTypeWithData = [[pubnubData filteredArrayUsingPredicate:predicate] mutableCopy];
    INRequestRideIntentResponse *response;
    if([selectedCarTypeWithData[0][@"masters"] count]) {
        NSDictionary *firstDriver = selectedCarTypeWithData[0][@"masters"][0];
        NSString *lt = [NSString stringWithFormat:@"%f", intent.pickupLocation.location.coordinate.latitude];
        NSString *lg = [NSString stringWithFormat:@"%f", intent.pickupLocation.location.coordinate.longitude];
        [self calculateTimeToDisplayOnMarker:[firstDriver[@"lt"] floatValue] Olong:[firstDriver[@"lg"] floatValue] Dlat:[lt floatValue] Dlong:[lg floatValue]];
        [NSThread sleepForTimeInterval: 1.5];
        NSLog(@"PICK UP LOCATION = %d", pickupTime);
        if (pickupTime < 1) {
            pickupTime = 1;
        }
        INRideOption *rideOption = [[INRideOption alloc] initWithName:[intent.rideOptionName.description uppercaseString] estimatedPickupDate:[[NSDate date] dateByAddingTimeInterval:pickupTime]];
        INRideStatus *rideStatus = [[INRideStatus alloc] init];
        rideStatus.rideOption = rideOption;
        rideStatus.rideIdentifier = [NSString stringWithFormat:@"%@", selectedCarTypeWithData[0][@"type_id"]];
        rideStatus.estimatedPickupDate = [[NSDate date] dateByAddingTimeInterval:pickupTime];
        response = [[INRequestRideIntentResponse alloc] initWithCode:(INRequestRideIntentResponseCodeSuccess) userActivity:nil];
        response.rideStatus = rideStatus;
    } else {
        response = [[INRequestRideIntentResponse alloc] initWithCode:INRequestRideIntentResponseCodeFailure userActivity:nil];
    }
    completion(response);
}


- (void)resolvePartySizeForRequestRide:(INRequestRideIntent *)intent
                        withCompletion:(void (^)(INIntegerResolutionResult *resolutionResult))completion NS_SWIFT_NAME(resolvePartySize(forRequestRide:with:)) {
    INIntegerResolutionResult *response;
    response= [INIntegerResolutionResult successWithResolvedValue:4];
    completion(response);
}


- (void)handleRequestRide:(INRequestRideIntent *)intent
               completion:(void (^)(INRequestRideIntentResponse *response))completion NS_SWIFT_NAME(handle(requestRide:completion:)) {
    
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    pubnubData = [shared objectForKey:@"pubnubData"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"type_name == [c] %@", intent.rideOptionName.description]; // CONTAINS[cd]
    selectedCarTypeWithData = [[pubnubData filteredArrayUsingPredicate:predicate] mutableCopy];
    if(selectedCarTypeWithData.count) {
        NSString *driverEmail = selectedCarTypeWithData[0][@"masters"][0][@"e"];
        NSString *pickupAddress = @"";
        NSString *dropoffAddress = @"";
        if([intent.pickupLocation.addressDictionary[@"FormattedAddressLines"] count]) {
            pickupAddress = [intent.pickupLocation.addressDictionary[@"FormattedAddressLines"] componentsJoinedByString:@", "];
        }
        if([intent.dropOffLocation.addressDictionary[@"FormattedAddressLines"] count]) {
            dropoffAddress = [intent.dropOffLocation.addressDictionary[@"FormattedAddressLines"] componentsJoinedByString:@", "];
        }
        
        NSDictionary *dict = @{
                               @"carTypeId":selectedCarTypeWithData[0][@"type_id"],
                               @"pickUpLat":[NSNumber numberWithFloat:intent.pickupLocation.location.coordinate.latitude],
                               @"pickUpLong":[NSNumber numberWithFloat:intent.pickupLocation.location.coordinate.longitude],
                               @"pickUpAddress" :pickupAddress,
                               @"dropOffLat":[NSNumber numberWithFloat:intent.dropOffLocation.location.coordinate.latitude],
                               @"dropOffLong":[NSNumber numberWithFloat:intent.dropOffLocation.location.coordinate.longitude],
                               @"dropOffAddress" :dropoffAddress,
                               };
        RequestModelClass *requestObj = [RequestModelClass getSharedInstance];
        [requestObj sendServiceToBookDriver:dict andDriverEmail:driverEmail];
        requestObj.onCompletionForLiveBooking = ^(NSDictionary *serviceResponse) {
            NSLog(@"Booking Result = %@", serviceResponse);
            INRequestRideIntentResponse *response;
            int pickupTime = 300;
            INRideOption *rideOption = [[INRideOption alloc] initWithName:[intent.rideOptionName.description uppercaseString] estimatedPickupDate:[[NSDate date] dateByAddingTimeInterval:pickupTime]];
            INRideStatus *rideStatus = [[INRideStatus alloc] init];
            rideStatus.rideOption = rideOption;
            
            INRideVehicle *vehicle = [[INRideVehicle alloc] init];
            vehicle.model = serviceResponse[@"model"];
            vehicle.registrationPlate = serviceResponse[@"plateNo"];
            NSString *carMapImageURL = [NSString stringWithFormat:@"%@%@", @"http://188.166.48.202/safar/pics/", serviceResponse[@"carMapImage"]];
            vehicle.mapAnnotationImage = [INImage imageWithURL:[NSURL URLWithString: carMapImageURL]];
            rideStatus.vehicle = vehicle;
            
            rideStatus.phase = INRidePhaseConfirmed;
            
            NSString *driverMobile = @"1234567890";//serviceResponse[@"mobile"];
            NSString *driverName = serviceResponse[@"drivername"];
            NSPersonNameComponents *driverDetail = [[NSPersonNameComponents alloc] init];
            driverDetail.givenName = driverName;
            NSString *driverImageURL = [NSString stringWithFormat:@"%@%@", @"http://188.166.48.202/safar/pics/", serviceResponse[@"driverPic"]];
            INImage *driverImage = [INImage imageWithURL:[NSURL URLWithString:driverImageURL]];
            NSString *driverRating = serviceResponse[@"rating"];
            INRideDriver *driver = [[INRideDriver alloc] initWithPhoneNumber: driverMobile
                                                              nameComponents: driverDetail
                                                                 displayName: driverName
                                                                       image: driverImage
                                                                      rating: driverRating];
            rideStatus.driver = driver;
            
            rideStatus.pickupLocation = intent.pickupLocation;
            
            rideStatus.dropOffLocation = intent.dropOffLocation;
            
            rideStatus.rideIdentifier = [NSString stringWithFormat:@"%@", selectedCarTypeWithData[0][@"type_id"]];
            
            rideStatus.estimatedPickupDate = [[NSDate date] dateByAddingTimeInterval:pickupTime];
            if ([serviceResponse[@"errFlag"] integerValue] == 1) {
                response = [[INRequestRideIntentResponse alloc] initWithCode:INRequestRideIntentResponseCodeReady userActivity:nil];
                completion(response);
            } else if([serviceResponse[@"errFlag"] integerValue] == 0) {
                response = [[INRequestRideIntentResponse alloc] initWithCode:    INRequestRideIntentResponseCodeSuccess userActivity:nil];
            }
            response.rideStatus = rideStatus;
            completion(response);
        };
        NSLog(@"Waiting for service response");
    }
    NSLog(@"No driver is present for Live Booking");
}



#pragma mark - INGetRideStatusIntent

- (void)handleGetRideStatus:(INGetRideStatusIntent *)intent
                 completion:(void (^)(INGetRideStatusIntentResponse *response))completion NS_SWIFT_NAME(handle(getRideStatus:completion:)) {
    NSLog(@"handleGetRideStatus in INGetRideStatusIntent 123");
}

- (void)startSendingUpdatesForGetRideStatus:(INGetRideStatusIntent *)intent
                                 toObserver:(id<INGetRideStatusIntentResponseObserver>)observer NS_SWIFT_NAME(startSendingUpdates(forGetRideStatus:to:)) {
    NSLog(@"startSendingUpdatesForGetRideStatus in INGetRideStatusIntent 345");
}

- (void)stopSendingUpdatesForGetRideStatus:(INGetRideStatusIntent *)intent NS_SWIFT_NAME(stopSendingUpdates(forGetRideStatus:)) {
    
    NSLog(@"stopSendingUpdatesForGetRideStatus in INGetRideStatusIntent 567");
}



- (void)confirmGetRideStatus:(INGetRideStatusIntent *)intent
                  completion:(void (^)(INGetRideStatusIntentResponse *response))completion NS_SWIFT_NAME(confirm(getRideStatus:completion:)) {
    
    NSLog(@"confirmGetRideStatus in INGetRideStatusIntent 789");
}

- (void)getRideStatusResponseDidUpdate:(INGetRideStatusIntentResponse *)response NS_SWIFT_NAME(didUpdate(getRideStatus:)) {
    NSLog(@"getRideStatusResponseDidUpdate in INGetRideStatusIntentResponse 012");
}



- (void) calculateTimeToDisplayOnMarker:(double)originLat Olong:(double)originLong Dlat:(double)destinationLat Dlong:(double)destinationLong
{
    if (originLat == 0 || originLong == 0 || destinationLat == 0 || destinationLong == 0){
        return;
    }
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    //    NSURL *url = [NSURL URLWithString:@"http://188.166.48.202/safar/services_v2.php/getVehilesAndDriversTest"];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f",originLat,originLong,destinationLat,destinationLong];
    NSURL *url = [NSURL URLWithString:[self removeWhiteSpaceFromURL:strUrl]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:45.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    if(!sessionToken.length) {
        sessionToken = @"";
    }
    NSString *deviceID = [shared objectForKey:@"deviceidSiri"];
    if(!deviceID.length) {
        deviceID = @"";
    }
    NSDictionary *params = @{
                             @"lt":@"",
                             @"lg":@"",
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID
                             };
    
    NSError *error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil) {
                NSLog(@"error = %@",error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                if (!json) {
                } else {
                    BOOL checkStatus = [self containsKey:@"status" dictionary:json];
                    if (!checkStatus) {
                        NSString *str = @"Limit Exceed";
                        pickupTime = 300;
                    } else {
                        //            NSString *distance = response[@"ItemsList"][@"rows"][0][@"elements"][0][@"distance"][@"value"];
                        //            distanceOfClosetCar = [NSString stringWithFormat:@"%.2f %@",[distance doubleValue]/kPMDDistanceMetric, kPMDDistanceParameter];
                        int timeInSec =  [json[@"rows"][0][@"elements"][0][@"duration"][@"value"] intValue];
                        pickupTime = timeInSec;
                    }
                }
            });
        }
        else{
            NSLog(@"Error : %@",error.description);
        }
    }];
    [postDataTask resume];
}

#pragma mark Webservice Handler(Response) -

- (void)calculatedTime:(NSDictionary *)response
{
}
- (NSString *)removeWhiteSpaceFromURL:(NSString *)url
{
    NSMutableString *string = [[NSMutableString alloc] initWithString:url] ;
    [string replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [string length])];
    return string;
}


- (BOOL)containsKey: (NSString *)key dictionary:(NSDictionary *) dict {
    BOOL retVal = 0;
    NSArray *allKeys = [dict allKeys];
    retVal = [allKeys containsObject:key];
    if (retVal) {
        if ([dict[@"status"] isEqualToString:@"OK"]) {
            retVal = YES;
        } else {
            retVal = NO;
        }
    }
    return retVal;
}

-(int) convertTimeInMin:(int) timeInSec {
    int min;
    if (timeInSec < 60 && timeInSec <= 0) {
        min = 1;
    } else if (timeInSec > 60) {
        min = timeInSec/60;
        int remainingTime = timeInSec%60;
        if (remainingTime < 60 && remainingTime >= 30) {
            min++;
        }
    } else {
        min = 1;
    }
    return min;
}
























#pragma mark - INSendMessageIntentHandling

// Implement resolution methods to provide additional information about your intent (optional).
- (void)resolveRecipientsForSendMessage:(INSendMessageIntent *)intent withCompletion:(void (^)(NSArray<INPersonResolutionResult *> *resolutionResults))completion {
    NSArray<INPerson *> *recipients = intent.recipients;
    // If no recipients were provided we'll need to prompt for a value.
    if (recipients.count == 0) {
        completion(@[[INPersonResolutionResult needsValue]]);
        return;
    }
    NSMutableArray<INPersonResolutionResult *> *resolutionResults = [NSMutableArray array];
    
    for (INPerson *recipient in recipients) {
        NSArray<INPerson *> *matchingContacts = @[recipient]; // Implement your contact matching logic here to create an array of matching contacts
        if (matchingContacts.count > 1) {
            // We need Siri's help to ask user to pick one from the matches.
            [resolutionResults addObject:[INPersonResolutionResult disambiguationWithPeopleToDisambiguate:matchingContacts]];
            
        } else if (matchingContacts.count == 1) {
            // We have exactly one matching contact
            [resolutionResults addObject:[INPersonResolutionResult successWithResolvedPerson:recipient]];
        } else {
            // We have no contacts matching the description provided
            [resolutionResults addObject:[INPersonResolutionResult unsupported]];
        }
    }
    completion(resolutionResults);
}

- (void)resolveContentForSendMessage:(INSendMessageIntent *)intent withCompletion:(void (^)(INStringResolutionResult *resolutionResult))completion {
    NSString *text = intent.content;
    if (text && ![text isEqualToString:@""]) {
        completion([INStringResolutionResult successWithResolvedString:text]);
    } else {
        completion([INStringResolutionResult needsValue]);
    }
}

// Once resolution is completed, perform validation on the intent and provide confirmation (optional).

- (void)confirmSendMessage:(INSendMessageIntent *)intent completion:(void (^)(INSendMessageIntentResponse *response))completion {
    // Verify user is authenticated and your app is ready to send a message.
    
    NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSStringFromClass([INSendMessageIntent class])];
    INSendMessageIntentResponse *response = [[INSendMessageIntentResponse alloc] initWithCode:INSendMessageIntentResponseCodeReady userActivity:userActivity];
    completion(response);
}

// Handle the completed intent (required).

- (void)handleSendMessage:(INSendMessageIntent *)intent completion:(void (^)(INSendMessageIntentResponse *response))completion {
    // Implement your application logic to send a message here.
    
    NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSStringFromClass([INSendMessageIntent class])];
    INSendMessageIntentResponse *response = [[INSendMessageIntentResponse alloc] initWithCode:INSendMessageIntentResponseCodeSuccess userActivity:userActivity];
    completion(response);
}

// Implement handlers for each intent you wish to handle.  As an example for messages, you may wish to also handle searchForMessages and setMessageAttributes.

#pragma mark - INSearchForMessagesIntentHandling

- (void)handleSearchForMessages:(INSearchForMessagesIntent *)intent completion:(void (^)(INSearchForMessagesIntentResponse *response))completion {
    // Implement your application logic to find a message that matches the information in the intent.
    
    NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSStringFromClass([INSearchForMessagesIntent class])];
    INSearchForMessagesIntentResponse *response = [[INSearchForMessagesIntentResponse alloc] initWithCode:INSearchForMessagesIntentResponseCodeSuccess userActivity:userActivity];
    // Initialize with found message's attributes
    response.messages = @[[[INMessage alloc]
                           initWithIdentifier:@"identifier"
                           content:@"I am so excited about SiriKit!"
                           dateSent:[NSDate date]
                           sender:[[INPerson alloc] initWithPersonHandle:[[INPersonHandle alloc] initWithValue:@"sarah@example.com" type:INPersonHandleTypeEmailAddress] nameComponents:nil displayName:@"Sarah" image:nil contactIdentifier:nil customIdentifier:nil]
                           recipients:@[[[INPerson alloc] initWithPersonHandle:[[INPersonHandle alloc] initWithValue:@"+1-415-555-5555" type:INPersonHandleTypePhoneNumber] nameComponents:nil displayName:@"John" image:nil contactIdentifier:nil customIdentifier:nil]]
                           ]];
    completion(response);
}

#pragma mark - INSetMessageAttributeIntentHandling

- (void)handleSetMessageAttribute:(INSetMessageAttributeIntent *)intent completion:(void (^)(INSetMessageAttributeIntentResponse *response))completion {
    // Implement your application logic to set the message attribute here.
    
    NSUserActivity *userActivity = [[NSUserActivity alloc] initWithActivityType:NSStringFromClass([INSetMessageAttributeIntent class])];
    INSetMessageAttributeIntentResponse *response = [[INSetMessageAttributeIntentResponse alloc] initWithCode:INSetMessageAttributeIntentResponseCodeSuccess userActivity:userActivity];
    completion(response);
}




@end
