//
//  WalletHistoryViewController.m
//  RoadyoDispatch
//
//  Created by 3Embed on 23/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "WalletHistoryViewController.h"
#import "walletHistoryTableViewCell.h"

@interface WalletHistoryViewController ()<UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@end

@implementation WalletHistoryViewController
@synthesize tableBackgroundScrollView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    [tableBackgroundScrollView setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width*3, 454)];
    tableBackgroundScrollView.contentOffset = CGPointZero;
}

#pragma mark - UIButton Action - 

- (IBAction)allBtnAction:(id)sender
{
    tableBackgroundScrollView.contentOffset = CGPointZero;
}
- (IBAction)moneyInBtnAction:(id)sender
{
    tableBackgroundScrollView.contentOffset = CGPointMake([UIScreen mainScreen].bounds.size.width*2, 0);
}
- (IBAction)moneyOutBtnAction:(id)sender
{
    tableBackgroundScrollView.contentOffset = CGPointMake([UIScreen mainScreen].bounds.size.width*3, 0);
}

#pragma mark - UITableView DataSource - 

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    walletHistoryTableViewCell *cell = (walletHistoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"walletHistoryCell"];
    return cell;
}

@end
