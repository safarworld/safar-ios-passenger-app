//
//  MapViewController.h
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "WildcardGestureRecognizer.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import "MyAppTimerClass.h"

typedef enum {
    isBookingDetailsServieCalled = 0,
    isBookingDetailsViewShown = 1
}BookingStatus;

@interface MapViewController : UIViewController<GMSMapViewDelegate,UITextFieldDelegate,UIActionSheetDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UIPickerViewDataSource,UIPickerViewDelegate, UIScrollViewDelegate>
{
    BOOL isNurseSelected;
    BOOL isLaterSelected;
    BOOL isNowSelected;
    BOOL isCustomMarkerSelected;
    BOOL isFareButtonClicked;
    BOOL isRequestingButtonClicked;
    
    PatientAppDelegate *appDelegate;
    NSMutableArray		*arrDBResult;
	NSManagedObjectContext *context;
    
    UIDatePicker *datePicker;
    UIActionSheet *newSheet;
    
    MFMailComposeViewController *mailer;
    NSInteger selectedRowForTip;
}

@property(nonatomic,assign)BOOL isSelectinLocation;
@property(nonatomic,strong) UIPickerView *pkrView;
@property(nonatomic,strong) UIPickerView *pkrViewForAddTip;
@property (strong, nonatomic) NSMutableArray *drivers ;
@property (strong, nonatomic) NSArray *nurses ;
@property (strong, nonatomic) NSArray *buttonArray;
@property (strong, nonatomic) NSArray *buttonArray2;
@property (strong, nonatomic) NSDictionary *dictSelectedDoctor;
@property (strong, nonatomic) UITextField *textFeildAddress;
@property (assign, nonatomic) BookingStatus bookingStatus;

- (IBAction)pickupLocationAction:(UIButton *)sender;

+ (instancetype) getSharedInstance;
-(void)publishPubNubStream;
-(void)hideActivityIndicatorWithMessage;
- (void) sendRequestgetETAnDistance;
-(void)checkBookingStatus;
@end
