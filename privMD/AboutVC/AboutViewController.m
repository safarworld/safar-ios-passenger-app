//
//  AboutViewController.m
//  UBER
//
//  Created by Rahul Sharma on 21/05/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "AboutViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "WebViewController.h"

@interface AboutViewController ()<CustomNavigationBarDelegate>

@end

@implementation AboutViewController
@synthesize topView;
@synthesize topViewevery1Label;
@synthesize topviewroadyoLabel;
@synthesize likeButton;
@synthesize legalButton;
@synthesize rateButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self addCustomNavigationBar];
}

-(void)viewWillAppear:(BOOL)animated
{
    [Helper setToLabel:topViewevery1Label Text:NSLocalizedString(@"'EVERYONE'S PRIVATE TAXI'", @"'EVERYONE'S PRIVATE TAXI'") WithFont:Roboto_Regular FSize:13 Color:UIColorFromRGB(0x333333)];
    [Helper setButton:topviewroadyoLabel Text:websiteLabel WithFont:Roboto_Regular FSize:11 TitleColor:UIColorFromRGB(0x3399ff) ShadowColor:nil];
    
    [Helper setButton:rateButton Text:NSLocalizedString(@"RATE US IN THE APP STORE", @"RATE US IN THE APP STORE") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x000000) ShadowColor:nil];
    [rateButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:likeButton Text:NSLocalizedString(@"LIKE US ON FACEBOOK", @"LIKE US ON FACEBOOK") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [likeButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    [Helper setButton:legalButton Text:NSLocalizedString(@"LEGAL", @"LEGAL") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [legalButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish] && kRTLSupport)
    {
        rateButton.titleLabel.textAlignment = NSTextAlignmentRight;
        likeButton.titleLabel.textAlignment = NSTextAlignmentRight;
        legalButton.titleLabel.textAlignment = NSTextAlignmentRight;
        
        rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    }
    else
    {
        rateButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        likeButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        legalButton.titleLabel.textAlignment = NSTextAlignmentLeft;
        
        rateButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        rateButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        likeButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        likeButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
        legalButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        legalButton.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"ABOUT", @"ABOUT")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonPressedAccount];
}

- (void)menuButtonPressedAccount {
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (IBAction)rateButtonClicked:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://%@",itunesURL]]];
}

- (IBAction)likeonFBButtonClicked:(id)sender
{
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    webView.title = NSLocalizedString(@"LIKE", @"LIKE");
    webView.weburl = facebookURL;
    [self.navigationController pushViewController:webView animated:YES];
}

- (IBAction)legalButtonClicked:(id)sender
{
    WebViewController *webView = [self.storyboard instantiateViewControllerWithIdentifier:@"webView"];
    webView.title = NSLocalizedString(@"LEGAL", @"LEGAL");
    webView.weburl = privacyPolicy;
    [self.navigationController pushViewController:webView animated:YES];
}

- (IBAction) webButtonClicked:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:websiteURL]];
}

@end
