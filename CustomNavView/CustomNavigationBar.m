//
//  CustomNavigationBar.m
//  privMD
//
//  Created by Surender Rathore on 15/04/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CustomNavigationBar.h"

@interface CustomNavigationBar()
@property(nonatomic,strong)UILabel *labelTitle;
@property(nonatomic,strong)UIButton *rightbarButton;
@property(nonatomic,strong)UIButton *rightbarButton2;
@property(nonatomic,strong)UIButton *leftbarButton;
@end
@implementation CustomNavigationBar
@synthesize labelTitle;
@synthesize rightbarButton;
@synthesize leftbarButton;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        [self setBackgroundColor:UIColorFromRGB(0x0d143b)];
       
        //title
        labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, screenWidth, 40)];
        labelTitle.textAlignment = NSTextAlignmentCenter;
        [Helper setToLabel:labelTitle Text:@"" WithFont:Roboto_Regular FSize:16 Color:UIColorFromRGB(0xffffff)];
        [self addSubview:labelTitle];
        
        leftbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
        UIImage *imageLeftbarButton = [UIImage imageNamed:@"home_btn_menu_on"];
        leftbarButton.frame = CGRectMake(10, 20, imageLeftbarButton.size.width, imageLeftbarButton.size.height);
        leftbarButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];

        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_btn_menu"] forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_btn_menu_on"] forState:UIControlStateHighlighted];
        [leftbarButton addTarget:self action:@selector(leftBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:leftbarButton];

    }
    return self;
}

-(void)createRightBarButton
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightbarButton.frame = CGRectMake(screenWidth-buttonImage.size.width-10, 20,buttonImage.size.width, buttonImage.size.height);
    rightbarButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [rightbarButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [rightbarButton setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateSelected];
    [rightbarButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightbarButton];

}

-(void)addTitleButton{
    
    UIImage *buttonImage = [UIImage imageNamed:@"Navigationbar_logo.png"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage: buttonImage];
    CGFloat height = imageView.frame.size.height;
    CGFloat width = imageView.frame.size.width;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    _rightbarButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightbarButton2 setUserInteractionEnabled:NO];
    _rightbarButton2.frame = CGRectMake((screenWidth-width)/2,(64-height)/2+10,width,height);
    [_rightbarButton2 setBackgroundImage:buttonImage forState:UIControlStateNormal];
    _rightbarButton2.tag = 100;
    [self addSubview:_rightbarButton2];
    [_rightbarButton2 setHidden:NO];
    [labelTitle setHidden:YES];
}

-(void)addButtonRight {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if(rightbarButton)
    {
        [rightbarButton removeFromSuperview];
        rightbarButton = nil;
    }
    rightbarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightbarButton.frame = CGRectMake(screenWidth-70, 20, 70, 44);
    [rightbarButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [Helper setButton:rightbarButton Text:@"" WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [rightbarButton setTitleColor:[UIColor colorWithWhite:0.384 alpha:1.000] forState:UIControlStateHighlighted];
    [rightbarButton addTarget:self action:@selector(rightBarButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:rightbarButton];
}

-(void)removeRightBarButton {
    
    [rightbarButton setHidden:YES];
    [rightbarButton removeFromSuperview];
}

-(void)hideTitleButton:(BOOL)toHide {
    
    if (toHide) {
        [_rightbarButton2 setHidden:YES];
        [labelTitle setHidden:NO];
        
    }
    else {
        [_rightbarButton2 setHidden:NO];
        [labelTitle setHidden:YES];
    }
}

-(void)hideLeftMenuButton:(BOOL)toHide
{
    if (toHide)
    {
        [leftbarButton setBackgroundImage:nil forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:nil forState:UIControlStateHighlighted];;
    }
    else
    {
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_btn_menu"] forState:UIControlStateNormal];
        [leftbarButton setBackgroundImage:[UIImage imageNamed:@"home_btn_menu_on"] forState:UIControlStateHighlighted];
    }
}

-(void)hideRightBarButton:(BOOL)hide;
{
    [rightbarButton setHidden:hide];
}
-(void)setTitle:(NSString*)title
{
    [Helper setToLabel:labelTitle Text:title WithFont:Roboto_Regular FSize:16 Color:UIColorFromRGB(0xffffff)];
    //[self setNeedsDisplay];
}
-(void)setRightBarButtonTitle:(NSString*)title
{
    [rightbarButton setTitle:title forState:UIControlStateNormal];
    rightbarButton.titleLabel.numberOfLines = 0;
    rightbarButton.titleLabel.adjustsFontSizeToFitWidth = true;
    if (title.length && title.length > 3 && [title containsString:NSLocalizedString(@"TIP", @"TIP")] )
    {
        rightbarButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:10];
    }
    else
    {
        rightbarButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:12];
    }
}

-(void)setLeftBarButtonTitle:(NSString*)title{
    [leftbarButton setTitle:title forState:UIControlStateNormal];
}
-(void)setleftBarButtonImage:(UIImage*)imageOn :(UIImage *)imageOff {
    
    [leftbarButton setImage:imageOn forState:UIControlStateNormal];
    [leftbarButton setImage:imageOff forState:UIControlStateNormal];
}

-(void)rightBarButtonClicked:(UIButton*)sender{
    if (delegate && [delegate respondsToSelector:@selector(rightBarButtonClicked:)]) {
        [delegate rightBarButtonClicked:sender];
    }
}
-(void)leftBarButtonClicked:(UIButton*)sender {
    if (delegate && [delegate respondsToSelector:@selector(leftBarButtonClicked:)]) {
        [delegate leftBarButtonClicked:sender];
    }
}
@end
