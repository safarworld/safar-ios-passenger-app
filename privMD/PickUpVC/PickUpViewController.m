//
//  PickUpViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 04/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "PickUpViewController.h"
#import "ProgressIndicator.h"
#import "WebServiceHandler.h"
#import "fareCalculatorViewController.h"
#import "Database.h"
#import "SourceAddress.h"
#import "DestinationAddress.h"
#import "MapViewController.h"

//static NSCache *addressCache = nil;


@interface PickUpViewController ()
@property(nonatomic,assign) BOOL isSearchResultCome;
@property NSTimer *autoCompleteTimer;
@property NSString *substring;
@property NSMutableArray *pastSearchWords;
@property NSMutableArray *pastSearchResults;

@end

@implementation PickUpViewController
@synthesize onCompletion;
@synthesize latitude;
@synthesize longitude;
@synthesize locationType;
@synthesize isSearchResultCome;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    isSearchResultCome = NO;
    /**
     *  get all the address for current user
     */
    appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
    context = [appDelegate managedObjectContext];
    
    if(locationType == kSourceAddress)
    {
        self.navigationItem.title = NSLocalizedString(@"PICKUP LOCATION", @"PICKUP LOCATION");
        _searchBarController.placeholder = NSLocalizedString(@"Pickup Location", @"Pickup Location");
        if (context!=nil)
        {
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
        }
    }
    else if(locationType == kDestinationAddress)
    {
        self.navigationItem.title = NSLocalizedString(@"DROP OFF LOCATION", @"DROP OFF LOCATION");
        _searchBarController.placeholder = NSLocalizedString(@"Drop Off Location", @"Drop Off Location");
        if (context!=nil)
        {
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getDestinationAddressFromDataBase]];
        }
        
    }
   
    
    [self.navigationItem setHidesBackButton:YES animated:YES];
    
    /**
     *  if Address not present fetch all the cards form service
     */
    if (arrDBResult.count != 0) {
         _mAddress = [arrDBResult mutableCopy];
        
       // _mAddress = [[[arrDBResult reverseObjectEnumerator] allObjects] mutableCopy];
    }
    else {
        _mAddress = [NSMutableArray array];
    }
    
    self.searchBarController.searchBarStyle = UISearchBarStyleProminent;
    [self createNavLeftButton];
    self.pastSearchWords = [NSMutableArray array];
    self.pastSearchResults = [NSMutableArray array];
    
    
    [self createFooterViewForTable];
}
- (void)refresh:(UIRefreshControl *)refreshControl
{
    
    [refreshControl endRefreshing];
}

-(void) createNavLeftButton
{
    // UIView *navView = [[UIView new]initWithFrame:CGRectMake(0, 0,50, 44)];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(dismissViewController:) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self.navigationItem setHidesBackButton:YES animated:YES];
    [_searchBarController becomeFirstResponder];
}
-(void)viewWillDisappear:(BOOL)animated{
    
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dismissViewController:(UIButton *)sender {
    
    //    if (locationType == kDestinationAddress && _isComingFromMapVCFareButton == NO) {
    //
    //        [self.navigationController popToRootViewControllerAnimated:YES];
    //    }
    //    else {
    //
    //        MapViewController *obj = [MapViewController getSharedInstance];
    //        obj.isSelectinLocation = NO;
    //
    //        [self dismissViewControllerAnimated:YES completion:nil];
    //    }
    
    MapViewController *obj = [MapViewController getSharedInstance];
    obj.isSelectinLocation = NO;
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
#pragma mark -
#pragma mark UITableView DataSource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _mAddress.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"Cell";
    UITableViewCell *cell=nil;
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.backgroundColor=[UIColor clearColor];
    
    if(cell==nil)
    {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        cell.selectionStyle=UITableViewCellSelectionStyleGray;
        cell.backgroundColor=[UIColor clearColor];
        cell.textLabel.font = [UIFont fontWithName:Roboto_Regular size:15];
        cell.textLabel.textColor = UIColorFromRGB(0x000000);
        cell.detailTextLabel.font = [UIFont fontWithName:Roboto_Regular size:13];
        cell.detailTextLabel.textColor = UIColorFromRGB(0xa0a0a0);
        cell.detailTextLabel.numberOfLines = 2;
    }
    
    id address =  _mAddress[indexPath.section];
    if ([address isKindOfClass:[SourceAddress class]]) {
        SourceAddress *add = (SourceAddress*)address;
        cell.textLabel.text = add.srcAddress;
        cell.detailTextLabel.text = add.srcAddress2;
    }
    else if ([address isKindOfClass:[DestinationAddress class]]) {
        DestinationAddress *add = (DestinationAddress*)address;
        cell.textLabel.text = add.desAddress;
        cell.detailTextLabel.text =add.desAddress2;
    }
    else {
        NSDictionary *searchResult = [_mAddress objectAtIndex:indexPath.section];
        cell.textLabel.text = [searchResult[@"terms"] objectAtIndex:0][@"value"];
        cell.detailTextLabel.text = searchResult[@"description"];
        
    }
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 80;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 25;
    }
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Database *db = [[Database alloc]init];
    if(isSearchResultCome == YES && (locationType == kSourceAddress))
    {
        NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.section];
        NSString *placeID = [searchResult objectForKey:@"reference"];
        [self.searchBarController resignFirstResponder];
        [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place)
         {
            NSDictionary *d = [place mutableCopy];
            [db addSourceAddressInDataBase:d];
            NSDictionary *dict = [NSDictionary dictionary];
            if (onCompletion)
            {
                NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"name"])];
                NSArray *arr = [place valueForKey:@"address_components"];
                NSString *add2 =@"";
                NSString *zipCode = @"";
                for (int addrCount=1 ; addrCount< [arr count]; addrCount++)
                {
                    NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
                    if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
                    {
                        add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
                    }
                    if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
                    {
                        zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
                    }
                }
                if ([add2 hasPrefix:@", "]) {
                    add2 = [add2 substringFromIndex:2];
                }
                
                if ([add2 hasSuffix:@", "]) {
                    add2 = [add2 substringToIndex:[add2 length]];
                }
                if (add1.length == 0)
                {
                    NSArray *arr = [place valueForKey:@"address_components"];
                    NSString *add2 =@"";
                    for (int addrCount=0 ; addrCount< [arr count]; addrCount++)
                    {
                        NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
                        if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
                        {
                            add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
                        }
                        if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
                        {
                            zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
                        }
                    }
                    if ([add2 hasPrefix:@","]) {
                        add2 = [add2 substringFromIndex:1];
                    }
                    
                    if ([add2 hasSuffix:@", "]) {
                        add2 = [add2 substringToIndex:[add2 length]-2];
                    }
                    if(add2.length)
                    {
                        add1 = [NSString stringWithFormat:@"%@, %@",add1, add2];
                        add2 = @"";
                    }
                    else
                    {
                        add1 = [NSString stringWithFormat:@"%@ %@",add1, add2];
                        add2 = @"";
                    }
                }
                NSString *late = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lat"]];
                NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
                NSString *keyId = [NSString stringWithFormat:@"%@",[place valueForKey:@"place_id"]];
                
                dict = @{ @"address1":add1,
                          @"address2":add2,
                          @"lat":late,
                          @"lng":longi,
                          @"keyId":keyId,
                          @"zipCode":zipCode,
                          };
                
                onCompletion(dict,locationType);
            }
            [self gotoViewController:dict];
        }];
    }
    else if(isSearchResultCome == YES && (locationType == kDestinationAddress) )
    {
        NSDictionary *searchResult = [self.mAddress objectAtIndex:indexPath.section];
        NSString *placeID = [searchResult objectForKey:@"reference"];
        [self.searchBarController resignFirstResponder];
        [self retrieveJSONDetailsAbout:placeID withCompletion:^(NSArray *place) {
            NSDictionary *d = [place mutableCopy];
            [db addDestinationAddressInDataBase:d];
            NSDictionary *dict = [NSDictionary dictionary];
            if (onCompletion) {
                
                NSString *add1 = [NSString stringWithFormat:@"%@",flStrForStr([place valueForKey:@"name"])];
                NSArray *arr = [place valueForKey:@"address_components"];
                NSString *add2 = @"";
                NSString *zipCode = @"";
                for (int addrCount=1 ; addrCount< [arr count]; addrCount++)
                {
                    NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
                    if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
                    {
                        add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
                    }
                    if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
                    {
                        zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
                    }
                }
                if ([add2 hasPrefix:@", "]) {
                    add2 = [add2 substringFromIndex:2];
                }
                
                if ([add2 hasSuffix:@", "]) {
                    add2 = [add2 substringToIndex:[add2 length]-2];
                }
                if (add1.length == 0)
                {
                    NSArray *arr = [place valueForKey:@"address_components"];
                    NSString *add2 =@"";
                    for (int addrCount=0 ; addrCount< [arr count]; addrCount++)
                    {
                        NSArray *arrCountryType = [[arr objectAtIndex:addrCount] objectForKey:@"types"];
                        if(![[arrCountryType objectAtIndex:0] isEqualToString:@"country"])
                        {
                            add2 = [add2 stringByAppendingString:[NSString stringWithFormat:@", %@", flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"])]];
                        }
                        if([[arrCountryType objectAtIndex:0] isEqualToString:@"postal_code"])
                        {
                            zipCode = flStrForStr([[arr objectAtIndex:addrCount] objectForKey:@"long_name"]);
                        }
                    }
                    if ([add2 hasPrefix:@","]) {
                        add2 = [add2 substringFromIndex:1];
                    }
                    
                    if ([add2 hasSuffix:@", "]) {
                        add2 = [add2 substringToIndex:[add2 length]-2];
                    }
                    if(add2.length)
                    {
                        add1 = [NSString stringWithFormat:@"%@, %@",add1, add2];
                        add2 = @"";
                    }
                    else
                    {
                        add1 = [NSString stringWithFormat:@"%@ %@",add1, add2];
                        add2 = @"";
                    }
                }
                NSString *late = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lat"]];
                NSString *longi = [NSString stringWithFormat:@"%@",[place valueForKey:@"geometry"][@"location"][@"lng"]];
                NSString *keyId = [NSString stringWithFormat:@"%@",[place valueForKey:@"place_id"]];
                
                dict = @{ @"address1":add1,
                          @"address2":add2,
                          @"lat":late,
                          @"lng":longi,
                          @"keyId":keyId,
                          @"zipCode":zipCode,
                          };
                
                
                onCompletion(dict,locationType);
                [self gotoViewController:dict];
            }
        }];
    }
    else{
        
        NSDictionary *dict = [NSDictionary dictionary];
        if (locationType == kSourceAddress) {
            
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getSourceAddressFromDataBase]];
            SourceAddress *add = (SourceAddress *)arrDBResult[indexPath.section];
            
            dict = @{ @"address1": add.srcAddress,
                      @"address2":add.srcAddress2,
                      @"lat":add.srcLatitude,
                      @"lng":add.srcLongitude,
                      @"keyId":flStrForStr(add.keyId),
                      @"zipCode":flStrForStr(add.zipCode),
                      };
            
            if (onCompletion) {
                onCompletion(dict,locationType);
            }
        }
        else {
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getDestinationAddressFromDataBase]];
            DestinationAddress *add = (DestinationAddress *)arrDBResult[indexPath.section];
            dict = @{ @"address1": add.desAddress,
                      @"address2":add.desAddress2,
                      @"lat":add.desLatitude,
                      @"lng":add.desLongitude,
                      @"keyId":flStrForStr(add.keyId),
                      @"zipCode":flStrForStr(add.zipCode),
                      };
            if (onCompletion) {
                onCompletion(dict,locationType);
            }
        }
        [self gotoViewController:dict];
    }
}

-(void)gotoViewController:(NSDictionary *)dictionary {
    
    if (_isComingFromMapVCFareButton == YES)
    {
        AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
        NSDictionary *dict = [dictionary mutableCopy];
        
        NSString *add = [NSString stringWithFormat:@"%@",flStrForStr(dict[@"address1"])];
        NSString *add2 = [NSString stringWithFormat:@"%@",flStrForStr(dict[@"address2"])];
        apLocaion.desAddressLine1 = add;
        apLocaion.desAddressLine2 = add2;
        
        add = flStrForStr(dict[@"address2"]);
        apLocaion.desAddressLine2 = add;
        if (apLocaion.srcAddressLine1.length == 0)
        {
            apLocaion.srcAddressLine1 = apLocaion.srcAddressLine2;
        }
        
        apLocaion.dropOffLatitude =  dict[@"lat"];
        apLocaion.dropOffLongitude = dict[@"lng"];
        
        NSDictionary *params = @{@"cLoc":[NSString stringWithFormat:@"%@",apLocaion.currentLongitude],
                                 @"cLat":[NSString stringWithFormat:@"%@",apLocaion.currentLatitude],
                                 @"pLoc":[NSString stringWithFormat:@"%@",apLocaion.pickupLongitude],
                                 @"pLat":[NSString stringWithFormat:@"%@",apLocaion.pickupLatitude],
                                 @"pAddr":apLocaion.srcAddressLine1,
                                 @"dLat":[NSString stringWithFormat:@"%@",apLocaion.dropOffLatitude],
                                 @"dLon":[NSString stringWithFormat:@"%@",apLocaion.dropOffLongitude],
                                 @"dAddr":apLocaion.desAddressLine1,
                                 @"dAddr2":apLocaion.desAddressLine2,
                                 @"typeid":[NSNumber numberWithInteger:_typeID],
                                 };
        
        fareCalculatorViewController *fareVC = [self.storyboard instantiateViewControllerWithIdentifier:@"fareVC"];
        fareVC.locationDetails = params;
        fareVC.onCompletion = ^(NSDictionary *dict){
            
            
        };
        
        [self.navigationController pushViewController:fareVC animated:YES];
        
    }
    else {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"Result", @"Result");
}

- (void)createFooterViewForTable {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIView *footerView  = [[UIView alloc] initWithFrame:CGRectMake(0,0, screenWidth,98/2)];
    footerView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.3];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"poweredby_google"]];
    imageView.frame = CGRectMake(0,0, screenWidth,98/2);
    [footerView addSubview:imageView];
    self.tblView.tableFooterView = footerView;
}

#pragma mark -
#pragma mark Search Bar Delegates
#pragma mark - Autocomplete SearchBar methods
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    [self.autoCompleteTimer invalidate];
    [self searchAutocompleteLocationsWithSubstring:self.substring];
    [self.searchBarController resignFirstResponder];
    [self.tblView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    NSString *searchWordProtection = [self.searchBarController.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (searchWordProtection.length != 0) {
        [self runScript];
        
    } else {
       
    }
}

-(BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    self.substring = [NSString stringWithString:self.searchBarController.text];
    self.substring= [self.substring stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    self.substring = [self.substring stringByReplacingCharactersInRange:range withString:text];
    
    if ([self.substring hasPrefix:@"+"] && self.substring.length >1) {
        self.substring  = [self.substring substringFromIndex:1];
    }
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [searchBar setShowsCancelButton:YES animated:YES];
    
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.view endEditing:YES];
}

- (void)runScript
{
    [self.autoCompleteTimer invalidate];
    self.autoCompleteTimer = [NSTimer scheduledTimerWithTimeInterval:0.65f
                                                              target:self
                                                            selector:@selector(searchAutocompleteLocationsWithSubstring:)
                                                            userInfo:nil
                                                             repeats:NO];
}

- (void)searchAutocompleteLocationsWithSubstring:(NSString *)substring
{
    [self.mAddress removeAllObjects];
    [self.tblView reloadData];
    
    if (![self.pastSearchWords containsObject:self.substring]) {
        [self.pastSearchWords addObject:self.substring];
        [self retrieveGooglePlaceInformation:self.substring withCompletion:^(NSArray * results) {
            [self.mAddress addObjectsFromArray:results];
            isSearchResultCome = YES;
            NSDictionary *searchResult = @{@"keyword":self.substring,@"results":results};
            [self.pastSearchResults addObject:searchResult];
            [self.tblView reloadData];
            
        }];
        
    }else {
        
        for (NSDictionary *pastResult in self.pastSearchResults) {
            if([[pastResult objectForKey:@"keyword"] isEqualToString:self.substring]){
                [self.mAddress addObjectsFromArray:[pastResult objectForKey:@"results"]];
                [self.tblView reloadData];
            }
        }
    }
}


#pragma mark - Google API Requests


-(void)retrieveGooglePlaceInformation:(NSString *)searchWord withCompletion:(void (^)(NSArray *))complete{
    NSString *searchWordProtection = [searchWord stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if (searchWordProtection.length != 0) {
        NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=establishment|geocode&location=%@,%@&radius=500&amplanguage=en&key=%@",searchWord,latitude,longitude,kPMDServerGoogleMapsAPIKey];
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
        NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSArray *results = [jSONresult valueForKey:@"predictions"];
            
            if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
                if (!error){
                    NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                    NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                    complete(@[@"API Error", newError]);
                    return;
                }
                complete(@[@"Actual Error", error]);
                return;
            }else{
                complete(results);
            }
        }];
        
        [task resume];
    }
    
}

-(void)retrieveJSONDetailsAbout:(NSString *)place withCompletion:(void (^)(NSArray *))complete {
   
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?reference=%@&key=%@",place,kPMDServerGoogleMapsAPIKey];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSURLSessionConfiguration *defaultConfigObject = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *delegateFreeSession = [NSURLSession sessionWithConfiguration: defaultConfigObject delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDataTask *task = [delegateFreeSession dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSDictionary *jSONresult = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSArray *results = [jSONresult valueForKey:@"result"];
        
        if (error || [jSONresult[@"status"] isEqualToString:@"NOT_FOUND"] || [jSONresult[@"status"] isEqualToString:@"REQUEST_DENIED"]){
            if (!error){
                NSDictionary *userInfo = @{@"error":jSONresult[@"status"]};
                NSError *newError = [NSError errorWithDomain:@"API Error" code:666 userInfo:userInfo];
                complete(@[@"API Error", newError]);
                return;
            }
            complete(@[@"Actual Error", error]);
            return;
        }else{
            complete(results);
        }
    }];
    
    [task resume];
}


@end
