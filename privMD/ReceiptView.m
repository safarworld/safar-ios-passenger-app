//
//  ReceiptView.m
//  RoadyoDispatch
//
//  Created by 3Embed on 25/05/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "ReceiptView.h"
#import "ReceiptCell.h"
#import "Receipt2TabelViewCell.h"

@implementation ReceiptView
@synthesize onCompletion;



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)init
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"ReceiptView" owner:self options:nil] firstObject];
    UINib *cellNib = [UINib nibWithNibName:@"ReceiptCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ReceiptCell"];
    [self.tableView registerNib:cellNib forCellReuseIdentifier:@"ReceiptCell2"];
    return self;
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (invoiceData == nil)
    {
        return 0;
    }
    else if (invoiceData && [invoiceData[@"subTotal"] floatValue]<[invoiceData[@"min_fare"] floatValue])
    {
        return 10;
    }
    else
    {
        return 9;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *cellArray = [[NSBundle mainBundle] loadNibNamed:@"ReceiptCell" owner:self options:nil];
    
    if (indexPath.row == 0)
    {
        ReceiptCell *cell = [cellArray firstObject];
        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Base Fare", @"Base Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        NSString *baseFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"baseFee"]) floatValue]];
        [Helper setToLabel:cell.priceLabel Text:baseFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        cell.lineLabel.hidden = YES;
        return cell;
    }
    else if (indexPath.row == 1)
    {
        ReceiptCell *cell = [cellArray firstObject];
        NSString *distance = NSLocalizedString(@"Distance Fare", @"Distance Fare");
        distance = [NSString stringWithFormat:@"%@ (%@ %@)", distance, flStrForStr(invoiceData[@"dis"]), kPMDDistanceParameter];
        [Helper setToLabel:cell.itemLabel Text:distance WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        NSString *distanceFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"distanceFee"]) floatValue]];
        [Helper setToLabel:cell.priceLabel Text:distanceFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        cell.lineLabel.hidden = YES;
        return cell;
    }
    else if (indexPath.row == 2)
    {
        ReceiptCell *cell = [cellArray firstObject];
        NSString *timeForDisplay;
        NSString *time = NSLocalizedString(@"Time Fare", @"Time Fare");
        
        if ([flStrForStr(invoiceData[@"dur"]) intValue] != 0)
        {
            timeForDisplay = [self timeFormatted:[flStrForStr(invoiceData[@"dur"])intValue]];
            time = [NSString stringWithFormat:@"%@ (%@)", time, timeForDisplay];
            [Helper setToLabel:cell.itemLabel Text:time WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        }
        else
        {
            [Helper setToLabel:cell.itemLabel Text:time WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        }
        
        NSString *timeFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"timeFee"]) floatValue]];
        [Helper setToLabel:cell.priceLabel Text:timeFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        cell.lineLabel.hidden = YES;
        return cell;
    }
    //    else if (indexPath.row == 3)
    //    {
    //        ReceiptCell *cell = [cellArray firstObject];
    //        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Parking Fare", @"Parking Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
    //        NSString *parkingFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"parkingFee"]) floatValue]];
    //        [Helper setToLabel:cell.priceLabel Text:parkingFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
    //        cell.lineLabel.hidden = YES;
    //        return cell;
    //    }
//    else if (indexPath.row == 3)
//    {
//        ReceiptCell *cell = [cellArray firstObject];
//        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Airport Fare", @"Airport Fare") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
//        NSString *airportFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"airportFee"]) floatValue]];
//        [Helper setToLabel:cell.priceLabel Text:airportFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
//        cell.lineLabel.hidden = YES;
//        return cell;
//    }
    //    else  if (indexPath.row == 5)
    //    {
    //        ReceiptCell *cell = [cellArray firstObject];
    //        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Toll", @"Toll") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
    //        NSString *tollFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"tollFee"]) floatValue]];
    //        [Helper setToLabel:cell.priceLabel Text:tollFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
    //        cell.lineLabel.hidden = YES;
    //        return cell;
    //    }
    else if (indexPath.row == 3)
    {
        ReceiptCell *cell = [cellArray firstObject];
        NSString *discountLabel = NSLocalizedString(@"Discount", @"Discount");
        if([invoiceData[@"code"] length])
        {
            if ([invoiceData[@"discountType"] integerValue] == 1)
            {
                discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ %% )",discountLabel, flStrForStr(invoiceData[@"code"]), flStrForStr(invoiceData[@"discountVal"])];
            }
            else if ([invoiceData[@"discountType"] integerValue] == 2)
            {
                NSString *discountWithCurrency = [PatientGetLocalCurrency getCurrencyLocal:[flStrForStr(invoiceData[@"discountVal"]) floatValue]];
                discountLabel = [NSString stringWithFormat:@"%@ ( %@ ) ( %@ )", discountLabel, flStrForStr(invoiceData[@"code"]), discountWithCurrency];
            }
        }
        [Helper setToLabel:cell.itemLabel Text:discountLabel WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        NSString *discountFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"discount"]) floatValue]];
        [Helper setToLabel:cell.priceLabel Text:discountFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        cell.lineLabel.hidden = NO;
        return cell;
    }
    else if (indexPath.row == 4)
    {
        ReceiptCell *cell = [cellArray firstObject];
        [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Subtotal", @"Subtotal") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        NSString *subTotal =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"subTotal"]) floatValue]];
        [Helper setToLabel:cell.priceLabel Text:subTotal WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
        cell.lineLabel.hidden = YES;
        return cell;
    }
    if (invoiceData && [invoiceData[@"subTotal"] floatValue]<[invoiceData[@"min_fare"] floatValue])
    {
        if (indexPath.row == 5)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Minimum Fare", @"Minimum Fare") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *minimumFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"min_fare"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:minimumFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        
        else if (indexPath.row == 6)
        {
            ReceiptCell *cell = [cellArray firstObject];
            NSString *tip = NSLocalizedString(@"Tip", @"Tip");
            if ([invoiceData[@"tipPercent"] length] && [invoiceData[@"tipPercent"] integerValue] !=0)
            {
                tip = [NSString stringWithFormat:@"%@ (%@%%)", tip, invoiceData[@"tipPercent"]];
            }
            [Helper setToLabel:cell.itemLabel Text:tip WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *tipFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"tip"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:tipFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else  if (indexPath.row == 7)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Total", @"Total") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 8)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Payment Type", @"Payment Type") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            [Helper setToLabel:cell.priceLabel Text:@"" WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 9)
        {
            Receipt2TabelViewCell *cell = [cellArray lastObject];
            if ([invoiceData[@"payType"] integerValue] == 1)
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Card", @"Card") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"card"];
            }
            else
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Cash", @"Cash") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"cash"];
            }
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
    }
    else
    {
        if (indexPath.row == 5)
        {
            ReceiptCell *cell = [cellArray firstObject];
            NSString *tip = NSLocalizedString(@"Tip", @"Tip");
            if ([invoiceData[@"tipPercent"] length] && [invoiceData[@"tipPercent"] integerValue] !=0)
            {
                tip = [NSString stringWithFormat:@"%@ (%@%%)", tip, invoiceData[@"tipPercent"]];
            }
            [Helper setToLabel:cell.itemLabel Text:tip WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *tipFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"tip"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:tipFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = NO;
            return cell;
        }
        else  if (indexPath.row == 6)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Total", @"Total") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 7)
        {
            ReceiptCell *cell = [cellArray firstObject];
            [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Payment Type", @"Payment Type") WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            [Helper setToLabel:cell.priceLabel Text:@"" WithFont:Lato_Bold FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
        else  if (indexPath.row == 8)
        {
            Receipt2TabelViewCell *cell = [cellArray lastObject];
            if ([invoiceData[@"payType"] integerValue] == 1)
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Card", @"Card") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"card"];
            }
            else
            {
                [Helper setToLabel:cell.itemLabel Text:NSLocalizedString(@"Cash", @"Cash") WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
                cell.cashCardImageView.image = [UIImage imageNamed:@"cash"];
            }
            NSString *totalFee =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(invoiceData[@"amount"]) floatValue]];
            [Helper setToLabel:cell.priceLabel Text:totalFee WithFont:Lato_Regular FSize:13 Color:UIColorFromRGB(0x5b5b5b)];
            cell.lineLabel.hidden = YES;
            return cell;
        }
    }
    return nil;
}


- (NSString *)timeFormatted:(int)totalSeconds
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02dH:%02dM:%02dS",hours, minutes, seconds];
}


- (IBAction)closeBtnAction:(id)sender
{
    onCompletion(1);
    [self hidePOPup];
}

-(void)showPopUpWithDetailedDict:(NSDictionary *)dict Onwindow:(UIWindow *)window
{
    self.frame = window.frame;
    [window addSubview:self];
    [self layoutIfNeeded];
    self.contentView.alpha = 0.3;
    invoiceData = [dict mutableCopy];
    [self.tableView reloadData];
    
    NSString *headingLabel = NSLocalizedString(@"RECEIPT", @"RECEIPT");
    NSString *bookingID = NSLocalizedString(@"BOOKING ID : ", @"BOOKING ID : ");
    bookingID = [NSString stringWithFormat:@"%@%@", bookingID, flStrForStr(invoiceData[@"bid"])];
    
    
    
    NSMutableAttributedString *headingAttributedLabel = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", headingLabel, bookingID]];
    
    [headingAttributedLabel addAttribute:NSForegroundColorAttributeName
                                   value:UIColorFromRGB(0x333333)
                                   range:NSMakeRange(0,[headingLabel length])];

    [headingAttributedLabel addAttribute:NSForegroundColorAttributeName
                                   value:UIColorFromRGB(0x6f6f6f)
                                   range:NSMakeRange([headingLabel length], [bookingID length]+1)];

    [headingAttributedLabel addAttribute:NSFontAttributeName
                  value:[UIFont fontWithName:Lato_Regular size:16.0]
                  range:NSMakeRange(0, [headingLabel length])];
    
    [headingAttributedLabel addAttribute:NSFontAttributeName
                                   value:[UIFont fontWithName:Lato_Regular size:12.0]
                                   range:NSMakeRange([headingLabel length], [bookingID length]+1)];

    [self.receiptLabel setAttributedText:headingAttributedLabel];

    
//    headingLabel = [NSString stringWithFormat:@"%@\n%@%@", headingLabel, bookingID, flStrForStr(invoiceData[@"bid"])];
//    [Helper setToLabel:self.receiptLabel Text:headingLabel WithFont:Lato_Regular FSize:14 Color:UIColorFromRGB(0x333333)];
    
    self.contentView.layer.cornerRadius = 8.0f;
    self.contentView.layer.masksToBounds = YES;

    self.closeBtn.layer.cornerRadius = 4.0f;
    self.closeBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.closeBtn.layer.borderWidth = 1.0f;

    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                     }];
}

-(void)hidePOPup
{
    self.contentView.alpha = 1;
    [UIView animateWithDuration:0.5
                     animations:^{
                         self.contentView.alpha = 0.1;
                     }
                     completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}
@end
