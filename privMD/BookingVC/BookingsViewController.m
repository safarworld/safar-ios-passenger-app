//
//  BookingsViewController.m
//  UBER
//
//  Created by Rahul Sharma on 05/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "BookingsViewController.h"
#import "BookingEvent.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"
#import "EmptyCell.h"
#import "BookingHistoryCell.h"
#import "BookingHistoryWithoutDropAddressCell.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "PatientViewController.h"
#import "InvoiceView.h"
#import "CancelView.h"

@interface BookingsViewController ()<CustomNavigationBarDelegate, UIAlertViewDelegate>
{
    BookingHistoryCell *cell1;
    BookingHistoryWithoutDropAddressCell *cell2;
    NSInteger indicatorCell;
    BOOL reachedLastCell;
    NSIndexPath *selectedCellIndex;
}

@property (nonatomic,strong) NSMutableArray *events;


@end
@implementation BookingsViewController
@synthesize pageIndex;
@synthesize footerView;

#pragma Mark-
#pragma WebServiceCall

-(NSString *)getMonths
{
    NSDate *date = [NSDate date];
    NSCalendar *calendarLoc = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendarLoc components:(NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSNumberFormatter * numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setPaddingCharacter:@"0"];
    [numberFormatter setMinimumIntegerDigits:2];
    NSString * monthString = [numberFormatter stringFromNumber:[NSNumber numberWithInteger:month]];
    NSString *retMonth = [NSString stringWithFormat:@"%ld-%@",(long)year,monthString];
    return retMonth;
}


-(void)sendServicegetPatientAppointment:(NSInteger)index;
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Loading...", @"Loading...")];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *month = [self getMonths];
    NSDictionary *params =  @{ @"ent_sess_token":sessionToken,
                               @"ent_dev_id": deviceId,
                               @"ent_date_time":[Helper getCurrentDateTime],
                               @"ent_appnt_dt":month,
                               @"ent_page_index":[NSNumber numberWithInteger:index]
                               };
    
    NetworkHandler *network = [NetworkHandler sharedInstance];
    [network composeRequestWithMethod:@"getSlaveAppointments" paramas:params
                         onComplition:^(BOOL success , NSDictionary *response){
                             
                             [[ProgressIndicator sharedInstance] hideProgressIndicator];
                             if (success)
                             {
                                 [self getPatientAppointmentResponse:response];
                             }
                             else
                             {
                                 [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                 indicatorCell = 0;
                                 [[self table] reloadData];
                                 --pageIndex;
                             }
                         }];
}

-(void)getPatientAppointmentResponse:(NSDictionary *)response
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (!response)
    {
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
    }
    else if ([response[@"errFlag"] intValue] == 1 && ([response[@"errNum"] intValue] == 6 || [response[@"errNum"] intValue] == 7 || [response[@"errNum"] intValue] == 99 || [response[@"errNum"] intValue] == 101 )) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            NSMutableArray *appointmentsArr = [dictResponse objectForKey:@"appointments"];
            [self addEvents:appointmentsArr];
            reachedLastCell = [dictResponse[@"lastcount"] boolValue];
            if (reachedLastCell)
            {
                indicatorCell = 0;
            }
            [[self table] reloadData];
            ++pageIndex;
        }
        else if([[dictResponse objectForKey:@"errFlag"] intValue] == 1 && [[dictResponse objectForKey:@"errNum"] intValue] == 65)
        {
            indicatorCell = 0;
            reachedLastCell = YES;
            [[self table] reloadData];
        }
    }
}

-(void)addEvents:(NSArray *)eventsArray
{
    NSMutableDictionary *eventsDict = nil;
    for (int i =0; i< eventsArray.count ;i++)
    {
        eventsDict = eventsArray[i];
        BookingEvent *event = [[BookingEvent alloc] initWithDictionary:eventsDict];
        [_events addObject: event];
    }
}


/**
 *  cancel live booking
 */
-(void)sendRequestForCancelAppointment:(NSDictionary *)dictionary
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Cancelling...", @"Cancelling...")];
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *driverEmail = flStrForStr(dictionary[@"email"]);
    NSString *appointmntDate = flStrForStr(dictionary[@"apntDt"]);
    NSString *bid = flStrForStr(dictionary[@"bid"]);

    NSString *currentDate = [Helper getCurrentDateTime];
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":flStrForStr(driverEmail),
                             @"ent_appnt_dt":flStrForStr(appointmntDate),
                             @"ent_bid":flStrForStr(bid),
                             @"ent_date_time":currentDate,
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       [self parseCancelAppointmentResponse:response];
                                   }
                               }];
}

-(void)parseCancelAppointmentResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if([response[@"errFlag"] integerValue] == 0 && ([response[@"errNum"] integerValue] == 42 || [response[@"errNum"] integerValue] == 43))
    {
        BookingEvent *eventToChange = [[self events]objectAtIndex:selectedCellIndex.section];
        
        NSMutableDictionary *changedDict = [eventToChange.info mutableCopy];
        changedDict[@"status"] = @"You cancelled.";
        changedDict[@"statCode"] = @"9";
        
        [[self events] removeObjectAtIndex:selectedCellIndex.section];
        
        BookingEvent *eventChanged = [[BookingEvent alloc] initWithDictionary:changedDict];
        [[self events] insertObject:eventChanged atIndex:selectedCellIndex.section];
        
        NSMutableIndexSet *indetsetToUpdate = [[NSMutableIndexSet alloc]init];
        [indetsetToUpdate addIndex:selectedCellIndex.section];

        [self.table reloadSections:indetsetToUpdate withRowAnimation:UITableViewRowAnimationNone];
    }
    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addCustomNavigationBar];
    
    _events = [NSMutableArray array];
    pageIndex = 0;
    indicatorCell = 1;
    reachedLastCell = NO;
    [self sendServicegetPatientAppointment:pageIndex];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark- Custom Methods

- (void) addCustomNavigationBar{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"BOOKINGS", @"BOOKINGS")];
    [self.view addSubview:customNavigationBarView];
    
}
-(void)leftBarButtonClicked:(UIButton *)sender{
    [self menuButtonclicked];
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSInteger count = [self events].count;
    if (count == 0)
    {
        count = 1;
    }
    else
    {
        count = count + indicatorCell;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger count = [[self events] count];
    if (count == 0)
    {
        static NSString *CellIdentifier = @"emptyCell";
        tableView.backgroundColor = UIColorFromRGB(0xE1E1E1);

        EmptyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    }
    else if (indexPath.section == count)
    {
        UITableViewCell *cell = [[UITableViewCell alloc] init];
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]init];
        activityIndicator.frame = CGRectMake((self.table.frame.size.width-25)/2, (125-25)/2, 25, 25);
        [cell addSubview:activityIndicator];
        activityIndicator.backgroundColor=[UIColor clearColor];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [activityIndicator startAnimating];
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"bokingHistoryWithDropAddress";
        static NSString *CellIdentifier1 = @"bookingHistoryWithOutDropAddress";
        tableView.backgroundColor = UIColorFromRGB(0xFFFFFF);

        BookingEvent *event = [[self events] objectAtIndex:[indexPath section]];
        
        cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        cell2 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];

        if ([event desAdd].length )
        {
            NSString *strImageUrl = flStrForStr([event image]);
            if (strImageUrl.length)
            {
                strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,[event image]];
                [cell1.driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                         placeholderImage:[UIImage imageNamed:@"driverImage"]
                                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                    
                                                }];
            }
            else
            {
                cell1.driverImageView.image = [UIImage imageNamed:@"driverImage"];
            }
            
            
            [cell1.driverNameLabel setText:[event name].uppercaseString];
            NSString *amount =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj([event amount]) floatValue]];
            
            cell1.totalAmountLabel.text = [NSString stringWithFormat:@"%@",amount];
            [cell1.pickUpAddresslabel setText:[event pickAdd]];
            [cell1.dropOffAddressLabel setText:[event desAdd]];
            [cell1.bookingStatusLabel setText:[event status].uppercaseString];
            [cell1.dateLabel setText:[Helper getDateandTime:[event time]]];
            // border radius
            [cell1.headerView.layer setCornerRadius:10.0f];
            [self addShadowToView:cell1.dummyBackgroundView];
            return cell1;
        }
        else
        {
            NSString *strImageUrl = flStrForStr([event image]);
            if (strImageUrl.length)
            {
                strImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,[event image]];
                [cell2.driverImageView sd_setImageWithURL:[NSURL URLWithString:strImageUrl]
                                         placeholderImage:[UIImage imageNamed:@"driverImage"]
                                                completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                                    
                                                }];
            }
            else
            {
                cell2.driverImageView.image = [UIImage imageNamed:@"driverImage"];
            }

            [cell2.driverNameLabel setText:[event name].uppercaseString];
            NSString *amount =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj([event amount]) floatValue]];
            
            cell2.totalAmountLabel.text = [NSString stringWithFormat:@"%@",amount];
            [cell2.pickUpAddresslabel setText:[event pickAdd]];
            [cell2.bookingStatusLabel setText:[event status].uppercaseString];
            [cell2.dateLabel setText:[Helper getDateandTime:[event time]]];
            [cell2.headerView.layer setCornerRadius:10.0f];
            [self addShadowToView:cell2.dummyBackgroundView];
            return cell2;
        }
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self events] count] == 0)
    {
        return;
    }
    BookingEvent *event = [[self events]objectAtIndex:indexPath.section];
    NSDictionary *dictionary = [event.info mutableCopy];
    if ([dictionary[@"apptType"] integerValue] == 2 && [dictionary[@"statCode"] integerValue] == 1)
    {
        selectedCellIndex = indexPath;
        CancelView* cancelView = [[CancelView alloc]init];
        [cancelView showPopUpWithDetailedDict:dictionary];
        cancelView.onCompletion = ^(NSInteger isCancel)
        {
            if (isCancel)
            {
                BookingEvent *eventToChange = [[self events]objectAtIndex:selectedCellIndex.section];
                
                NSMutableDictionary *changedDict = [eventToChange.info mutableCopy];
                changedDict[@"status"] = @"You cancelled.";
                changedDict[@"statCode"] = @"9";
                
                [[self events] removeObjectAtIndex:selectedCellIndex.section];
                
                BookingEvent *eventChanged = [[BookingEvent alloc] initWithDictionary:changedDict];
                [[self events] insertObject:eventChanged atIndex:selectedCellIndex.section];
                
                NSMutableIndexSet *indetsetToUpdate = [[NSMutableIndexSet alloc]init];
                [indetsetToUpdate addIndex:selectedCellIndex.section];
                
                [self.table reloadSections:indetsetToUpdate withRowAnimation:UITableViewRowAnimationNone];
            }
        };
    }
    else if([dictionary[@"statCode"] integerValue] == 4 || [dictionary[@"statCode"] integerValue] == 5 || [dictionary[@"statCode"] integerValue] == 9)
    {
        if([dictionary[@"payStatus"] integerValue] == 1 || [dictionary[@"payStatus"] integerValue] == 3 )
        {
            InvoiceView* invoiceView = [[InvoiceView alloc]init];
            invoiceView.driverEmail = dictionary[@"email"];;
            invoiceView.apptDate = dictionary[@"apntDt"];
            invoiceView.isComingFromBookingHistory = YES;
            [invoiceView showPopUpWithDetailedDict:nil];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSInteger count = [self events].count;
    if (count == 0)
    {
        return 300;
    }
    return 125;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}
- (void)reloadSections:(NSIndexSet *)sections withRowAnimation:(UITableViewRowAnimation)animation
{
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 101 && buttonIndex == 1)
    {
        BookingEvent *event = [[self events]objectAtIndex:selectedCellIndex.section];
        NSDictionary *dictionary = [event.info mutableCopy];
        [self sendRequestForCancelAppointment:dictionary];
    }
}


- (void)scrollViewDidScroll: (UIScrollView *)scroll
{
    CGFloat currentOffset = scroll.contentOffset.y;
    CGFloat contentHeight = scroll.contentSize.height;
    CGFloat scrollHeight  = scroll.frame.size.height;
    if (currentOffset+scrollHeight == contentHeight && !reachedLastCell)
    {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable])
        {
            pageIndex++;
            indicatorCell = 1;
            [self sendServicegetPatientAppointment:pageIndex];
        }
        else
        {
            indicatorCell = 0;
            [[self table] reloadData];
        }
    }
}

- (void) addShadowToView:(UIView *)view
{
    // border radius
    [view.layer setCornerRadius:10.0f];
    
    // border
    [view.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [view.layer setBorderWidth:1.0f];
    
    // drop shadow
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOpacity:0.5];
    [view.layer setShadowRadius:2.0];
    [view.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
}



@end
