//
//  ViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "ViewController.h"
#import "PickUpViewController.h"
#import "PaymentViewController.h"

@interface ViewController ()<XDKAirMenuDelegate>
@property (nonatomic, strong) UITableView *tableView;
@end

@implementation ViewController


- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:UIColorFromRGB(0x0d143b)];
    [_tableView setBackgroundColor:UIColorFromRGB(0x0d143b)];
    
    if (IS_IOS7)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.airMenuController = [XDKAirMenuController sharedMenu];
    self.airMenuController.airDelegate = self;
    [self.view addSubview:self.airMenuController.view];
    [self addChildViewController:self.airMenuController];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"TableViewSegue"])
    {
        self.tableView = ((UITableViewController*)segue.destinationViewController).tableView;
    }
}


#pragma mark - XDKAirMenuDelegate

- (UIViewController*)airMenu:(XDKAirMenuController*)airMenu viewControllerAtIndexPath:(NSIndexPath*)indexPath
{
    UIStoryboard *storyboard = self.storyboard;
    UIViewController *vc = nil;
    
    vc.view.autoresizesSubviews = TRUE;
    vc.view.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
   
    if (indexPath.row == 0)
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController1"];
        return vc;
    }
    else if (indexPath.row == 1)
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"appointmentViewController"];
        return vc;
    }
//    else if (indexPath.row == 2)
//    {
//        vc = [storyboard instantiateViewControllerWithIdentifier:@"walletVC"];
//        return vc;
//    }
    else if (indexPath.row == 2)
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"paymentVC"];
        return vc;
    }
    else if (indexPath.row == 3)
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"profileVC"];
        return vc;
    }
    else if(indexPath.row == 4)
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"supportTableVC"];
        return vc;
    }
    else if(indexPath.row == 5)
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"inviteVC"];
        return vc;
    }
    else if(indexPath.row == 6)
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"aboutVC"];
        return vc;
    }
    else
    {
        vc = [storyboard instantiateViewControllerWithIdentifier:@"ViewController1"];
        return vc;
    }
}

#pragma mark Webservice Handler(Request) -

- (UITableView*)tableViewForAirMenu:(XDKAirMenuController*)airMenu
{
    return self.tableView;
}

@end
