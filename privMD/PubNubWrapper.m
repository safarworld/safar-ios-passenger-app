//
//  PubNubWrapper.m
//  Homappy
//
//  Created by Rahul Sharma on 29/02/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "PubNubWrapper.h"

@implementation PubNubWrapper

static PubNubWrapper *share;

/*********************************PubNub User Defined Methods********************************/
/**
 *  Shared Instance
 *
 *  @return returns Object of its own Class
 */
+ (id)sharedInstance
{
    
    if (!share) {
        share = [[self alloc] init];
    }
    return share;
}
- (instancetype)init
{
    if (self == [super init])
    {
        NSString *publishKey;
        NSString *subscribeKey;
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        if ([[ud objectForKey:KNSUPubnubPublishKey] length] && [[ud objectForKey:kNSUPubnubSubscribeKey] length])
        {
            publishKey = [ud objectForKey:KNSUPubnubPublishKey];
            subscribeKey = [ud objectForKey:kNSUPubnubSubscribeKey];
        }
        else
        {
            publishKey = kPMDPubNubPublisherKey;
            subscribeKey = kPMDPubNubSubcriptionKey;
        }
        
        if (!publishKey.length && !subscribeKey.length)
        {
            return nil;
        }
        PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:publishKey subscribeKey:subscribeKey];
        configuration.heartbeatNotificationOptions = PNHeartbeatNotifyAll;
        
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserEmailID] length])
        {
            configuration.uuid = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserEmailID];
        }
        
        configuration.presenceHeartbeatValue = 10;
        configuration.presenceHeartbeatInterval = 5;
        self.client = [PubNub clientWithConfiguration:configuration];
    }
    return self;
}

/**
 *  Initiation of PubNub Method
 *  Call this method from AppDelagate, with Calling sharedInstance
 */
- (void)initiatePubNub
{
    self.my_channel = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserPubnubChannel]);
    if (self.my_channel.length)
    {
        [self.client subscribeToChannels: @[self.my_channel] withPresence:YES];
    }
}

#pragma marks - Subcription Methods
/**
 *  Subscribe to MyChannels
 *  To the channel that I get on login or Signup
 */
- (void)subscribeToUserChannel
{
    self.my_channel = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserPubnubChannel]);
    NSString *presenseChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUPresensePubnubChannel]);
    
    if (self.my_channel.length)
    {
        [self.client addListener:self];
        [self.client subscribeToChannels:@[self.my_channel] withPresence:YES];
    }
    if (presenseChn.length)
    {
        [self.client subscribeToChannels:@[presenseChn] withPresence:YES];
    }
}

/**
 *  Subscribe to channel
 *
 */
- (void)subscribeToChannel:(NSString *)channel
{
    [self.client addListener:self];
    [self.client subscribeToChannels:@[channel] withPresence:YES];
}
/**
 *  Subscribe with Channels
 *
 *  @param Channels array of Channels
 */
- (void)subscribeToChannels:(NSArray *)channels
{
    [self.client addListener:self];
    [self.client subscribeToChannels:channels withPresence:YES];
    self.subscribed_channels = [channels mutableCopy];
}

#pragma marks - Publishing Methods
/**
 *  Publish with Parameter to Channel
 *
 *  @param parameter Parameter Dict
 *  @param channel   toChannel
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
{
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError)
              {
                  // Message successfully published to specified channel.
              }
              // Request processing failed.
              else
              {
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  // Request can be resent using: [status retry];
                  NSLog(@"Publish error : %@",[status stringifiedCategory]);
              }
          }];
}

/**
 *  Publish with complition block
 *
 *  @param parameter       Parameter Dict
 *  @param channel         channel Publish to
 *  @param completionBlock complition block
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
              withCompletion:(void(^)(bool success))completionBlock{
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError)
              {
                  // Message successfully published to specified channel.
                  completionBlock(YES);
              }
              // Request processing failed.
              else
              {
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  // Request can be resent using: [status retry];
                  NSLog(@"Publish error:%@", [status stringifiedCategory]);
                  completionBlock(NO);
              }
          }];
}

/**
 *  Publush with Parameter To Channel and Start timer
 *
 *  @param parameter  Parameter Dict
 *  @param channel    ChannelTo
 *  @param startTimer Start Timer YES if You want, else NO
 */
- (void)publishWithParameter:(NSDictionary *)parameter
                   toChannel:(NSString *)channel
                  startTimer:(BOOL)startTimer {
    
    [self.client publish:parameter
               toChannel:channel
          storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError)
              {
                  // Message successfully published to specified channel.
              }
              // Request processing failed.
              else
              {
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  // Request can be resent using: [status retry];
                  NSLog(@"Publish error : %@",[status stringifiedCategory]);
              }
          }];
    self.params = [parameter mutableCopy];
    self.toChannel = channel;
    if (startTimer && !self.timerObj)
    {
        self.timerObj = [NSTimer scheduledTimerWithTimeInterval:5
                                                         target:self
                                                       selector:@selector(startTimer)
                                                       userInfo:nil
                                                        repeats:YES];
    }
}


-(void)getOccupancyCountOnDriverChannel:(NSDictionary *)message onChannel:(NSString *)channel
{
    [self.client hereNowForChannel:channel withVerbosity:PNHereNowOccupancy completion:^(PNPresenceChannelHereNowResult * _Nullable result, PNErrorStatus * _Nullable status)
     {
        if (!status.isError)
        {
            if (result != nil)
            {
                NSInteger count = [[NSString stringWithFormat:@"%@", result.data.occupancy] integerValue];
                if (count > 0)
                {
                    NSLog(@"Passenger is Online");
                    [self publishWithParameter:message toChannel:channel];
                }
                else
                {
                    NSLog(@"Passenger Went Offline");
                }
            }
        }
        else
        {
            NSLog(@"Pubnub Occupancy Error = %@", [status errorData]);
        }
    }];
}

/**
 *  Start timer
 */
- (void)startTimer
{
    [self publishWithParameter:self.params
                     toChannel:self.toChannel
                    startTimer:YES];
}

/**
 *  Stop timer
 */
- (void)stopTimer
{
    if (self.timerObj)
    {
        [self.timerObj invalidate];
        self.timerObj = nil;
    }
}

/**********************************PubNub SDK Methods*******************************************/
/**
 @brief  Notify listener about new message which arrived from one of remote data object's live feed
 on which client subscribed at this moment.
 @param client  Reference on \b PubNub client which triggered this callback method call.
 @param message Reference on \b PNResult instance which store message information in \c data
 property.
 @since 4.0
 */

- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message
{
    // Handle new message stored in message.data.message
    if (message.data.actualChannel)
    {
        // Message has been received on channel group stored in
        // message.data.subscribedChannel
    }
    else
    {
        // Message has been received on channel stored in
        // message.data.subscribedChannel
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(receivedMessage:andChannel:)])
    {
        [self.delegate receivedMessage:message.data.message
                            andChannel:message.data.subscribedChannel];
    }
}

/**
 @brief      Notify listener about subscription state changes.
 @discussion This callback can fire when client tried to subscribe on channels for which it doesn't
 have access rights or when network went down and client unexpectedly disconnected.
 
 @param client Reference on \b PubNub client which triggered this callback method call.
 @param status  Reference on \b PNStatus instance which store subscriber state information.
 
 @since 4.0
 */
- (void)client:(PubNub *)client didReceiveStatus:(PNSubscribeStatus *)status
{
    if (status.operation == PNUnsubscribeOperation && status.category == PNDisconnectedCategory)
    {
        //         This is the expected category for an unsubscribe. This means there was no error in unsubscribing from everything.
    }
    else if (status.category == PNUnexpectedDisconnectCategory)
    {
        // This event happens when radio / connectivity is lost
    }
    else if (status.category == PNConnectedCategory)
    {
        // Connect event. You can do stuff like publish, and know you'll get it.
        // Or just use the connected event to confirm you are subscribed for
        // UI / internal notifications, etc
    }
    else if (status.category == PNReconnectedCategory)
    {
        // Happens as part of our regular operation. This event happens when
        // radio / connectivity is lost, then regained.
    }
    else if (status.category == PNDecryptionErrorCategory)
    {
        // Handle messsage decryption error. Probably client configured to
        // encrypt messages and on live data feed it received plain text.
    }
    NSLog(@"Pubnub Status = %@",status.stringifiedCategory);
}


#pragma mark - Presence Method

- (void)client:(PubNub *)client didReceivePresenceEvent:(PNPresenceEventResult *)event
{
    if (event.data.actualChannel)
    {
        
        // Message has been received on channel group stored in
        // message.data.subscribedChannel
    }
    else
    {
        // Message has been received on channel stored in
        // message.data.subscribedChannel
    }
    NSLog(@"Did receive presence event: %@\n%@\n", event.data.presenceEvent,event.data.presence);
}

#pragma mark -  Unsubscribe from Channels 

//Unsubscription process results arrive to listener
//which should adopt to PNObjectEventListener protocol and registered using:
/**
 *  Unsubscribe from My Channels
 */
- (void)unsubscribeFromMyChannel
{
    if (!_my_channel.length)
    {
        self.my_channel = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserPubnubChannel]);
    }
    NSString *presenseChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUPresensePubnubChannel]);
    if (presenseChn.length)
    {
        [self.client unsubscribeFromChannels:@[_my_channel, presenseChn] withPresence:YES];
    }
    else
    {
        [self.client unsubscribeFromChannels:@[_my_channel] withPresence:YES];
    }
}

/**
 *  Unsubscribe from My Channels
 */
- (void)unsubscribeFromChannel:(NSString *)channel
{
    [self.client unsubscribeFromChannels:@[channel]
                            withPresence:NO];
}

/**
 *  Unsubscribe from All Channels
 */
- (void)unsubscribeFromAllChannel
{
    [self.client unsubscribeFromChannels:self.subscribed_channels
                            withPresence:NO];
}

/**
 *  Unsubscribe from Custom Channels
 *
 *  @param channels Channels Array
 */
- (void)unsubscribeFromChannels:(NSArray *)channels
{
    [self.client unsubscribeFromChannels:channels
                            withPresence:NO];
}

@end
