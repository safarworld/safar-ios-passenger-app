//
//  BookedController.m
//  SimpleWeather
//
//  Created by 3Embed on 09/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import "BookedController.h"

@implementation BookedController
@synthesize map;
@synthesize image;

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    NSString *imageURL = [NSString stringWithFormat:@"http://188.166.48.202/safar/pics/%@", @"6418905570502.png"];
    [self loadImage:imageURL];
    // Determine a location to display - Apple headquarters
}

-(void)willActivate {
    NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"];
    NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"];
    CLLocationCoordinate2D pickUpLoc = CLLocationCoordinate2DMake([lat doubleValue], [log doubleValue]);
    MKCoordinateSpan coordinateSpan = MKCoordinateSpanMake(1, 1);
    [self.map addAnnotation:pickUpLoc withPinColor: WKInterfaceMapPinColorPurple];
    [self.map setRegion:(MKCoordinateRegionMake(pickUpLoc, coordinateSpan))];
    
    NSString *driverLat = @"13.028628349304";//[[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"];
    NSString *driverLog = @"77.589508056641";//[[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"];

    CLLocationCoordinate2D driverLoc = CLLocationCoordinate2DMake([driverLat doubleValue], [driverLog doubleValue]);
    if(!image)
        image = [UIImage new];
    [self.map addAnnotation:driverLoc withImage:image centerOffset:CGPointMake(20, -20)];
    [self.map setRegion:(MKCoordinateRegionMake(driverLoc, coordinateSpan))];
    
}

-(void)loadImage:(NSString *)imageURL {
    NSURL *url = [NSURL URLWithString:imageURL];
    NSURLSessionDataTask *downloadTask = [[NSURLSession sharedSession]
                                          dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              UIImage *downloadedImage = [UIImage imageWithData:data];
                                              // Do what you like with your image
                                              image = downloadedImage;
                                          }];
    
    [downloadTask resume];
    
}


@end
