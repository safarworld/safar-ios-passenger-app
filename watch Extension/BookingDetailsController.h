//
//  BookingDetailsController.h
//  SimpleWeather
//
//  Created by 3Embed on 02/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface BookingDetailsController : WKInterfaceController

@property (strong, nonatomic) NSDictionary *selectedType;
@property (strong, nonatomic) NSString *pickupAddress;
@property (strong, nonatomic) NSDictionary *firstDriver;

@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceLabel *bookingNotAcceptedLabel;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceImage *carImage;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceLabel *timeLabel;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceButton *getRideButton;

- (IBAction)getRideButtonAction;

@end
