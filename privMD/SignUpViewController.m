//
//  SignUpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "SignUpViewController.h"
#import "VerifyiMobileViewController.h"
#import "CardLoginViewController.h"
#import "TermsnConditionViewController.h"
#import "SignInViewController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import "ViewController.h"
#import "CountryPicker.h"
#import "CountryNameTableViewController.h"
#import "LocationServicesMessageVC.h"


#define REGEX_PASSWORD_ONE_UPPERCASE @"^(?=.*[A-Z]).*$"  //Should contains one or more uppercase letters
#define REGEX_PASSWORD_ONE_LOWERCASE @"^(?=.*[a-z]).*$"  //Should contains one or more lowercase letters
#define REGEX_PASSWORD_ONE_NUMBER @"^(?=.*[0-9]).*$"  //Should contains one or more number
#define REGEX_PASSWORD_ONE_SYMBOL @"^(?=.*[!@#$%&_]).*$"  //Should contains one or more symbol


@interface SignUpViewController ()
{
    BOOL isEmailValid;
    BOOL isReferralCodeValid;
    float initialOffset_Y;
    float keyboardHeight;
}
@property(nonatomic,strong) CLLocationManager *locationManager;
@property (assign ,nonatomic) BOOL isImageNeedsToUpload;
@property (assign ,nonatomic) BOOL isKeyboardIsShown;
typedef enum {
    PasswordStrengthTypeWeak,
    PasswordStrengthTypeModerate,
    PasswordStrengthTypeStrong
}PasswordStrengthType;

-(int)checkPasswordStrength:(NSString *)password;

@end

@implementation SignUpViewController

@synthesize mainView, mainScrollView;
@synthesize activeTextField;
@synthesize firstNameTextField;
@synthesize lastNameTextField;
@synthesize emailTextField;
@synthesize passwordTextField;
@synthesize phoneNoTextField;
@synthesize helperCountry;
@synthesize helperCity;
@synthesize navNextButton;
@synthesize saveSignUpDetails;
@synthesize profileButton;
@synthesize profileImageView;
@synthesize tncButton;
@synthesize tncCheckButton;
@synthesize creatingLabel;
@synthesize isKeyboardIsShown;
@synthesize locationManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton = NO;
    isEmailValid = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissKeyboard)];
    [self.mainScrollView addGestureRecognizer:tapGesture];

    [self createNavLeftButton];
    [self createNavRightButton];
    
    initialOffset_Y = -64;
    keyboardHeight = 220;
    
    [Helper setToLabel:creatingLabel Text:NSLocalizedString(@"By creating an account you agree to our ", @"By creating an account you agree to our ") WithFont:HELVETICANEUE_LIGHT FSize:14 Color:UIColorFromRGB(0x000000)];
    [Helper setButton:tncButton Text:NSLocalizedString(@"Terms & Conditions.", @"Terms & Conditions.") WithFont:HELVETICANEUE_LIGHT FSize:14 TitleColor:UIColorFromRGB(0x0000ff) ShadowColor:nil];
    
    
    [firstNameTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    [lastNameTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    [emailTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    [passwordTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    [phoneNoTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    [_referalCodeTextField setValue:UIColorFromRGB(0x333333) forKeyPath:@"_placeholderLabel.textColor"];
    
    firstNameTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    firstNameTextField.textColor = UIColorFromRGB(0x333333);
    lastNameTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    lastNameTextField.textColor = UIColorFromRGB(0x333333);
    emailTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    emailTextField.textColor = UIColorFromRGB(0x333333);
    passwordTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    passwordTextField.textColor = UIColorFromRGB(0x333333);
    phoneNoTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    phoneNoTextField.textColor = UIColorFromRGB(0x333333);
    _referalCodeTextField.font = [UIFont fontWithName:Roboto_Regular size:13];
    _referalCodeTextField.textColor = UIColorFromRGB(0x333333);
    
    profileImageView.image = [UIImage imageNamed:@"driverImage.png"];
    mainScrollView.scrollEnabled = YES;
    isTnCButtonSelected = NO;
    
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryCode = [locale objectForKey: NSLocaleCountryCode];
    NSDictionary *dictionaryWithCode = [CountryPicker dictionaryWithCountryCodes];
    NSString *stringCountryCode = [dictionaryWithCode objectForKey:countryCode];
    NSString *imagePath = [NSString stringWithFormat:@"CountryPicker.bundle/%@", countryCode];
    [_phoneNoCountryCode setTitle:[NSString stringWithFormat:@"+%@",stringCountryCode] forState:UIControlStateNormal];
    _phoneNoCountryFlag.image = [UIImage imageNamed:imagePath];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self createNavView];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    mainScrollView.contentSize = CGSizeMake(screenSize.size.width, screenSize.size.height);
}

-(void)viewDidAppear:(BOOL)animated
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    if([[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat] && [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong] == 0)
    {
        [self getCurrentLocationFromGPS];
    }
}
-(void) viewDidDisappear:(BOOL)animated
{
    self.navigationItem.titleView  = nil;
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)getCurrentLocationFromGPS
{
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled])
    {
        if (!locationManager)
        {
            locationManager = [[CLLocationManager alloc] init];
            locationManager.delegate = self;
            locationManager.distanceFilter = kCLDistanceFilterNone;
            locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])//requestAlwaysAuthorization
            {
                [self.locationManager requestWhenInUseAuthorization];//requestAlwaysAuthorization
            }
        }
        [locationManager startUpdatingLocation];
    }
    else
    {
        [self gotoLocationServicesMessageViewController];
    }
}
#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    NSString *lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString * log = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    [[NSUserDefaults standardUserDefaults]setObject:lat forKey:KNUCurrentLat];
    [[NSUserDefaults standardUserDefaults]setObject:log forKey:KNUCurrentLong];
    [[NSUserDefaults standardUserDefaults] synchronize];

    if([[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat] && [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong] == 0)
    {
        [self getCurrentLocationFromGPS];
    }
    else
    {
        [locationManager stopUpdatingLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
    {
        // The user denied your app access to location information.
        [self gotoLocationServicesMessageViewController];
    }
}

-(void)locationServicesChanged:(NSNotification*)notification {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)//kCLAuthorizationStatusAuthorized
    {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController
{
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}




-(void)createNavView
{
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(80,-10, 160, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,0, 147, 30)];
    navTitle.text = NSLocalizedString(@"CREATE ACCOUNT", @"CREATE ACCOUNT");
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Roboto_Light size:13];
    [navView addSubview:navTitle];
    UIImageView *navImage = [[UIImageView alloc]initWithFrame:CGRectMake(0,30,147,7)];
    navImage.image = [UIImage imageNamed:@"signup_timer_first.png"];
    [navView addSubview:navImage];
    self.navigationItem.titleView = navView;
   
}


-(void) createNavLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];
    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
  
    // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}


-(void)createNavRightButton
{
    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImage.size.width,buttonImage.size.height)];

    [navNextButton addTarget:self action:@selector(NextButtonClicked) forControlEvents:UIControlEventTouchUpInside];
   
    
    [Helper setButton:navNextButton Text:NSLocalizedString(@"NEXT", @"NEXT") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"NEXT", @"NEXT") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"NEXT", @"NEXT") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [navNextButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];

}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    [firstNameTextField resignFirstResponder];
    [lastNameTextField resignFirstResponder];
    [emailTextField resignFirstResponder];
    [passwordTextField resignFirstResponder];
    [phoneNoTextField resignFirstResponder];
    [_referalCodeTextField resignFirstResponder];

}

-(void)signUp
{
    NSString *signupFirstName = firstNameTextField.text;
    NSString *signupEmail = emailTextField.text;
    NSString *signupPassword = passwordTextField.text;
    NSString *signupPhoneno = phoneNoTextField.text;
    
    if ((unsigned long)signupFirstName.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter First Name", @"Enter First Name")];
    }
    else if ((unsigned long)signupEmail.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Email ID", @"Enter Email ID")];
    }
    else if ([Helper emailValidationCheck:signupEmail] == 0 )
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Invalid Email Id", @"Invalid Email Id")];
    }
    else if ((unsigned long)signupPhoneno.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Phone Number", @"Enter Phone Number")];
    }
    else if ((unsigned long)signupPassword.length == 0)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Password", @"Enter Password")];
    }
    else if ([self checkPasswordStrength:signupPassword] == 0)
    {
    }
    else if (_referalCodeTextField.text.length && !isReferralCodeValid)
    {
        [self validateRefferalCode];
    }
    else if (!isTnCButtonSelected)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please select our Terms and Condition", @"Please select our Terms and Condition")];
    }
    else
    {
        if ((unsigned long)lastNameTextField.text.length == 0)
        {
            lastNameTextField.text = @"";
        }
        NSString *mobNo = [_phoneNoCountryCode.titleLabel.text stringByAppendingString:phoneNoTextField.text];
        saveSignUpDetails = [[NSArray alloc]initWithObjects:firstNameTextField.text,lastNameTextField.text,emailTextField.text,mobNo,passwordTextField.text, _referalCodeTextField.text, nil];
        [self getVerificationCode:mobNo];
    }
}


-(void)sendServiceToSignedUpUser
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator showPIOnView:self.view withMessage:NSLocalizedString(@"Registering...", @"Registering...")];
    
    NSString *pToken = @"";
    if([[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken])
    {
        pToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAgetPushToken];
    }
    else
    {
        pToken = @"garbagevalue";
    }
    NSString *country = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCountry])
        country = @"";
    else
        country = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCountry];
    
    
    NSString *city = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity])
        city = @"";
    else
        city = [[NSUserDefaults standardUserDefaults] objectForKey:KNUserCurrentCity];
    
    
    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    
    NSString *deviceId;
    if (IS_SIMULATOR)
    {
        deviceId = kPMDTestDeviceidKey;
    }
    else
    {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{KDASignUpFirstName:[saveSignUpDetails objectAtIndex:0],
                             KDASignUpLastName:[saveSignUpDetails objectAtIndex:1],
                             KDASignUpEmail:[saveSignUpDetails objectAtIndex:2],
                             KDASignUpMobile:[saveSignUpDetails objectAtIndex:3],
                             KDASignUpPassword:[saveSignUpDetails objectAtIndex:4],
                             KDASignUpReferralCode: [saveSignUpDetails objectAtIndex:5],
                             KDASignUpDeviceId:deviceId,
                             KDASignUpAccessToken:@"",
                             KDASignUpCity:city,
                             KDASignUpCountry:country,
                             KDASignUpPushToken:pToken,
                             KDASignUpLatitude:lat,
                             KDASignUpLongitude:lon,
                             KDASignUpTandC:@"1",
                             KDASignUpPricing:@"1",
                             KDASignUpDeviceType:@"1",                             KDASignUpLanguage:[[NSUserDefaults standardUserDefaults] objectForKey:@"selectedLanguage"],
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodPatientSignUp
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self signupResponse:response];
                                   }
                               }];
    
    [[NSUserDefaults standardUserDefaults] setObject:[saveSignUpDetails objectAtIndex:0] forKey:KDAFirstName];
    [[NSUserDefaults standardUserDefaults] setObject:[saveSignUpDetails objectAtIndex:1] forKey:KDALastName];
    [[NSUserDefaults standardUserDefaults] setObject:[saveSignUpDetails objectAtIndex:2] forKey:KDAEmail];
    [[NSUserDefaults standardUserDefaults] setObject:[saveSignUpDetails objectAtIndex:3] forKey:KDAPhoneNo];
    [[NSUserDefaults standardUserDefaults] setObject:[saveSignUpDetails objectAtIndex:4] forKey:KDAPassword];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}


-(void)signupResponse:(NSDictionary*)response
{
    if(response == nil)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        return;
    }    
    if (response[@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:response[@"Error"]];
    }
    else
    {
        
        //handle response here
        NSDictionary *itemList = [[NSDictionary alloc]init];
        itemList = [response mutableCopy];
        
        
        if ([[itemList objectForKey:@"errFlag"]integerValue] == 0 || ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115))
        {
            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            [ud setObject:flStrForStr(itemList[@"token"]) forKey:KDAcheckUserSessionToken];
            [ud setObject:flStrForStr(itemList[@"apiKey"]) forKey:kNSUMongoDataBaseAPIKey];
            
            [ud setObject:flStrForStr(itemList[@"pub"]) forKey:KNSUPubnubPublishKey];
            [ud setObject:flStrForStr(itemList[@"sub"]) forKey:kNSUPubnubSubscribeKey];
            
            [ud setObject:flStrForStr(itemList[@"chn"]) forKey:kNSUUserPubnubChannel];
            [ud setObject:flStrForStr(itemList[@"serverChn"]) forKey:kNSUServerPubnubChannel];
            [ud setObject:flStrForStr(itemList[@"presenseChn"]) forKey:kNSUPresensePubnubChannel];
            
            [ud setObject:flStrForStr(itemList[@"ClientmapKey"]) forKey:kNSUPlaceAPIKey];
            [ud setObject:flStrForStr(itemList[@"stipeKey"]) forKey:kNSUStripeKey];
            
            [ud setObject:flStrForStr(itemList[@"email"]) forKey:kNSUUserEmailID];
            [ud setObject:flStrForStr(itemList[@"coupon"]) forKey:kNSUUserReferralCode];
            [ud setObject:flStrForStr(itemList[@"shareMessage"]) forKey:kNSUUserShareMessage];

            [ud synchronize];
            
            NSMutableArray *carTypes = [[NSMutableArray alloc]initWithArray:itemList[@"types"]];
            if (!carTypes || !carTypes.count) {
            }
            else
            {
                [[NSUserDefaults standardUserDefaults] setObject:carTypes forKey:KUBERCarArrayKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            
            if(![ud objectForKey:KDAcheckUserSessionToken])
            {
                return;
            }
            
            if(_pickedImage)
            {
                UploadFile * upload = [[UploadFile alloc]init];
                upload.delegate = self;
                [upload uploadImageFile:_pickedImage];
                
            }
            else
            {
                ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
                [progressIndicator hideProgressIndicator];
                [self performSegueWithIdentifier:@"CardController" sender:self];
            }
            
            if ([[itemList objectForKey:@"errFlag"]integerValue] == 1 && [[itemList objectForKey:@"errNum"]integerValue] == 115) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message",@"Message") message:[itemList objectForKey:@"errMsg"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                [alertView show];
            }
        }
        else if ([itemList[@"errFlag"] intValue] == 1) { //error
            
            ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
            [progressIndicator hideProgressIndicator];
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:itemList[@"errMsg"]];
        }
    }
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFile *)uploadfile didUploadSuccessfullyWithUrl:(NSArray *)imageUrls
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    [self performSegueWithIdentifier:@"CardController" sender:self];
    
}

#pragma UploadFileDelegate

-(void)uploadFile:(UploadFile *)uploadfile didFailedWithError:(NSError *)error
{
    ProgressIndicator *progressIndicator = [ProgressIndicator sharedInstance];
    [progressIndicator hideProgressIndicator];
    
    [self performSegueWithIdentifier:@"CardController" sender:self];
    
    
    [Helper showAlertWithTitle:NSLocalizedString(@"Oops!", @"Oops!") Message:NSLocalizedString(@"Your profile photo has not been updated try again.", @"Your profile photo has not been updated try again.")];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) validateEmailAndPostalCode
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Validating Email...", @"Validating Email...")];

    NSDictionary *params = @{
                             @"ent_email":emailTextField.text,
                             @"zip_code":@"560024",
                             @"ent_user_type":@"2",
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"validateEmailZip"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self validateEmailResponse:response];
                                   }
                               }];
}

-(void)validateEmailResponse :(NSDictionary *)response
{
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
    
        if (!response)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [alertView show];
            
        }
        else if ([response objectForKey:@"Error"])
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];

        }
        else
        {
            NSDictionary *dictResponse=[response mutableCopy];
            if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
            {
                isEmailValid = YES;
               
            }
            else
            {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
                self.emailTextField.text = @"";
                [self.emailTextField becomeFirstResponder];
            }
        }
}


- (void) validateRefferalCode
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Validating Referral Code...", @"Validating Referral Code...")];

    NSString *lat = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat])
        lat = @"";
    else
        lat = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLat];
    
    NSString *lon = @"";
    if(![[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong])
        lon = @"";
    else
        lon = [[NSUserDefaults standardUserDefaults] objectForKey:KNUCurrentLong];
    
    NSDictionary *params = @{
                             @"ent_coupon":_referalCodeTextField.text,
                             @"ent_lat":lat,
                             @"ent_long":lon,
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"verifyCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self validateRefferalCodeResponse:response];
                                   }
                               }];
    
    
}

-(void)validateRefferalCodeResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        _referalCodeTextField.text = @"";
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [self NextButtonClicked];
            isReferralCodeValid = YES;
        }
        else
        {
            _referalCodeTextField.text = @"";
            isReferralCodeValid = NO;
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[dictResponse objectForKey:@"errMsg"]];
        }
    }
}

-(void)getVerificationCode:(NSString *)phoneNumber {
    
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Please wait...", @"Please wait...")];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    
    NSDictionary *params = @{
                             @"ent_dev_id":deviceID,
                             @"ent_mobile":phoneNumber,
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"getVerificationCode"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self validateVerificationCodeResponse:response];
                                   }
                               }];
}

-(void)validateVerificationCodeResponse :(NSDictionary *)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    
    if (!response)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
    }
    else
    {
        NSDictionary *dictResponse=[response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
            [self performSegueWithIdentifier:@"verifyiMobileVC" sender:self];
        }
        else if ([dictResponse[@"errFlag"] intValue] == 1 && [dictResponse[@"errNum"] intValue] == 113)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
            [phoneNoTextField becomeFirstResponder];
        }
        else if ([dictResponse[@"errFlag"] intValue] == 1 && [dictResponse[@"errNum"] intValue] == 108)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response [@"test"][@"response"][@"error"]];
            [phoneNoTextField becomeFirstResponder];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
        }
    }
}



- (void) validatemobilePhoneNumber
{
    [[ProgressIndicator sharedInstance] showPIOnWindow:[UIApplication sharedApplication].keyWindow withMessge:NSLocalizedString(@"Validating Mobile Number...", @"Validating Mobile Number...")];
    
    NSString *mobNo = [_phoneNoCountryCode.titleLabel.text stringByAppendingString:phoneNoTextField.text];
    
    NSDictionary *params = @{
                             @"ent_mobile":mobNo,
                             @"ent_user_type":@"2",
                             KDASignUpDateTime:[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"checkMobile"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   [[ProgressIndicator sharedInstance]hideProgressIndicator];
                                   
                                   if (success) { //handle success response
                                       
                                       if (!response)
                                       {
                                           UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
                                           [alertView show];
                                           
                                       }
                                       else if ([response objectForKey:@"Error"])
                                       {
                                           phoneNoTextField.text = @"";
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"Error"]];
                                           
                                       }
                                       else
                                       {
                                           NSDictionary *dictResponse=[response mutableCopy];
                                           if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
                                           {
                                               
                                              	
                                           }
                                           else
                                           {
                                               phoneNoTextField.text = @"";
                                               [phoneNoTextField becomeFirstResponder];
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey:@"errMsg"]];
                                               
                                           }
                                       }
                                       
                                       
                                   }
                               }];
}

#pragma mark - TextFields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (![textField isEqual:emailTextField] && [emailTextField.text length] > 0 && ![Helper emailValidationCheck:emailTextField.text])
    {
        [emailTextField becomeFirstResponder];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter a valid email", @"Please enter a valid email")];
    }
    else if (![textField isEqual:passwordTextField] && [passwordTextField.text length] > 0 && [self checkPasswordStrength:passwordTextField.text] == 0)
    {
       // [passwordTextField becomeFirstResponder];
    }
    else
    {
        if ([textField isEqual:_referalCodeTextField])
        {
            isReferralCodeValid = NO;
        }
        [textField becomeFirstResponder];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField isEqual:emailTextField]) {
        if ([textField.text length] > 0) {
            if ([Helper emailValidationCheck:emailTextField.text])
            {
                PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
                if ([reachability isNetworkAvailable])
                {
                    [self validateEmailAndPostalCode];
                }
                else
                {
                    [[ProgressIndicator sharedInstance] showMessage:@"No Network" On:self.view];
                }
            }
        }
    }
    self.activeTextField = nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if ([textField isEqual:self.firstNameTextField])
    {
        [textField resignFirstResponder];
        [self.lastNameTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.lastNameTextField])
    {
        [textField resignFirstResponder];
        [self.emailTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.emailTextField])
    {
        [textField resignFirstResponder];
        [self.phoneNoTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.phoneNoTextField])
    {
        [textField resignFirstResponder];
        [self.referalCodeTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.referalCodeTextField])
    {
        [textField resignFirstResponder];
        [self.passwordTextField becomeFirstResponder];
    }
    else if ([textField isEqual:self.passwordTextField])
    {
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)NextButtonClicked
{
    [self dismissKeyboard];
    [self signUp];
}

- (IBAction)CountryCode:(id)sender
{
    CountryNameTableViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"countryPicker"];
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    pickController.oncomplete = ^(NSString * code, UIImage *flagimg, NSString *countryName)
    {
        self.phoneNoCountryFlag.image = flagimg;
        NSString *countryCode = [NSString stringWithFormat:@"+%@", code];
        [Helper setButton:self.phoneNoCountryCode Text:countryCode WithFont:Roboto_Light FSize:12 TitleColor:[UIColor blackColor] ShadowColor:nil];
    };
    [self presentViewController:navBar animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"CardController"])
    {
        CardLoginViewController *CLVC = (CardLoginViewController*)[segue destinationViewController];
        CLVC.isComingFromPayment = 3;
    }
    else if ([[segue identifier] isEqualToString:@"verifyiMobileVC"])
    {
        VerifyiMobileViewController *verifyVC = (VerifyiMobileViewController*)[segue destinationViewController];
        
        verifyVC.myNumber = [saveSignUpDetails objectAtIndex:3];
        verifyVC.getSignupDetails = [saveSignUpDetails mutableCopy];
        verifyVC.pickedImage = _pickedImage;
    }
    else if ([[segue identifier] isEqualToString:@"gotoTerms"])
    {
    }
    else if ([[segue identifier] isEqualToString:@"CountryPickerVCSegue"])
    {
    }
}

- (void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)TermsNconButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"gotoTerms" sender:self];
}

- (IBAction)checkButtonClicked:(id)sender
{
    UIButton *mBut = (UIButton *)sender;    
    mBut.userInteractionEnabled = YES;
    
    if(mBut.isSelected)
    {
        isTnCButtonSelected = NO;
        mBut.selected=NO;
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_btn_checkbox_off.png"] forState:UIControlStateNormal];

    }
    else
    {
        isTnCButtonSelected = YES;
        mBut.selected=YES;
        [tncCheckButton setImage:[UIImage imageNamed:@"signup_btn_checkbox_on.png"] forState:UIControlStateSelected];
        
    }
}

- (IBAction)profileButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    UIActionSheet *actionSheet;
    if (_pickedImage != nil) {
        
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture")
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),
                                                           NSLocalizedString(@"Choose From Library", @"Choose From Library"),
                                                           NSLocalizedString(@"Remove Profile Photo", @"Remove Profile Photo"),nil];
        actionSheet.tag = 200;
    }
    else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Picture", @"Select Picture")
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"Take Photo", @"Take Photo"),
                                                           NSLocalizedString(@"Choose From Library", @"Choose From Library"),nil];
        
        actionSheet.tag = 100;
    }
    [actionSheet showInView:self.view];
}


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100)
    {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked];
                break;
            }
            default:
                break;
        }
    }
    else  if (actionSheet.tag == 200) {
        switch (buttonIndex)
        {
            case 0:
            {
                [self cameraButtonClicked];
                break;
            }
            case 1:
            {
                [self libraryButtonClicked];
                break;
            }
            case 2:
            {
                [self deSelectProfileImage];
                break;
            }

            default:
                break;
        }
    }
}

-(void)deSelectProfileImage {
    
    profileImageView.image =  [UIImage imageNamed:@"signup_profile_image.png"];;
    _pickedImage = nil;

}

-(void)cameraButtonClicked
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate =self;
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Message", @"Message") message: NSLocalizedString(@"Camera is not available", @"Camera is not available") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil, nil];
        [alert show];
        
    }
}
-(void)libraryButtonClicked
{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate =self;
    picker.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
    picker.allowsEditing = YES;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
    }
    else
    {
        [self presentViewController:picker animated:YES completion:nil];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:YES completion:nil];
    _pickedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    profileImageView.image = _pickedImage;
    _pickedImage = [self imageWithImage:_pickedImage scaledToSize:CGSizeMake(100,100)];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {

    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Password Checking

-(int)checkPasswordStrength:(NSString *)password
{
    unsigned long int  len = password.length;
    //will contains password strength
    int strength = 0;
    
    if (len == 0) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter password first", @"Please enter password first")];
        
        return 0;//PasswordStrengthTypeWeak;
    } else if (len <= 5) {
        strength++;
    } else if (len <= 10) {
        strength += 2;
    } else{
        strength += 3;
    }
    int kp = strength;
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_UPPERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Password must contain one uppercase letter", @"Password must contain one uppercase letter")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_LOWERCASE caseSensitive:YES];
    if (kp >= strength)
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Password must contain one lowercase letter", @"Password must contain one lowercase letter")];
        return 0;
    }
    else
    {
        kp++;
    }
    strength += [self validateString:password withPattern:REGEX_PASSWORD_ONE_NUMBER caseSensitive:YES];
    if (kp >= strength) {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Please enter at least one number in your password", @"Please enter at least one number in your password")];
        // [passwordTextField becomeFirstResponder];
        return 0;
    }
    return 1;
}

// Validate the input string with the given pattern and
// return the result as a boolean
- (int)validateString:(NSString *)string withPattern:(NSString *)pattern caseSensitive:(BOOL)caseSensitive
{
    NSError *error = nil;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:((caseSensitive) ? 0 : NSRegularExpressionCaseInsensitive) error:&error];
    
    NSAssert(regex, @"Unable to create regular expression");
    
    NSRange textRange = NSMakeRange(0, string.length);
    NSRange matchRange = [regex rangeOfFirstMatchInString:string options:NSMatchingReportProgress range:textRange];
    
    BOOL didValidate = 0;
    
    // Did we find a matching range
    if (matchRange.location != NSNotFound)
        didValidate = 1;
   
    return didValidate;
}


/*--------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*--------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification
{
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    //Given size may not account for screen rotation
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    //float width  = MAX(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}
/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self moveViewDown];
}
/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height
{
    float textfieldMaxY = CGRectGetMinY([[[textfield superview] superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([[textfield superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([textfield superview].frame);
    
    textfieldMaxY = textfieldMaxY + CGRectGetMaxY(textfield.frame);
    
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height) + initialOffset_Y;
    if (remainder >= 0) {
        return;
    }
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y - remainder);
                     }];
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown
{
    mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y);
}


@end


