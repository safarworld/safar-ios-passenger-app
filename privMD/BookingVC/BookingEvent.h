//
//  BookingEvent.h
//  UBER
//
//  Created by Rahul Sharma on 05/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BookingEvent : NSObject

@property (nonatomic,strong) NSDate *date;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *image;
@property (nonatomic,strong) NSString *pickAdd;
@property (nonatomic,strong) NSString *desAdd;
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *distance;
@property (nonatomic,strong) NSString *amount;
@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSDictionary *info;
@property (nonatomic,strong) NSString *bid;
-(instancetype)initWithDictionary:(NSDictionary *)eventsDict;

@end
