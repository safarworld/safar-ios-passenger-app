//
//  DestinationAddress+CoreDataProperties.h
//  RoadyoDispatch
//
//  Created by Rahul Sharma on 20/10/15.
//  Copyright © 2015 Rahul Sharma. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DestinationAddress.h"

NS_ASSUME_NONNULL_BEGIN

@interface DestinationAddress (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *desAddress;
@property (nullable, nonatomic, retain) NSString *desAddress2;
@property (nullable, nonatomic, retain) NSNumber *desLatitude;
@property (nullable, nonatomic, retain) NSNumber *desLongitude;
@property (nullable, nonatomic, retain) NSString *keyId;
@property (nullable, nonatomic, retain) NSString *zipCode;

@end

NS_ASSUME_NONNULL_END
