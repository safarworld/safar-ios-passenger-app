//
//  HelpViewController.m
//  privMD
//
//  Created by Rahul Sharma on 11/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "HelpViewController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import <Canvas/CSAnimationView.h>
#import "UILabel+DynamicHeight.h"
#import <AVFoundation/AVFoundation.h>
#import "LocationServicesMessageVC.h"
#import "LanguageManager.h"
#import "Locale.h"

static NSBundle *bundle = nil;
@interface HelpViewController ()

@end

@implementation HelpViewController
@synthesize topView;
@synthesize signInButton;
@synthesize registerButton;
@synthesize languageView;
@synthesize langaugeChangeBtn;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

-(void)locationServicesChanged:(NSNotification*)notification {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)//kCLAuthorizationStatusAuthorized
    {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController
{
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *imageview = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    
    if ([UIScreen mainScreen].bounds.size.height < 568)
    {
        imageview.image = [UIImage imageNamed:@"Default"];
    } else if ([UIScreen mainScreen].bounds.size.height > 568)
    {
        imageview.image = [UIImage imageNamed:@"Default-768h"];
    } else {
        imageview.image = [UIImage imageNamed:@"Default-568h"];
    }
    [self.view addSubview:imageview];

    [self.view bringSubviewToFront:topView];
    
    if (kLanguageSupport)
    {
        self.languageView.hidden = NO;
    }
    else
    {
        self.languageView.hidden = YES;
    }
    LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
    Locale *localeObj;
    NSString *str;
    if ([[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:kLangaugeCodeOtherThanEnglish])
    {
        localeObj = languageManager.availableLocales[1];
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"selectedLanguage"];
        str = @"ENGLISH";
        [langaugeChangeBtn setSelected:YES];
    }
    else
    {
        localeObj = languageManager.availableLocales[0];
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"selectedLanguage"];
        str = @"عربي";
        [langaugeChangeBtn setSelected:NO];
    }
    [languageManager setLanguageWithLocale:localeObj];
    [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Roboto_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
   
    [Helper setButton:signInButton Text:NSLocalizedString(@"LOGIN", @"LOGIN") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x0d143b) ShadowColor:nil];
    signInButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [signInButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateHighlighted];
    signInButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [signInButton setBackgroundImage:[UIImage imageNamed:@"loginSignUp_on"] forState:UIControlStateHighlighted];
    
    
    [Helper setButton:registerButton Text:NSLocalizedString(@"REGISTER", @"REGISTER") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [registerButton setTitleColor:UIColorFromRGB(0x0d143b) forState:UIControlStateHighlighted];
    registerButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    registerButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [registerButton setBackgroundImage:[UIImage imageNamed:@"loginSignUp_off"] forState:UIControlStateHighlighted];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
    
    
    [self.view bringSubviewToFront:topView];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    topView.frame = CGRectMake(0,screenHeight, screenWidth,65);
    [self animateIn];
}

- (void)animateIn {
    CGRect frameTopview = topView.frame;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    frameTopview.origin.y = screenHeight - 65;;
    [UIView animateWithDuration:0.6f animations:^{
        topView.frame = frameTopview;
    }];
}


- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)signInButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"SignIn" sender:self];
}

- (IBAction)registerButtonClicked:(id)sender
{
    [self performSegueWithIdentifier:@"SignUp" sender:self];
}

- (IBAction)langugaeChange:(id)sender
{
    if (self.langaugeChangeBtn.isSelected)
    {
        [self.langaugeChangeBtn setSelected:NO];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-568h1"]];
        NSString *str = @"عربي";
        [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Roboto_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"1" forKey:@"selectedLanguage"];
        LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
        Locale *localeObj = languageManager.availableLocales[0];
        [languageManager setLanguageWithLocale:localeObj];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [self updateUI];
    }
    else
    {
        [self.langaugeChangeBtn setSelected:YES];
        self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Default-568h2"]];
        NSString *str = @"ENGLISH";
        [Helper setButton:self.langaugeChangeBtn Text:str WithFont:Roboto_Regular FSize:14 TitleColor:[UIColor whiteColor] ShadowColor:nil];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"2" forKey:@"selectedLanguage"];
        LanguageManager *languageManager = [LanguageManager sharedLanguageManager];
        Locale *localeObj = languageManager.availableLocales[1];
        [languageManager setLanguageWithLocale:localeObj];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
        [self updateUI];
    }
}

-(void) updateUI
{
    [Helper setButton:signInButton Text:NSLocalizedString(@"SIGN IN", @"SIGN IN") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [Helper setButton:registerButton Text:NSLocalizedString(@"REGISTER", @"REGISTER") WithFont:Roboto_Regular FSize:15 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADXDK" object:nil userInfo:nil];
}

@end
