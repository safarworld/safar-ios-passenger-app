//
//  TermsnConditionViewController.m
//  privMD
//
//  Created by Rahul Sharma on 27/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "TermsnConditionViewController.h"
#import "WebViewController.h"
@interface TermsnConditionViewController ()

@end

@implementation TermsnConditionViewController
@synthesize link1Button;
@synthesize link2Button;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self createNavLeftButton];
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    self.navigationItem.title = NSLocalizedString(@"TERMS & CONDITIONS", @"TERMS & CONDITIONS");
    
    link1Button.tag = 100;
    link2Button.tag = 200;
    
    link1Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    link1Button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [link1Button setTitle:NSLocalizedString(@"Terms & Condition", @"Terms & Condition") forState:UIControlStateNormal];
    [link1Button setTitle:NSLocalizedString(@"Terms & Condition", @"Terms & Condition") forState:UIControlStateSelected];
    [link1Button setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [link1Button setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];
    link1Button.titleLabel.font = [UIFont fontWithName:Roboto_Regular size:15];
    
    link2Button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    link2Button.contentEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [link2Button setTitle:NSLocalizedString(@"Privacy Policy", @"Privacy Policy") forState:UIControlStateNormal];
    [link2Button setTitle:NSLocalizedString(@"Privacy Policy", @"Privacy Policy") forState:UIControlStateSelected];
    [link2Button setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [link2Button setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];
    link2Button.titleLabel.font = [UIFont fontWithName:Roboto_Regular size:15];
    [link1Button addTarget:self action:@selector(linkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [link2Button addTarget:self action:@selector(linkButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationItem setHidesBackButton:YES animated:YES];
    self.navigationController.navigationBarHidden = NO;
}

-(void)linkButtonClicked :(id)sender
{
    [self performSegueWithIdentifier:@"gotoWebView" sender:sender];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"gotoWebView"])
    {
        UIButton *mBtn = (UIButton *)sender;
       
        if (mBtn.tag == 100) {
            
            WebViewController *webView = (WebViewController*)[segue destinationViewController];
            webView.title = NSLocalizedString(@"Terms & Condition", @"Terms & Condition");
            webView.weburl = termsAndCondition;
        }
        else if (mBtn.tag == 200) {
            
            WebViewController *webView = (WebViewController*)[segue destinationViewController];
            webView.title = NSLocalizedString(@"Privacy Policy", @"Privacy Policy");
            webView.weburl = privacyPolicy;
        }
    }
}

-(void) createNavLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];


    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
   // UIBarButtonItem *homeButton = [[UIBarButtonItem alloc] initWithCustomView:segmentView];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
    
}

-(void)cancelButtonClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
