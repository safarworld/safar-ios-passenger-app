
//  MapViewController.m
//  DoctorMapModule
//
//  Created by Rahul Sharma on 03/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "MapViewController.h"
#import "PickUpViewController.h"
#import "XDKAirMenuController.h"
#import "UIImageView+WebCache.h"
#import "SDWebImageDownloader.h"
#import <QuartzCore/QuartzCore.h>
#import <QuartzCore/CALayer.h>
#import "PubNubWrapper.h"
#import "fareCalculatorViewController.h"
#import "AppointedDoctor.h"
#import "PaymentViewController.h"
#import "CustomNavigationBar.h"
#import "NetworkHandler.h"
#import "UploadProgress.h"
#import <AFNetworking.h>
#import "LocationServicesMessageVC.h"
#import "Entity.h"
#import "Database.h"
#import "DirectionService.h"
#import <AXRatingView.h>
#import "UIImageView+WebCache.h"
#import <FacebookSDK/FacebookSDK.h>
#import "PatientAppDelegate.h"
#import "RoundedImageView.h"
#import "STKSpinnerView.h"
#import "MyAppTimerClass.h"
#import "PromoCodeViewController.h"
#import "PatientViewController.h"
#import "User.h"
#import "surgePricePopUp.h"

//Define Constants tag for my view
#define myProgressTag  5001
#define mapZoomLevel 16
#define pickupAddressTag 2500
#define addProgressViewTag 6000
#define carType1TAG 7000
#define carType2TAG 7001
#define carType3TAG 7002
#define carType4TAG 7003
#define msgLabelTag 7004

#define myCustomMarkerTag 5000
#define curLocImgTag 5001
#define driverArrivedViewTag 3000
#define driverMessageViewTag 3001
#define _height  110
#define _heightCenter  160
#define _width   274
#define _widthLabel   216

#define bottomViewWithCarTag 106
#define topViewTag 90

#define rightArrowTag 7
#define leftArrowTag 8
//Localization strings
#define timeLabelForLocalization NSLocalizedString(@"Time :", @"Time :")
#define distanceLabelForLocalization NSLocalizedString(@"Distance :", @"Distance :")


static MapViewController *sharedInstance = nil;

enum doctorType {
    Now = 1,
    Later = 2
};

typedef enum
{
    isChanging = 0,
    isFixed = 1
}locationchange;

@interface CoordsList : NSObject
@property(nonatomic, readonly, copy) GMSPath *path;
@property(nonatomic, readonly) NSUInteger target;


- (id)initWithPath:(GMSPath *)path;

- (CLLocationCoordinate2D)next;

@end

@implementation CoordsList

- (id)initWithPath:(GMSPath *)path {
    if ((self = [super init])) {
        _path = [path copy];
        _target = 0;
    }
    return self;
}

- (CLLocationCoordinate2D)next {
    ++_target;
    if (_target == [_path count]) {
        _target = 0;
    }
    return [_path coordinateAtIndex:_target];
}

@end


@interface MapViewController ()<PubNubWrapperDelegate,CLLocationManagerDelegate,CustomNavigationBarDelegate, UserDelegate>
{
    GMSMapView *mapView_;
    BOOL firstLocationUpdate_;
    GMSGeocoder *geocoder_;
    PubNubWrapper *pubNub;
    float desLat;
    float desLong;
    NSString *desAddr;
    NSString *desAddrline2;
    float srcLat;
    float srcLong;
    NSString *srcAddr;
    NSString *srcAddrline2;
    NSInteger carTypesForLiveBooking;
    NSInteger carTypesForLiveBookingServer;
    NSInteger paymentTypesForLiveBooking;
    //Path Plotting Variables
    NSMutableArray *waypoints_;
    NSMutableArray *waypointStrings_;
    float newLat;
    float newLong;
    float actual;
    float actualGMview;
    int docCount;
    NSString *distanceOfClosetCar;
    NSInteger carTypesArrayCountValue;
    NSString *shareUrlViaSMS;
    NSString *promocode;
    
    //Dynaic vehicle type
    NSMutableArray *arrayOfMapIcons;
    NSMutableArray *arrayOfMapImages;
    NSMutableArray *allHorizontalData;
    UIScrollView *scroller;
    NSMutableArray *arrayOfButtons;
    NSArray *pubnubDriverEmailResponse;
    NSMutableArray *arrayOfImages;
    NSMutableArray *arrayOfVehicleIcons;
    UIButton *leftArrow;
    UIButton *rightArrow;
    BOOL laterBooking;
    NSString *laterBookingDate;
    PatientAppDelegate *appdelegate;
    NSArray *notRatedBookings;
    NSInteger ratingCounts;
    UIWindow *window;
    UITextView *activeTextView;
    NSString *ratingEmail;
    NSString *ratingAppointmentDate;
    BOOL isRatingShowing;
    double surgePrice;
    NSString *surgePriceTime;
    UIBackgroundTaskIdentifier bgTask;
    
    BOOL checkFareQuote;
    CGPoint oldOffset;
    
    int imageDownloadMethodCallingCount;
}

@property (strong, nonatomic) UIImageView *disputeMsgView;
@property(nonatomic,strong) NSString *laterSelectedDate;
@property(nonatomic,strong) NSString *laterSelectedDateServer;
@property(nonatomic,strong) CustomNavigationBar *customNavigationBarView;
@property(nonatomic,strong) NSString *userPubNubChannel;
@property(nonatomic,strong) NSString *serverPubNubChannel;
@property(nonatomic,strong) NSString *patientEmail;
@property(nonatomic,strong) NSMutableDictionary *allMarkers;
@property(nonatomic,strong) WildcardGestureRecognizer * tapInterceptor;
@property(nonatomic,strong) UIActionSheet *actionSheet;
@property(nonatomic,assign) float currentLatitude;
@property(nonatomic,assign) float currentLongitude;
@property(nonatomic,assign) float currentLatitudeFare;
@property(nonatomic,assign) float currentLongitudeFare;
@property(nonatomic,strong) CLLocationManager *locationManager;
@property(nonatomic,assign) BOOL isUpdatedLocation;
@property(nonatomic,strong) UIView *PikUpViewForConfirmScreen;
@property(nonatomic,strong) UIView *DropOffViewForConfirmScreen;
//Progress Indicator
@property(nonatomic,strong) UIProgressView * threadProgressView;
@property(nonatomic,strong) UIView *myProgressView;
@property(nonatomic,strong) UIView *addProgressBarView;
@property(nonatomic,strong) NSString *cardId;
@property(nonatomic,strong) NSString *subscribedChannel;
@property(nonatomic,assign)BOOL isDriverArrived;
@property(nonatomic,assign)BOOL isDriverOnTheWay;
//Path Plotting Variables
@property(nonatomic,assign)BOOL isPathPlotted;
@property(nonatomic,assign)CLLocationCoordinate2D previouCoord;
@property(nonatomic,strong)GMSMarker *destinationMarker;
@property(nonatomic,strong)NSString *startLocation;
@property(nonatomic,weak)NSTimer *pubnubStreamTimer;
@property(nonatomic,weak) NSTimer *pollingTimer;
@property(nonatomic,assign) double driverCurLat;
@property(nonatomic,assign) double driverCurLong;
@property(nonatomic,strong) NSDictionary *disnEta;
@property(nonatomic,strong) NSMutableArray *arrayOfDriverEmail;
@property(nonatomic,strong) NSMutableArray *arrayOfCarTypes;
@property(nonatomic,strong) NSMutableArray *arrayOfCarColor;
@property(nonatomic,strong) NSMutableArray *arrayOfMastersAround;
@property(nonatomic,strong) NSMutableArray *arrayOfpickerData;
@property(nonatomic,strong) UIView *spinnerView;

//suri
@property(nonatomic,assign)BOOL isBookingStatusCheckedOnce;
@property(nonatomic,assign)BOOL isAddressManuallyPicked;
@end

static int isLocationChanged;

@implementation MapViewController
@synthesize userPubNubChannel, serverPubNubChannel;
@synthesize drivers;
@synthesize dictSelectedDoctor;
@synthesize threadProgressView;
@synthesize PikUpViewForConfirmScreen;
@synthesize DropOffViewForConfirmScreen;
@synthesize addProgressBarView;
@synthesize customNavigationBarView;
@synthesize cardId;
@synthesize laterSelectedDate,disputeMsgView;
@synthesize disnEta,pollingTimer,pubnubStreamTimer,subscribedChannel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        // Custom initialization
    }
    return self;
}

+ (instancetype) getSharedInstance
{
    if (!sharedInstance)
    {
        sharedInstance  = [[self alloc] init];
    }
    return sharedInstance;
}

#pragma mark-
#pragma WebServices
/**
 *  Update Drop off Address
 */
-(void)sendServiceToUpdateDropAddress {
    
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    
    NSString *sessionToken = [udPlotting objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_booking_id":flStrForObj([udPlotting objectForKey:@"BOOKINGID"]),
                             @"ent_drop_addr1":desAddr,
                             @"ent_drop_addr2":desAddrline2,
                             @"ent_lat":[NSNumber numberWithFloat:desLat],
                             @"ent_long":[NSNumber numberWithFloat:desLong],
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateDropOff"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self getupdateAddressResponse:response];
                                   }
                               }];
}

/**
 *  Booking for later
 *
 *  @param responseDictionary return type void and takes no argument
 */

-(void)getupdateAddressResponse:(NSDictionary *)responseDictionary{
    
    NSString *titleMsg = NSLocalizedString(@"Message", @"Message");
    NSString *errMsg = @"";
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error" message:[responseDictionary objectForKey:@"Message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        //[Helper showAlertWithTitle:@"Error" Message:[responseDictionary objectForKey:@"Error"]];
        errMsg = [responseDictionary objectForKey:@"errMsg"];
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
            UIButton *dropOffAddress = (UIButton *) [[self.view viewWithTag:driverArrivedViewTag] viewWithTag:175];
            [Helper setButton:dropOffAddress Text:[NSString stringWithFormat:@"%@ %@", desAddr, desAddrline2] WithFont:Roboto_Regular FSize:13 TitleColor:[UIColor blackColor] ShadowColor:nil];
            
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            [ud setDouble:desLat   forKey:@"desLat"];
            [ud setDouble:desLong  forKey:@"desLong"];
            [ud setObject:desAddr  forKey:@"desadd1"];
            [ud setObject:desAddrline2  forKey:@"desadd2"];
            [ud synchronize];
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
    }
    [Helper showAlertWithTitle:titleMsg Message:errMsg];
}

- (void) sendRequestgetETAnDistance
{
    NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    
    double lDesLat  = _driverCurLat;
    double lDesLong = _driverCurLong;
    
    if (bookingStatus == kNotificationTypeBookingOnMyWay)
    {
        _currentLatitude = [udPlotting doubleForKey:@"srcLat"];
        _currentLongitude = [udPlotting doubleForKey:@"srcLong"];
        if (lDesLat == 0 || lDesLong == 0)
        {
            lDesLat = [udPlotting doubleForKey:@"driLat"];
            lDesLong = [udPlotting doubleForKey:@"driLong"];
        }
    }
    
    if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted)
    {
        lDesLat = [udPlotting doubleForKey:@"desLat"];
        lDesLong = [udPlotting doubleForKey:@"desLong"];
    }
    
    if (_currentLatitude == 0 || _currentLongitude == 0 || lDesLat == 0 || lDesLong == 0) {
        
        [self updateDistance:@"..." estimateTime:@"..."];
        return;
    }
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f",lDesLat,lDesLong,_currentLatitude,_currentLongitude];
    NSURL *url = [NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strUrl]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(addressResponse:)];
}

#pragma mark Webservice Handler(Response) -

- (void)addressResponse:(NSDictionary *)response
{
    if (!response)
    {
        
    }else if ([response objectForKey:@"Error"])
    {
        
    }
    else
    {
        BOOL checkStatus = [self containsKey:@"status" dictionary:response[@"ItemsList"]];
        if (!checkStatus)
        {
            NSString *str = @"Limit Exceed";
            [self updateDistance:@"..." estimateTime:[NSString stringWithFormat:@"%@", str]];
        }
        else
        {
            NSString *distance = response[@"ItemsList"][@"rows"][0][@"elements"][0][@"distance"][@"value"];
            distance = [NSString stringWithFormat:@"%.2f %@",[distance doubleValue]/kPMDDistanceMetric, kPMDDistanceParameter];
            int timeInSec =  [response[@"ItemsList"][@"rows"][0][@"elements"][0][@"duration"][@"value"] intValue];
            int timeInMin = [self convertTimeInMin:timeInSec];
            NSString *str = NSLocalizedString(@"Min", @"Min");
            [self updateDistance:distance estimateTime:[NSString stringWithFormat:@"%d %@", timeInMin, str]];
        }
    }
}


/**
 *  cancel live booking
 */
-(void)sendAppointmentRequestForLiveBookingCancellation{
    
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Cancel...", @"Cancel...")];    //setup parameters
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelOngoingAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       [self getLiveCancelResponse:response];
                                   }
                               }];
}

-(void)sendAppointmentRequestForLiveBooking
{
    if(kPMDPaymentType)
    {
        if (! (paymentTypesForLiveBooking == 1 || paymentTypesForLiveBooking == 2) )
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",nil) Message:NSLocalizedString(@"Please select payment mode.", @"Please select payment mode.")];
            return;
        }
    }
    else
    {
        if (!(paymentTypesForLiveBooking == 1) && kPMDCardOrCash )
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message",nil) Message:NSLocalizedString(@"Please Add card for payment.", @"Please Add card for payment.")];
            return;
        }
    }
    if(kPMDWithDispatch)
    {
        if (isLaterSelected)
        {
            PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
            if ( [reachability isNetworkAvailable])
            {
                [self callServiceToBookDriver:@""];
            }
            else
            {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"No Network Connection", @"No Network Connection")];
            }
        }
        else if (_arrayOfDriverEmail.count)
        {
            isRequestingButtonClicked = YES;
            [self driverBooking];
        }
        else
        {
            [self bookingNotAccetedByAnyDriverScreenLayout];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"It seems like no driver around you.Please try again.", @"It seems like no driver around you.Please try again.")];
        }
    }
    else
    {
        if (_arrayOfDriverEmail.count)
        {
            isRequestingButtonClicked = YES;
            [self driverBooking];
        }
        else
        {
            [self bookingNotAccetedByAnyDriverScreenLayout];
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"It seems like no driver around you.Please try again.", @"It seems like no driver around you.Please try again.")];
        }
    }
}

-(void)driverBooking
{
    if(docCount <= _arrayOfDriverEmail.count-1 && isNowSelected)
    {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ( [reachability isNetworkAvailable])
        {
            [self callServiceToBookDriver: [_arrayOfDriverEmail objectAtIndex:docCount]];
            docCount++;
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"No Network Connection", @"No Network Connection")];
        }
    }
    else
    {
        [self bookingNotAccetedByAnyDriverScreenLayout];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Currently all drivers are busy,Please try again.", @"Currently all drivers are busy,Please try again.")];
    }
}

-(void)bookingNotAccetedByAnyDriverScreenLayout
{
    docCount = 0;
    actual = 1;
    desLat = 0;
    desLong = 0;
    isRequestingButtonClicked = NO;
    paymentTypesForLiveBooking = 0;
    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
    v.hidden = NO;
    [self removeAddProgressView];
    [self addCustomLocationView];
    [self getCurrentLocation:nil];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    [self getAddress:position];
    [self handleTap:nil];
    [self stopBackgroundTask];
}

-(void)bookingAccetedBySomeDriverScreenLayout
{
    [[MyAppTimerClass sharedInstance] stopPublishTimer];
    [_allMarkers removeAllObjects];
    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
    v.hidden = YES;
    [self removeAddProgressView];
    [mapView_ clear];
}

/**
 *  send request for live booking
 */
-(void)callServiceToBookDriver:(NSString *)driverEmail
{
    isLocationChanged = isChanging;
    [_myProgressView setHidden:NO];
    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
    v.hidden = YES;
    
    //Createview For Cancellation
    [self makeMyProgressBarMoving];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    //! is for nil [NSNull class] is for other type of like Null NULL null and length is for checking
    if (!srcAddrline2 || srcAddrline2 == (id)[NSNull null] || srcAddrline2.length == 0 )
    {
        srcAddrline2 = @"";
    }
    if (isNowSelected == YES)
    {
        _laterSelectedDateServer = @"";
    }
    
    NSString *zipcode = @"90210";
    NSString *pickupLatitude =  [NSString stringWithFormat:@"%f",srcLat];
    NSString *pickupLongitude = [NSString stringWithFormat:@"%f",srcLong];
    
    if (srcAddr == (id)[NSNull null] || srcAddr.length == 0 )
    {
        UITextField *textAddress = (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
        srcAddr = textAddress.text;
        
    }
    NSString *pickAddress = srcAddr;
    NSString *dropLatitude;
    NSString *dropLongitude;
    NSString *dropAddress;
    NSString *dropAddress2;
    if (!desAddrline2 || desAddrline2 == (id)[NSNull null] || desAddrline2.length == 0 )
    {
        desAddrline2 = @"";
    }
    if(desLat == 0 && desLong == 0)
    {
        dropLatitude = @"";
        dropLongitude =@"";
        dropAddress =  @"";
        dropAddress2 = @"";
    }
    else
    {
        dropLatitude = [NSString stringWithFormat:@"%f",desLat];
        dropLongitude = [NSString stringWithFormat:@"%f",desLong];
        dropAddress = desAddr;
        dropAddress2 = desAddrline2;
    }
    if(!cardId || [cardId isKindOfClass:[NSNull class]])
    {
        cardId = @"";
    }
    NSString *promo = @"";
    if (promocode.length > 0) {
        promo = promocode;
    }
    NSString *dateTime = [Helper getCurrentDateTime];
    @try {
        NSDictionary *params = @{@"ent_sess_token":sessionToken,
                                 @"ent_dev_id":deviceID,
                                 @"ent_wrk_type":[NSString stringWithFormat:@"%ld",(long)carTypesForLiveBookingServer],
                                 @"ent_lat":pickupLatitude,
                                 @"ent_long":pickupLongitude,
                                 @"ent_addr_line1":pickAddress,
                                 @"ent_addr_line2":srcAddrline2,
                                 @"ent_drop_lat":dropLatitude,
                                 @"ent_drop_long":dropLongitude,
                                 @"ent_drop_addr_line1":dropAddress,
                                 @"ent_drop_addr_line2":dropAddress2,
                                 @"ent_zipcode":zipcode,
                                 @"ent_extra_notes":@"",
                                 @"ent_date_time":dateTime,
                                 @"ent_later_dt":_laterSelectedDateServer,
                                 @"ent_payment_type":[NSNumber numberWithInteger:paymentTypesForLiveBooking],
                                 @"ent_card_id":cardId,
                                 @"ent_dri_email":driverEmail,
                                 @"ent_coupon":promo,
                                 @"ent_surge":[NSNumber numberWithDouble:surgePrice]
                                 };
        NSLog(@"Live Booking Params = %@", params);
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:kSMLiveBooking
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       NSLog(@"Live Booking response = %@", response);
                                       if (success)
                                       {
                                           NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
                                           [udPlotting setDouble:srcLat   forKey:@"srcLat"];
                                           [udPlotting setDouble:srcLong  forKey:@"srcLong"];
                                           [udPlotting setDouble:desLat   forKey:@"desLat"];
                                           [udPlotting setDouble:desLong  forKey:@"desLong"];
                                           [udPlotting setObject:srcAddr forKey:@"srcAdd1"];
                                           [udPlotting setObject:srcAddrline2 forKey:@"srcAdd2"];
                                           [udPlotting setObject:dropAddress  forKey:@"desadd1"];
                                           [udPlotting setObject:dropAddress2  forKey:@"desadd2"];
                                           [self getBookingAppointment:response];
                                       }
                                       else
                                       {
                                           [self bookingNotAccetedByAnyDriverScreenLayout];
                                           [self stopBackgroundTask];
                                       }
                                   }];
    }
    @catch (NSException *exception)
    {
    }
}

/**
 *  Booking for later
 *
 *  @param responseDictionary return type void and takes no argument
 */
-(void)getLiveCancelResponse:(NSDictionary *)responseDictionary{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    NSString *titleMsg = NSLocalizedString(@"Message", @"Message");
    NSString *errMsg = @"";
    [self bookingNotAccetedByAnyDriverScreenLayout];
    
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[responseDictionary objectForKey: @"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        errMsg = [responseDictionary objectForKey:@"errMsg"];
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
            [self publishPubNubStream];
            [self showBottomViews];
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1)
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
        else
        {
            errMsg = [responseDictionary objectForKey:@"errMsg"];
        }
    }
    [Helper showAlertWithTitle:titleMsg Message:errMsg];
}

-(void)getBookingAppointment:(NSDictionary *)responseDictionary
{
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[responseDictionary objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        [self stopBackgroundTask];
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDictionary objectForKey:@"Error"]];
        [self stopBackgroundTask];
    }
    else if ([responseDictionary[@"errFlag"] intValue] == 1 && ([responseDictionary[@"errNum"] intValue] == 6 || [responseDictionary[@"errNum"] intValue] == 7 || [responseDictionary[@"errNum"] intValue] == 96 || [responseDictionary[@"errNum"] intValue] == 94 )) { //session Expired
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [XDKAirMenuController relese];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
        [self stopBackgroundTask];
    }
    else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0 &&[[responseDictionary objectForKey:@"errNum"] intValue] == 78)
    {
        UIButton *nowButton =  (UIButton*)(UIView*)[self.view viewWithTag:2050];
        UIButton *laterButton = (UIButton*)(UIView*)[self.view viewWithTag:2051];
        
        [nowButton setSelected:YES];
        [laterButton setSelected:NO];
        isNowSelected = YES;
        isLaterSelected = NO;
        [self showBottomViews];
        
        [self bookingNotAccetedByAnyDriverScreenLayout];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        [self stopBackgroundTask];
    }
    else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1 &&[[responseDictionary objectForKey:@"errNum"] intValue] == 122)
    {
        [self bookingNotAccetedByAnyDriverScreenLayout];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        [self stopBackgroundTask];
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 1)
        {
            if ([[responseDictionary objectForKey:@"errNum"] intValue] == 1)
            {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDictionary[@"errMsg"]];
                [self stopBackgroundTask];
            }
            else if ([responseDictionary[@"errNum"] intValue] == 100)
            {
                [self bookingNotAccetedByAnyDriverScreenLayout];
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDictionary[@"errMsg"]];
            }
            else 
            {
                [self sendAppointmentRequestForLiveBooking];
            }
        }
        else if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0 &&[[responseDictionary objectForKey:@"errNum"] intValue] == 39)
        {
            BOOL isNewBooking = [self checkBookingID:[responseDictionary[@"bid"] integerValue]];
            if (isNewBooking)
            {
                [[NSUserDefaults standardUserDefaults] setObject:responseDictionary[@"bid"] forKey:@"BOOKINGID"];
                isRequestingButtonClicked = NO;
                docCount = 0;
                NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
                [ud setObject:responseDictionary[@"email"] forKey:KUDriverEmail];
                [ud setObject:responseDictionary[@"chn"] forKey:kNSUDriverPubnubChannel];
                [ud setObject:responseDictionary[@"apptDt"] forKey:KUBookingDate];
                [ud setObject:responseDictionary[@"rating"] forKey:@"Rating"];
                [ud setObject:responseDictionary[@"model"] forKey:@"MODEL"];
                [ud setObject:responseDictionary[@"plateNo"] forKey:@"PLATE"];
                [ud setObject:responseDictionary[@"carMapImage"] forKey:@"carMapImage"];
                [ud setObject:flStrForStr(responseDictionary[@"carImage"]) forKey:@"CARIMAGE"];
                [ud setObject:flStrForStr(responseDictionary[@"driverPic"]) forKey:@"driverPic"];
                [ud setObject:flStrForStr(responseDictionary[@"drivername"]) forKey:@"drivername"];
                [ud setObject:flStrForStr(responseDictionary[@"mobile"]) forKey:@"DriverTelNo"];
                
                [ud synchronize];
                
                UIImageView *img = (UIImageView*)[self.view viewWithTag:curLocImgTag];
                img.hidden = YES;
                
                UIView *markerView =[self.view viewWithTag:myCustomMarkerTag];
                markerView.hidden = YES;
                
                UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
                bottomContainerView.hidden = YES;
                
                UIView *topContainerView =[self.view viewWithTag:topViewTag];
                topContainerView.hidden = YES;
                
                if ([ud boolForKey:kNSUIsPassengerBookedKey])
                {
                }
                else
                {
                    [ud setBool:YES forKey:kNSUIsPassengerBookedKey];
                    BOOL isAlreadyCalled = [self checkServiceCalledAtleastOnce];
                    if (!isAlreadyCalled)
                    {
                        [self sendRequestToGetDriverDetails:responseDictionary[@"email"] :responseDictionary[@"apptDt"]];
                    }
                }
                [[MyAppTimerClass sharedInstance] stopPublishTimer];
                [self stopBackgroundTask];
            }
        }
    }
}


#pragma mark ViewLifeCycle-
- (void)makeMyProgressBarMoving
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if(!addProgressBarView)
    {
        addProgressBarView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
        addProgressBarView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bgg"]];
        [addProgressBarView setUserInteractionEnabled:YES];
        _myProgressView = [[UIView alloc]initWithFrame:CGRectMake(10,screenHeight/2-50/2,screenWidth-20,50)];
        _myProgressView.layer.cornerRadius = 5.0f;
        _myProgressView.tag = myProgressTag;
        _myProgressView.backgroundColor =  CLEAR_COLOR;
        
        UILabel *reqText = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth-20, 25)];
        reqText.textAlignment = NSTextAlignmentCenter;
        [Helper setToLabel:reqText Text:NSLocalizedString(@"Requesting...", @"Requesting...") WithFont:Roboto_Regular FSize:12 Color:UIColorFromRGB(0xffffff)];
        [_myProgressView addSubview:reqText];
        threadProgressView = [[UIProgressView alloc] initWithFrame:CGRectMake(10,25,screenWidth-40,10)];
        threadProgressView.progress = 0;
        [_myProgressView addSubview:threadProgressView];
        [addProgressBarView addSubview:_myProgressView];
        
        UIView *cancelAnimationView = [[UIView alloc]initWithFrame:CGRectMake((screenWidth-37)/2,screenHeight-50,37,37)];
        cancelAnimationView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cancel_btn"]];
        [cancelAnimationView setUserInteractionEnabled:YES];
        cancelAnimationView.multipleTouchEnabled = NO;
        [addProgressBarView addSubview:cancelAnimationView];
        
        UILabel *tapLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, screenHeight-120, screenWidth-20, 40)];
        tapLabel.textAlignment = NSTextAlignmentCenter;
        tapLabel.tag = 2000;
        tapLabel.hidden = NO;
        [Helper setToLabel:tapLabel Text:NSLocalizedString(@"Tap and hold to cancel", @"Tap and hold to cancel") WithFont:Roboto_Regular FSize:20 Color:UIColorFromRGB(0xffffff)];
        [addProgressBarView addSubview:tapLabel];
        
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(longpressToCancel:)];
        longPress.minimumPressDuration = 0.5;
        [cancelAnimationView addGestureRecognizer:longPress];
        
        [self.view addSubview:addProgressBarView];
        [self.view bringSubviewToFront:addProgressBarView];
        actual = [threadProgressView progress];
        [self updateThreadProgressView];
    }
}

-(void)updateThreadProgressView
{
    if (actual < 1)
    {
        threadProgressView.progress = actual;
        actual = actual + 0.005533335;
        
        [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(updateThreadProgressView) userInfo:nil repeats:NO];
    }
    else if (actual > 1)
    {
        [self bookingNotAccetedByAnyDriverScreenLayout];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    sharedInstance = self;
    pubNub = [PubNubWrapper sharedInstance];

    carTypesForLiveBooking = 1;
    carTypesForLiveBookingServer = 0;
    isNowSelected = YES;
    isLaterSelected = NO;
    threadProgressView.progress = 0.0;
    self.navigationController.navigationBarHidden = YES;
    CGRect rect = self.view.frame;
    rect.origin.y = -44;
    [self.view setFrame:rect];
    
    NSUserDefaults *ud =   [NSUserDefaults standardUserDefaults];
    double curLat = [[ud objectForKey:KNUCurrentLat] doubleValue];
    double curLong = [[ud objectForKey:KNUCurrentLong] doubleValue];
    _patientEmail = [ud objectForKey:kNSUUserEmailID];
    userPubNubChannel = [ud objectForKey:kNSUUserPubnubChannel];
    serverPubNubChannel = [ud objectForKey:kNSUServerPubnubChannel];
    newLat = [[ud objectForKey:KNUCurrentLat]floatValue];
    newLong = [[ud objectForKey:KNUCurrentLong]floatValue];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:curLat longitude:curLong zoom:mapZoomLevel];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView *customMapView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenWidth, screenHeight)];
    CGRect rectMap = customMapView.frame;
    
    mapView_ = [GMSMapView mapWithFrame:rectMap camera:camera];
    mapView_.settings.compassButton = YES;
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    [mapView_ clear];
    customMapView = mapView_;
    geocoder_ = [[GMSGeocoder alloc] init];
    [self.view addSubview:customMapView];
    [self subscribeToPassengerChannel];
    [self addCustomNavigationBar];
    
    [[ProgressIndicator sharedInstance] showPIOnView:self.view withMessage:NSLocalizedString(@"Checking Booking Status...", @"Checking Booking Status...")];
    [self checkBookingStatus];
    
    //CHECK FOR NOTIFICATION AND CHANGE CONTROLLER
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"DRIVERONTHEWAY" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"DRIVERREACHED"  object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"JOBSTARTED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeContentOfPresentController:) name:@"JOBCOMPLETED" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:@"LOGOUT" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookingConfirmed:) name:kNotificationBookingConfirmationNameKey object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationServicesChanged:) name:kNotificationLocationServicesChangedNameKey object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        mapView_.myLocationEnabled = YES;
        [self hideActivityIndicatorWithMessage];
        if(_arrayOfMastersAround.count > 0)
        {
            if (_arrayOfMastersAround.count < carTypesForLiveBooking)
            {
                carTypesForLiveBooking = 1;
            }
            carTypesForLiveBookingServer = [_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
            [self changeMapMarker:carTypesForLiveBooking-1];
        }
    });
    
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    msgLabel.hidden = YES;
    indicatorView.hidden = YES;
    
    self.navigationController.navigationBarHidden = YES;
    NSString *driverChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel]);
    if (driverChn.length)
    {
        [self subsCribeToPubNubChannel:driverChn];
    }

    if(checkFareQuote)
    {
        NSString *fareEstimateValue =  [[NSUserDefaults standardUserDefaults] objectForKey:@"FareEstimate"];
        if (fareEstimateValue.length)
        {
            UIButton *fareBtn  = (UIButton *)[[[self.view viewWithTag:499] viewWithTag:501] viewWithTag:505];
            NSString *Str = NSLocalizedString(@"FARE ESTIMATE", @"FARE ESTIMATE");
            fareBtn.titleLabel.numberOfLines = 0;
            fareBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            [Helper setButton:fareBtn Text:[NSString stringWithFormat:@"%@\n%@", Str, fareEstimateValue] WithFont:Roboto_Light FSize:12 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        }
        else
        {
            UIButton *fareBtn  = (UIButton *)[[[self.view viewWithTag:499] viewWithTag:501] viewWithTag:505];
            NSString *Str = NSLocalizedString(@"FARE QUOTE", @"FARE QUOTE");
            fareBtn.titleLabel.numberOfLines = 0;
            fareBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            [Helper setButton:fareBtn Text:Str WithFont:Roboto_Light FSize:12 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FareEstimate"];
    }
    checkFareQuote = NO;
}

-(void)startBackgroundTask
{
    UIApplication *app = [UIApplication sharedApplication];
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
}

-(void)stopBackgroundTask
{
    UIApplication *app = [UIApplication sharedApplication];
    [app endBackgroundTask:bgTask];
}

- (void)viewWillDisappear:(BOOL)animated
{
    if (!_isSelectinLocation)
    {
        _isSelectinLocation = NO;
        
        //First unsubscribe to the channel you are listening to,then clear all the array contents so that the map will launch freshly
        [_allMarkers removeAllObjects];
        [mapView_ clear];
        [self removeAddProgressView];
        [self stopPubNubStream];
        
//        NSString *driverChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel]);
//        if (driverChn.length)
//        {
//            [self unSubsCribeToPubNubChannel:driverChn];
//        }
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [[MyAppTimerClass sharedInstance] stopEtaNDisTimer];
    }
}


/**
 *  Stop the receiving data from pubnub server
 */
-(void)stopPubNubStream
{
    pubNub = [PubNubWrapper sharedInstance];
    pubNub.delegate = nil;
}

-(void)removeAddProgressView
{
    if(addProgressBarView != nil)
    {
        [addProgressBarView removeFromSuperview];
        threadProgressView = nil;
        addProgressBarView = nil;
        actual = 1;
    }
}

- (void) viewDidAppear:(BOOL)animated
{
    [self addgestureToMap];
    if (IS_SIMULATOR)
    {
        _currentLatitude = 13.028866;
        _currentLongitude = 77.589760;
        CLLocationCoordinate2D locationCord = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
        mapView_.camera = [GMSCameraPosition cameraWithTarget:locationCord zoom:14];
    }
}

-(void)clearUserDefaultsAfterBookingCompletion
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud removeObjectForKey:kNSUPassengerBookingStatusKey];
    [ud removeObjectForKey:kNSUDriverPubnubChannel];
    [ud setBool:NO forKey:kNSUIsPassengerBookedKey];
    [ud setBool:NO forKey:@"isServiceCalledOnce"];
    [ud removeObjectForKey:@"DriverTelNo"];
    [ud removeObjectForKey:@"MODEL"];
    [ud removeObjectForKey:@"PLATE"];
    [ud removeObjectForKey:@"drivername"];
    [ud removeObjectForKey:@"driverpic"];
    [ud removeObjectForKey:@"drivertip"];
    [ud removeObjectForKey:@"srcLat"];
    [ud removeObjectForKey:@"srcLong"];
    [ud removeObjectForKey:@"desLat"];
    [ud removeObjectForKey:@"desLong"];
    [ud removeObjectForKey:@"desadd1"];
    [ud removeObjectForKey:@"desadd2"];
    [ud removeObjectForKey:@"driLat"];
    [ud removeObjectForKey:@"driLong"];
    [ud removeObjectForKey:KUDriverEmail];
    [ud removeObjectForKey:KUBookingDate];
    [ud synchronize];
}

#pragma mark - UserDelegate

-(void)logout:(NSNotification *)notification
{
    User *user = [User alloc];
    user.delegate = self;
    [user logout];
}

-(void)userDidLogoutSucessfully:(BOOL)sucess
{    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
}

-(void)userDidFailedToLogout:(NSError *)error{
    
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    [XDKAirMenuController relese];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
    [[PubNubWrapper sharedInstance] unsubscribeFromMyChannel];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    XDKAirMenuController *controller = [XDKAirMenuController sharedMenu];
    if (controller.isMenuOpened)
    {
        [controller closeMenuAnimated];
    }
    return YES;
}

#pragma Mark - ChangeController
-(void)bookingConfirmed:(NSNotification*)notification
{
    NSDictionary *userinfo = notification.userInfo;
    NSString *email = userinfo[@"e"];
    NSString *bookingDate = userinfo[@"d"];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:email forKey:KUDriverEmail];
    [ud setObject:bookingDate forKey:KUBookingDate];
    [ud synchronize];
    
    [self bookingNotAccetedByAnyDriverScreenLayout];
    BOOL isAlreadyCalled = [self checkServiceCalledAtleastOnce];
    if (!isAlreadyCalled)
    {
        [self sendRequestToGetDriverDetails:email :bookingDate];
    }
}

-(void)changeContentOfPresentController:(NSNotification *)notification
{
    if ([notification.name isEqualToString:@"JOBCOMPLETED"])
    {
        _isDriverOnTheWay = _isDriverArrived = isRequestingButtonClicked = isLaterSelected = NO;
        _isDriverArrived = NO;
        isRequestingButtonClicked = NO;
        isLaterSelected = NO;
        isNowSelected = YES;
        docCount = 0;
        [mapView_ clear];
        [self removeDriverDetailScreen];
        actual = 1;
        NSString *driverChn = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel]);
        if (driverChn.length)
        {
            [self unSubsCribeToPubNubChannel:driverChn];
        }
        [self subscribeToPassengerChannel];
        [self showBottomViews];
        [self publishPubNubStream];
        [self clearUserDefaultsAfterBookingCompletion];
        
        [mapView_ clear];
        
        [_arrayOfDriverEmail removeAllObjects];
        _arrayOfDriverEmail = nil;
        [_allMarkers removeAllObjects];
        _allMarkers = nil;
        UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
        [markerView setUserInteractionEnabled:YES];
        UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
        UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
        msgLabel.hidden = NO;
        msgLabel.text = @"No Drivers";
        indicatorView.hidden = YES;

        desAddr = desAddrline2 = @"";
        desLat = desLong = 0;
        [[MyAppTimerClass sharedInstance] stopPublishTimer];
        [[MyAppTimerClass sharedInstance] stopEtaNDisTimer];
        [[MyAppTimerClass sharedInstance] startPublishTimer];
        [self publishPubNubStream];

        NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:KUBERCarArrayKey]];
        _arrayOfCarTypes = [carTypes mutableCopy];
        if (_arrayOfCarTypes.count > 0)
        {
            UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
            bottomContainerView.hidden = NO;
            [self clearTheMapBeforeChagingTheCarTypes];
            [self addBottomView];
        }
    }
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    if (bookingStatus == kNotificationTypeBookingOnMyWay)
    {
        if (!_isDriverOnTheWay)
        {
            _isDriverOnTheWay = YES;
            _isPathPlotted = NO;
            waypoints_ = [[NSMutableArray alloc]init];
            waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
            userPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUUserPubnubChannel];
            [mapView_ clear];
            [self subscribeOnlyBookedDriver];
            
            UIImageView *img = (UIImageView*)[self.view viewWithTag:curLocImgTag];
            img.hidden = YES;
            
            UIView *markerView =[self.view viewWithTag:myCustomMarkerTag];
            markerView.hidden = YES;
            
            UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
            bottomContainerView.hidden = YES;
            
            UIView *topContainerView =[self.view viewWithTag:topViewTag];
            topContainerView.hidden = YES;
            
            //Add custom navigation to the viewcontroller
            [self addCustomNavigationBar];
            [self createDriverArrivedView:bookingStatus];
            [self createDriverMessageShowingView:bookingStatus];
            
            NSUserDefaults *udLat = [NSUserDefaults standardUserDefaults];
            if (_currentLatitude == 0 || _currentLongitude == 0) {
                _currentLatitude =  [udLat doubleForKey:@"srcLat"];
                _currentLongitude = [udLat doubleForKey:@"srcLong"];
            }
            if(!(_currentLatitude == 0 || _currentLongitude == 0)) {
                [self setStartLocationCoordinates:_currentLatitude Longitude:_currentLongitude :2];
                GMSCameraPosition *srcLocation = [GMSCameraPosition cameraWithLatitude:_currentLatitude
                                                                             longitude:_currentLongitude
                                                                                  zoom:mapZoomLevel];
                [mapView_ setCamera:srcLocation];
            }
            [self sendRequestgetETAnDistance];
            [[MyAppTimerClass sharedInstance] startEtaNDisTimer];
        }
    }
    else if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted)
    {
        if (!_isDriverArrived)
        {
            _isDriverArrived = YES;
            UIImageView *img = (UIImageView*)[self.view viewWithTag:curLocImgTag];
            img.hidden = YES;
            
            UIView *bottomContainerView =[self.view viewWithTag:bottomViewWithCarTag];
            bottomContainerView.hidden = YES;
            
            UIView *markerView =[self.view viewWithTag:myCustomMarkerTag];
            markerView.hidden = YES;
            
            UIView *topContainerView =[self.view viewWithTag:topViewTag];
            topContainerView.hidden = YES;
            
            UIView *driverView = [self.view viewWithTag:driverArrivedViewTag];
            [driverView removeFromSuperview];
            
            UILabel *driverMsgLabel = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
            [driverMsgLabel removeFromSuperview];
            
            [self removeDriverDetailScreen];
            
            //Add custom navigation to the viewcontroller
            [self addCustomNavigationBar];
            [self createDriverArrivedView:bookingStatus];
            [self createDriverMessageShowingView:bookingStatus];
            _isPathPlotted = NO;
            [mapView_ clear];
            waypoints_ = [[NSMutableArray alloc]init];
            waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
            
            NSUserDefaults *udLat = [NSUserDefaults standardUserDefaults];
            desLat = [udLat doubleForKey:@"desLat"];
            desLong = [udLat doubleForKey:@"desLong"];
            
            if(! (desLong == 0 || desLong == 0))
            {
                [self setStartLocationCoordinates: desLat Longitude:desLong :3];
            }
            if (_currentLatitude == 0 || _currentLongitude == 0)
            {
                _currentLatitude =  [udLat doubleForKey:@"srcLat"];
                _currentLongitude = [udLat doubleForKey:@"srcLong"];
            }
            else if(! (_currentLatitude == 0 || _currentLongitude == 0))///// Driver lat long update here
            {
                CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
                GMSMarker *srcMarker = [GMSMarker markerWithPosition:position];
                srcMarker.map = mapView_;
                srcMarker.icon = [UIImage imageNamed:@"default_marker_p"];
                [self updateDestinationLocationWithLatitude:_currentLatitude Longitude:_currentLongitude];
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_currentLatitude
                                                                        longitude:_currentLongitude
                                                                             zoom:mapZoomLevel];
                [mapView_ setCamera:camera];
            }
            
            //For ETA n DISTANCE
            [[MyAppTimerClass sharedInstance] stopEtaNDisTimer];
            [self sendRequestgetETAnDistance];
            [[MyAppTimerClass sharedInstance] startEtaNDisTimer];
            [self subscribeOnlyBookedDriver];
            if (!_arrayOfpickerData)
            {
                _arrayOfpickerData = [NSMutableArray array];
                selectedRowForTip = 0;
                for (NSInteger i =0 ; i<=30; i+= 5)
                {
                    NSNumber *anumber = [NSNumber numberWithInteger:i];
                    NSString *aString = [NSString stringWithFormat:@"%@%%",anumber];
                    [_arrayOfpickerData addObject:aString];
                }
            }
        }
        else
        {
            [self addCustomNavigationBar];
            [self removeDriverDetailScreen];
            if (!_arrayOfpickerData)
            {
                _arrayOfpickerData = [NSMutableArray array];
                selectedRowForTip = 0;
                for (NSInteger i = 0 ; i<=30; i+= 5)
                {
                    NSNumber *anumber = [NSNumber numberWithInteger:i];
                    NSString *aString = [NSString stringWithFormat:@"%@%%",anumber];
                    [_arrayOfpickerData addObject:aString];
                }
            }
        }
    }
    else
    {
        [mapView_ animateToViewingAngle:0];
        UIView *driverView = [self.view viewWithTag:driverArrivedViewTag];
        [driverView removeFromSuperview];
        
        UILabel *driverMsgLabel = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
        [driverMsgLabel removeFromSuperview];
        
        UIImageView *img = (UIImageView*)[self.view viewWithTag:curLocImgTag];
        img.hidden = NO;
        
        UIView *markerView =[self.view viewWithTag:myCustomMarkerTag];
        markerView.hidden = NO;
        markerView.userInteractionEnabled = YES;
        
        UIView *topContainerView =[self.view viewWithTag:topViewTag];
        topContainerView.hidden = NO;
        
        //Add custom navigation to the viewcontroller
        [self addCustomNavigationBar];
        [self addCustomLocationView];
        [self getCurrentLocation:nil];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        [self getAddress:position];
        [self createCenterView];
        [self showBottomViews];
        if(kPMDBookLater)
        {
            [self addBookLaterView];
        }
        [self getCurrentLocationFromGPS];
        NSMutableArray *carTypes = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey: KUBERCarArrayKey]];
        _arrayOfCarTypes = [carTypes mutableCopy];
        if (_arrayOfCarTypes.count > 0)
        {
            [self clearTheMapBeforeChagingTheCarTypes];
            [self addBottomView];
            if (carTypesForLiveBookingServer == 0)
            {
                carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
            }
        }
        [self subscribeToPassengerChannel];
        [self publishPubNubStream];
    }
}

-(void)longpressToCancel:(UILongPressGestureRecognizer*)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        //Do Whatever You want on End of Gesture
    }
    else if (sender.state == UIGestureRecognizerStateBegan)
    {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ( [reachability isNetworkAvailable])
        {
            [self sendAppointmentRequestForLiveBookingCancellation];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"No Network Connection", @"No Network Connection")];
        }
        [self removeAddProgressView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - PubNub Methods

-(void)subsCribeToPubNubChannel:(NSString*)channel
{
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub subscribeToChannel:channel];
    [pubNub setDelegate:self];
}

-(void)unSubsCribeToPubNubChannel:(NSString *)channel
{
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub unsubscribeFromChannel:channel];
    [pubNub setDelegate:self];
}


-(void)subscribeToPassengerChannel
{
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub subscribeToUserChannel];
    [pubNub setDelegate:self];
}


-(void) unSubscribeToPassengerChannel
{
    pubNub = [PubNubWrapper sharedInstance];
    [pubNub unsubscribeFromMyChannel];
    [pubNub setDelegate:self];
}

-(void)checkDifferenceBetweenTwoLocationAndUpdateCache
{
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:newLat longitude:newLong];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:srcLat longitude:srcLong];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    if (distance > 50000)
    {
        carTypesForLiveBookingServer = 0;
        carTypesForLiveBooking = 1;
        isComingTocheckCarTypes = NO;
        [mapView_ clear];
        newLat = srcLat;
        newLong = srcLong;
    } //what if he changes his city
}

/**
 *  publishPubNubStream to receiving doctors near around patient current location
 */
-(void)publishPubNubStream
{
    PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
    int  status = [self getSelectedAppointmentType];
    NSString *dt;
    if(status == kAppointmentTypeLater)
    {
        dt = _laterSelectedDateServer;
    }
    else
    {
        dt = @"";
    }
    if (dt.length == 0)
    {
        dt = @"";
    }
    if ( [reachability isNetworkAvailable])
    {
        NSDictionary *message = @{@"a":[NSNumber numberWithInt:kPubNubStartStreamAction],
                                  @"pid": _patientEmail,
                                  @"lt": [NSNumber numberWithDouble:srcLat],
                                  @"lg": [NSNumber numberWithDouble:srcLong],
                                  @"chn": userPubNubChannel,
                                  @"st": [NSNumber numberWithInt:kAppointmentTypeNow],
                                  @"tp":[NSNumber numberWithInteger:carTypesForLiveBookingServer],
                                  @"dt":dt
                                  };
        if (!(srcLat == 0 || srcLong == 0))
        {
         //   [self checkDifferenceBetweenTwoLocationAndUpdateCache];
            NSLog(@"Publish Data = %@ and Server Channel = %@", message, serverPubNubChannel);
            if (!serverPubNubChannel.length)
            {
                serverPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUServerPubnubChannel];
            }
            [pubNub publishWithParameter:message toChannel:serverPubNubChannel];
        }
    }
}

-(BOOL)checkCurrentStatus:(NSInteger)Status
{
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    NSInteger newBookingStatus =   Status;
    if(bookingStatus != newBookingStatus && bookingStatus < newBookingStatus)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

-(BOOL)checkBookingID:(NSInteger)Status
{
    NSInteger bookingID = [[NSUserDefaults standardUserDefaults] integerForKey:@"BOOKINGID"];
    NSInteger newBookingID =   Status;
    if(bookingID != newBookingID && bookingID < newBookingID)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(BOOL)checkIfBokkedOrNot
{
    BOOL isBooked = [[NSUserDefaults standardUserDefaults] boolForKey:kNSUIsPassengerBookedKey];
    if(isBooked)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(BOOL)checkServiceCalledAtleastOnce
{
    BOOL isCalledOnce = [[NSUserDefaults standardUserDefaults] boolForKey:@"isServiceCalledOnce"];
    if(isCalledOnce)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

-(BOOL)checkBookingIDForCancel:(NSInteger)Status
{
    NSInteger bookingID = [[NSUserDefaults standardUserDefaults] integerForKey:@"BOOKINGID"];
    NSInteger newBookingID =   Status;
    if(bookingID == newBookingID)
    {
        return YES; //cancel it once
    }
    else
    {
        return NO; //old booking
    }
}

-(void)messageA5ComesHere:(NSDictionary *)messageDict
{
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    BOOL isDriverCancel = [[NSUserDefaults standardUserDefaults]boolForKey:@"isDriverCanceledBookingOnce"];
    if (!isDriverCancel)
    {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isDriverCanceledBookingOnce"];
    }
    if (isDriverCancel && isAlreadyBooked)
    {        
        [self clearUserDefaultsAfterBookingCompletion];
        int newBookingStatus =  10;
        
        NSDictionary *dictReason = @{@"r": messageDict[@"r"]};
        
        [(PatientAppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:dictReason :newBookingStatus];
    }
}

-(void)messageA6ComesHere:(NSDictionary *)messageDict
{
    BOOL isNewBooking = [self checkBookingID:[messageDict[@"bid"] integerValue]];
    if (isNewBooking)
    {
        [_allMarkers removeAllObjects];
        isRequestingButtonClicked = NO;
        docCount = 0;
        float latitude = [messageDict[@"lt"] floatValue];
        float longitude = [messageDict[@"lg"] floatValue];
        
        NSString *email = messageDict[@"e_id"];
        NSString *bookingDate = messageDict[@"dt"];
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setObject:messageDict[@"rating"] forKey:@"Rating"];
        [ud setObject:messageDict[@"bid"] forKey:@"BOOKINGID"];
        [ud setObject:messageDict[@"ph"] forKey:@"DriverTelNo"];
        [ud setBool:YES forKey:kNSUIsPassengerBookedKey];
        [[NSUserDefaults standardUserDefaults]synchronize];
        
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingOnMyWay];
        BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
        
        if (isAlreadyCame && isAlreadyBooked)
        {
        }
        else if(!isAlreadyBooked)
        {
        }
        else
        {
            [ud setObject:email forKey:KUDriverEmail];
            [ud setObject:bookingDate forKey:KUBookingDate];
            [ud synchronize];
            BOOL isAlreadyCalled = [self checkServiceCalledAtleastOnce];
            if (!isAlreadyCalled)
            {
                [self sendRequestToGetDriverDetails:email :bookingDate];
            }
            [self updateDestinationLocationWithLatitude:latitude Longitude:longitude];
        }
    }
}

-(void)messageA7ComesHere:(NSDictionary *)messageDict
{
    [_allMarkers removeAllObjects];
    float latitude = [messageDict[@"lt"] floatValue];
    float longitude = [messageDict[@"lg"] floatValue];
    BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingReachedLocation];
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    if (isAlreadyCame && isAlreadyBooked)
    {
    }
    else if(!isAlreadyBooked)
    {
    }
    else
    {
        [(PatientAppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingReachedLocation];
        [self updateDestinationLocationWithLatitude:latitude Longitude:longitude];
    }
}

-(void)messageA8ComesHere:(NSDictionary *)messageDict
{
    [_allMarkers removeAllObjects];
    float latitude = [messageDict[@"lt"] floatValue];
    float longitude = [messageDict[@"lg"] floatValue];
    BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingStarted];
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    if (isAlreadyCame && isAlreadyBooked)
    {
    }
    else if(!isAlreadyBooked)
    {
    }
    else
    {
        [(PatientAppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingStarted];
        [self updateDestinationLocationWithLatitude:latitude Longitude:longitude];
    }
}

-(void)messageA9Comeshere:(NSDictionary *)messageDict
{
    BOOL isAlreadyBooked = [self checkIfBokkedOrNot];
    BOOL isNewBooking = [self checkBookingID:[messageDict[@"bid"] integerValue]];
    if (isNewBooking || isAlreadyBooked)
    {
        [_allMarkers removeAllObjects];
        _driverCurLat = [messageDict[@"lt"] doubleValue];
        _driverCurLong = [messageDict[@"lg"] doubleValue];
        
        BOOL isAlreadyCame = [self checkCurrentStatus:kNotificationTypeBookingComplete];
        if (isAlreadyCame && isAlreadyBooked)
        {
        }
        else if(!isAlreadyBooked)
        {
        }
        else
        {
            [(PatientAppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:[NSDictionary dictionary] :kNotificationTypeBookingComplete];
        }
    }
}

-(void)messageA100Comeshere:(NSDictionary *)messageDict
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    if (!customNavigationBarView)
    {
        customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
        customNavigationBarView.tag = 78;
        customNavigationBarView.delegate = self;
        [customNavigationBarView addTitleButton];
        [customNavigationBarView removeRightBarButton];
        [self.view addSubview:customNavigationBarView];
    }
    [customNavigationBarView hideRightBarButton:YES];
}


-(void)messageA2ComeshereWithFlag1:(NSDictionary *)messageDict
{
    [mapView_ clear];
    [_arrayOfDriverEmail removeAllObjects] ;
    _arrayOfDriverEmail = nil;
    [_allMarkers removeAllObjects] ;
    _allMarkers = nil;
    if (_arrayOfMastersAround.count)
    {
        [_arrayOfMastersAround removeAllObjects];
        _arrayOfMastersAround = nil;
    }
    [scroller removeFromSuperview];
    [leftArrow removeFromSuperview];
    [rightArrow removeFromSuperview];
    scroller = nil;
    leftArrow = nil;
    rightArrow = nil;
    [self hideActivityIndicatorWithMessage];
    [[MyAppTimerClass sharedInstance] stopSpinTimer];
}

-(void)messageA2ForTesting:(NSDictionary *)messageDict
{
    NSArray *newArrayOfDrivers = [messageDict[@"masArr"] mutableCopy];
    if (!_arrayOfMastersAround)
    {
        //For the first time when the array in not initialised copy all the data from the array
        _arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
        drivers = [[NSMutableArray alloc] init];
        [drivers addObjectsFromArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
        if (drivers.count > 0)
        {
            double DLat = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lt"] doubleValue];
            double DLong = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lg"] doubleValue];
            [self calculateTimeToDisplayOnMarker:_currentLatitude Olong:_currentLongitude Dlat:DLat Dlong:DLong];
            [_allMarkers removeAllObjects];
            _allMarkers = nil;
            [mapView_ clear];
            [self addCustomMarkerFor];
        }
        else
        {
            [self hideActivityIndicatorWithMessage];
        }
    }
    if (newArrayOfDrivers.count != _arrayOfMastersAround.count)
    {
        //when array count from server changes
        if(_arrayOfMastersAround)
        {
            [_arrayOfMastersAround removeAllObjects];
            _arrayOfMastersAround = nil;
            _arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
            drivers = [[NSMutableArray alloc] init];
            [drivers addObjectsFromArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
            if (drivers.count > 0)
            {
                double DLat = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lt"] doubleValue];
                double DLong = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lg"] doubleValue];
                [self calculateTimeToDisplayOnMarker:_currentLatitude Olong:_currentLongitude Dlat:DLat Dlong:DLong];
                [self addCustomMarkerFor];
            }
            else
            {
                [self hideActivityIndicatorWithMessage];
            }
        }
        else
        {
            _arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
            drivers = [[NSMutableArray alloc] init];
            [drivers addObjectsFromArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
            if (drivers.count > 0)
            {
                double DLat = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lt"] doubleValue];
                double DLong = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lg"] doubleValue];
                [self calculateTimeToDisplayOnMarker:_currentLatitude Olong:_currentLongitude Dlat:DLat Dlong:DLong];
                [self addCustomMarkerFor];
            }
            else
            {
                [self hideActivityIndicatorWithMessage];
            }
        }
        NSMutableArray *arrOfDriverChannel = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in _arrayOfMastersAround)
        {
            NSArray *arr = [dict[@"mas"] mutableCopy];
            if ([arr count])
            {
                for (int i = 0; i < [arr count]; i++)
                {
                    [arrOfDriverChannel addObject:arr[i][@"chn"]];
                }
            }
        }
        NSMutableArray *newDriverChannel = [[NSMutableArray alloc] init];
        for(NSDictionary *dict in newArrayOfDrivers)
        {
            NSArray *arr = [dict[@"mas"] mutableCopy];
            if ([arr count])
            {
                for (int i = 0; i < [arr count]; i++)
                {
                    [newDriverChannel addObject:arr[i][@"chn"]];
                }
            }
        }
        for(NSString *channel in arrOfDriverChannel)
        {
            if (![newDriverChannel containsObject:channel])
            {
                GMSMarker *marker =  _allMarkers[channel];
                [self removeMarker:marker];
                [_allMarkers removeObjectForKey:channel];
            }
        }
    }
    else if (newArrayOfDrivers.count == _arrayOfMastersAround.count && ![newArrayOfDrivers isEqualToArray: _arrayOfMastersAround])
    {
        NSMutableArray *arrOfDriverChannel = [[NSMutableArray alloc] init];
        NSMutableArray *newDriverChannel = [[NSMutableArray alloc] init];
        
        NSArray *oldArrayData = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"] mutableCopy];
        NSArray *newArrayData = [newArrayOfDrivers[carTypesForLiveBooking-1][@"mas"] mutableCopy];
        
        if ([oldArrayData count])
        {
            for (int i=0; i<[oldArrayData count]; i++)
            {
                [arrOfDriverChannel addObject:oldArrayData[i][@"chn"]];
            }
        }
        if ([newArrayData count])
        {
            for (int i=0; i<[newArrayData count]; i++)
            {
                [newDriverChannel addObject:newArrayData[i][@"chn"]];
            }
        }
        for(NSString *channel in arrOfDriverChannel)
        {
            if (![newDriverChannel containsObject:channel])
            {
                GMSMarker *marker =  _allMarkers[channel];
                [self removeMarker:marker];
                [_allMarkers removeObjectForKey:channel];
            }
        }
        [_arrayOfMastersAround removeAllObjects];
        _arrayOfMastersAround = nil;
        _arrayOfMastersAround = [newArrayOfDrivers mutableCopy];
        drivers = [[NSMutableArray alloc] init];
        [drivers addObjectsFromArray:_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"]];
        if (drivers.count > 0)
        {
            double DLat = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lt"] doubleValue];
            double DLong = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lg"] doubleValue];
            [self calculateTimeToDisplayOnMarker:_currentLatitude Olong:_currentLongitude Dlat:DLat Dlong:DLong];
            [self addCustomMarkerFor];
        }
        else
        {
            [self hideActivityIndicatorWithMessage];
        }
    }
}

-(void)messageA2ComeshereWithDriverEmails:(NSDictionary *)messageDict
{
    NSMutableArray *emailData = [[NSMutableArray alloc] init];
    if ([messageDict[@"masArr"] count] < carTypesForLiveBooking)
    {
        carTypesForLiveBooking = 1;
        return;
    }
    for (int i = 0; i< [messageDict[@"masArr"][carTypesForLiveBooking-1][@"mas"] count]; i++)
    {
        [emailData addObject:messageDict[@"masArr"][carTypesForLiveBooking-1][@"mas"][i][@"e"] ];
    }
    if(!_arrayOfDriverEmail)
    {
        _arrayOfDriverEmail = [emailData mutableCopy];
    }
    if (emailData.count != _arrayOfDriverEmail.count)
    {
        if(_arrayOfDriverEmail)
        {
            [_arrayOfDriverEmail removeAllObjects];
            _arrayOfDriverEmail = nil;
            _arrayOfDriverEmail = [emailData mutableCopy];
        }
        else
        {
            _arrayOfDriverEmail = [emailData mutableCopy];
        }
    }
    else if (emailData.count == _arrayOfDriverEmail.count && ![emailData isEqualToArray: _arrayOfDriverEmail])
    {
        [_arrayOfDriverEmail removeAllObjects];
        _arrayOfDriverEmail = nil;
        _arrayOfDriverEmail = [emailData mutableCopy];
    }
}

-(void)messageA2ComeshereWithCarTypes:(NSDictionary *)messageDict
{
    NSArray *carTypesArray = messageDict[@"types"];
    if (carTypesArray.count > 0)
    {
        if(!_arrayOfCarTypes)
        {
            _arrayOfCarTypes = [carTypesArray mutableCopy];
            carTypesForLiveBookingServer = [messageDict[@"types"][carTypesForLiveBooking-1][@"type_id"] integerValue];
            [self handleTap:nil];
            scroller = nil;
            [self clearTheMapBeforeChagingTheCarTypes];
            [self addBottomView];
            [[NSUserDefaults standardUserDefaults] setObject:_arrayOfCarTypes forKey:KUBERCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        if (carTypesArray.count != _arrayOfCarTypes.count)
        {
            [_arrayOfCarTypes removeAllObjects];
            _arrayOfCarTypes = nil;
            _arrayOfCarTypes = [carTypesArray mutableCopy];
            if ([messageDict[@"types"] count] < carTypesForLiveBooking)
            {
                carTypesForLiveBooking = 1;
            }
            carTypesForLiveBookingServer = [messageDict[@"types"][carTypesForLiveBooking-1][@"type_id"] integerValue];
            [self handleTap:nil];
            [scroller removeFromSuperview];
            if (carTypesForLiveBooking !=1)
            {
                oldOffset = scroller.contentOffset;
            }
            scroller= nil;
            [self clearTheMapBeforeChagingTheCarTypes];
            [self addBottomView];
            [[NSUserDefaults standardUserDefaults] setObject:_arrayOfCarTypes forKey:KUBERCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if (carTypesArray.count == _arrayOfCarTypes.count && ![carTypesArray isEqualToArray:_arrayOfCarTypes])
        {
            [_arrayOfCarTypes removeAllObjects];
            _arrayOfCarTypes = nil;
            _arrayOfCarTypes = [carTypesArray mutableCopy];
            carTypesForLiveBookingServer = [messageDict[@"types"][carTypesForLiveBooking-1][@"type_id"] integerValue];
            [self handleTap:nil];
            [scroller removeFromSuperview];
            oldOffset = scroller.contentOffset;
            scroller= nil;
            [self clearTheMapBeforeChagingTheCarTypes];
            [self addBottomView];
            [[NSUserDefaults standardUserDefaults] setObject:_arrayOfCarTypes forKey:KUBERCarArrayKey];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
    else
    {
        if (!isComingTocheckCarTypes)
        {
            isComingTocheckCarTypes = YES;
            [_arrayOfCarTypes removeAllObjects];
            _arrayOfCarTypes = nil;
            [self handleTap:nil];
            [scroller removeFromSuperview];
            scroller= nil;
            [self clearTheMapBeforeChagingTheCarTypes];
            [self addBottomView];
            for (UIView *va in [self.view subviews])
            {
                if (va.tag == bottomViewWithCarTag)
                {
                    [va removeFromSuperview];
                }
            }
        }
    }
}

-(void)handleMessageA2ComesHaere:(NSDictionary *)messageDict
{
    if ([[messageDict objectForKey:@"flag"] intValue] == 1)
    {
        [self messageA2ComeshereWithFlag1:messageDict];
        [self messageA2ComeshereWithCarTypes:messageDict];
    }
    else if ([[messageDict objectForKey:@"flag"] intValue] == 0)
    {
        [self messageA2ComeshereWithCarTypes:messageDict];
        if(!isRequestingButtonClicked)
        {
            [self messageA2ComeshereWithDriverEmails:messageDict];
        }
        [self messageA2ForTesting:messageDict];
    }
}

bool isComingTocheckCarTypes;

- (void)receivedMessage:(NSDictionary *)messageDict andChannel:(NSString *)channelName
{
    NSLog(@"Receive Message = %@ on channel =%@", messageDict, channelName);
    if (![messageDict isKindOfClass:[NSDictionary class]] || messageDict == nil)
    {
        return;
    }
    else
    {
        NSInteger status = [flStrForStr(messageDict[@"a"]) integerValue];
        BOOL isPassengerInBooking = [[NSUserDefaults standardUserDefaults] boolForKey:kNSUIsPassengerBookedKey];
        
        switch (status)
        {
            case 2:
                if ([channelName isEqualToString:userPubNubChannel] && !isPassengerInBooking)
                {
                    [self handleMessageA2ComesHaere:messageDict];
                }
                break;
            case 5:
                [self messageA5ComesHere:messageDict];
                break;
            case 6:
                [self messageA6ComesHere:messageDict];
                break;
            case 7:
                [self messageA7ComesHere:messageDict];
                break;
            case 8:
                [self messageA8ComesHere:messageDict];
                break;
            case 9:
                [self messageA9Comeshere:messageDict];
                break;
            case 100:
                [self messageA100Comeshere:messageDict];
                break;
            default:
                break;
        }
        if (![channelName isEqualToString:userPubNubChannel] && isPassengerInBooking)
        {
            [self updatedDrivermarkerInLiveBooking:messageDict];
        }
    }
}

/**
 *  Update driver car marker while live tracking.
 *
 *  @param messageDict pubnub response from driver
 */
-(void) updatedDrivermarkerInLiveBooking:(NSDictionary *)messageDict
{
    if ([messageDict[@"a"] integerValue] == 6  || [messageDict[@"a"] integerValue] == 7 || [messageDict[@"a"] integerValue] == 8  || [messageDict[@"a"] integerValue] == 9)
    {
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        if ([messageDict[@"bid"] integerValue] == [[ud objectForKey:@"BOOKINGID"] integerValue])
        {
            _driverCurLat = [messageDict[@"lt"] doubleValue];
            _driverCurLong = [messageDict[@"lg"] doubleValue];
            [self updateDestinationLocationWithLatitude:_driverCurLat Longitude:_driverCurLong];
        }
    }
}

#pragma Mark- Path Plotting

-(void)setStartLocationCoordinates:(float)latitude Longitude:(float)longitude :(int)type {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:mapZoomLevel];
    [mapView_ setCamera:camera];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude, longitude);
    GMSMarker *marker = [GMSMarker markerWithPosition:position];
    marker.map = mapView_;
    [waypoints_ addObject:marker];
    if (type == 2)
        marker.icon = [UIImage imageNamed:@"default_marker_p"];
    else
        marker.icon = [UIImage imageNamed:@"default_marker_d"];
    _startLocation = [NSString stringWithFormat:@"%.6f,%.6f",latitude, longitude];
    [waypointStrings_ insertObject:_startLocation atIndex:0];
}


/**
 *  Update Destination of an incoming Doctor
 *  Returns void and accept two arguments
 *  @param latitude  Doctor Latitude
 *  @param longitude incoming Doctor longitude
 */

-(void)updateDestinationLocationWithLatitude:(float)latitude Longitude:(float)longitude {
    if (!_isPathPlotted) {
        _isPathPlotted = YES;
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        _previouCoord = position;
        _destinationMarker = [GMSMarker markerWithPosition:position];
        _destinationMarker.flat = YES;
        _destinationMarker.groundAnchor = CGPointMake(0.5f, 0.5f);
        if (arrayOfMapImages.count) {
            _destinationMarker.icon = [arrayOfMapImages objectAtIndex:carTypesForLiveBooking-1];
            _destinationMarker.map = mapView_;
            [waypoints_ addObject:_destinationMarker];
            NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",latitude,longitude];
            [waypointStrings_ addObject:positionString];
            if([waypoints_ count] > 1) {
                waypoints_ = [[[waypoints_ reverseObjectEnumerator] allObjects] mutableCopy];
                waypointStrings_ = [[[waypointStrings_ reverseObjectEnumerator] allObjects] mutableCopy];
                
                NSString *sensor = @"false";
                NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                       nil];
                NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
                NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters forKeys:keys];
                DirectionService *mds=[[DirectionService alloc] init];
                SEL selector = @selector(addDirections:);
                [mds setDirectionsQuery:query
                           withSelector:selector
                           withDelegate:self];
            }
        } else {
            NSString *markerIconUrl;
            markerIconUrl = [NSString stringWithFormat:@"%@%@", baseUrlForOriginalImage, [[NSUserDefaults standardUserDefaults] objectForKey:@"carMapImage"]];
            if(markerIconUrl.length && _arrayOfCarTypes.count) {
                markerIconUrl = [NSString stringWithFormat:@"%@%@", baseUrlForOriginalImage, _arrayOfCarTypes[carTypesForLiveBooking-1][@"MapIcon"]];
            }
            if ([PMDReachabilityWrapper sharedInstance].isNetworkAvailable) {
                [self downloadDriverCarImage:markerIconUrl markerName:_destinationMarker];
            } else {
                _isPathPlotted = NO;
            }
        }
    } else {
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(latitude,longitude);
        CLLocationDirection heading = GMSGeometryHeading(_previouCoord, position);
        if (position.latitude == _previouCoord.latitude && position.longitude == _previouCoord.longitude)
            return;
        [CATransaction begin];
        [CATransaction setAnimationDuration:5.0];
        _destinationMarker.position = position;
        [CATransaction commit];
        if (heading > 0)
            _destinationMarker.rotation = heading;
        _previouCoord = position;
    }
}


-(void) downloadDriverCarImage:(NSString *) markerURL markerName:(GMSMarker* )marker {
    UIImageView *markerImageView;
    if (!markerImageView){
        markerImageView = [[UIImageView alloc] init];
    }
    if (markerURL.length){
        [markerImageView sd_setImageWithURL:[NSURL URLWithString:markerURL]
                                  completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                      if(image) {
                                          image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(50, 50)]];
                                          marker.icon = image;
                                          marker.map = mapView_;
                                          [waypoints_ addObject:marker];
                                          NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",marker.position.latitude,marker.position.longitude];
                                          [waypointStrings_ addObject:positionString];
                                          if([waypoints_ count] > 1) {
                                              waypoints_ = [[[waypoints_ reverseObjectEnumerator] allObjects] mutableCopy];
                                              waypointStrings_ = [[[waypointStrings_ reverseObjectEnumerator] allObjects] mutableCopy];
                                              
                                              NSString *sensor = @"false";
                                              NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                                                                     nil];
                                              NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
                                              NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters forKeys:keys];
                                              DirectionService *mds=[[DirectionService alloc] init];
                                              SEL selector = @selector(addDirections:);
                                              [mds setDirectionsQuery:query
                                                         withSelector:selector
                                                         withDelegate:self];
                                          }
                                          return;
                                      }
                                  }];
    }
    if (marker.icon == nil){
        _isPathPlotted = NO;
    }
}

- (void)addDirections:(NSDictionary *)json{
    if ([json[@"routes"] count]>0){
        NSDictionary *routes = [json objectForKey:@"routes"][0];
        NSDictionary *route = [routes objectForKey:@"overview_polyline"];
        NSString *overview_route = [route objectForKey:@"points"];
        GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
        GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
        [polyline setStrokeWidth:3.0];
        [polyline setStrokeColor:UIColorFromRGB(0x08B4E8)];
        polyline.map = mapView_;
    }
}


-(void)subscribeOnlyBookedDriver
{
    subscribedChannel = flStrForStr([[NSUserDefaults standardUserDefaults] objectForKey:kNSUDriverPubnubChannel]);
    if (subscribedChannel.length)
    {
        [self subsCribeToPubNubChannel:subscribedChannel];
    }
}

-(void)didFailedToConnectPubNub:(NSError *)error
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showMessage:[error localizedDescription] On:self.view];
    [self hideAcitvityIndicator];
}

-(void)addgestureToMap
{
    _tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    __weak typeof(self) weakSelf = self;
    _tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event , int touchtype)
    {
        if (touchtype == 1)
        {
            [weakSelf startAnimation];
        }
        else
        {
            [weakSelf endAnimation];
        }
    };
    [mapView_  addGestureRecognizer:_tapInterceptor];
}


#pragma mark GMSMapviewDelegate -

- (void) mapView:(GMSMapView *) mapView willMove:(BOOL)gesture
{
    if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen)
    {
        isLocationChanged = isFixed;
    }
    else
    {
        if(isFareButtonClicked == NO)
        {
            isLocationChanged = isChanging;
        }
        else
        {
            isFareButtonClicked = NO;
        }
    }
}

- (void) mapView:(GMSMapView *) mapView didChangeCameraPosition:(GMSCameraPosition *) position
{
}

- (void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker
{
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker
{
    isLocationChanged = isChanging;
}

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    if ((bookingStatus == kNotificationTypeBookingOnMyWay) || (bookingStatus == kNotificationTypeBookingReachedLocation))
    {
        return;
    }
    else
    {
        CGPoint point1 = mapView_.center;
        CLLocationCoordinate2D coor = [mapView_.projection coordinateForPoint:point1];
        _currentLatitude = coor.latitude;
        _currentLongitude = coor.longitude;
        
        if (_isAddressManuallyPicked)
        {
            _isAddressManuallyPicked = NO;
            return;
        }
        else
        {
            [self getAddress:coor];
            [self startSpinitAgain];
        }
    }
}

#pragma mark Custom Methods -

- (void) addCustomNavigationBar {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    if(!customNavigationBarView)
    {
        customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
        customNavigationBarView.tag = 78;
        customNavigationBarView.delegate = self;
        [customNavigationBarView addTitleButton];
        [customNavigationBarView removeRightBarButton];
        [self.view addSubview:customNavigationBarView];
    }
    if(isCustomMarkerSelected == YES)
    {
        [customNavigationBarView setTitle:NSLocalizedString(@"CONFIRMATION", @"CONFIRMATION")];
        [customNavigationBarView setLeftBarButtonTitle:NSLocalizedString(@"CANCEL",@"CANCEL")];
        [customNavigationBarView hideTitleButton:YES];
        [customNavigationBarView hideLeftMenuButton:YES];
        [customNavigationBarView hideRightBarButton:YES];
    }
    else if (bookingStatus == kNotificationTypeBookingOnMyWay)
    {
        [customNavigationBarView setTitle:NSLocalizedString(@"DRIVER ON THE WAY",@"DRIVER ON THE WAY")];
        [customNavigationBarView hideTitleButton:YES];
        [customNavigationBarView hideRightBarButton:YES];
    }
    else if (bookingStatus == kNotificationTypeBookingReachedLocation)
    {
        [customNavigationBarView setTitle:NSLocalizedString(@"DRIVER REACHED",@"DRIVER REACHED")];
        [customNavigationBarView hideTitleButton:YES];
        [customNavigationBarView hideRightBarButton:YES];
    }
    else if (bookingStatus == kNotificationTypeBookingStarted)
    {
        [customNavigationBarView setTitle:NSLocalizedString(@"JOURNEY STARTED",@"JOURNEY STARTED")];
        [customNavigationBarView hideTitleButton:YES];
        [customNavigationBarView hideRightBarButton:NO];
        [customNavigationBarView addButtonRight];
        NSString *ti = [[NSUserDefaults standardUserDefaults] objectForKey:@"drivertip"];
        NSString *tipString = NSLocalizedString(@"TIP", @"TIP");
        if (ti.length != 0 && ![ti isEqualToString:tipString])
        {
            [customNavigationBarView setRightBarButtonTitle:[NSString stringWithFormat:@"%@\n%@",tipString,ti]];
        }
        else
        {
            [customNavigationBarView setRightBarButtonTitle:tipString];
        }
    }
    else
    {
        [customNavigationBarView setTitle:@""];
        [customNavigationBarView addButtonRight];
        [customNavigationBarView setRightBarButtonTitle:@""];
        [customNavigationBarView setLeftBarButtonTitle:@""];
        [customNavigationBarView hideTitleButton:NO];
        [customNavigationBarView hideLeftMenuButton:NO];
        [customNavigationBarView hideRightBarButton:NO];
        [customNavigationBarView removeRightBarButton];
    }
}

-(void)addCustomMapChangeView
{
    UIView *vb = [self.view viewWithTag:topViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:vb])
    {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 44)];
        UIButton *buttonMapTypeChange = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonMapTypeChange.frame = CGRectMake(270,self.view.frame.size.height-150,45,45);
        [buttonMapTypeChange setBackgroundImage:[UIImage imageNamed:@"map_googlehybrid"] forState:UIControlStateNormal];
        [buttonMapTypeChange setBackgroundImage:[UIImage imageNamed:@"map_googlenormal_on"] forState:UIControlStateSelected];
        [buttonMapTypeChange addTarget:self action:@selector(changeMapType:) forControlEvents:UIControlEventTouchUpInside];
        buttonMapTypeChange.selected = NO;
        [customView addSubview:buttonMapTypeChange];
        [self.view addSubview:buttonMapTypeChange];
    }
}

-(void)changeMapType:(UIButton *)sender
{
    if (sender.isSelected == NO)
    {
        sender.selected = YES;
        mapView_.mapType = kGMSTypeSatellite;
    }
    else
    {
        sender.selected = NO;
        mapView_.mapType = kGMSTypeNormal;
    }
}

-(void)addCustomLocationView
{
    UIView *vb = [self.view viewWithTag:topViewTag];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:vb])
    {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        UIView *customView = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 44)];
        customView.backgroundColor = [UIColor whiteColor];
        customView.tag = topViewTag;
        [customView setHidden:NO];
        customView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_locationbar.png"]];
        
        UIButton *buttonSearchLoc = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonSearchLoc.frame = CGRectMake(2, 44/2 - 45/2,45,45);
        buttonSearchLoc.tag = 91;
        [Helper setButton:buttonSearchLoc Text:@"" WithFont:Roboto_Regular FSize:15 TitleColor:[UIColor blackColor] ShadowColor:nil];
        [buttonSearchLoc setImage:[UIImage imageNamed:@"home_search.png"] forState:UIControlStateNormal];
        [buttonSearchLoc addTarget:self action:@selector(searchButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:buttonSearchLoc];
        
        UILabel *labelPickUp = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/2-120/2,0,120,20)];
        [Helper setToLabel:labelPickUp Text:NSLocalizedString(@"PICKUP LOCATION", @"PICKUP LOCATION") WithFont:Roboto_Regular FSize:10 Color:[UIColor colorWithRed:0.030 green:0.030 blue:0.016 alpha:1.000]];
        labelPickUp.textAlignment = NSTextAlignmentCenter;
        [customView addSubview:labelPickUp];
        
        _textFeildAddress = [[UITextField alloc]initWithFrame:CGRectMake(50,20,screenWidth-100,22)];
        _textFeildAddress.placeholder = NSLocalizedString(@"Location", @"Location");
        _textFeildAddress.tag = 101;
        _textFeildAddress.enabled = NO;
        _textFeildAddress.delegate = self;
        _textFeildAddress.textAlignment = NSTextAlignmentCenter;
        _textFeildAddress.font = [UIFont fontWithName:Roboto_Regular size:10];
        [customView addSubview:_textFeildAddress];
        
        UIButton *buttonCurrentLocation = [UIButton buttonWithType:UIButtonTypeCustom];
        buttonCurrentLocation.frame = CGRectMake(screenWidth-50, 44/2 -45/2, 45, 45);
        buttonCurrentLocation.tag = 102;
        [buttonCurrentLocation setBackgroundImage:[UIImage imageNamed:@"home_currentlocation_off.png"] forState:UIControlStateNormal];
        [buttonCurrentLocation setBackgroundImage:[UIImage imageNamed:@"home_currentlocation_on.png"] forState:UIControlStateSelected];
        [buttonCurrentLocation addTarget:self action:@selector(getCurrentLocation:) forControlEvents:UIControlEventTouchUpInside];
        [customView addSubview:buttonCurrentLocation];
        [self.view addSubview:customView];
    }
}

/**
 *  after the completion of request submission the view in again bring into picture as it was hidden during the time of creating pikupviewcontroller
 */
/*
-(void)requestSubmittedAddthelocationView
{
    UIView *LV = [self.view viewWithTag:topViewTag];
    [LV setHidden:NO];
    CGRect RTEC = LV.frame;
    RTEC.origin.y = 64;
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         LV.frame = RTEC;
                     }
                     completion:^(BOOL finished){
                     }];
    [self startAnimation];
}
*/
/**
 *  add another view that will open after clicking setpickup location on map
 */

-(void)addCustomLocationPikUpViewForConfirmScreen
{
    if(!PikUpViewForConfirmScreen)
    {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        PikUpViewForConfirmScreen = [[UIView alloc]initWithFrame:CGRectMake(0,64,screenWidth,0)];
        PikUpViewForConfirmScreen.backgroundColor = [UIColor whiteColor];
        PikUpViewForConfirmScreen.tag = 150;
        PikUpViewForConfirmScreen.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_locationbar.png"]];
        
        CGRect basketTopFrame = PikUpViewForConfirmScreen.frame;
        basketTopFrame.size.height = 44;
        
        UIImageView *imgStr = [[UIImageView alloc]initWithFrame:CGRectMake(5,0, 45, 45)];
        imgStr.image =  [UIImage imageNamed:@"conformation_icn_currentlocation"];
        [PikUpViewForConfirmScreen addSubview:imgStr];
        
        UITextField *pickUpAddress = [[UITextField alloc]initWithFrame:CGRectMake(50,2,screenWidth-90, 42)];
        pickUpAddress.placeholder = NSLocalizedString(@"Current Location", @"Current Location");
        pickUpAddress.tag = pickupAddressTag;
        pickUpAddress.enabled = NO;
        pickUpAddress.delegate = self;
        pickUpAddress.textAlignment = NSTextAlignmentCenter;
        pickUpAddress.font = [UIFont fontWithName:Roboto_Regular size:10];
        [PikUpViewForConfirmScreen addSubview:pickUpAddress];
        
        UIButton *pickUpAddressButton = [UIButton buttonWithType:UIButtonTypeCustom];
        pickUpAddressButton.frame = CGRectMake(50,2,screenWidth-90, 42);
        pickUpAddressButton.tag = 155;
        [pickUpAddressButton addTarget:self action:@selector(pickupLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        [PikUpViewForConfirmScreen addSubview:pickUpAddressButton];
        
        UIButton *addDropLocButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addDropLocButton.frame = CGRectMake(screenWidth-40,11,30,30);
        addDropLocButton.tag = 151;
        [Helper setButton:addDropLocButton Text:@"+" WithFont:Roboto_Regular FSize:25.0f TitleColor:[UIColor blackColor] ShadowColor:nil];
        [addDropLocButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
        [addDropLocButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
        [addDropLocButton addTarget:self action:@selector(plusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [PikUpViewForConfirmScreen addSubview:addDropLocButton];
        
        [UIView animateWithDuration:0.3
                              delay:0.2
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             PikUpViewForConfirmScreen.frame = basketTopFrame;
                         }
                         completion:^(BOOL finished){
                         }];
        
        UIView *cuView = [self.view viewWithTag:topViewTag];
        UITextField *textAddress = (UITextField *)[cuView viewWithTag:101];
        pickUpAddress.text =  textAddress.text;
        
        [self.view addSubview:PikUpViewForConfirmScreen];
        
        /**
         *  Remove Location View From superview Add it again if Required
         *  / ADDED IN CANCELBUTTONCLICKED AND AFTER GETTING THE RESPONSE
         */
        UIView *customLocationView = [self.view viewWithTag:topViewTag];
        [customLocationView removeFromSuperview];
    }

    [self hideBottomViews];
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:20.0f];
    [mapView_ animateWithCameraUpdate:zoomCamera];
    _isAddressManuallyPicked = YES;
    [self addCustomLocationDropOffViewForConfirmScreen];
}

-(void)hideBottomViews
{
    UIView *nowLaterView = [self.view viewWithTag:2052];
    [nowLaterView setHidden:YES];
    [scroller setHidden:YES];
    [leftArrow setHidden:YES];
    [rightArrow setHidden:YES];
}

-(void)showBottomViews
{
    UIView *nowLaterView = [self.view viewWithTag:2052];
    [nowLaterView setHidden:NO];
    [scroller setHidden:NO];
    [leftArrow setHidden:NO];
    [rightArrow setHidden:NO];
}

/**
 *  Adding View For After clicking "+" button on PickUp location Label
 */
-(void)addCustomLocationDropOffViewForConfirmScreen
{
    if(!DropOffViewForConfirmScreen)
    {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        DropOffViewForConfirmScreen = [[UIView alloc]initWithFrame:CGRectMake(0,64,screenWidth,44)];
        DropOffViewForConfirmScreen.backgroundColor = [UIColor whiteColor];
        DropOffViewForConfirmScreen.tag = 160;
        DropOffViewForConfirmScreen.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_locationbar.png"]];
        
        CGRect basketTopFrame = DropOffViewForConfirmScreen.frame;
        basketTopFrame.origin.y = 108;
        
        UIImageView *imgStr = [[UIImageView alloc]initWithFrame:CGRectMake(5,0, 45, 45)];
        imgStr.image =  [UIImage imageNamed:@"conformation_icn_dropoff"];
        [DropOffViewForConfirmScreen addSubview:imgStr];
        
        UITextField *pickUpAddress = [[UITextField alloc]initWithFrame:CGRectMake(50,2,screenWidth-90, 42)];
        pickUpAddress.placeholder = NSLocalizedString(@"Drop Off Location", @"Drop Off Location");
        pickUpAddress.tag = 161;
        pickUpAddress.enabled = NO;
        pickUpAddress.delegate = self;
        pickUpAddress.textAlignment = NSTextAlignmentCenter;
        pickUpAddress.font = [UIFont fontWithName:Roboto_Regular size:10];
        [DropOffViewForConfirmScreen addSubview:pickUpAddress];
        
        UIButton *pickUpAddressButton = [UIButton buttonWithType:UIButtonTypeCustom];
        pickUpAddressButton.frame = CGRectMake(50,2,screenWidth-90, 42);
        pickUpAddressButton.tag = 165;
        //[buttonCurrentLocation setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@""]]];
        [pickUpAddressButton addTarget:self action:@selector(pickupLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        [DropOffViewForConfirmScreen addSubview:pickUpAddressButton];
        
        
        UIButton *addDropLocButton = [UIButton buttonWithType:UIButtonTypeCustom];
        addDropLocButton.frame = CGRectMake(screenWidth-40,11,30,30);
        addDropLocButton.tag = 162;
        [Helper setButton:addDropLocButton Text:@"-" WithFont:Roboto_Regular FSize:25.0f TitleColor:[UIColor blackColor] ShadowColor:nil];
        [addDropLocButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateNormal];
        [addDropLocButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
        [addDropLocButton addTarget:self action:@selector(minusButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [DropOffViewForConfirmScreen addSubview:addDropLocButton];
        
        [UIView animateWithDuration:0.3
                              delay:0.2
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             // basketTop.frame = basketTopFrame;
                             // basketBottom.frame = basketBottomFrame;
                             DropOffViewForConfirmScreen.frame = basketTopFrame;
                         }
                         completion:^(BOOL finished){
                         }];
        [self.view addSubview:DropOffViewForConfirmScreen];
    }
}

/**
 *  Add thid view for showing current position and a Button to choose pcikup location
 *
 */
-(void)createCenterView
{
    UIView *alreadyContains = [self.view viewWithTag:curLocImgTag];
    NSArray *arr = self.view.subviews;
    
    if (![arr containsObject:alreadyContains])
    {
        UIImageView *img = [[UIImageView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2 - 25/2,(mapView_.frame.size.height/2)-22, 25, 33)];
        img.image = [UIImage imageNamed:@"map_icn_currentlocation"];
        img.tag = curLocImgTag;
        [img setHidden:NO];
        [self.view addSubview:img];
        
        UIView *customMarkerview = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-231/2,(self.view.frame.size.height/2)-60,231,43)];
        customMarkerview.tag = myCustomMarkerTag;
        [customMarkerview setUserInteractionEnabled:NO];
        [customMarkerview setHidden:NO];
        UIButton *customMarkerTopBottom = [UIButton buttonWithType:UIButtonTypeCustom];
        customMarkerTopBottom.frame = CGRectMake(0, 0, 231, 43);
        UIImage *selImage = [UIImage imageNamed:@"home_pickuplocation_bg_on.png"];
        UIImage *norImage = [UIImage imageNamed:@"home_pickuplocation_bg.png"];
        [customMarkerTopBottom setBackgroundImage:selImage forState:UIControlStateHighlighted];
        [customMarkerTopBottom setBackgroundImage:norImage forState:UIControlStateNormal];
        [customMarkerTopBottom addTarget:self action:@selector(customMarkerTopBottomClicked) forControlEvents:UIControlEventTouchUpInside];
        customMarkerTopBottom.userInteractionEnabled = YES;
        [customMarkerview addSubview:customMarkerTopBottom];
        [Helper setButton:customMarkerTopBottom Text:NSLocalizedString(@"SET PICKUP LOCATION", @"SET PICKUP LOCATION") WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        [customMarkerTopBottom setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
        [customMarkerTopBottom setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateHighlighted];
        customMarkerTopBottom.titleLabel.textAlignment = NSTextAlignmentRight;
        customMarkerTopBottom.titleEdgeInsets = UIEdgeInsetsMake(-5,20,0,0);
        
        customMarkerTopBottom.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        customMarkerTopBottom.imageEdgeInsets = UIEdgeInsetsMake(-5,200,0,0);
        
        [customMarkerTopBottom setImage:[UIImage imageNamed:@"map_arrow"] forState:UIControlStateNormal];
        [customMarkerTopBottom setImage:[UIImage imageNamed:@"map_arrow"] forState:UIControlStateHighlighted];
        
        _spinnerView = [[UIView alloc] initWithFrame:CGRectMake(5,5, 30, 30)];
        _spinnerView.backgroundColor = [UIColor clearColor];
        _spinnerView.tag = msgLabelTag;
        [customMarkerview addSubview:_spinnerView];
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(5,5, 20, 20)];
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        [activityIndicator startAnimating];
        activityIndicator.tag = 200;
        [_spinnerView addSubview:activityIndicator];
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(2,0,30, 30)];
        messageLabel.tag = 100;
        messageLabel.numberOfLines = 2;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.hidden = YES;
        [Helper setToLabel:messageLabel Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:10 Color:[UIColor whiteColor]];
        [_spinnerView addSubview:messageLabel];
        
        customMarkerview.backgroundColor = CLEAR_COLOR;
        [self.view addSubview:customMarkerview];
    }
}

-(void)startSpinitAgain
{
    [[MyAppTimerClass sharedInstance] startSpinTimer];
    [self shoActivityIndicator];
}

-(void)updateTheNearestDriverinSpinitCircle:(id)disTance
{
    [[MyAppTimerClass sharedInstance] stopSpinTimer];
    [self hideAcitvityIndicator];
    id dis = disTance;
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    NSString *str = [NSString stringWithFormat:@"%@",dis];
    [Helper setToLabel:msgLabel Text:str WithFont:HELVETICANEUE_LIGHT FSize:9 Color:[UIColor whiteColor]];
    msgLabel.numberOfLines = 0;
    msgLabel.textAlignment = NSTextAlignmentCenter;
    if (isLaterSelected)
    {
        [msgLabel setText:[NSString stringWithFormat:@""]];
    }
}

-(void)hideAcitvityIndicator
{
    UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
    [markerView setUserInteractionEnabled:YES];
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    msgLabel.hidden = NO;
    indicatorView.hidden = YES;
}

-(void)shoActivityIndicator
{
    UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
    [markerView setUserInteractionEnabled:NO];
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    msgLabel.hidden = YES;
    indicatorView.hidden = NO;
}

-(void)hideActivityIndicatorWithMessage
{
    NSArray *arr = _arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"];
    UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
    UIActivityIndicatorView *indicatorView = (UIActivityIndicatorView *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:200];
    
    if (arr.count != 0)
    {
        double DLat = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lt"] doubleValue];
        double DLong = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lg"] doubleValue];
        [self calculateTimeToDisplayOnMarker:_currentLatitude Olong:_currentLongitude Dlat:DLat Dlong:DLong];
        [Helper setToLabel:msgLabel Text:@"" WithFont:HELVETICANEUE_LIGHT FSize:9 Color:[UIColor whiteColor]];
        msgLabel.hidden = YES;
        indicatorView.hidden = NO;
    }
    else
    {
        msgLabel.hidden = NO;
        [msgLabel setText:[NSString stringWithFormat:NSLocalizedString(@"No Drivers", @"No Drivers")]];
        [self hideAcitvityIndicator];
    }
    if (isLaterSelected)
    {
        [msgLabel setText:[NSString stringWithFormat:@""]];
    }
}

- (void) calculateTimeToDisplayOnMarker:(double)originLat Olong:(double)originLong Dlat:(double)destinationLat Dlong:(double)destinationLong
{
    if (originLat == 0 || originLong == 0 || destinationLat == 0 || destinationLong == 0)
    {
        return;
    }
    
    WebServiceHandler *handler = [[WebServiceHandler alloc] init];
    NSString *strUrl = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?origins=%f,%f&destinations=%f,%f",originLat,originLong,destinationLat,destinationLong];
    NSURL *url = [NSURL URLWithString:[Helper removeWhiteSpaceFromURL:strUrl]];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    [handler placeWebserviceRequestWithString:theRequest Target:self Selector:@selector(calculatedTime:)];
}

#pragma mark Webservice Handler(Response) -

- (void)calculatedTime:(NSDictionary *)response
{
    if (!response)
    {
    }
    else
    {
        BOOL checkStatus = [self containsKey:@"status" dictionary:response[@"ItemsList"]];
        if (!checkStatus)
        {
            NSString *str = @"Limit Exceed";
            [self updateTheNearestDriverinSpinitCircle:[NSString stringWithFormat:@"%@", str]];
        }
        else
        {
            NSString *distance = response[@"ItemsList"][@"rows"][0][@"elements"][0][@"distance"][@"value"];
            distanceOfClosetCar = [NSString stringWithFormat:@"%.2f %@",[distance doubleValue]/kPMDDistanceMetric, kPMDDistanceParameter];
            int timeInSec =  [response[@"ItemsList"][@"rows"][0][@"elements"][0][@"duration"][@"value"] intValue];
            int timeInMin = [self convertTimeInMin:timeInSec];
            [self hideAcitvityIndicator];
            NSString *time = NSLocalizedString(@"MIN", @"MIN");
            time = [NSString stringWithFormat:@"%d\n%@", timeInMin, time];
            [self updateTheNearestDriverinSpinitCircle:time];
        }
    }
}

- (BOOL)containsKey: (NSString *)key dictionary:(NSDictionary *) dict
{
    BOOL retVal = 0;
    NSArray *allKeys = [dict allKeys];
    retVal = [allKeys containsObject:key];
    if (retVal)
    {
        if ([dict[@"status"] isEqualToString:@"OK"])
        {
            retVal = YES;
        }
        else
        {
            retVal = NO;
        }
    }
    return retVal;
}

-(int) convertTimeInMin:(int) timeInSec
{
    int min;
    if (timeInSec < 60 && timeInSec <= 0)
    {
        min = 1;
    }
    else if (timeInSec > 60)
    {
        min = timeInSec/60;
        int remainingTime = timeInSec%60;
        if (remainingTime < 60 && remainingTime >= 30)
        {
            min++;
        }
    }
    else
    {
        min = 1;
    }
    return min;
}

- (void)animateInBottomView
{
    if(kPMDBookLater)
    {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview = bottomView1.frame;
        frameTopview.origin.y = self.view.bounds.size.height-120;
        
        UIView *bottomView2 = [self.view viewWithTag:2052];
        CGRect frameTopview2 = bottomView2.frame;
        frameTopview2.origin.y = self.view.bounds.size.height - 50;
        
        CGRect frameLeftArrow = leftArrow.frame;
        frameLeftArrow.origin.y = self.view.bounds.size.height-95;
        
        CGRect frameRightArrow = rightArrow.frame;
        frameRightArrow.origin.y = self.view.bounds.size.height-95;

        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview;
            bottomView2.frame = frameTopview2;
            leftArrow.frame = frameLeftArrow;
            rightArrow.frame = frameRightArrow;
        }];
    }
    else
    {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview = bottomView1.frame;
        frameTopview.origin.y = self.view.bounds.size.height-70;
        
        CGRect frameLeftArrow = leftArrow.frame;
        frameLeftArrow.origin.y = self.view.bounds.size.height-95;
        
        CGRect frameRightArrow = rightArrow.frame;
        frameRightArrow.origin.y = self.view.bounds.size.height-95;

        
        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview;
            leftArrow.frame = frameLeftArrow;
            rightArrow.frame = frameRightArrow;
        }];
    }
}

- (void)animateOutBottomView
{
    if(kPMDBookLater)
    {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview1 = bottomView1.frame;
        frameTopview1.origin.y = self.view.bounds.size.height-70;
        
        UIView *bottomView2 = [self.view viewWithTag:2052];
        CGRect frameTopview2 = bottomView2.frame;
        frameTopview2.origin.y = self.view.bounds.size.height;
        
        CGRect frameLeftArrow = leftArrow.frame;
        frameLeftArrow.origin.y = self.view.bounds.size.height-45;
        
        CGRect frameRightArrow = rightArrow.frame;
        frameRightArrow.origin.y = self.view.bounds.size.height-45;
        
        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview1;
            bottomView2.frame = frameTopview2;
            leftArrow.frame = frameLeftArrow;
            rightArrow.frame = frameRightArrow;
        }];
    }
    else
    {
        UIView *bottomView1 = [self.view viewWithTag:bottomViewWithCarTag];
        CGRect frameTopview1 = bottomView1.frame;
        frameTopview1.origin.y = self.view.bounds.size.height;
        
        CGRect frameLeftArrow = leftArrow.frame;
        frameLeftArrow.origin.y = self.view.bounds.size.height-45;
        
        CGRect frameRightArrow = rightArrow.frame;
        frameRightArrow.origin.y = self.view.bounds.size.height-45;
        
        [UIView animateWithDuration:0.6f animations:^{
            bottomView1.frame = frameTopview1;
            leftArrow.frame = frameLeftArrow;
            rightArrow.frame = frameRightArrow;
        }];
    }
}


-(void)addBookLaterView
{
    UIView *vb = [self.view viewWithTag:2052];
    NSArray *arr = self.view.subviews;
    if (![arr containsObject:vb])
    {
        CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
        CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
        UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight-50, screenWidth, 50)];
        navView.backgroundColor =  [UIColor lightGrayColor];
        navView.tag = 2052;
        UIButton *_buttonNow = [UIButton buttonWithType:UIButtonTypeCustom];
        _buttonNow.frame = CGRectMake(0, 0, (screenWidth/2)-1, 50);
        
        [Helper setButton:_buttonNow Text:NSLocalizedString(@"BOOK NOW",@"BOOK NOW") WithFont:Roboto_Regular FSize:16 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        _buttonNow.tag = 2050;
        [_buttonNow setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
        
        [_buttonNow addTarget:self action:@selector(buttonNowLaterClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonNow setBackgroundImage:[UIImage imageNamed:@"now_btn_off"] forState:UIControlStateSelected];
        
        [_buttonNow setSelected:YES];
        [navView addSubview:_buttonNow];
        
        UILabel *divider = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth/2)-1, 0, 1, 50)];
        divider.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_booknowlater_divider.png"]];
        [navView addSubview:divider];
        
        UIButton *_buttonLater = [UIButton buttonWithType:UIButtonTypeCustom];
        _buttonLater.frame = CGRectMake((screenWidth/2)+1,0,screenWidth/2,50);
        [Helper setButton:_buttonLater Text:NSLocalizedString(@"BOOK LATER",@"BOOK LATER") WithFont:Roboto_Regular FSize:16 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        _buttonLater.tag = 2051;
        [_buttonLater addTarget:self action:@selector(buttonNowLaterClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_buttonLater setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
        [_buttonLater setBackgroundImage:[UIImage imageNamed:@"now_btn_off"] forState:UIControlStateSelected];
        
        [navView addSubview:_buttonLater];
        [self.view addSubview:navView];
    }
}


-(void)buttonNowLaterClicked:(UIButton *)sender
{
    UIButton *_buttonNow = (UIButton*)[(UIView*)[sender superview] viewWithTag:2050];
    UIButton *_buttonLater = (UIButton*)[(UIView*)[sender superview] viewWithTag:2051];
    
    UIButton *mBtn = (UIButton *)sender;
    if (sender.tag == 2050)
    {
        if(mBtn.selected == NO)
        {
            [_buttonNow setSelected:YES];
            [_buttonLater setSelected:NO];
            isNowSelected = YES;
            isLaterSelected = NO;
        }
    }
    else if(sender.tag == 2051)
    {
        mBtn.selected = NO;
        if(mBtn.selected == NO)
        {
            [_buttonNow setSelected:NO];
            [_buttonLater setSelected:YES];
            [self showDatePickerWithTitle:NSLocalizedString(@"LATER BOOKING", @"LATER BOOKING")];
            isNowSelected = NO;
            isLaterSelected = YES;
        }
    }
}


/**
 *  add bottom view on map for the selection of Medical specialist
 */
- (void) addBottomView
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    carTypesArrayCountValue = _arrayOfCarTypes.count;
    if (carTypesArrayCountValue == 0)
    {
        NSString *msg =  @"Sorry we are not available in your area, please contact mail@safar.world to request our service in your area.";
        
        UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil
                                                        message:msg
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [toast show];
        
        UILabel *msgLabel = (UILabel *)[[[self.view viewWithTag:myCustomMarkerTag] viewWithTag:msgLabelTag] viewWithTag:100];
        [Helper setToLabel:msgLabel Text:NSLocalizedString(@"No Drivers", @"No Drivers") WithFont:HELVETICANEUE_LIGHT FSize:9 Color:[UIColor whiteColor]];
    }
    else
    {
        NSArray *arr = self.view.subviews;
        if (![arr containsObject:scroller])
        {
            scroller.delegate = self;
            NSDictionary *type_name = [[NSDictionary alloc]init];
            allHorizontalData = [[NSMutableArray alloc]init];
            arrayOfImages = [[NSMutableArray alloc]init];
            arrayOfVehicleIcons = [[NSMutableArray alloc]init];
            arrayOfMapIcons = [[NSMutableArray alloc] init];
            for(type_name in _arrayOfCarTypes)
            {
                UIImageView *images = [[UIImageView alloc]init];
                [allHorizontalData addObject:flStrForObj([type_name objectForKey:@"type_name"])];
                [arrayOfMapIcons addObject:flStrForObj([type_name objectForKey:@"MapIcon"])];
                [arrayOfImages addObject:images];
                UIImageView *images1 = [[UIImageView alloc]init];
                [arrayOfVehicleIcons addObject:images1];
            }
            if(scroller)
            {
                scroller = nil;
            }
            if (kPMDBookLater)
            {
                scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0,screenHeight-120, screenWidth,70)];
            }
            else
            {
                scroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0,screenHeight-70, screenWidth, 70)];
            }
            scroller.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"home_last_white_background"]];
            scroller.tag = bottomViewWithCarTag;
            scroller.delegate = self;
            [scroller setScrollEnabled:YES];
            scroller.hidden = NO;
            scroller.showsHorizontalScrollIndicator = NO;
            [scroller setContentSize:CGSizeMake(allHorizontalData.count*screenWidth/3,70)];
            [self.view addSubview:scroller];

            if (_arrayOfCarTypes.count > 3)
            {
                leftArrow = nil;
                rightArrow = nil;
                leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
                leftArrow = [[UIButton alloc]initWithFrame:CGRectMake(6,screenHeight-95,20, 20)];
                rightArrow = [[UIButton alloc]initWithFrame:CGRectMake(screenWidth-26, screenHeight-95, 20, 20)];
                [self.view addSubview:leftArrow];
                [self.view addSubview:rightArrow];
                
                [leftArrow setImage:[UIImage imageNamed:@"home_left_side_back_icon_off"] forState:UIControlStateNormal];
                [leftArrow setImage:[UIImage imageNamed:@"home_left_side_back_icon_on"] forState:UIControlStateHighlighted];
                [rightArrow setImage:[UIImage imageNamed:@"home_right_side_next_icon_off"] forState:UIControlStateNormal];
                [rightArrow setImage:[UIImage imageNamed:@"home_right_side_next_icon_on"] forState:UIControlStateHighlighted];
                [leftArrow addTarget:self action:@selector(leftArrowClicked) forControlEvents:UIControlEventTouchUpInside];
                [rightArrow addTarget:self action:@selector(rightArrowClicked) forControlEvents:UIControlEventTouchUpInside];
                rightArrow.tag = rightArrowTag;
                leftArrow.tag = leftArrowTag;
            }
            
            arrayOfButtons = [[NSMutableArray alloc]init];
            arrayOfMapImages = [[NSMutableArray alloc]init];
            for (int i=0; i < _arrayOfCarTypes.count; i++)
            {
                UIButton *buttonCategory = [UIButton buttonWithType:UIButtonTypeCustom];
                buttonCategory.frame = CGRectMake(screenWidth/3*i,0,screenWidth/3,70);
                buttonCategory.tag=201+i+1;
                [arrayOfButtons addObject:buttonCategory];
                if (carTypesForLiveBooking <= _arrayOfCarTypes.count)
                {
                    if(i == carTypesForLiveBooking-1)
                    {
                        [buttonCategory setSelected:YES];
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
                        scroller.contentOffset = oldOffset;
                    }
                }
                else if (i == 0)
                {
                    [buttonCategory setSelected:YES];
                    carTypesForLiveBooking = 1;
                    carTypesForLiveBookingServer = [_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
                }
                else
                {
                    return;
                }
                
                [Helper setButton:buttonCategory Text:[NSString stringWithFormat:@"%@",[allHorizontalData objectAtIndex:i]] WithFont:Roboto_Regular FSize:10 TitleColor:UIColorFromRGB(0x616161) ShadowColor:nil];
                
                [buttonCategory setTitleColor:UIColorFromRGB(0xef4836) forState:UIControlStateSelected];
                [buttonCategory addTarget:self action:@selector(vehicleButtonAction:) forControlEvents:UIControlEventTouchUpInside];
                [scroller addSubview:buttonCategory];
                [scroller bringSubviewToFront:buttonCategory];
            }
            
            [self downloadUnSelectedVehicleTypeImages];
            [self downloadSelectedVehicleTypeImages];
            [self downloadMapImages];
            [self.view bringSubviewToFront:leftArrow];
            [self.view bringSubviewToFront:rightArrow];
            [leftArrow setHidden:YES];
            [self addCustomMarkerFor];
            if(_arrayOfCarTypes.count > 3)
            {
                [rightArrow setHidden:NO];
            }
            else
            {
                [rightArrow setHidden:YES];
            }
        }
    }
}



-(void)leftArrowClicked
{
    //    NSInteger x = scroller.contentOffset.x;
    //    scroller.contentOffset = CGPointMake(x-108,70);
}

-(void)rightArrowClicked
{
    //    NSInteger x = scroller.contentOffset.x;
    //    scroller.contentOffset = CGPointMake(x+108,70);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;

    NSInteger count = (_arrayOfCarTypes.count - 3) * screenWidth/3;
    if(scrollView.contentOffset.x == 0 && scrollView.contentOffset.x < count)
    {
        [rightArrow setHidden:NO];
        [leftArrow setHidden:YES];
    }
    else if(scrollView.contentOffset.x == count )
    {
        [rightArrow setHidden:YES];
        [leftArrow setHidden:NO];
    }
    else
    {
        [rightArrow setHidden:NO];
        [leftArrow setHidden:NO];
    }
}

-(void)downloadMapImages
{
    arrayOfMapImages = [[NSMutableArray alloc]init];
    for (int i=0; i < _arrayOfCarTypes.count; i++)
    {
        NSString *mapImageURL = flStrForStr(arrayOfMapIcons[i]);
        if (mapImageURL.length)
        {
            mapImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,arrayOfMapIcons[i]];
            [arrayOfImages[i] sd_setImageWithURL:[NSURL URLWithString:mapImageURL]
                                placeholderImage:nil
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                           
                                           if(!error && image)
                                           {
                                               image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(50, 50)]];
                                               [arrayOfMapImages addObject:image];
                                           }
                                           else
                                           {
                                               [arrayOfMapImages addObject:[UIImage imageNamed:@"markerForNoImage"]];
                                           }
                                       }];
        }
    }
}

-(void)downloadSelectedVehicleTypeImages
{    
    for (int i=0; i < _arrayOfCarTypes.count; i++)
    {
        UIButton *buttonCategory = arrayOfButtons[i];
        NSString *selectedVehicleImageURL = flStrForStr(_arrayOfCarTypes[i][@"vehicle_img"]);
        if (selectedVehicleImageURL.length)
        {
            selectedVehicleImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,_arrayOfCarTypes[i][@"vehicle_img"]];
            [arrayOfVehicleIcons[i] sd_setImageWithURL:[NSURL URLWithString:selectedVehicleImageURL]
                                      placeholderImage:nil
                                             completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                                 
                                                 if(!error)
                                                 {
                                                     image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(40, 40)]];
                                                     [buttonCategory setImage:image forState:UIControlStateSelected];
                                                     
                                                     CGFloat spacing = 6.0;
                                                     CGSize imageSize = buttonCategory.imageView.frame.size;
                                                     buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(- (imageSize.height + spacing), - imageSize.width, 0.0, 0.0);
                                                     CGSize titleSize = buttonCategory.titleLabel.frame.size;
                                                     buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, - (titleSize.height + spacing), -titleSize.width);
                                                 }
                                             }];
        }
    }
}

-(void)downloadUnSelectedVehicleTypeImages
{
    for (int i=0; i < _arrayOfCarTypes.count; i++)
    {
        UIButton *buttonCategory = arrayOfButtons[i];
        NSString *unSelectedVehicleImageURL = flStrForStr(_arrayOfCarTypes[i][@"vehicle_img_off"]);
        if (unSelectedVehicleImageURL.length)
        {
            NSString *unSelectedVehicleImageURL = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage,_arrayOfCarTypes[i][@"vehicle_img_off"]];
            [buttonCategory.imageView sd_setImageWithURL:[NSURL URLWithString:unSelectedVehicleImageURL]
                                        placeholderImage:nil
                                               completed:^(UIImage *image,NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                                                   
                                                   if(!error)
                                                   {
                                                       image = [self imageWithImage:image scaledToSize:[self makeSize:image.size fitInSize:CGSizeMake(40, 40)]];
                                                       [buttonCategory setImage:image forState:UIControlStateNormal];
                                                       
                                                       CGFloat spacing = 6.0;
                                                       CGSize imageSize = buttonCategory.imageView.frame.size;
                                                       buttonCategory.titleEdgeInsets = UIEdgeInsetsMake(- (imageSize.height + spacing), - imageSize.width, 0.0, 0.0);
                                                       CGSize titleSize = buttonCategory.titleLabel.frame.size;
                                                       buttonCategory.imageEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, - (titleSize.height + spacing), -titleSize.width);
                                                   }
                                               }];
        }
    }
}

- (CGSize)makeSize:(CGSize)originalSize fitInSize:(CGSize)boxSize
{
    float widthScale = 0;
    float heightScale = 0;
    widthScale = boxSize.width/originalSize.width;
    heightScale = boxSize.height/originalSize.height;
    float scale = MIN(widthScale, heightScale);
    CGSize newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale);
    return newSize;
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(void)vehicleButtonAction:(UIButton *)sender
{
    UIButton *mBtn = (UIButton *)sender;
    if(mBtn.selected == YES)
    {
        [self handleSingleTap:nil];
    }
    for (UIView *button in mBtn.superview.subviews)
    {
        if ([button isKindOfClass:[UIButton class]])
        {
            [(UIButton *)button setSelected:NO];
        }
    }
    
    mBtn.selected = YES;
    carTypesForLiveBooking = mBtn.tag - 201;
    carTypesForLiveBookingServer = [_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
    [self changeMapMarker:carTypesForLiveBooking-1];
    [self publishPubNubStream];
}


/**
 *  Scale Label Text
 *
 *  @param  It will scale the text of the Label
 *
 *  @return returns void and accept integer value
 */

-(void)scaleLabelText:(int)labeltag {
    
    UILabel *lbl = (UILabel *)[[self.view viewWithTag:bottomViewWithCarTag]viewWithTag:labeltag];
    lbl.font = [UIFont fontWithName:Roboto_Bold size:10];
    lbl.transform = CGAffineTransformScale(lbl.transform, 0.25, 0.25);
    
    [UIView animateWithDuration:1.0 animations:^{
        lbl.transform = CGAffineTransformScale(lbl.transform, 4, 4);
    }];
}

/**
 *  Touch event on car types
 *
 *  @param typeCar <#typeCar description#>
 */

- (void)handleSingleTap:(UIGestureRecognizer *)recognizer
{
    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
    v.hidden = YES;
    CALayer *mainViewLayer = self.view.layer;
    UIViewAnimationOptions options = UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction;
    [UIView animateWithDuration:0.5 delay:0.0 options:options animations:^
     {
         mainViewLayer.transform = CATransform3DScale(mainViewLayer.transform, 0.9, (self.view.frame.size.height - 40) / self.view.frame.size.height, 1);
     } completion:^(BOOL finished) {
     }];
    
    NSInteger type_id = [_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"]integerValue];
    [self hideBottomViews];
    [self addCustomCarButtonPressedView:type_id];
    
}
-(void)clearTheMapBeforeChagingTheCarTypes
{
    [_allMarkers removeAllObjects];
    [_arrayOfDriverEmail removeAllObjects];
    _arrayOfDriverEmail = nil;
    [mapView_ clear];
}

-(void)changeMapMarker:(NSInteger)type
{
    [_allMarkers removeAllObjects];
    [mapView_ clear];
    drivers = [[NSMutableArray alloc] init];
    if (_arrayOfMastersAround.count < type && _arrayOfCarTypes.count < type)
    {
        carTypesForLiveBooking = 1;
        return;
    }
    [drivers addObjectsFromArray:_arrayOfMastersAround[type][@"mas"]];
    
    if (drivers.count > 0)
    {
        if (drivers.count >3)
        {
            GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel+1];
            [mapView_ animateWithCameraUpdate:zoomCamera];
        }
        else
        {
            GMSCameraUpdate *zoomCamera = [GMSCameraUpdate zoomTo:mapZoomLevel];
            [mapView_ animateWithCameraUpdate:zoomCamera];
        }
        
        double DLat = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lt"] doubleValue];
        double DLong = [_arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"][0][@"lg"] doubleValue];
        [self calculateTimeToDisplayOnMarker:_currentLatitude Olong:_currentLongitude Dlat:DLat Dlong:DLong];
        [self addCustomMarkerFor];
    }
    else
    {
        [self hideActivityIndicatorWithMessage];
    }
}

- (void)handleSingleTap
{
    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
    v.hidden = YES;
    CALayer *mainViewLayer = self.view.layer;
    
    UIViewAnimationOptions options = UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction;
    [UIView animateWithDuration:0.5 delay:0.0 options:options animations:^
     {
         mainViewLayer.transform = CATransform3DScale(mainViewLayer.transform, 0.9, (self.view.frame.size.height - 40) / self.view.frame.size.height, 1);
         
     } completion:^(BOOL finished) {
         
     }];
    
    NSInteger type_id = [_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"]integerValue];
    [self addCustomCarButtonPressedView:type_id];
}


-(void)customBottomViewhandleTap:(UIGestureRecognizer *)gestureRecognizer {
    
    UIView *piece = [gestureRecognizer view];
    UIImageView *imgView = (UIImageView *)[piece viewWithTag:10000];
    CGPoint currentlocation = [gestureRecognizer locationInView:self.view];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        
    }
    else if([gestureRecognizer state] == UIGestureRecognizerStateEnded)
    {
        CGFloat delta_x = 40;
        switch (carTypesArrayCountValue) {
            case 2:
            {
                if (currentlocation.x > 150) {
                    if (carTypesForLiveBooking != 2) {
                        delta_x = 270;//location.x - previousLocation.x;
                        carTypesForLiveBooking =  2;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                        imgView.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self scaleLabelText:181];
                        [self changeMapMarker:1];
                        [self publishPubNubStream];
                    }
                    else{
                        delta_x = 270;
                    }
                }
                else {
                    if (carTypesForLiveBooking != 1) {
                        delta_x = 40;//location.x - previousLocation.x;
                        carTypesForLiveBooking =  1;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                        imgView.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        [self scaleLabelText:180];
                        [self changeMapMarker:0];
                        [self publishPubNubStream];
                    }
                    else {
                        delta_x = 40;
                    }
                }
            }
                break;
                
            case 3:
            {
                if (currentlocation.x > 220) {
                    if (carTypesForLiveBooking != 3) {
                        delta_x = 270;//location.x - previousLocation.x;
                        carTypesForLiveBooking =  3;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[2][@"type_id"] integerValue];
                        imgView.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self scaleLabelText:182];
                        [self changeMapMarker:2];
                        [self publishPubNubStream];
                    }
                    else{
                        delta_x = 270;
                    }
                }
                else if (currentlocation.x > 100 && currentlocation.x < 220) {
                    
                    if (carTypesForLiveBooking != 2) {
                        delta_x = 160;
                        carTypesForLiveBooking =  2;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                        imgView.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        [self scaleLabelText:181];
                        [self changeMapMarker:1];
                        [self publishPubNubStream];
                    }
                    else {
                        delta_x = 160;
                    }
                }
                else {
                    if (carTypesForLiveBooking != 1) {
                        delta_x = 40;
                        carTypesForLiveBooking =  1;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                        imgView.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        [self scaleLabelText:180];
                        [self changeMapMarker:0];
                        [self publishPubNubStream];
                    }
                    else {
                        delta_x = 40;
                    }
                }
            }
                break;
                
            case 4:
            {
                if (currentlocation.x > 220) {
                    
                    if (carTypesForLiveBooking != 4) {
                        delta_x = 270;
                        carTypesForLiveBooking =  4;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[3][@"type_id"] integerValue];
                        [self scaleLabelText:183];
                        [self changeMapMarker:3];
                        [self publishPubNubStream];
                    }
                    else {
                        delta_x = 270;
                        carTypesForLiveBooking =  4;
                    }
                }
                else if (currentlocation.x > 40 && currentlocation.x < 80 ) {
                    
                    if (carTypesForLiveBooking != 1) {
                        delta_x = 40;//location.x - previousLocation.x;
                        carTypesForLiveBooking =  1;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                        [self scaleLabelText:180];
                        [self changeMapMarker:0];
                        [self publishPubNubStream];
                    }
                    else{
                        delta_x = 40;
                        carTypesForLiveBooking =  1;
                    }
                }
                else if (currentlocation.x > 80 && currentlocation.x < 160 ) {
                    
                    if (carTypesForLiveBooking != 2) {
                        delta_x = 130;
                        carTypesForLiveBooking =  2;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                        [self scaleLabelText:181];
                        [self changeMapMarker:1];
                        [self publishPubNubStream];
                    }
                    else {
                        delta_x = 130;
                        carTypesForLiveBooking =  2;
                    }
                }
                else if (currentlocation.x > 160 && currentlocation.x < 220 ) {
                    
                    
                    if (carTypesForLiveBooking != 3) {
                        delta_x = 200;
                        carTypesForLiveBooking =  3;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[2][@"type_id"] integerValue];
                        [self scaleLabelText:182];
                        [self changeMapMarker:2];
                        [self publishPubNubStream];
                    }
                    else {
                        delta_x = 200;
                        carTypesForLiveBooking =  3;
                    }
                }
            }
                break;
                
            default:
                break;
        }
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             [imgView setCenter:CGPointMake(delta_x,44)];
                         }
                         completion:^(BOOL finished){
                         }];
    }
}


- (void)handlePan:(UIPanGestureRecognizer *)gestureRecognizer
{
    UIImageView *piece = (UIImageView *)[gestureRecognizer view];
    
    CGPoint currentlocation = [gestureRecognizer locationInView:self.view];
    
    if ([gestureRecognizer state] == UIGestureRecognizerStateBegan || [gestureRecognizer state] == UIGestureRecognizerStateChanged) {
        
        CGPoint translation = [gestureRecognizer translationInView:gestureRecognizer.view];
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             [piece setCenter:CGPointMake([piece center].x + translation.x,33)];
                         }
                         completion:^(BOOL finished){
                         }];
        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
    }
    else if([gestureRecognizer state] == UIGestureRecognizerStateEnded)
    {
        CGFloat delta_x = 40;
        switch (carTypesArrayCountValue) {
            case 2:
            {
                if (currentlocation.x > 150) {
                    
                    if ([piece center].x >= 150 && carTypesForLiveBooking != 2) {
                        delta_x = 270;
                        carTypesForLiveBooking =  2;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self scaleLabelText:181];
                        [self changeMapMarker:1];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else {
                        delta_x = 270;
                        [self scaleLabelText:181];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                    }
                }
                else {
                    
                    if ([piece center].x <= 150 && carTypesForLiveBooking != 1) {
                        delta_x = 40;//location.x - previousLocation.x;
                        carTypesForLiveBooking =  1;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        [self scaleLabelText:180];
                        [self changeMapMarker:0];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else{
                        delta_x = 40;
                        [self scaleLabelText:180];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                    }
                }
            }
                break;
                
            case 3:
            {
                if (currentlocation.x > 220) {
                    
                    if ([piece center].x >= 200 && carTypesForLiveBooking != 3) {
                        delta_x = 270;
                        carTypesForLiveBooking =  3;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[2][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self changeMapMarker:2];
                        [self scaleLabelText:182];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else {
                        delta_x = 270;
                        carTypesForLiveBooking =  3;
                        [self scaleLabelText:182];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[2][@"type_id"] integerValue];
                    }
                }
                else if (currentlocation.x > 40 && currentlocation.x < 100 ) {
                    
                    
                    if ([piece center].x >= 60 && carTypesForLiveBooking != 1) {
                        delta_x = 40;//location.x - previousLocation.x;
                        carTypesForLiveBooking =  1;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        [self changeMapMarker:0];
                        [self scaleLabelText:180];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else{
                        delta_x = 160;
                        carTypesForLiveBooking =  2;
                        [self scaleLabelText:181];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                    }
                }
                else if (currentlocation.x > 100 && currentlocation.x < 220 ) {
                    
                    
                    if ([piece center].x >= 170 && carTypesForLiveBooking != 2) {
                        delta_x = 160;
                        carTypesForLiveBooking =  2;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self changeMapMarker:1];
                        [self scaleLabelText:181];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else {
                        delta_x = 40;
                        carTypesForLiveBooking =  1;
                        [self scaleLabelText:180];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                    }
                }
            }
                break;
                
            case 4:
            {
                if (currentlocation.x > 220) {
                    
                    if (carTypesForLiveBooking != 4) {
                        delta_x = 270;
                        carTypesForLiveBooking =  4;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[3][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self changeMapMarker:3];
                        [self scaleLabelText:183];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else {
                        delta_x = 270;
                        carTypesForLiveBooking =  4;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[3][@"type_id"] integerValue];
                        [self scaleLabelText:183];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                    }
                }
                else if (currentlocation.x > 160 && currentlocation.x < 220 ) {
                    
                    if ([piece center].x >= 130 && carTypesForLiveBooking != 3) {
                        delta_x = 200;
                        carTypesForLiveBooking =  3;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[2][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self scaleLabelText:182];
                        [self changeMapMarker:2];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else {
                        delta_x = 200;
                        carTypesForLiveBooking =  3;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[2][@"type_id"] integerValue];
                        [self scaleLabelText:182];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                    }
                }
                else if (currentlocation.x > 80 && currentlocation.x < 160 ) {
                    
                    if ([piece center].x >= 60 && carTypesForLiveBooking != 2) {
                        delta_x = 130;
                        carTypesForLiveBooking =  2;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                        [self changeMapMarker:1];
                        [self scaleLabelText:181];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else {
                        delta_x = 130;
                        carTypesForLiveBooking =  2;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[1][@"type_id"] integerValue];
                        [self scaleLabelText:181];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon_on"];
                    }
                }
                else if (currentlocation.x < 80 ) {
                    if ([piece center].x >= 60 && carTypesForLiveBooking != 1) {
                        delta_x = 40;//location.x - previousLocation.x;
                        carTypesForLiveBooking =  1;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                        [self scaleLabelText:180];
                        [self changeMapMarker:0];
                        [self publishPubNubStream];
                        [gestureRecognizer setTranslation:CGPointZero inView:gestureRecognizer.view];
                    }
                    else{
                        delta_x = 40;
                        carTypesForLiveBooking =  1;
                        carTypesForLiveBookingServer = [_arrayOfCarTypes[0][@"type_id"] integerValue];
                        [self scaleLabelText:180];
                        piece.image = [UIImage imageNamed:@"home_carinfo_caricon"];
                    }
                }
                
            }
                break;
                
            default:
                break;
        }
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options: UIViewAnimationOptionCurveLinear
                         animations:^{
                             [piece setCenter:CGPointMake(delta_x,44)];
                         }
                         completion:^(BOOL finished){
                         }];
    }
}

-(void)addCustomCarButtonPressedView:(NSUInteger)typeCar
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView* coverView = [[UIView alloc] initWithFrame:screenRect];
    coverView.tag = 300;
    coverView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardetails_screen_bg-568h"]];
    [self.view addSubview:coverView];
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [coverView addGestureRecognizer:tapRecognizer];
    
    
    UIView *centerView = [[UIView alloc]initWithFrame:CGRectMake(23, screenHeight/2-50,screenWidth-50,106)];
    centerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardetails_bg"]];
    centerView.tag = 600;
    [coverView addSubview:centerView];
    
    UILabel *centerLabel1 = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth-60, 50)];
    centerLabel1.textAlignment = NSTextAlignmentCenter;
    
    NSMutableArray *carTypes = [_arrayOfCarTypes mutableCopy];
    NSDictionary *dict = [[NSDictionary alloc]init];
    
    for(dict in carTypes)
    {
        if(typeCar == [dict[@"type_id"]integerValue])
        {
            break;
        }
    }
    
    float surgePrice1;
    NSString *str = flStrForStr(dict[@"surg_price"]);
    if(str.length)
    {
        surgePrice1 = [str floatValue];
    }
    else
    {
        surgePrice1 = 1;
    }
    NSString *curreny =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dict[@"basefare"]) floatValue]*surgePrice1];
    NSString *base_fare = NSLocalizedString(@"BASE FARE", @"BASE FARE");
    [Helper setToLabel:centerLabel1 Text:[NSString stringWithFormat:@"%@ %@",curreny, base_fare] WithFont:Roboto_Light FSize:15 Color:UIColorFromRGB(0xffffff)];
    [centerView addSubview:centerLabel1];
    UILabel *centerLabel3 = [[UILabel alloc]initWithFrame:CGRectMake(5,50,screenWidth-57,6)];
    centerLabel3.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"new_cardetails_divider"]];
    [centerView addSubview:centerLabel3];
    
    UILabel *centerLabel2 = [[UILabel alloc]initWithFrame:CGRectMake(0,56,screenWidth-60,50)];
    centerLabel2.textAlignment = NSTextAlignmentCenter;
    NSString *price_per_min =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dict[@"price_per_min"]) floatValue]*surgePrice1];
    NSString *price_per_km =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dict[@"price_per_km"]) floatValue]*surgePrice1];
    NSString *str1 = NSLocalizedString(@"Min and", @"Min and");
    [Helper setToLabel:centerLabel2 Text:[NSString stringWithFormat:@"%@ / %@ %@ / %@",price_per_min, str1, price_per_km, kPMDDistanceParameter] WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0xffffff)];
    [centerView addSubview:centerLabel2];
    
    
    UIView *bottomView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight, screenWidth,100)];
    bottomView.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:1.0f];
    CGRect basketTopFrame = bottomView.frame;
    basketTopFrame.origin.y = screenHeight -100;
    
    UILabel *carName = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 26)];
    carName.text = dict[@"type_name"];
    [Helper setToLabel:carName Text:carName.text WithFont:Roboto_Bold FSize:14 Color:UIColorFromRGB(0x333333)];
    carName.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardetails_carname.png"]];
    carName.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:carName];
    
    UIImage *img = [UIImage imageNamed:@"cardetails_crossbutton"];
    UIImageView *cross = [[UIImageView alloc]initWithFrame:CGRectMake(screenWidth-30,0,28, 28)];
    [bottomView addSubview:cross];
    cross.image = img;
    
    UIView *BSV = [[UIView alloc]initWithFrame:CGRectMake(0,26, screenWidth, 74)];
    [bottomView addSubview:BSV];
    BSV.backgroundColor = [UIColor whiteColor];
    
    UILabel *eta = [[UILabel alloc]initWithFrame:CGRectMake(0,3,(screenWidth/3)-1.5,20)];
    [Helper setToLabel:eta Text:NSLocalizedString(@"DISTANCE", @"DISTANCE") WithFont:Roboto_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
    eta.textAlignment = NSTextAlignmentCenter;
    [BSV addSubview:eta];
    
    UILabel *minFare = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth/3)-1.5,3,(screenWidth/3)-1.5,20)];
    [Helper setToLabel:minFare Text:NSLocalizedString(@"MIN FARE", @"MIN FARE") WithFont:Roboto_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
    minFare.textAlignment = NSTextAlignmentCenter;
    [BSV addSubview:minFare];
    
    UILabel *maxSize = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth*2/3)-1.5,3,(screenWidth/3)-1.5,20)];
    [Helper setToLabel:maxSize Text:NSLocalizedString(@"MAX SIZE", @"MAX SIZE") WithFont:Roboto_Bold FSize:14 Color:UIColorFromRGB(0x666666)];
    maxSize.textAlignment = NSTextAlignmentCenter;
    [BSV addSubview:maxSize];
    
    UILabel *etafield = [[UILabel alloc]initWithFrame:CGRectMake(0,15,(screenWidth/3)-1.5,54)];
    NSArray *arr = _arrayOfMastersAround[carTypesForLiveBooking-1][@"mas"];
    etafield.textAlignment = NSTextAlignmentCenter;
    
    if (arr.count != 0)
    {
        [Helper setToLabel:etafield Text:distanceOfClosetCar WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0x333333)];
    }
    else
    {
        [Helper setToLabel:etafield Text:NSLocalizedString(@"No Cabs", @"No Cabs") WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0x333333)];
    }
    [BSV addSubview:etafield];
    
    
    UILabel *Hline = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth/3)-1.5,15,1,45)];
    [BSV addSubview:Hline];
    Hline.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardetails_carinfo_divider.png"]];
    
    
    UILabel *minFareField = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth/3)-0.5,15,(screenWidth/3),54)];
    NSString *txt =  [PatientGetLocalCurrency getCurrencyLocal:[flStrForObj(dict[@"min_fare"]) floatValue]*surgePrice1];
    
    [Helper setToLabel:minFareField Text:txt WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0x333333)];
    minFareField.textAlignment = NSTextAlignmentCenter;
    [BSV addSubview:minFareField];
    
    UILabel *Hline1 = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth*2/3),15,1,45)];
    [BSV addSubview:Hline1];
    Hline1.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cardetails_carinfo_divider.png"]];
    
    
    UILabel *maxSizefield = [[UILabel alloc]initWithFrame:CGRectMake((screenWidth*2/3)+1.5,15,(screenWidth/3),58)];
    NSString *people = NSLocalizedString(@"People", @"People");
    maxSizefield.text = [NSString stringWithFormat:@"%@ %@",dict[@"max_size"], people];
    [Helper setToLabel:maxSizefield Text:maxSizefield.text WithFont:Roboto_Light FSize:14 Color:UIColorFromRGB(0x333333)];
    maxSizefield.textAlignment = NSTextAlignmentCenter;
    [BSV addSubview:maxSizefield];
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         bottomView.frame = basketTopFrame;
                     }
                     completion:^(BOOL finished){
                     }];
    
    [coverView addSubview:bottomView];
    
    if (surgePrice1 > 1)
    {
        CGFloat yValue = ((bottomView.frame.origin.y - (centerView.frame.origin.y + centerView.frame.size.height))-60)/2+(centerView.frame.origin.y + centerView.frame.size.height);
        UIView *centerView1 = [[UIView alloc]initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2-30, yValue,60,60)];
        centerView1.backgroundColor = UIColorFromRGB(0x333333);
        centerView1.layer.cornerRadius = 30.0f;
        centerView1.layer.masksToBounds = YES;
        centerView1.tag = 601;
        [coverView addSubview:centerView1];
        
        UILabel *centerSurgePrice = [[UILabel alloc]initWithFrame:CGRectMake(0,0,60,60)];
        centerSurgePrice.layer.cornerRadius = 30.0f;
        centerSurgePrice.layer.masksToBounds = YES;
        centerSurgePrice.backgroundColor = UIColorFromRGB(0x333333);
        [Helper setToLabel:centerSurgePrice Text:[NSString stringWithFormat:@"%0.1fx", [ dict[@"surg_price"] floatValue]] WithFont:Roboto_Bold FSize:20 Color:[UIColor whiteColor]];
        centerSurgePrice.textAlignment = NSTextAlignmentCenter;
        [centerView1 addSubview:centerSurgePrice];
        
        CGFloat yValue1 = ((bottomView.frame.origin.y - (centerView.frame.origin.y + centerView.frame.size.height))-60)/2+(centerView.frame.origin.y + centerView.frame.size.height)-20;
        
        UIView *centerView2 = [[UIView alloc]initWithFrame:CGRectMake(screenWidth/2-30, yValue1, 60, 15)];
        centerView2.backgroundColor = UIColorFromRGB(0x333333);
        centerView2.tag = 603;
        [coverView addSubview:centerView2];
        UILabel *surge = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 15)];
        [Helper setToLabel:surge Text:NSLocalizedString(@"SURGE", @"SURGE") WithFont:Roboto_Bold FSize:13 Color:[UIColor whiteColor]];
        surge.textAlignment = NSTextAlignmentCenter;
        [centerView2 addSubview:surge];
        
    }
}

-(void)addCustomViewForShowingCardFareWithAnimation
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;

    //Adding View For animation
    UIView *basketBottom = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight, screenWidth, 150)];
    basketBottom.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.5];;
    [self.view addSubview:basketBottom];
    basketBottom.tag = 499;
    
    //changing frame for anoimation
    CGRect basketTopFrame = basketBottom.frame;
    basketTopFrame.origin.y = screenHeight - 150;//-basketTopFrame.size.height;
    
    CGRect basketBottomFrame = basketBottom.frame;
    basketBottomFrame.origin.y = screenHeight;
    
    //REquest bottom
    UIButton *requestButton = [UIButton buttonWithType:UIButtonTypeCustom];
    requestButton.frame = CGRectMake(10, 100, screenWidth-20, 40);
    NSString *msgText = NSLocalizedString(@"REQUEST ", @"REQUEST ");
    
    if (isNowSelected == YES)
    {
        NSString *str = flStrForStr(_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_name"]);
        msgText = [msgText stringByAppendingString:str];
        msgText = [msgText uppercaseString];
    }
    else if (isLaterSelected == YES)
    {
        msgText = laterSelectedDate;
        msgText = [msgText stringByAppendingString:@"  "];
        msgText = [msgText stringByAppendingString:_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_name"]];
        msgText = [msgText uppercaseString];
    }
    
    [Helper setButton:requestButton Text:msgText WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    
    [requestButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
    [requestButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateSelected];
    [requestButton setBackgroundImage:[UIImage imageNamed:@"conformation_btn_pickup_bg.png"] forState:UIControlStateNormal];
    [requestButton setBackgroundImage:[UIImage imageNamed:@"conformation_btn_pickup_bg_on.png"] forState:UIControlStateSelected];
    [requestButton addTarget:self action:@selector(sendAppointmentRequestForLiveBooking) forControlEvents:UIControlEventTouchUpInside];
    requestButton.tag = 500;
    [self startBackgroundTask];
    [basketBottom addSubview:requestButton];
    
    UIView *customBottomView = [[UIView alloc]initWithFrame:CGRectMake(10,basketBottom.frame.size.height-150,screenWidth-20,90)];
    customBottomView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"conformation_farequotebtn_bg.png"]];
    customBottomView.tag = 501;
    [basketBottom addSubview:customBottomView];
    
    UIButton *CardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CardButton.frame = CGRectMake(10,5,screenWidth-40,31);
    CardButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [CardButton setTitleColor:UIColorFromRGB(0x000000) forState:UIControlStateNormal];
    [CardButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
    [CardButton setBackgroundImage:[UIImage imageNamed:@"conformation_cardinfo_header.png"] forState:UIControlStateNormal];
    
    if(kPMDPaymentType)
    {
        [CardButton addTarget:self action:@selector(showPaymentTypeWithTitle) forControlEvents:UIControlEventTouchUpInside];
    }
    else if(kPMDCardOrCash)
    {
        [CardButton addTarget:self action:@selector(goToPaymentController) forControlEvents:UIControlEventTouchUpInside];
    }
    
    CardButton.tag = 502;
    
    [customBottomView addSubview:CardButton];
    //FAirView
    
    if(kPMDPaymentType)
    {
        appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
        context = [appDelegate managedObjectContext];
        if (context != nil)
        {
            arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
            if(arrDBResult.count != 0){
                Entity *fav = arrDBResult[0];
                NSString *imageName = [self setPlaceholderToCardType:fav.cardtype];
                [CardButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
                [CardButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, -80.0, 0.0, 0.0)];
                [CardButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0,-20.0, 0.0, 0.0)];
                NSString *str1 = NSLocalizedString(@"PERSONAL ****", @"PERSONAL ****");
                [Helper setButton:CardButton Text:[NSString stringWithFormat:@"%@%@",str1,fav.last4] WithFont:Roboto_Light FSize:13 TitleColor:[UIColor whiteColor] ShadowColor:nil];
                cardId = fav.idCard;
                paymentTypesForLiveBooking = 1;
                [CardButton setSelected:YES];
            }
            else {
                [Helper setButton:CardButton Text:NSLocalizedString(@"CASH", @"CASH") WithFont:Roboto_Light FSize:13 TitleColor:[UIColor whiteColor] ShadowColor:nil];
                paymentTypesForLiveBooking = 2;
            }
        }
    }
    else
    {
        if(kPMDCardOrCash) {
            
            appDelegate = (PatientAppDelegate*)[UIApplication sharedApplication].delegate;
            context = [appDelegate managedObjectContext];
            if (context != nil)
            {
                arrDBResult = [[NSMutableArray alloc] initWithArray:[Database getCardDetails]];
                if(arrDBResult.count != 0)
                {
                    Entity *fav = arrDBResult[0];
                    NSString *imageName = [self setPlaceholderToCardType:fav.cardtype];
                    [CardButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
                    [CardButton setImageEdgeInsets:UIEdgeInsetsMake(0.0, -80.0, 0.0, 0.0)];
                    [CardButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0,-20.0, 0.0, 0.0)];
                    NSString *str1 = NSLocalizedString(@"PERSONAL ****", @"PERSONAL ****");
                    [Helper setButton:CardButton Text:[NSString stringWithFormat:@"%@%@",str1,fav.last4] WithFont:Roboto_Light FSize:13 TitleColor:[UIColor whiteColor] ShadowColor:nil];
                    cardId = fav.idCard;
                    paymentTypesForLiveBooking = 1;
                    [CardButton setSelected:YES];
                }
                else
                {
                    [Helper setButton:CardButton Text:NSLocalizedString(@"ADD CARD", @"ADD CARD") WithFont:Roboto_Light FSize:13 TitleColor:[UIColor whiteColor] ShadowColor:nil];
                    paymentTypesForLiveBooking = 0;
                }
            }
        }
        else
        {
            [Helper setButton:CardButton Text:NSLocalizedString(@"CASH", @"CASH") WithFont:Roboto_Light FSize:13 TitleColor:[UIColor whiteColor] ShadowColor:nil];
            paymentTypesForLiveBooking = 2;
        }
    }
    
    UIButton *FareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    FareButton.frame = CGRectMake(10,45,(screenWidth-40)/2-10,36);
    [Helper setButton:FareButton Text:NSLocalizedString(@"FARE QUOTE", @"FARE QUOTE") WithFont:Roboto_Light FSize:12 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [FareButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [FareButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateSelected];
    [FareButton setBackgroundImage:[UIImage imageNamed:@"conformation_btn_farequote_off.png"] forState:UIControlStateNormal];
    [FareButton setBackgroundImage:[UIImage imageNamed:@"conformation_btn_farequote_on.png"] forState:UIControlStateSelected];
    [FareButton addTarget:self action:@selector(fareButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    FareButton.tag = 505;
    [customBottomView addSubview:FareButton];
    
    
    UIButton *promoButton = [UIButton buttonWithType:UIButtonTypeCustom];
    promoButton.frame = CGRectMake(screenWidth/2, 45, (screenWidth-40)/2-10, 36);
    promoButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    [promoButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [promoButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateSelected];
    [promoButton setBackgroundImage:[UIImage imageNamed:@"conformation_btn_farequote_off.png"] forState:UIControlStateNormal];
    [promoButton setBackgroundImage:[UIImage imageNamed:@"conformation_btn_farequote_on.png"] forState:UIControlStateSelected];
    [promoButton addTarget:self action:@selector(promoCodeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    promoButton.tag = 504;
    [Helper setButton:promoButton Text:NSLocalizedString(@"PROMO CODE", @"PROMO CODE") WithFont:Roboto_Light FSize:13 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    [customBottomView addSubview:promoButton];
    
    [UIView animateWithDuration:0.2
                          delay:0.4
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         
                         basketBottom.frame = basketTopFrame;
                     }
                     completion:^(BOOL finished){
                     }];
}


#pragma mark - WEB SERVICES

-(void)sendAServiceForFareCalculator
{
    if (srcLat == 0 || srcLong == 0 || _currentLatitude == 0 || _currentLongitude == 0 || desLat == 0 || desLong == 0)
    {
        return;
    }
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    carTypesForLiveBookingServer = [_arrayOfCarTypes[carTypesForLiveBooking-1][@"type_id"] integerValue];
    
    NSString *currentLatitude = [NSString stringWithFormat:@"%f",_currentLatitude];
    NSString *currentLongitude = [NSString stringWithFormat:@"%f",_currentLongitude];
    NSString *pickupLatitude = [NSString stringWithFormat:@"%f",srcLat];
    NSString *pickupLongitude = [NSString stringWithFormat:@"%f",srcLong];
    NSString *dropLatitude = [NSString stringWithFormat:@"%f",desLat];
    NSString *dropLongitude = [NSString stringWithFormat:@"%f",desLong];
    NSString *dateTime = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_type_id":[NSString stringWithFormat:@"%ld",(long)carTypesForLiveBookingServer],
                             @"ent_curr_lat":currentLatitude,
                             @"ent_curr_long":currentLongitude,
                             @"ent_from_lat":pickupLatitude,
                             @"ent_from_long":pickupLongitude,
                             @"ent_to_lat":dropLatitude,
                             @"ent_to_long":dropLongitude,
                             @"ent_date_time":dateTime
                             };
    NSLog(@"Fare Calculator params =%@", params);
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:MethodFareCalculator
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   NSLog(@"Fare Calculator response =%@", response);
                                   if (success)
                                   {
                                       [self parsegetBookingAppointment:response];
                                   }
                                   else
                                   {
                                       ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                                       [pi hideProgressIndicator];
                                   }
                               }];
}

#pragma mark - WEB SERVICE RESPONSE

-(void)parsegetBookingAppointment:(NSDictionary *)responseDictionary
{
    if (!responseDictionary)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[responseDictionary objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([responseDictionary objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDictionary objectForKey:@"Error"]];
    }
    else
    {
        if ([[responseDictionary objectForKey:@"errFlag"] intValue] == 0)
        {
            NSString *fareEstimateValue =  [PatientGetLocalCurrency getCurrencyLocal:[responseDictionary[@"fare"] floatValue]];
            UIButton *fareBtn  = (UIButton *)[[[self.view viewWithTag:499] viewWithTag:501] viewWithTag:505];
            NSString *Str = NSLocalizedString(@"FARE ESTIMATE", @"FARE ESTIMATE");
            fareBtn.titleLabel.numberOfLines = 0;
            fareBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
            [Helper setButton:fareBtn Text:[NSString stringWithFormat:@"%@\n%@", Str, fareEstimateValue] WithFont:Roboto_Light FSize:12 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
        }
        else
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[responseDictionary objectForKey:@"errMsg"]];
        }
    }
}


-(void)goToPaymentController {
    
    PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
    vc.callback = ^(NSString *cardIde , NSString *type){
        UIView *v = [self.view viewWithTag:501];
        UIButton *btn = (UIButton *)[v viewWithTag:502];
        NSString *imageName = [self setPlaceholderToCardType:type];
        
        [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
        [btn setImageEdgeInsets:UIEdgeInsetsMake(0.0, -80.0, 0.0, 0.0)];
        [btn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,-20.0, 0.0, 0.0)];
        NSString *str1 = NSLocalizedString(@"PERSONAL ****", @"PERSONAL ****");
        [btn setTitle:[NSString stringWithFormat:@"%@%@",str1,cardIde] forState:UIControlStateNormal];
        cardId = cardIde;
        paymentTypesForLiveBooking = 1;
    };
    vc.isComingFromSummary = YES;
    _isSelectinLocation = YES;
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
    
    [self presentViewController:navBar animated:YES completion:nil];
}

/**
 *  This methods get called after calling Handletap method,it brings the main view to its original position
 */

- (void)closeAnimation
{
    [UIView animateWithDuration:0.5f animations:^{
        self.view.transform = CGAffineTransformIdentity;
    }];
}

- (void)closeTransparentView{
    
    UIView *viewToClose = [self.view viewWithTag:50];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    [viewToClose removeFromSuperview];
    [UIView commitAnimations];
    
}

- (void) changeCurrentLocation:(NSDictionary *)dictAddress{
    
    NSString *latitude = [dictAddress objectForKey:@"lat"];
    NSString *longitude = [dictAddress objectForKey:@"lng"];
    
    CLLocation *location = [[CLLocation alloc]initWithLatitude:latitude.doubleValue longitude:longitude.doubleValue];
    mapView_.camera = [GMSCameraPosition cameraWithTarget:location.coordinate zoom:mapZoomLevel];
    
}

- (void)custimizeMyLocationButton{
    UIButton *myLocationButton = (UIButton*)[[mapView_ subviews] lastObject];
    myLocationButton.autoresizingMask = UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleTopMargin;
    CGRect frame = myLocationButton.frame;
    frame.origin.y = -310;
    frame.origin.x = -10;
    myLocationButton.frame = frame;
}

#pragma mark - Animations
- (void)startAnimation{
    
    [self.view endEditing:YES];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
    UIView *customNaviagtionBar = [self.view viewWithTag:78];
    CGRect rect2 = customNaviagtionBar.frame;
    rect2.origin.y = -44;
    customNaviagtionBar.frame = rect2;
    
    
    if(isCustomMarkerSelected == YES)
    {
        if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen){
            CGRect rectPick = PikUpViewForConfirmScreen.frame;
            rectPick.origin.y = 20;
            PikUpViewForConfirmScreen.frame = rectPick;
            
            CGRect rectDrop = DropOffViewForConfirmScreen.frame;
            rectDrop.origin.y = 64;
            DropOffViewForConfirmScreen.frame = rectDrop;
        }
        else{
            CGRect rect = PikUpViewForConfirmScreen.frame;
            rect.origin.y = 20;
            PikUpViewForConfirmScreen.frame = rect;
        }
    }
    else
    {
        UIView *customLocationView = [self.view viewWithTag:topViewTag];
        CGRect rect = customLocationView.frame;
        rect.origin.y = 20;
        customLocationView.frame = rect;
        
    }
    [UIView commitAnimations];
    [self animateOutBottomView];
    
}

- (void)endAnimation{
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    
    UIView *customNaviagtionBar = [self.view viewWithTag:78];
    CGRect rect2 = customNaviagtionBar.frame;
    
    
    rect2.origin.y = 0;
    customNaviagtionBar.frame = rect2;
    
    
    
    if(isCustomMarkerSelected == YES)
    {
        if(PikUpViewForConfirmScreen && DropOffViewForConfirmScreen){
            CGRect rectPick = PikUpViewForConfirmScreen.frame;
            rectPick.origin.y = 64;
            PikUpViewForConfirmScreen.frame = rectPick;

            CGRect rectDrop = DropOffViewForConfirmScreen.frame;
            rectDrop.origin.y = 108;
            DropOffViewForConfirmScreen.frame = rectDrop;
        }
        else
        {
            CGRect rect = PikUpViewForConfirmScreen.frame;
            rect.origin.y = 64;
            PikUpViewForConfirmScreen.frame = rect;
        }
    }
    else
    {
        UIView *customLocationView = [self.view viewWithTag:topViewTag];
        CGRect rect = customLocationView.frame;
        rect.origin.y = 64;
        customLocationView.frame = rect;
    }
    
    [UIView commitAnimations];
    [self animateInBottomView];
}


#pragma mark -UIButton Action

-(void)plusButtonClicked:(id)sender
{
    isLocationChanged = isFixed;
    isFareButtonClicked = YES;
    
    UIButton *btn = (UIButton *)[PikUpViewForConfirmScreen viewWithTag:151];
    [self pickupLocationAction:btn];
}

-(void)minusButtonClicked:(id)sender
{
    UIView *dropOff = [self.view viewWithTag:160];
    CGRect dropOffTop = dropOff.frame;
    dropOffTop.origin.y = 0;
    [UIView animateWithDuration:0.6f animations:^{
        dropOff.frame = dropOffTop;
    }];
    [DropOffViewForConfirmScreen removeFromSuperview];
    DropOffViewForConfirmScreen = nil;
    desLong = desLat = 0;
    desAddr = desAddrline2 = @"";
}

- (void)handleTap:(UITapGestureRecognizer*)recognizer
{
    if(isCustomMarkerSelected == YES)
    {
        isCustomMarkerSelected = NO;
        [self addCustomNavigationBar];
    }
    
    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
    v.hidden = NO;
    
    UIView *coverView = [self.view viewWithTag:300];
    [coverView removeFromSuperview];
    
    
    UIView *basketBottom = [self.view viewWithTag:499];
    UIView *pickUp = [self.view viewWithTag:150];
    UIView *dropOff = [self.view viewWithTag:160];
    
    //    UIView *dropOff = [self.view viewWithTag:160];
    
    CGRect basketTopFrame = basketBottom.frame;
    basketTopFrame.origin.y = 568;//-basketTopFrame.size.height;
    
    CGRect pickUpTop = pickUp.frame;
    pickUpTop.origin.y = 0;
    
    CGRect dropOffTop = dropOff.frame;
    dropOffTop.origin.y = 0;
    
    UIView *LV = [self.view viewWithTag:topViewTag];
    CGRect RTEC = LV.frame;
    RTEC.origin.y = 64;
    
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options: UIViewAnimationOptionCurveLinear
                     animations:^{
                         basketBottom.frame = basketTopFrame;
                         pickUp.frame = pickUpTop;
                         dropOff.frame = dropOffTop;
                         LV.frame = RTEC;
                     }
                     completion:^(BOOL finished){
                     }];
    
    [PikUpViewForConfirmScreen removeFromSuperview];
    PikUpViewForConfirmScreen = nil;
    [self minusButtonClicked:nil];
    [self showBottomViews];
    [basketBottom removeFromSuperview];
    [self closeAnimation];
}

-(void)navCancelButtonClciked
{
    isLocationChanged = isChanging;
    [self addCustomLocationView];
    [self getCurrentLocation:nil];
    [self handleTap:nil];
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
    [self getAddress:position];
    [self showBottomViews];
    UIButton *nowButton =  (UIButton*)(UIView*)[self.view viewWithTag:2050];
    [self buttonNowLaterClicked:nowButton];
    GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
    [mapView_ animateWithCameraUpdate:zoomCamera];
    [self stopBackgroundTask];
}

-(void)rightBarButtonClicked:(UIButton *)sender
{
    [self showPickerViewForAddingTipForDriver];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    if(isCustomMarkerSelected == YES)
    {
        [self navCancelButtonClciked];
    }
    else
    {
        [self menuButtonPressed:sender];
    }
}

-(void)customMarkerTopBottomClicked
{
    if (_arrayOfCarTypes.count > 0)
    {
        surgePrice = [_arrayOfCarTypes[carTypesForLiveBooking - 1][@"surg_price"] doubleValue];
        surgePriceTime = _arrayOfCarTypes[carTypesForLiveBooking - 1][@"surgePriceTime"];
        if (_textFeildAddress.text.length == 0)
        {
            UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
            [markerView setUserInteractionEnabled:NO];
        }
        else if (surgePrice > 1.0)
        {
            UIImageView *imageView = arrayOfVehicleIcons[carTypesForLiveBooking-1];
            NSDictionary *dataDict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSString stringWithFormat:@"%0.1fx",surgePrice],@"surgePrice",
                                      [NSString stringWithFormat:@"%@",allHorizontalData [carTypesForLiveBooking-1]], @"vehicleName",
                                      imageView.image, @"vehicleImage",
                                      nil];
            surgePricePopUp* surgePopView = [[surgePricePopUp alloc]init];
            UIWindow *window2 = [[UIApplication sharedApplication] keyWindow];
            [surgePopView showPopUpWithDetailedDict:dataDict Onwindow:window2];
            surgePopView.onCompletion = ^(NSInteger acceptOrReject)
            {
                if (acceptOrReject == 1)
                {
                    UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
                    [markerView setUserInteractionEnabled:YES];
                    UIView *v = [self.view viewWithTag:myCustomMarkerTag];
                    v.hidden = YES;
                    isCustomMarkerSelected = YES;
                    [self addCustomNavigationBar];
                    [self addCustomViewForShowingCardFareWithAnimation];
                    [self addCustomLocationPikUpViewForConfirmScreen];
                }
            };
        }
        else
        {
            UIView *markerView = (UIView *)[self.view viewWithTag:myCustomMarkerTag];
            [markerView setUserInteractionEnabled:YES];
            UIView *v = [self.view viewWithTag:myCustomMarkerTag];
            v.hidden = YES;
            isCustomMarkerSelected = YES;
            [self addCustomNavigationBar];
            [self addCustomViewForShowingCardFareWithAnimation];
            [self addCustomLocationPikUpViewForConfirmScreen];
        }
    }
    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"No car types availabe in your area.", @"No car types availabe in your area.")];
    }
}

/**
 * this button will lead you to select card for payment screen
 */
-(void)cardButtonClicked
{
    //for details see the map delegate method " will move "
    
    isLocationChanged = isFixed; //to fix the pickup location
    isFareButtonClicked = YES; //to make sure that the pickup location is fixed
    
    [self goToPaymentController];
}

/**
 *  when the search button on the home screen is clicked
 *
 *  @param sender sender of the button whose tag is 91,This method is to make sure the location is not changing after picking up the fixing the pickup location
 */
-(void)searchButtonClicked:(id)sender
{
    isLocationChanged = isFixed;
    isFareButtonClicked = YES;
    
    UIButton *mBtn = (UIButton *)sender;
    [self pickupLocationAction:mBtn];
}

/**
 *  this button will either lead you to select drop location or if both the location is selected then directly to fare calculator screen
 */
-(void)fareButtonClicked
{
    isLocationChanged = isFixed;
    isFareButtonClicked = YES;
    checkFareQuote = YES;
    if(srcAddr == nil)
    {
        UITextField *textAddress = (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
        srcAddr = textAddress.text;
        srcAddrline2 = @"";
    }
    if(DropOffViewForConfirmScreen == nil || desLat == 0 || desLong == 0)
    {
        [self pickupLocationAction:nil];
        if(desAddr == nil)
            desAddr = @"";
        desAddrline2 = @"";
    }
    else
    {
        NSDictionary *params = @{@"cLoc":[NSString stringWithFormat:@"%f",_currentLongitude],
                                 @"cLat":[NSString stringWithFormat:@"%f",_currentLatitude],
                                 @"pLoc":[NSString stringWithFormat:@"%f",srcLong],
                                 @"pLat":[NSString stringWithFormat:@"%f",srcLat],
                                 @"pAddr":srcAddr,
                                 @"dLat":[NSString stringWithFormat:@"%f",desLat],
                                 @"dLon":[NSString stringWithFormat:@"%f",desLong],
                                 @"dAddr":desAddr,
                                 @"typeid":[NSNumber numberWithInteger:carTypesForLiveBookingServer],
                                 };
        fareCalculatorViewController *fareCalculatorVC = [self.storyboard instantiateViewControllerWithIdentifier:@"fareVC"];
        fareCalculatorVC.locationDetails = params;
        fareCalculatorVC.isComingFromMapVC = YES;
        _isSelectinLocation = YES;
        UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:fareCalculatorVC];
        [self presentViewController:navBar animated:YES completion:nil];
    }
}

-(void)promoCodeButtonClicked:(id)sender
{
    PromoCodeViewController *promoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"promoVC"];
    promoVC.userLatitude = _currentLatitude;
    promoVC.userLongitude = _currentLongitude;
    _isSelectinLocation = YES;
    promoVC.onCompletion = ^(NSString *locationType){
        if (locationType.length > 0) {
            
            UIView *v = [self.view viewWithTag:501];
            UIButton *btn = (UIButton *)[v viewWithTag:504];
            [btn setTitle:locationType forState:UIControlStateNormal];
            promocode = locationType;
        }
    };
    
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:promoVC];
    [self presentViewController:navBar animated:YES completion:nil];
}

- (IBAction)menuButtonPressed:(id)sender
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}

- (void)getCurrentLocation:(UIButton *)sender
{
    CLLocation *location = mapView_.myLocation;
    _currentLatitude = location.coordinate.latitude;
    _currentLongitude = location.coordinate.longitude;
    
    if (location)
    {
        GMSCameraUpdate *zoomCamera = [GMSCameraUpdate setTarget:CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude) zoom:mapZoomLevel];
        [mapView_ animateWithCameraUpdate:zoomCamera];
    }
}

- (void)pickupLocationAction:(UIButton *)sender
{
    PickUpViewController *pickController = [self.storyboard instantiateViewControllerWithIdentifier:@"pick"];
    if(sender.tag == 155 || sender.tag == 91)
    {
        pickController.locationType = kSourceAddress;
        pickController.isComingFromMapVCFareButton = NO;
    }
    else if(sender.tag == 175)
    {
        pickController.locationType = kDestinationAddress;
        pickController.isComingFromMapVCFareButton = NO;
    }
    else
    {
        pickController.locationType = kDestinationAddress;
        pickController.isComingFromMapVCFareButton = YES;
        checkFareQuote = YES;
    }
    
    pickController.typeID = carTypesForLiveBookingServer;
    UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:pickController];
    
    pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
    pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
    pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType)
    {
        _isAddressManuallyPicked = YES;
        NSString *addressText = flStrForStr(dict[@"address1"]);
        if ([dict[@"address2"] rangeOfString:addressText].location == NSNotFound)
        {
            addressText = [addressText stringByAppendingString:@","];
            addressText = [addressText stringByAppendingString:flStrForStr(dict[@"address2"])];
        }
        else
        {
            addressText = flStrForStr(dict[@"address2"]);
        }
        if(locationType == 1)
        {
            UITextField *textAddress = (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            textAddress.text = addressText;
            srcAddr = flStrForStr(dict[@"address1"]);
            srcAddrline2 = flStrForStr(dict[@"address2"]);
            srcLat = [dict[@"lat"] floatValue];
            srcLong = [dict[@"lng"] floatValue];
            _textFeildAddress.text = addressText;
            [self performSelectorOnMainThread:@selector(changeCurrentLocation:) withObject:dict waitUntilDone:YES];
        }
        else if(sender.tag == 175)
        {
            desAddr = flStrForStr(dict[@"address1"]);
            desAddrline2 = flStrForStr(dict[@"address2"]);
            desLat = [dict[@"lat"] floatValue];
            desLong = [dict[@"lng"] floatValue];
            UIView *customView = [self.view viewWithTag:170];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:171];
            textAddress.text = addressText;
            [self sendServiceToUpdateDropAddress];
            NSUserDefaults *udLat = [NSUserDefaults standardUserDefaults];
            [udLat setDouble:desLat forKey:@"desLat"];
            [udLat setDouble:desLong forKey:@"desLong"];
            _currentLatitude =  [udLat doubleForKey:@"srcLat"];
            _currentLongitude = [udLat doubleForKey:@"srcLong"];
            NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
            if (bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
                _isPathPlotted = NO;
                [mapView_ clear];
                waypoints_ = [[NSMutableArray alloc]init];
                waypointStrings_ = [[NSMutableArray alloc]initWithCapacity:2];
                [self setStartLocationCoordinates: desLat Longitude:desLong :3];
                if(! (_currentLatitude == 0 || _currentLongitude == 0)) {
                    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude,_currentLongitude);
                    GMSMarker *srcMarker = [GMSMarker markerWithPosition:position];
                    srcMarker.map = mapView_;
                    srcMarker.icon = [UIImage imageNamed:@"default_marker_p"];
                    [self updateDestinationLocationWithLatitude:_currentLatitude Longitude:_currentLongitude];
                    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:_currentLatitude
                                                                            longitude:_currentLongitude
                                                                                 zoom:mapZoomLevel];
                    
                    [mapView_ setCamera:camera];
                }
            } else {
                [self sendAServiceForFareCalculator];
            }
        }
        else
        {
            UIView *customView = [self.view viewWithTag:160];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:161];
            textAddress.text = addressText;
            desAddr = flStrForStr(dict[@"address1"]);
            desAddrline2 = flStrForStr(dict[@"address2"]);
            desLat = [dict[@"lat"] floatValue];
            desLong = [dict[@"lng"] floatValue];
        }
    };
    AppointmentLocation *apLocaion = [AppointmentLocation sharedInstance];
    apLocaion.currentLatitude = [NSNumber numberWithDouble:_currentLatitude];
    apLocaion.currentLongitude =  [NSNumber numberWithDouble:_currentLongitude];
    apLocaion.pickupLatitude = [NSNumber numberWithDouble:srcLat];
    apLocaion.pickupLongitude = [NSNumber numberWithDouble:srcLong];
    apLocaion.dropOffLatitude = [NSNumber numberWithDouble:desLat];
    apLocaion.dropOffLongitude = [NSNumber numberWithDouble:desLong];
    apLocaion.srcAddressLine1 = flStrForObj(srcAddr);
    apLocaion.srcAddressLine2 = flStrForObj(srcAddrline2);
    apLocaion.desAddressLine1 = flStrForObj(desAddr);
    apLocaion.desAddressLine2 = flStrForObj(desAddrline2);
    _isSelectinLocation = YES;
    [self presentViewController:navBar animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([[segue identifier] isEqualToString:@"pickup"])
    {
        PickUpViewController *pickController = [segue destinationViewController];
        pickController.latitude =  [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.latitude];
        pickController.longitude = [NSString stringWithFormat:@"%f",mapView_.myLocation.coordinate.longitude];
        pickController.onCompletion = ^(NSDictionary *dict,NSInteger locationType){
            UIView *customView = [mapView_ viewWithTag:90];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:101];
            textAddress.text = dict[@"formatted_address"];
            
            NSString *latitude = [dict valueForKeyPath:@"geometry.location.lat"];
            NSString *longitude = [dict valueForKeyPath:@"geometry.location.lng"];
            
            NSMutableDictionary *mDict = [[NSMutableDictionary alloc]init];
            [mDict setValue:latitude forKey:@"Latitude"];
            [mDict setValue:longitude forKey:@"Longitude"];
            [self performSelectorOnMainThread:@selector(changeCurrentLocation:) withObject:dict waitUntilDone:YES];
        };
    }
}

- (void)getAddress:(CLLocationCoordinate2D)coordinate
{
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        if (response && response.firstResult)
        {
            GMSAddress *address = response.firstResult;
            NSString *addressText = [NSString stringWithFormat:@"%@, %@, %@, %@, %@.", flStrForStr(address.thoroughfare) ,flStrForStr(address.subLocality), flStrForStr(address.locality), flStrForStr(address.postalCode), flStrForStr(address.administrativeArea)];
            if([addressText hasPrefix:@", "])
            {
                addressText = [addressText substringFromIndex:2];
            }
            if([addressText hasSuffix:@", "])
            {
                addressText = [addressText substringToIndex:addressText.length-2];
            }
            addressText = [addressText stringByReplacingOccurrencesOfString:@", ," withString:@", "];
            UIView *customView = [self.view viewWithTag:topViewTag];
            UITextField *textAddress = (UITextField *)[customView viewWithTag:101];
            textAddress.text =  addressText;
            
            UITextField *pickUpAddress= (UITextField *)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            if(!(isLocationChanged == isFixed))
            {
                pickUpAddress.text = addressText;
            }
            
            pickUpAddress.text = addressText;
            srcLat = response.firstResult.coordinate.latitude;
            srcLong = response.firstResult.coordinate.longitude;
            srcAddr = addressText;
            srcAddrline2 = flStrForObj(@"");
        }
        else
        {
            UITextField *pickUpAddress= (UITextField*)[PikUpViewForConfirmScreen viewWithTag:pickupAddressTag];
            if(!(isLocationChanged == isFixed))
            {
                pickUpAddress.text = flStrForObj(@"");
            }
            pickUpAddress.text = flStrForObj(@"");
            srcLat = _currentLatitude;
            srcLong = _currentLongitude;
            srcAddr = flStrForObj(@"");
            srcAddrline2 = flStrForObj(@"");
        }
    };
    [geocoder_ reverseGeocodeCoordinate:coordinate completionHandler:handler];
}

#pragma mark UITextField Delegate -
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)getCurrentLocationFromGPS {
    
    //check location services is enabled
    if ([CLLocationManager locationServicesEnabled]) {
        
        if (!_locationManager) {
            _locationManager = [[CLLocationManager alloc] init];
            _locationManager.delegate = self;
            _locationManager.distanceFilter = kCLDistanceFilterNone;
            _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            if  ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])//requestAlwaysAuthorization
            {
                [self.locationManager requestWhenInUseAuthorization];//requestAlwaysAuthorization
            }
        }
        [_locationManager startUpdatingLocation];
        
        
    }
    else {
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location Service", @"Location Service")
                                                            message:NSLocalizedString(@"Unable to find your location,Please enable location services.", @"Unable to find your location,Please enable location services.")
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"Ok", @"Ok")
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
    
}
#pragma mark - PAYMENT
-(NSString *)setPlaceholderToCardType:(NSString *)mycardType
{
    NSString* cardTypeName   = NSLocalizedString(@"placeholder", @"placeholder");
    if([mycardType isEqualToString:@"amex"])
        cardTypeName = NSLocalizedString(@"amex", @"amex");
    else if([mycardType isEqualToString:@"diners"])
        cardTypeName = NSLocalizedString(@"diners", @"diners");
    else if([mycardType isEqualToString:@"discover"])
        cardTypeName = NSLocalizedString(@"discover", @"discover");
    else if([mycardType isEqualToString:@"jcb"])
        cardTypeName = NSLocalizedString(@"jcb", @"jcb");
    else if([mycardType isEqualToString:@"MasterCard"])
        cardTypeName = NSLocalizedString(@"mastercard", @"mastercard");
    else if([mycardType isEqualToString:@"Visa"])
        cardTypeName = NSLocalizedString(@"visa.png", @"visa.png");
    
    return cardTypeName;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    
    if (bookingStatus == kNotificationTypeBookingOnMyWay || bookingStatus == kNotificationTypeBookingReachedLocation || bookingStatus == kNotificationTypeBookingStarted) {
        if (!_isUpdatedLocation) {
            [_locationManager stopUpdatingLocation];
            _isUpdatedLocation = YES;
            [_allMarkers removeAllObjects];
            [mapView_ clear];
            
        }
    }
    else {
        
        if (!_isUpdatedLocation) {
            _isUpdatedLocation = YES;
            [_locationManager stopUpdatingLocation];
            //change map camera postion to current location
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
                                                                    longitude:newLocation.coordinate.longitude
                                                                         zoom:mapZoomLevel];
            [mapView_ setCamera:camera];
            
            
            //add marker at current location
            CLLocationCoordinate2D position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude);
            //save current location to plot direciton on map
            _currentLatitude = newLocation.coordinate.latitude;
            _currentLongitude =  newLocation.coordinate.longitude;
            
            //get address for current location
            [self getAddress:position];
            
            MyAppTimerClass *obj = [MyAppTimerClass sharedInstance];
            [obj startPublishTimer];
            
        }
    }
    
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
    {
        // The user denied your app access to location information.
         [self gotoLocationServicesMessageViewController];
    }
}

-(void)locationServicesChanged:(NSNotification*)notification {
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        [self gotoLocationServicesMessageViewController];
    }
    else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)//kCLAuthorizationStatusAuthorized
    {
        [self.navigationController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)gotoLocationServicesMessageViewController
{
    LocationServicesMessageVC *locationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"locationVC"];
    UINavigationController *navigationVC  = [[UINavigationController alloc] initWithRootViewController:locationVC];
    [self.navigationController presentViewController:navigationVC animated:YES completion:nil];
}

/**
 *  swithc between appointment type
 *
 *  @param sender button
 */
- (void)checkDoctorsAvailability:(UIButton *)sender
{
    if (sender.tag == 107) // Now
    {
        isNowSelected = YES;
        isLaterSelected = NO;
        [sender setSelected:isNowSelected];
        UIButton *laterButton =  (UIButton*)[(UIView*)[sender superview] viewWithTag:108];
        [laterButton setSelected:isLaterSelected];
    }
    else if(sender.tag == 108) //Later
    {
        isNowSelected = NO;
        isLaterSelected = YES;
        [sender setSelected:isLaterSelected];
        UIButton *nowButton =  (UIButton*)[(UIView*)[sender superview] viewWithTag:107];
        [nowButton setSelected:isNowSelected];
    }
}


/**
 *  will return the Appointment Type
 *
 *  @return Appointment type Nor or Later
 */
-(AppointmentType)getSelectedAppointmentType
{
    if (isNowSelected)
    {
        return  kAppointmentTypeNow;
    }
    return kAppointmentTypeLater;
}

/**
 *  Plots Marker on Map
 *
 *  @param medicalSpecialist doctor or nurse
 */
- (void)addCustomMarkerFor
{
    if (!_allMarkers)
    {
        _allMarkers = [[NSMutableDictionary alloc] init];
    }
    for (int i = 0; i < drivers.count; i++)
    {
        NSDictionary *dict = drivers[i];
        float latitude = [dict[@"lt"] floatValue];
        float longitude = [dict[@"lg"] floatValue];
        if (!_allMarkers[dict[@"chn"]])
        {
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
            marker.tappable = YES;
            marker.flat = YES;
            marker.groundAnchor = CGPointMake(0.5f, 0.5f);
            marker.userData =  dict;
            if (arrayOfMapImages.count && arrayOfMapImages.count == _arrayOfCarTypes.count)
            {
                marker.icon = [arrayOfMapImages objectAtIndex:carTypesForLiveBooking-1];
            }
            else
            {
                NSString *markerURL;
                if(_arrayOfCarTypes.count)
                {
                    markerURL = [NSString stringWithFormat:@"%@%@", baseUrlForOriginalImage, _arrayOfCarTypes[carTypesForLiveBooking-1][@"MapIcon"]];
                }
                else
                {
                    markerURL = [NSString stringWithFormat:@"%@%@", baseUrlForOriginalImage, [[NSUserDefaults standardUserDefaults] objectForKey:@"carMapImage"]];
                }
                if ([PMDReachabilityWrapper sharedInstance].isNetworkAvailable)
                {
                    imageDownloadMethodCallingCount = 0;
                    [self downloadDriverCarImage:markerURL markerName:marker];
                }
            }
            marker.map = mapView_;
            [_allMarkers setObject:marker forKey:dict[@"chn"]];
        }
        else
        {
            GMSMarker *marker = _allMarkers[dict[@"chn"]];
            CLLocationCoordinate2D lastPosition =  marker.position;
            CLLocationCoordinate2D newPosition = CLLocationCoordinate2DMake(latitude, longitude);
            CLLocationDirection heading = GMSGeometryHeading(lastPosition, newPosition);
            marker.position = newPosition;
            
            [CATransaction begin];
            [CATransaction setAnimationDuration:5.0];
            marker.position = CLLocationCoordinate2DMake(latitude, longitude);
            [CATransaction commit];
            if (heading > 0)
            {
                marker.rotation = heading;
            }
        }
    }
    [drivers removeAllObjects];
}

-(void)removeMarker:(GMSMarker*)marker
{
    marker.map = mapView_;
    marker.map = nil;
}

/**
 *  checking for push is coming or not
 */

#pragma mark - WebServiceRequest

-(void)sendRequestToGetDriverDetails:(NSString *)driEmail :(NSString *)apptDt{
    
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isServiceCalledOnce"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self bookingNotAccetedByAnyDriverScreenLayout];
    [self bookingAccetedBySomeDriverScreenLayout];
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please Wait...", @"Please Wait...")];
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *docEmail = driEmail;
    NSString *appointmntDate = apptDt;
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_email":docEmail,
                             @"ent_user_type":@"2",
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             };
    
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMGetAppointmentDetial
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                   {
                                       [self parseDriverDetailResponse:response];
                                   }
                                   else
                                   {
                                       [pi hideProgressIndicator];
                                   }
                               }];
}

#pragma mark - WebService Response

-(void)parseDriverDetailResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if ([response objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:@"Error" Message:[response objectForKey:@"Error"]];
    }
    else
    {
        if ([[response objectForKey:@"errFlag"] integerValue] == 0)
        {
            NSArray *driverLatitudeLongitude = [response[@"ltg"] componentsSeparatedByString:@","];
            NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
            [ud setObject:flStrForStr(response[@"model"]) forKey:@"MODEL"];
            [ud setObject:flStrForStr(response[@"plateNo"]) forKey:@"PLATE"];
            [ud setObject:flStrForStr(response[@"carMapImage"]) forKey:@"carMapImage"];
            [ud setObject:flStrForStr(response[@"chn"]) forKey:kNSUDriverPubnubChannel];
            [ud setObject:flStrForStr(response[@"carImage"]) forKey:@"CARIMAGE"];
            [ud setObject:[NSString stringWithFormat:@"%@ %@",flStrForStr(response[@"fName"]), flStrForStr(response[@"lName"])] forKey:@"drivername"];
            [ud setObject:flStrForStr(response[@"pPic"]) forKey:@"driverPic"];
            [ud setObject:flStrForStr(response[@"mobile"]) forKey:@"DriverTelNo"];
            [ud synchronize];
            
            _driverCurLat = [driverLatitudeLongitude[0] doubleValue];
            _driverCurLong = [driverLatitudeLongitude[1]doubleValue];
            _currentLatitude = [response[@"pickLat"] doubleValue];
            _currentLongitude = [response[@"pickLong"] doubleValue];
            shareUrlViaSMS = response[@"share"];
            subscribedChannel = response[@"chn"];
            [self subscribeOnlyBookedDriver];
            [(PatientAppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:response :6];
        }
    }
}

/**
 *  date picker
 *
 *  @param  Select date for the later booking
 *
 *  @return return void and accept title string to show title on its view
 */

#pragma UIPicker Delegate

- (void)addCustomActionSheet
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView *customActnSheet = [[UIView alloc] initWithFrame:CGRectMake(0, screenHeight, screenWidth, screenHeight)];
    customActnSheet.backgroundColor = [UIColor clearColor];
    customActnSheet.tag = 9001;
    [self.view addSubview:customActnSheet];
}

- (void)showDatePickerWithTitle:(NSString*)title
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;

    [self addCustomActionSheet];
    UIView *customActnSheet = [self.view viewWithTag:9001];
    
    CGRect pickerFrame = CGRectMake(0,35,0, 0);
    datePicker = [[UIDatePicker alloc] initWithFrame:pickerFrame];
    [datePicker setDatePickerMode:UIDatePickerModeDateAndTime];
    CGRect frame = datePicker.frame;
    frame.size.width = screenWidth;
    [datePicker setFrame:frame];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:1];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [comps setYear:-30];
    
    NSDate *endDate = [NSDate date];
    NSTimeInterval oneHours = 1 * 60 * 60;
    NSDate *dateOneHourAhead = [endDate dateByAddingTimeInterval:oneHours];
    
    [datePicker setMaximumDate:maxDate];
    [datePicker setMinimumDate:dateOneHourAhead];
    datePicker.minuteInterval = 15;
    [datePicker setBackgroundColor:UIColorFromRGB(0xffffff)];
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight-250, screenWidth, 250)];
    topView.tag = 9002;
    topView.backgroundColor = [UIColor blackColor];
    UIButton *doneButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:NSLocalizedString(@"Done",@"Done") forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    doneButton.frame = CGRectMake(screenWidth-60, 7.0f, 60.0f, 30.0f);
    [doneButton addTarget:self action:@selector(saveActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:doneButton];
    
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:NSLocalizedString(@"Cancel",@"Cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10,7.0f, 60.0f, 30.0f);
    [cancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [cancelButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(cancelActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancelButton];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/4, 7, screenWidth/2, 30)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text =title;
    titleLabel.textColor =  UIColorFromRGB(0xffffff);
    [topView addSubview:titleLabel];
    [topView addSubview:datePicker];
    [customActnSheet addSubview:topView];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         CGRect frame = customActnSheet.frame;
                         frame.origin.y = 0;
                         customActnSheet.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void)saveActionSheet:(id)type
{
    UIView *customActnSheet = [self.view viewWithTag:9001];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:9001];
                         [customActionSht removeFromSuperview];
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
    NSDate *dateSelected = [datePicker date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    _laterSelectedDateServer = [dateFormatter stringFromDate:dateSelected];
    [dateFormatter setDateFormat:@"dd MMM yyyy hh:mm a"];
    laterSelectedDate = [dateFormatter stringFromDate:dateSelected];
    [self customMarkerTopBottomClicked];
}

- (void)cancelActionSheet:(id)type
{
    UIView *customActnSheet = [self.view viewWithTag:9001];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:9001];
                         [customActionSht removeFromSuperview];
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    UIButton *nowButton =  (UIButton*)(UIView*)[self.view viewWithTag:2050];
    [self buttonNowLaterClicked:nowButton];
}

-(void)showPaymentTypeWithTitle
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    [self addCustomActionSheet];
    UIView *customActnSheet = [self.view viewWithTag:9001];
    _pkrView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, 0, 0)];
    [_pkrView setBackgroundColor:[UIColor whiteColor]];
    [_pkrView setDelegate:self];
    [_pkrView setShowsSelectionIndicator:YES];
    _pkrView.tag = 5500;
    
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight-250, screenWidth, 250)];
    topView.backgroundColor = [UIColor blackColor];
    topView.tag = 9002;
    
    UIButton *doneButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [Helper setButton:doneButton Text:NSLocalizedString(@"DONE", @"DONE") WithFont:Roboto_Regular FSize:14 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [doneButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    doneButton.frame = CGRectMake(screenWidth-60, 7, 60, 30);
    doneButton.tag = 5700;
    [doneButton addTarget:self action:@selector(btnDoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:doneButton];
    
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 7, 60, 30);
    cancelButton.tag = 5700;
    [Helper setButton:cancelButton Text:NSLocalizedString(@"CANCEL", @"CANCEL") WithFont:Roboto_Regular FSize:14 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [cancelButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(btnCloseTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancelButton];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/4, 7, screenWidth/2, 30)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text =NSLocalizedString(@"PAYMENT TYPE", @"PAYMENT TYPE");
    titleLabel.textColor =  UIColorFromRGB(0xffffff);
    [topView addSubview:titleLabel];
    [topView addSubview:_pkrView];
    
    [customActnSheet addSubview:topView];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         CGRect frame = customActnSheet.frame;
                         frame.origin.y = 0;
                         customActnSheet.frame = frame;
                         
                     } completion:^(BOOL finished) {
                         
                     }];
}

- (void)btnDoneTapped:(UIButton *)sender
{
    if (sender.tag == 5600)
    {
        [self sendRequestToAddTip:selectedRowForTip];
    }
    else
    {
        if ([_pkrView selectedRowInComponent:0]==0)
        {
            PaymentViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"paymentView"];
            vc.callback = ^(NSString *cardIde , NSString *type)
            {
                NSString *imageName = [self setPlaceholderToCardType:type];
                UIView *v = [self.view viewWithTag:501];
                UIButton *btn = (UIButton *)[v viewWithTag:502];
                [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
                [btn setImageEdgeInsets:UIEdgeInsetsMake(0.0, -80.0, 0.0, 0.0)];
                [btn setTitleEdgeInsets:UIEdgeInsetsMake(0.0,-20.0, 0.0, 0.0)];
                NSString *str1 = NSLocalizedString(@"PERSONAL ****", @"PERSONAL ****");
                [btn setTitle:[NSString stringWithFormat:@"%@%@",str1,cardIde] forState:UIControlStateNormal];
                cardId = cardIde;
                paymentTypesForLiveBooking = 1;
            };
            
            vc.isComingFromSummary = YES;
            _isSelectinLocation = YES;
            UINavigationController *navBar=[[UINavigationController alloc]initWithRootViewController:vc];
            [self presentViewController:navBar animated:YES completion:nil];
        }
        else
        {
            UIView *v = [self.view viewWithTag:501];
            UIButton *btn = (UIButton *)[v viewWithTag:502];
            btn.selected = NO;
            [btn setTitle:[NSString stringWithFormat:NSLocalizedString(@"  CASH", @"  CASH")] forState:UIControlStateNormal];
            [btn setImage:nil forState:UIControlStateNormal];
            paymentTypesForLiveBooking = 2;
        }
    }
    UIView *customActnSheet = [self.view viewWithTag:9001];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:9001];
                         [customActionSht removeFromSuperview];
                         
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
}
-(void)btnCloseTapped:(UIButton *)sender{
    if (sender.tag == 5600) {
        
    }
    UIView *customActnSheet = [self.view viewWithTag:9001];
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         UIView *customActionShtSubView = [customActnSheet viewWithTag:9002];
                         [customActionShtSubView removeFromSuperview];
                         UIView *customActionSht = [customActnSheet viewWithTag:9001];
                         [customActionSht removeFromSuperview];
                     } completion:^(BOOL finished) {
                         
                     }];
}

#pragma mark -
#pragma mark PickerView DataSource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView.tag == 5600) {
        return _arrayOfpickerData.count;
    } else {
        return 2;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (pickerView.tag == 5600) {
        NSString *returnStr = [NSString stringWithFormat:@"%@",_arrayOfpickerData[row]];
        return returnStr;
    } else {
        NSString *returnStr = @"";
        if (row == 0) {
            returnStr = NSLocalizedString(@"CARD", @"CARD");
        } else if(row == 1) {
            returnStr = NSLocalizedString(@"CASH", @"CASH");
        }
        return returnStr;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView.tag == 5600) {
        selectedRowForTip = row;
    }
}

-(void)showPickerViewForAddingTipForDriver {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;

    [self addCustomActionSheet];
    selectedRowForTip = 0;
    UIView *customActnSheet = [self.view viewWithTag:9001];
    _pkrViewForAddTip = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, screenWidth, 210)];
    [_pkrViewForAddTip setBackgroundColor:[UIColor whiteColor]];
    [_pkrViewForAddTip setDelegate:self];
    [_pkrViewForAddTip setShowsSelectionIndicator:YES];
    _pkrViewForAddTip.tag = 5600;
    //Adding a upper view
    UIView *topView = [[UIView alloc]initWithFrame:CGRectMake(0, screenHeight-250, screenWidth, 250)];
    topView.backgroundColor = [UIColor blackColor];
    topView.tag = 9002;
    
    UIButton *doneButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [Helper setButton:doneButton Text:NSLocalizedString(@"DONE", @"DONE") WithFont:Roboto_Regular FSize:14 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [doneButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    doneButton.frame = CGRectMake(screenWidth-60, 7, 60, 30);
    doneButton.tag = 5600;
    [doneButton addTarget:self action:@selector(btnDoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:doneButton];
    
    UIButton *cancelButton= [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.frame = CGRectMake(10, 7, 60, 30);
    cancelButton.tag = 5600;
    [Helper setButton:cancelButton Text:NSLocalizedString(@"CANCEL", @"CANCEL") WithFont:Roboto_Regular FSize:14 TitleColor:UIColorFromRGB(0xffffff) ShadowColor:nil];
    [cancelButton setTitleColor:UIColorFromRGB(0x666666) forState:UIControlStateHighlighted];
    [cancelButton addTarget:self action:@selector(btnCloseTapped:) forControlEvents:UIControlEventTouchUpInside];
    [topView addSubview:cancelButton];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(screenWidth/4, 7, screenWidth/2, 30)];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = NSLocalizedString(@"DRIVER TIP", @"DRIVER TIP");
    titleLabel.textColor =  UIColorFromRGB(0xffffff);
    [topView addSubview:titleLabel];
    [topView addSubview:_pkrViewForAddTip];
    
    [customActnSheet addSubview:topView];
    
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         
                         
                         CGRect frame = customActnSheet.frame;
                         frame.origin.y = 0;
                         customActnSheet.frame = frame;
                         
                         
                     } completion:^(BOOL finished) {
                         
                     }];
    
}

-(void)sendRequestToAddTip:(NSInteger) tipForDriver
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please Wait...", @"Please Wait...")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR) {
        
        deviceID = kPMDTestDeviceidKey;
    }
    else {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSString *bids =  flStrForObj([[NSUserDefaults standardUserDefaults]objectForKey:@"BOOKINGID"]);
    NSString *currentDate = [Helper getCurrentDateTime];
    tipForDriver = tipForDriver*5;
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_tip":[NSNumber numberWithInteger:tipForDriver],
                             @"ent_booking_id":bids,
                             @"ent_date_time":currentDate,
                             };
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"updateTip"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   if (success)
                                   {
                                       [[ProgressIndicator sharedInstance] hideProgressIndicator];
                                       if (response == nil)
                                       {
                                           return;
                                       }
                                       else if ([response objectForKey:@"Error"])
                                       {
                                           [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
                                       }
                                       else
                                       {
                                           if ([[response objectForKey:@"errFlag"] integerValue] == 0)
                                           {
                                               [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[response objectForKey: @"errMsg"]];
                                               NSString *tip;
                                               if (tipForDriver == 0)
                                               {
                                                   tip = [NSString stringWithFormat:NSLocalizedString(@"TIP", @"TIP")];
                                               }
                                               else
                                               {
                                                   tip = [NSString stringWithFormat:@"%ld%%", (long)tipForDriver];
                                               }
                                               [[NSUserDefaults standardUserDefaults] setObject:tip forKey:@"drivertip"];
                                               [[NSUserDefaults standardUserDefaults]synchronize];
                                               NSString *ti = [[NSUserDefaults standardUserDefaults] objectForKey:@"drivertip"];
                                               NSString *tipString = NSLocalizedString(@"TIP", @"TIP");
                                               if (ti.length != 0 && ![ti isEqualToString:tipString])
                                               {
                                                   [customNavigationBarView setRightBarButtonTitle:[NSString stringWithFormat:@"%@\n%@",tipString,ti]];
                                               }
                                               else
                                               {
                                                   [customNavigationBarView setRightBarButtonTitle:tipString];
                                               }
                                           }
                                           else
                                           {
                                               [customNavigationBarView setRightBarButtonTitle:@"TIP"];
                                           }
                                       }
                                   }
                                   else {
                                       
                                       [pi hideProgressIndicator];
                                   }
                               }];
}



/**
 *  polling check Returns nothing and accepts nothing
 */

-(void)checkBookingStatus
{    
    if (isRequestingButtonClicked)
    {
        return;
    }
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud objectForKey:KUDriverEmail];
    [ud objectForKey:KUBookingDate];
    [ud synchronize];
    NSString *d = flStrForObj([ud objectForKey:KUBookingDate]);
    [self checkOngoingAppointmentStatus:d];
}

/**
 *  Polling check
 */

-(void)checkOngoingAppointmentStatus:(NSString *)appDate {
    
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    NSString *currentDate = [Helper getCurrentDateTime];
    @try
    {
        NSDictionary *params = @{@"ent_sess_token":sessionToken,
                                 @"ent_dev_id":deviceID,
                                 @"ent_user_type":@"2",
                                 @"ent_appnt_dt":appDate,
                                 @"ent_date_time":currentDate,
                                 };
        NSLog(@"Check On Going params = %@", params);
        NetworkHandler *networHandler = [NetworkHandler sharedInstance];
        [networHandler composeRequestWithMethod:@"getApptStatus"
                                        paramas:params
                                   onComplition:^(BOOL success, NSDictionary *response){
                                       
                                       NSLog(@"Check On Going response = %@", response);
                                       if (success)
                                       {
                                           [self checkOngoingAppointmentStatusResponse:response];
                                       }
                                   }];
    }
    @catch (NSException *exception) {
        
        [[ProgressIndicator sharedInstance] hideProgressIndicator];
        [self clearUserDefaultsAfterBookingCompletion];
        [self changeContentOfPresentController:nil];
    }
}

-(void)checkOngoingAppointmentStatusResponse:(NSDictionary *)responseDict
{
    [[ProgressIndicator sharedInstance] hideProgressIndicator];
    if (responseDict == nil)
    {
        return;
    }
    else if ([responseDict objectForKey:@"Error"])
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[responseDict objectForKey: @"Error"]];
    }
    else if ([responseDict[@"errFlag"] intValue] == 1 && ([responseDict[@"errNum"] intValue] == 6 || [responseDict[@"errNum"] intValue] == 7 || [responseDict[@"errNum"] intValue] == 96 || [responseDict[@"errNum"] intValue] == 94 ))
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:KDAcheckUserSessionToken];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [XDKAirMenuController relese];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:responseDict[@"errMsg"]];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        PatientViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"splash"];
        self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
        [self unSubscribeToPassengerChannel];
    }
    else if ([[responseDict objectForKey:@"errFlag"] integerValue] == 1 && [responseDict[@"errNum"] intValue] == 49)
    {
        NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
        [ud setObject:flStrForStr(responseDict[@"pub"]) forKey:KNSUPubnubPublishKey];
        [ud setObject:flStrForStr(responseDict[@"sub"]) forKey:kNSUPubnubSubscribeKey];
        [ud setObject:flStrForStr(responseDict[@"serverChn"]) forKey:kNSUServerPubnubChannel];
        [ud setObject:flStrForStr(responseDict[@"presenseChn"]) forKey:kNSUPresensePubnubChannel];
        
        [ud setObject:flStrForStr(responseDict[@"ClientmapKey"]) forKey:kNSUPlaceAPIKey];
        [ud setObject:flStrForStr(responseDict[@"stipeKey"]) forKey:kNSUStripeKey];
        [ud synchronize];
        serverPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUServerPubnubChannel];
        [self clearUserDefaultsAfterBookingCompletion];
        [self changeContentOfPresentController:nil];
    }
    else
    {
        if ([[responseDict objectForKey:@"errFlag"] integerValue] == 0)
        {
            NSDictionary *dictionary;
            if (responseDict[@"data"])
            {
                dictionary =  responseDict[@"data"][0];
                
                NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
                [ud setObject:flStrForStr(responseDict[@"pub"]) forKey:KNSUPubnubPublishKey];
                [ud setObject:flStrForStr(responseDict[@"sub"]) forKey:kNSUPubnubSubscribeKey];
                [ud setObject:flStrForStr(responseDict[@"serverChn"]) forKey:kNSUServerPubnubChannel];
                [ud setObject:flStrForStr(responseDict[@"presenseChn"]) forKey:kNSUPresensePubnubChannel];
                
                [ud setObject:flStrForStr(responseDict[@"ClientmapKey"]) forKey:kNSUPlaceAPIKey];
                [ud setObject:flStrForStr(responseDict[@"stipeKey"]) forKey:kNSUStripeKey];
                [ud synchronize];
                
                serverPubNubChannel = [[NSUserDefaults standardUserDefaults] objectForKey:kNSUServerPubnubChannel];
            }
            else
            {
                dictionary =  responseDict;
            }
            

            shareUrlViaSMS = dictionary[@"share"];
            _currentLatitude = [dictionary[@"pickLat"] doubleValue];
            _currentLongitude = [dictionary[@"pickLong"] doubleValue];
            NSInteger newBookingStatus = [flStrForStr(dictionary[@"status"]) integerValue];
            NSInteger rateStatus = [flStrForStr(dictionary[@"rateStatus"]) integerValue];
            NSArray *driverLatitudeLongitude = [dictionary[@"ltg"] componentsSeparatedByString:@","];
            _driverCurLat = [driverLatitudeLongitude[0] doubleValue];
            _driverCurLong = [driverLatitudeLongitude[1]doubleValue];
            
            desAddr = dictionary[@"dropAddr1"];
            desAddrline2 = dictionary[@"dropAddr2"];
            desLat = [dictionary[@"dropLat"] doubleValue];
            desLong = [dictionary[@"dropLong"] doubleValue];
            
            NSUserDefaults *udPlotting = [NSUserDefaults standardUserDefaults];
            [udPlotting setDouble:desLat   forKey:@"desLat"];
            [udPlotting setDouble:desLong  forKey:@"desLong"];
            [udPlotting setDouble:_driverCurLat forKey:@"driLat"];
            [udPlotting setDouble:_driverCurLong forKey:@"driLong"];
            [udPlotting setObject:flStrForStr(dictionary[@"addr1"]) forKey:@"srcAdd1"];
            [udPlotting setObject:flStrForStr(dictionary[@"addr2"]) forKey:@"srcAdd2"];
            [udPlotting setObject:flStrForStr(dictionary[@"dropAddr1"])   forKey:@"desadd1"];
            [udPlotting setObject:flStrForStr(dictionary[@"dropAddr2"])   forKey:@"desadd2"];
            [udPlotting setObject:flStrForStr(dictionary[@"mobile"]) forKey:@"DriverTelNo"];
            [udPlotting synchronize];
            if (newBookingStatus == 5)
            {
                [self clearUserDefaultsAfterBookingCompletion];
                newBookingStatus = 10;
            }
            else if (newBookingStatus == 4)
            {
                [self clearUserDefaultsAfterBookingCompletion];
                newBookingStatus = 11;
            }
            if (newBookingStatus == 6 || newBookingStatus == 7 || newBookingStatus == 8 || (newBookingStatus == 9 && rateStatus == 1))
            {
                NSUserDefaults *ud =[NSUserDefaults standardUserDefaults];
                [ud setInteger:newBookingStatus forKey:@"STATUSKEY"];
                [ud setObject:flStrForStr(dictionary[@"email"]) forKey:KUDriverEmail];
                [ud setObject:flStrForStr(dictionary[@"apptDt"]) forKey:KUBookingDate];
                [ud setObject:flStrForStr(dictionary[@"bid"]) forKey:@"BOOKINGID"];
                [ud setObject:flStrForStr(dictionary[@"chn"]) forKey:kNSUDriverPubnubChannel];
                [ud setObject:flStrForStr(dictionary[@"model"]) forKey:@"MODEL"];
                [ud setObject:flStrForStr(dictionary[@"plateNo"]) forKey:@"PLATE"];
                [ud setObject:flStrForStr(dictionary[@"carMapImage"]) forKey:@"carMapImage"];
                [ud setObject:flStrForStr(dictionary[@"carImage"]) forKey:@"CARIMAGE"];
                [ud setObject:flStrForStr(dictionary[@"pPic"]) forKey:@"driverPic"];
                [ud setObject:[NSString stringWithFormat:@"%@ %@",flStrForStr(dictionary[@"fName"]), flStrForStr(dictionary[@"lName"])] forKey:@"drivername"];
                [ud setBool:YES forKey:kNSUIsPassengerBookedKey];
                                          
                NSInteger tipForDriver = [dictionary[@"tipPercent"] integerValue];
                NSString *tip;
                if (tipForDriver == 0)
                {
                    tip = [NSString stringWithFormat:NSLocalizedString(@"TIP", @"TIP")];
                }
                else
                {
                    tip = [NSString stringWithFormat:@"%ld%%", (long)tipForDriver];
                }
                [customNavigationBarView setRightBarButtonTitle:tip];
                [ud setObject:tip forKey:@"drivertip"];
                [ud synchronize];
                if(newBookingStatus != 9)
                {
                    [self subscribeOnlyBookedDriver];
                }
                [(PatientAppDelegate*)[[UIApplication sharedApplication] delegate]noPushForceChangingController:dictionary :(int)newBookingStatus];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
                [self changeContentOfPresentController:nil];
            }
        }
        else
        {
            [self changeContentOfPresentController:nil];
        }
    }
}

/**
 *  CREATING A VIEW THAT WILL SHOW THE DRIVER STATUS
 *
 *  @return RETURNS VOID ACCEPT NOTHING
 */

-(void)createDriverMessageShowingView:(NSInteger)bookingStatus {
    
//    UILabel *vb = (UILabel *)[self.view viewWithTag:driverMessageViewTag];
//    NSArray *arr = self.view.subviews;
//    
//    if (![arr containsObject:vb]) {
//        
//        UILabel *msgLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,84,280,40)];
//        msgLabel.tag = driverMessageViewTag;
//        NSString *bookingId= NSLocalizedString(@"BOOKING ID :", @"BOOKING ID :");
//        [Helper setToLabel:msgLabel Text:[NSString stringWithFormat:@" %@",bookingId] WithFont:Roboto_Bold FSize:15 Color:[UIColor blackColor]];
//        msgLabel.textAlignment = NSTextAlignmentCenter;
//        msgLabel.layer.borderWidth = 0.5f;
//        msgLabel.layer.borderColor = [UIColor blackColor].CGColor;
//        msgLabel.backgroundColor = [UIColor colorWithWhite:1.00f alpha:1.000];
//        [self.view addSubview:msgLabel];
//        
//        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//        NSString *appointmentID = @"";
//        if (bookingStatus == kNotificationTypeBookingReachedLocation){
//            appointmentID = [NSString stringWithFormat:@"%@ %@",bookingId,[appointmentID  stringByAppendingString:flStrForObj([ud objectForKey:@"BOOKINGID"])]];
//        }
//        else{
//            appointmentID = [NSString stringWithFormat:@"%@ %@",bookingId,[appointmentID  stringByAppendingString:flStrForObj([ud objectForKey:@"BOOKINGID"])]];
//        }
//        msgLabel.text = appointmentID;
//    }
}


/**
 *  CREATING A VIEW THAT WILL APPEAR AFTER GETTING THE FIRST PUSH
 */

-(void)createDriverArrivedView:(NSInteger )bookingStatus
{
    UIView *vb = [self.view viewWithTag:driverArrivedViewTag];
    NSArray *arr = self.view.subviews;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    if (![arr containsObject:vb])
    {
        UIView *bottomContainerView = [[UIView alloc]initWithFrame:CGRectMake(0,screenHeight-115, screenWidth, 235)];
        bottomContainerView.backgroundColor = UIColorFromRGB(0xffffff);
        [bottomContainerView setHidden:NO];
        bottomContainerView.tag = driverArrivedViewTag;
        
        UIView *getstureView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 115)];
        getstureView.backgroundColor = [UIColor clearColor];
        UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(driverDetailsScreenMove:)];
        
        [getstureView setUserInteractionEnabled:YES];
        [getstureView addGestureRecognizer:panGesture];
        [self.view setUserInteractionEnabled:YES];
        [self.view addGestureRecognizer:panGesture];
        
        UIButton *upDownBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,screenWidth,15)];
        upDownBtn.tag = 11000;
        [upDownBtn setImage:[UIImage imageNamed:@"up_arrow"] forState:UIControlStateNormal];
        [upDownBtn setImage:[UIImage imageNamed:@"down_arrow"] forState:UIControlStateSelected];
        [upDownBtn addTarget:self action:@selector(driverDetailsScreenMoveUsingBtn:) forControlEvents:UIControlEventTouchUpInside];
        [bottomContainerView addSubview:upDownBtn];
        
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(5,upDownBtn.frame.size.height,screenWidth/2-10,25)];
        [Helper setToLabel:timeLabel Text:NSLocalizedString(@"Time : ...", @"Time : ...") WithFont:Roboto_Bold FSize:13 Color:UIColorFromRGB(0x575757)];
        timeLabel.tag = 110011;
        timeLabel.backgroundColor = UIColorFromRGB(0xf1f1f1);
        timeLabel.textAlignment = NSTextAlignmentLeft;
        [bottomContainerView addSubview:timeLabel];
        
        
        UILabel *distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenWidth/2+5,upDownBtn.frame.size.height,screenWidth/2-10,25)];
        [Helper setToLabel:distanceLabel Text:NSLocalizedString(@"Distance : ...", @"Distance : ...") WithFont:Roboto_Bold FSize:13 Color:UIColorFromRGB(0x575757)];
        distanceLabel.tag = 11001;
        distanceLabel.backgroundColor = UIColorFromRGB(0xf1f1f1);
        distanceLabel.textAlignment = NSTextAlignmentRight;
        [bottomContainerView addSubview:distanceLabel];
        
        
        
        UILabel *vehicleName = [[UILabel alloc] initWithFrame:CGRectMake(10,57.5,screenWidth/4,20)];
        [Helper setToLabel:vehicleName Text:flStrForStr([ud objectForKey:@"MODEL"]).uppercaseString WithFont:Roboto_Bold FSize:13 Color:UIColorFromRGB(0x333333)];
        vehicleName.textAlignment = NSTextAlignmentLeft;
        vehicleName.numberOfLines = 1;
        vehicleName.adjustsFontSizeToFitWidth = YES;
        
        vehicleName.tag = 11002;
        [bottomContainerView addSubview:vehicleName];
        
        UILabel *vehicleNumberPlate = [[UILabel alloc] initWithFrame:CGRectMake(10,77.5,screenWidth/4,20)];
        [Helper setToLabel:vehicleNumberPlate Text:flStrForStr([ud objectForKey:@"PLATE"]).uppercaseString WithFont:Roboto_Bold FSize:13 Color:UIColorFromRGB(0x7b7b7b)];
        vehicleNumberPlate.textAlignment = NSTextAlignmentLeft;
        vehicleNumberPlate.tag = 11003;
        vehicleNumberPlate.numberOfLines = 1;
        vehicleNumberPlate.adjustsFontSizeToFitWidth = YES;
        [bottomContainerView addSubview:vehicleNumberPlate];
        
        UIImageView *vehicleImageView = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth/4+15,47.5,60,60)];
        vehicleImageView.layer.cornerRadius = 30.0f;
        vehicleImageView.layer.borderWidth = 1.0f;
        vehicleImageView.layer.borderColor = [UIColor clearColor].CGColor;
        vehicleImageView.layer.masksToBounds = YES;
        vehicleImageView.tag = 11004;
        
        NSString *vehicleImageUrl = [ud objectForKey:@"CARIMAGE"];
        if (vehicleImageUrl.length)
        {
            vehicleImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForOriginalImage, [ud objectForKey:@"CARIMAGE"]];
            [vehicleImageView sd_setImageWithURL:[NSURL URLWithString:vehicleImageUrl]
                                placeholderImage:[UIImage imageNamed:@"carImage"]
                                       completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                           if (error || image == nil)
                                           {
                                               vehicleImageView.image = [UIImage imageNamed:@"carImage"];
                                           }
                                       }];
        }
        else
        {
            vehicleImageView.image = [UIImage imageNamed:@"carImage"];
        }
        [bottomContainerView addSubview:vehicleImageView];
        
        
        UIImageView *driverImageView = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth/2+5,47.5,60,60)];
        driverImageView.layer.cornerRadius = 30.0f;
        driverImageView.layer.borderWidth = 1.0f;
        driverImageView.layer.borderColor = [UIColor colorWithWhite:0.7 alpha:0.5].CGColor;
        driverImageView.layer.masksToBounds = YES;
        driverImageView.tag = 11005;
        NSString *str = [ud objectForKey:@"driverpic"];
        if (!str.length)
        {
            driverImageView.image = [UIImage imageNamed:@"driverImage"];
        }
        else
        {
            NSString *driverImageUrl = [NSString stringWithFormat:@"%@%@",baseUrlForXXHDPIImage,str];
            [driverImageView sd_setImageWithURL:[NSURL URLWithString:driverImageUrl]
                               placeholderImage:[UIImage imageNamed:@"driverImage"]
                                      completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType,NSURL *imgUrl) {
                                          if (error || image == nil)
                                          {
                                              driverImageView.image = [UIImage imageNamed:@"driverImage"];
                                          }
                                      }];
        }
        [bottomContainerView addSubview:driverImageView];
        
        
        UILabel *driverName = [[UILabel alloc] initWithFrame:CGRectMake((screenWidth*3/4)-10,57.5,screenWidth/4,20)];
        [Helper setToLabel:driverName Text:flStrForStr([ud objectForKey:@"drivername"]).uppercaseString WithFont:Roboto_Bold FSize:13 Color:UIColorFromRGB(0x333333)];
        driverName.textAlignment = NSTextAlignmentRight;
        driverName.tag = 11006;
        driverName.numberOfLines = 1;
        driverName.adjustsFontSizeToFitWidth = YES;
        [bottomContainerView addSubview:driverName];
        
        UILabel *driverRating = [[UILabel alloc] initWithFrame:CGRectMake((screenWidth*3/4)-40,77.5,screenWidth/4,20)];
        [Helper setToLabel:driverRating Text:[NSString stringWithFormat:@"%0.01f",[ud floatForKey:@"Rating"]].uppercaseString WithFont:Roboto_Bold FSize:13 Color:UIColorFromRGB(0x7b7b7b)];
        driverRating.textAlignment = NSTextAlignmentRight;
        driverRating.tag = 11007;
        driverRating.numberOfLines = 1;
        driverRating.adjustsFontSizeToFitWidth = YES;
        [bottomContainerView addSubview:driverRating];
        
        UIImageView *driverRateImageView = [[UIImageView alloc] initWithFrame:CGRectMake(screenWidth-30,77.5,20,20)];
        driverRateImageView.image = [UIImage imageNamed:@"star_icon"];
        [bottomContainerView addSubview:driverRateImageView];
        
        
        UIView *greenDot = [[UIView alloc] initWithFrame:CGRectMake(10,125,10,10)];
        greenDot.layer.cornerRadius = 5.0f;
        greenDot.layer.masksToBounds = YES;
        greenDot.backgroundColor = [UIColor greenColor];
        [bottomContainerView addSubview: greenDot];
        
        UILabel *horizonatalLine = [[UILabel alloc] initWithFrame:CGRectMake(15, 138, 1, 25)];
        horizonatalLine.backgroundColor = UIColorFromRGB(0xd2d2d2);
        [bottomContainerView addSubview:horizonatalLine];
        
        UIView *redDot = [[UIView alloc] initWithFrame:CGRectMake(10,165,10,10)];
        redDot.layer.cornerRadius = 5.0f;
        redDot.layer.masksToBounds = YES;
        redDot.backgroundColor = [UIColor redColor];
        [bottomContainerView addSubview: redDot];
        
        UIButton *pickUpAddress = [[UIButton alloc] initWithFrame:CGRectMake(25, 115, screenWidth-35, 35)];
        [Helper setButton:pickUpAddress Text:[NSString stringWithFormat:@"%@ %@", [ud objectForKey:@"srcAdd1"], [ud objectForKey:@"srcAdd2"]] WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
        pickUpAddress.tag = 11008;
        pickUpAddress.titleLabel.textAlignment = NSTextAlignmentCenter;
        pickUpAddress.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        //        [pickUpAddress addTarget:self action:@selector(pickupLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomContainerView addSubview:pickUpAddress];
        
        UILabel *line3Label = [[UILabel alloc] initWithFrame:CGRectMake(20, 150, screenWidth-20, 1)];
        line3Label.backgroundColor = UIColorFromRGB(0xd2d2d2);
        [bottomContainerView addSubview:line3Label];
        
        UIButton *dropOffAddress = [[UIButton alloc] initWithFrame:CGRectMake(25, 151, screenWidth-35, 35)];
        desAddr = [ud objectForKey:@"desadd1"];
        desAddrline2 = [ud objectForKey:@"desadd2"];
        if(!desAddr.length)
        {
            [Helper setButton:dropOffAddress Text:NSLocalizedString(@"Add Drop Off Location", @"Add Drop Off Location") WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
        }
        else
        {
            [Helper setButton:dropOffAddress Text:[NSString stringWithFormat:@"%@ %@", desAddr, desAddrline2] WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
        }
        
        dropOffAddress.tag = 175;
        dropOffAddress.titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        dropOffAddress.titleLabel.textAlignment = NSTextAlignmentCenter;
        [dropOffAddress addTarget:self action:@selector(pickupLocationAction:) forControlEvents:UIControlEventTouchUpInside];
        [bottomContainerView addSubview:dropOffAddress];
        
        UILabel *line4Label = [[UILabel alloc] initWithFrame:CGRectMake(0, 188, screenWidth, 1)];
        line4Label.backgroundColor = UIColorFromRGB(0xd2d2d2);
        [bottomContainerView addSubview:line4Label];
        
        
        UIButton *contactBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 192, (screenWidth-20)/3, 40)];
        [Helper setButton:contactBtn Text:NSLocalizedString(@"CONTACT", @"CONTACT") WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
        [contactBtn setImage:[UIImage imageNamed:@"contact_icon_off"] forState:UIControlStateNormal];
        [contactBtn setImage:[UIImage imageNamed:@"contact_icon_on"] forState:UIControlStateHighlighted];
        contactBtn.tag = 11010;
        [contactBtn addTarget:self action:@selector(contactButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self adjustBtnImageAndTitle:contactBtn];
        [bottomContainerView addSubview:contactBtn];
        
      //  if (bookingStatus == 6)
        {
            UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake((screenWidth-20)/3+15, 192, (screenWidth-20)/3, 40)];
            [Helper setButton:cancelBtn Text:NSLocalizedString(@"CANCEL", @"CANCEL") WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
            [cancelBtn setImage:[UIImage imageNamed:@"cancelBooking_icon_off"] forState:UIControlStateNormal];
            [cancelBtn setImage:[UIImage imageNamed:@"cancelBooking_icon_on"] forState:UIControlStateHighlighted];
            cancelBtn.tag = 11011;
            [cancelBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [self adjustBtnImageAndTitle:cancelBtn];
            [bottomContainerView addSubview:cancelBtn];
        }
        
        UIButton *shareETABtn = [[UIButton alloc] initWithFrame:CGRectMake(((screenWidth-20)*2/3)+20, 192, (screenWidth-20)/3, 40)];
        [Helper setButton:shareETABtn Text:NSLocalizedString(@"SHARE ETA", @"SHARE ETA") WithFont:Roboto_Regular FSize:13 TitleColor:UIColorFromRGB(0x333333) ShadowColor:nil];
        [shareETABtn setImage:[UIImage imageNamed:@"eta_icon_off"] forState:UIControlStateNormal];
        [shareETABtn setImage:[UIImage imageNamed:@"eta_icon_on"] forState:UIControlStateHighlighted];
        shareETABtn.tag = 11012;
        [shareETABtn addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self adjustBtnImageAndTitle:shareETABtn];
        
        [bottomContainerView addSubview:shareETABtn];
        
        [self.view addSubview:bottomContainerView];
    }
}

-(void) adjustBtnImageAndTitle:(UIButton *)button
{
    CGFloat spacing = 2.0;
    CGSize imageSize = button.imageView.frame.size;
    button.titleEdgeInsets = UIEdgeInsetsMake( 0.0, - imageSize.width, - (imageSize.height + spacing), 0.0);
    CGSize titleSize = button.titleLabel.frame.size;
    button.imageEdgeInsets = UIEdgeInsetsMake(- (titleSize.height + spacing), 0.0, 0.0, - titleSize.width);
}

-(IBAction)driverDetailsScreenMove:(UIPanGestureRecognizer *)sender
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView *driverDetailsView = [self.view viewWithTag:driverArrivedViewTag];
    CGPoint translate = [sender translationInView:driverDetailsView];
    if (translate.y<0)
    {
        CGRect frameTopview = driverDetailsView.frame;
        frameTopview.origin.y = screenHeight-235;
        UIButton *upDownBtn = (UIButton *)[driverDetailsView viewWithTag:11000];
        upDownBtn.selected = YES;
        [upDownBtn setImage:[UIImage imageNamed:@"down_arrow"] forState:UIControlStateSelected];
        
        [UIView animateWithDuration:0.2f animations:^{
            driverDetailsView.frame = frameTopview;
        }];
    }
    else
    {
        CGRect frameTopview = driverDetailsView.frame;
        frameTopview.origin.y = screenHeight-115;
        UIButton *upDownBtn = (UIButton *)[driverDetailsView viewWithTag:11000];
        upDownBtn.selected = NO;
        [upDownBtn setImage:[UIImage imageNamed:@"up_arrow"] forState:UIControlStateNormal];
        
        [UIView animateWithDuration:0.2f animations:^{
            driverDetailsView.frame = frameTopview;
        }];
    }
}

-(IBAction)driverDetailsScreenMoveUsingBtn:(UIButton *)sender
{
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView *driverDetailsView = [self.view viewWithTag:driverArrivedViewTag];
    UIButton *upDownBtn = (UIButton *)sender;
    if(upDownBtn.isSelected)
    {
        upDownBtn.selected = NO;
        [upDownBtn setImage:[UIImage imageNamed:@"up_arrow"] forState:UIControlStateNormal];
        CGRect frameTopview = driverDetailsView.frame;
        frameTopview.origin.y = screenHeight-115;
        
        [UIView animateWithDuration:0.2f animations:^{
            driverDetailsView.frame = frameTopview;
        }];
    }
    else
    {
        upDownBtn.selected = YES;
        [upDownBtn setImage:[UIImage imageNamed:@"down_arrow"] forState:UIControlStateSelected];
        CGRect frameTopview = driverDetailsView.frame;
        frameTopview.origin.y = screenHeight-235;
        
        [UIView animateWithDuration:0.2f animations:^{
            driverDetailsView.frame = frameTopview;
        }];
    }
}


-(void)detailsButtonClicked:(id)sender
{
    UIButton *mBut = (UIButton *)sender;
    if(mBut.isSelected)
    {
        mBut.selected = NO;
        [self removeDriverDetailScreen];
    }
    else
    {
        mBut.selected = YES;
        [self createDriverDetailScreen];
        
    }
}

/**
 *  view that will add on the center of the window (Mainly showing share contact and cancel trip)
 */

-(void)createDriverDetailScreen
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    UIView *coverView = [[UIView alloc]initWithFrame:CGRectMake(0,0,screenWidth,screenHeight-110)];
    coverView.tag = 499;
    coverView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapForDetailButtonClicked:)];
    [coverView addGestureRecognizer:tapRecognizer];
    
    UIView *cd = [[UIView alloc]initWithFrame:CGRectMake(22,(screenHeight-64)/2-screenWidth/4,274, _heightCenter)];
    cd.tag = 500;
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    shareButton.frame = CGRectMake(0,0,_width,52);
    shareButton.tag = 501;
    [Helper setButton:shareButton Text:NSLocalizedString(@"SHARE ETA", @"SHARE ETA") WithFont:Roboto_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    shareButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    shareButton.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [shareButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [shareButton setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];
    [shareButton addTarget:self action:@selector(shareButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cd addSubview:shareButton];
    
    UILabel *line1Label = [[UILabel alloc]initWithFrame:CGRectMake(20,52,_widthLabel,1)];
    line1Label.tag = 101;
    line1Label.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"drivermenu_popup_divider"]];
    [cd addSubview:line1Label];
    
    UIButton *contactButton = [UIButton buttonWithType:UIButtonTypeCustom];
    contactButton.frame = CGRectMake(0,53,_width,52);
    contactButton.tag = 502;
    [Helper setButton:contactButton Text:NSLocalizedString(@"CONTACT DRIVER", @"CONTACT DRIVER") WithFont:Roboto_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
    contactButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    contactButton.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [contactButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [contactButton setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];
    [contactButton addTarget:self action:@selector(contactButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cd addSubview:contactButton];
    
    NSInteger bookingStatus = [[NSUserDefaults standardUserDefaults] integerForKey:@"STATUSKEY"];
    
    if (bookingStatus == kNotificationTypeBookingStarted) {
        
        
        cd.frame = CGRectMake(22,508/2-160/2,274, _heightCenter-54);
    }
    else {
        
        UILabel *line2Label = [[UILabel alloc]initWithFrame:CGRectMake(20,105,_widthLabel,1)];
        line2Label.tag = 102;
        line2Label.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"drivermenu_popup_divider"]];
        [cd addSubview:line2Label];
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelButton.frame = CGRectMake(0,106, 253,52);
        cancelButton.tag = 502;
        [Helper setButton:cancelButton Text:NSLocalizedString(@"CANCEL TRIP", @"CANCEL TRIP") WithFont:Roboto_Regular FSize:15 TitleColor:[UIColor whiteColor] ShadowColor:nil];
        cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        cancelButton.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
        [cancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
        [cancelButton setTitleColor:UIColorFromRGB(0xffcc00) forState:UIControlStateHighlighted];
        [cancelButton addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cd addSubview:cancelButton];
        
    }
    cd.backgroundColor = [UIColor colorWithWhite:0.151 alpha:1.000];
    [coverView addSubview:cd];
    
    [self.view addSubview:coverView];
    
}


-(void)removeDriverDetailScreen
{
    UIView *cd = (UIView *)[self.view viewWithTag:499];
    [cd removeFromSuperview];
}

- (void)handleTapForDetailButtonClicked:(UITapGestureRecognizer*)recognizer
{
    UIButton *mBtn = (UIButton *)[self.view viewWithTag:201];
    [self detailsButtonClicked:mBtn];
}

-(void)shareButtonClicked:(id)sender{
    
    //    UIButton *mBtn = (UIButton *)[self.view viewWithTag:201];
    //    [self detailsButtonClicked:mBtn];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:NSLocalizedString(@"Share", @"Share")
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel")
                                              destructiveButtonTitle:nil
                                                   otherButtonTitles:@"Email",@"SMS",nil];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

-(void)contactButtonClicked:(id)sender
{
    NSString *number = [[NSUserDefaults standardUserDefaults]objectForKey:@"DriverTelNo"];
    NSURL* callUrl=[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",flStrForStr(number)]];
    
    //check  Call Function available only in iphone
    if([[UIApplication sharedApplication] canOpenURL:callUrl])
    {
        [[UIApplication sharedApplication] openURL:callUrl];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"ALERT", @"ALERT")
                                                     message:NSLocalizedString(@"This function is only available on the iPhone", @"This function is only available on the iPhone")  delegate:nil
                                           cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                           otherButtonTitles:nil];
        [alert show];
    }
}

-(void)buttonClicked:(id)sender
{
    [self sendRequestForCancelAppointment];
}

#pragma mark- Service Call

-(void)sendRequestForCancelAppointment
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi showPIOnView:self.view withMessage:NSLocalizedString(@"Please Wait...", @"Please Wait...")];
    
    //setup parameters
    NSString *sessionToken = [[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken];
    
    NSString *deviceID = @"";
    if (IS_SIMULATOR)
    {
        deviceID = kPMDTestDeviceidKey;
    }
    else
    {
        deviceID = [[NSUserDefaults standardUserDefaults]objectForKey:kPMDDeviceIdKey];
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *driverEmail  = flStrForStr([ud objectForKey:KUDriverEmail]);
    NSString *appointmntDate = flStrForStr([ud objectForKey:KUBookingDate]);
    NSString *bookingID = flStrForStr([ud objectForKey:@"BOOKINGID"]);
    NSString *currentDate = [Helper getCurrentDateTime];
    
    NSDictionary *params = @{@"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID,
                             @"ent_dri_email":driverEmail,
                             @"ent_bid":bookingID,
                             @"ent_appnt_dt":appointmntDate,
                             @"ent_date_time":currentDate,
                             };
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:kSMCancelAppointment
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       //handle success response
                                       [self parseCancelAppointmentResponse:response];
                                   }
                               }];
}

-(void)parseCancelAppointmentResponse:(NSDictionary*)response
{
    ProgressIndicator *pi = [ProgressIndicator sharedInstance];
    [pi hideProgressIndicator];
    if (response == nil)
    {
        return;
    }
    else if([response[@"errFlag"] integerValue] == 0 && [response[@"errNum"] integerValue] == 42)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
        [self getCurrentLocation:nil];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        [self getAddress:position];
        [self publishPubNubStream];
        [self showBottomViews];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
    else if([response[@"errFlag"] integerValue] == 0 && [response[@"errNum"] integerValue] == 43)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"JOBCOMPLETED" object:nil userInfo:nil];
        [self getCurrentLocation:nil];
        CLLocationCoordinate2D position = CLLocationCoordinate2DMake(_currentLatitude, _currentLongitude);
        [self getAddress:position];
        [self publishPubNubStream];
        [self showBottomViews];
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
    else
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:response[@"errMsg"]];
    }
}

#pragma mark -
#pragma mark UIActionSheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        [self EmaiSharing];
    }
    else if (buttonIndex == 1) {
        
        [self shareViaSMS];
    }
    
}

-(void)shareViaSMS
{
    if(![MFMessageComposeViewController canSendText])
    {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ALERT", @"ALERT")
                                                               message:NSLocalizedString(@"Your device doesn't support SMS!", @"Your device doesn't support SMS!")
                                                              delegate:nil
                                                     cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                                     otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *shareUrl = shareUrlViaSMS;
    NSString *str1 = NSLocalizedString(@"Hi, You can track me live on Safar", @"Hi, You can track me live on Safar");
    NSString *message = [NSString stringWithFormat:@"%@\n %@",str1,shareUrl];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [[messageController navigationBar] setTintColor: [UIColor blackColor]];
    messageController.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
    [messageController setBody:message];
    
    _isSelectinLocation = YES;
    [self presentViewController:messageController animated:YES completion:nil];
}


- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result)
    {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:NSLocalizedString(@"Failed to send !", @"Failed to send !") delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
            [warningAlert show];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)EmaiSharing
{
    if ([MFMailComposeViewController canSendMail])
    {
        NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:Roboto_Bold size:14], NSFontAttributeName, [UIColor blackColor], NSForegroundColorAttributeName, nil];
        
        [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        mailer = [[MFMailComposeViewController alloc] init];
        mailer.mailComposeDelegate = self;
        [[mailer navigationBar] setTintColor: [UIColor blackColor]];
        mailer.navigationBar.titleTextAttributes = [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        
        
        NSString *str1 = NSLocalizedString(@"Hi, You can track me live on Safar", @"Hi, You can track me live on Safar");
        [mailer setSubject:NSLocalizedString(@"Track me live on Safar", @"Track me live on Safar")];
        NSArray *toRecipents = [NSArray arrayWithObject:@""];
        NSMutableString* message =[NSMutableString stringWithFormat:@"%@\n %@",str1,flStrForStr(shareUrlViaSMS)];

        
        [mailer setMessageBody:message isHTML:NO];
        [mailer setToRecipients:toRecipents];
        NSString *strURL = [NSString stringWithFormat:@"%@",imgLinkForSharing];
        UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:strURL]]];
        
        NSData *photoData = UIImageJPEGRepresentation(image,1);
        [mailer addAttachmentData:photoData mimeType:@"image/jpg" fileName:[NSString stringWithFormat:@"photo.png"]];
        
        [self presentViewController:mailer animated:YES completion:^{
                        [[UINavigationBar appearance] setTitleTextAttributes:attributes];
        }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Failure", @"Failure")
                                                        message:NSLocalizedString(@"Your device doesn't support the composer sheet", @"Your device doesn't support the composer sheet")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - MFMailComposeViewController Delegate
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller dismissViewControllerAnimated:YES completion:nil];
}

-(void)updateDistance:(NSString*)distane estimateTime:(NSString*)eta
{
    UIView *btmView = [self.view viewWithTag:driverArrivedViewTag];
    
    UILabel *distanceLabel = (UILabel*)[btmView viewWithTag:11001];
    distanceLabel.text = [NSString stringWithFormat:@"%@ %@",distanceLabelForLocalization,distane];
    
    UILabel *timeLabel = (UILabel*)[btmView viewWithTag:110011];
    timeLabel.text = [NSString stringWithFormat:@"%@ %@",timeLabelForLocalization,eta];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DRIVERONTHEWAY" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"DRIVERREACHED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"JOBSTARTED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"JOBCOMPLETED" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationBookingConfirmationNameKey object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationLocationServicesChangedNameKey object:nil];
}

@end
