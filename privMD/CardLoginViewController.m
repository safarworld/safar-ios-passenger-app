//
//  CardLoginViewController.m
//  privMD
//
//  Created by Rahul Sharma on 13/02/14.
//  Copyright (c) 2014 Rahul Sharma. All rights reserved.
//

#import "CardLoginViewController.h"
#import "MapViewController.h"
#import "CardIO.h"
#import "UploadFiles.h"
#import "ViewController.h"
#import "Database.h"
#import "Entity.h"
#import "PTKTextField.h"
#import "PKView+Private.h"

#define kPayPalEnvironment PayPalEnvironmentSandbox

@interface CardLoginViewController ()<CardIOPaymentViewControllerDelegate>
@property(nonatomic,strong)PTKCard *card;
@property(nonatomic,strong)STPCard *stripeCard;
@end

@implementation CardLoginViewController

@synthesize navNextButton;
@synthesize getSignupDetails;
@synthesize scanButton;
@synthesize infoLabel;
@synthesize cvvLabel;
@synthesize postalLabel;
@synthesize expLabel;
@synthesize doneButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

//pkviewDelegate
- (void) paymentView:(PTKView*)paymentView withCard:(PTKCard *)card isValid:(BOOL)valid
{
    _card = card;
}

- (void)setupCardPostalField
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    postalText = [[UITextField alloc] initWithFrame:CGRectMake(screenWidth-30,0,55,20)];
    postalText.delegate = self;
    postalText.placeholder = NSLocalizedString(@"Postal", @"Postal");
    postalText.keyboardType = UIKeyboardTypeNumberPad;
    postalText.textColor = [UIColor lightGrayColor];
    [postalText.layer setMasksToBounds:YES];
    [self.view addSubview:postalText];
}


- (void)viewDidLoad
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.hidesBackButton = YES;
    self.infoLabel.text = @"";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"login_bg.png"]];
    self.paymentView = [[PTKView alloc] initWithFrame:CGRectMake(10, 70, screenWidth-50, 43)];
    self.paymentView.delegate = self;
    [self.view addSubview:self.paymentView];
    [self setUpFrameForScanButton];
}

-(void)setUpFrameForScanButton
{
    CGRect frameOfDoneButton = doneButton.frame;
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    frameOfDoneButton.origin.x = screenWidth-50;
    frameOfDoneButton.origin.y = 70;
    frameOfDoneButton.size.height = 43;
    frameOfDoneButton.size.width = 43;
    doneButton.frame = frameOfDoneButton;
    [doneButton setTitle:NSLocalizedString(@"Done", @"Done") forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [doneButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    doneButton.titleLabel.font = [UIFont fontWithName:Roboto_Regular size:13];
    
    [scanButton setTitle:NSLocalizedString(@"Scan the card", @"Scan the card") forState:UIControlStateNormal];
    [scanButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [scanButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    scanButton.titleLabel.font = [UIFont fontWithName:Roboto_Regular size:13];
    [scanButton addTarget:self action:@selector(scanCardClicked:) forControlEvents:UIControlEventTouchUpInside];
}

-(void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = NO;
}

-(void)viewDidAppear:(BOOL)animated
{
    if((_isComingFromPayment ==1)||(_isComingFromPayment == 2))
    {
        if (_isComingFromPayment == 2)
        {
            CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
            self.paymentView.frame = CGRectMake(10,7,screenWidth-50,43);
            CGRect frameOfDoneButton = doneButton.frame;
            frameOfDoneButton.origin.y = 7;
            doneButton.frame = frameOfDoneButton;
            CGRect fr = scanButton.frame;
            fr.origin.y = 60;
            scanButton.frame = fr;
        }
        self.navigationItem.title = NSLocalizedString(@"ADD CARD", @"ADD CARD");
        [self createNavLeftButton];
    }
    else
    {
        [self createNavView];
        [self createNavRightButton];
    }
}

-(void)createNavView
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIView *navView = [[UIView alloc]initWithFrame:CGRectMake(screenWidth/4, -10, screenWidth/2, 50)];
    UILabel *navTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,0, screenWidth/2-10, 30)];
    navTitle.text = NSLocalizedString(@"CREATE ACCOUNT", @"CREATE ACCOUNT");
    navTitle.textColor = UIColorFromRGB(0xffffff);
    navTitle.textAlignment = NSTextAlignmentCenter;
    navTitle.font = [UIFont fontWithName:Roboto_Light size:13];
    [navView addSubview:navTitle];
    
    UIImageView *navImage = [[UIImageView alloc]initWithFrame:CGRectMake(0,30,screenWidth/2-10,7)];
    navImage.image = [UIImage imageNamed:@"signup_timer_third"];
    [navView addSubview:navImage];
    self.navigationItem.titleView = navView;
}

-(void)createNavRightButton
{
    navNextButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    [navNextButton setFrame:CGRectMake(0,0,buttonImage.size.width,buttonImage.size.height)];

    [navNextButton addTarget:self action:@selector(skipButtonClicked) forControlEvents:UIControlEventTouchUpInside];
  
    [Helper setButton:navNextButton Text:NSLocalizedString(@"SKIP", @"SKIP") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navNextButton setTitle:NSLocalizedString(@"SKIP", @"SKIP") forState:UIControlStateNormal];
    [navNextButton setTitle:NSLocalizedString(@"SKIP", @"SKIP") forState:UIControlStateSelected];
    [navNextButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navNextButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    [navNextButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navNextButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setRightBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void) createNavLeftButton
{
    UIImage *buttonImage = [UIImage imageNamed:@"signup_btn_back_bg_on.png"];
    UIButton *navCancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    
    [navCancelButton addTarget:self action:@selector(cancelButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [navCancelButton setFrame:CGRectMake(0.0f,0.0f,buttonImage.size.width,buttonImage.size.height)];

    [Helper setButton:navCancelButton Text:NSLocalizedString(@"BACK", @"BACK") WithFont:Roboto_Light FSize:11 TitleColor:[UIColor blueColor] ShadowColor:nil];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateNormal];
    [navCancelButton setTitle:NSLocalizedString(@"BACK", @"BACK") forState:UIControlStateSelected];
    [navCancelButton setTitleColor:UIColorFromRGB(0xffffff) forState:UIControlStateNormal];
    [navCancelButton setTitleColor:UIColorFromRGB(0x333333) forState:UIControlStateHighlighted];
    navCancelButton.titleLabel.font = [UIFont fontWithName:Roboto_Light size:11];
    [navCancelButton setBackgroundImage:buttonImage forState:UIControlStateHighlighted];
    
    // Create a container bar button
    UIBarButtonItem *containingcancelButton = [[UIBarButtonItem alloc] initWithCustomView:navCancelButton];
    UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                       target:nil action:nil];
    negativeSpacer.width = -16;// it was -6 in iOS 6  you can set this as per your preference
    [self.navigationItem setLeftBarButtonItems:[NSArray arrayWithObjects:negativeSpacer,containingcancelButton, nil] animated:NO];
}

-(void)checkMandatoryFields
{
    NSString *cardno = _card.number;
    NSUInteger cardexpMnt = _card.expMonth;
    NSUInteger cardexpYr = _card.expYear;
    NSString *cardcvc = _card.cvc;
    
    if (((unsigned long)cardno.length == 0) && ((unsigned long)infoLabel.text.length== 0))
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter Card No", @"Enter Card No")];
        
    }
    else if (((unsigned long)cardcvc.length == 0) && ((unsigned long)infoLabel.text.length== 0))
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter CVC", @"Enter CVC")];
    }
    else if (((unsigned long)infoLabel.text.length== 0) && (((unsigned long)cardexpMnt == 0) || ((unsigned long)cardexpYr == 0)))
    {
        [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:NSLocalizedString(@"Enter expiry details", @"Enter expiry details")];
    }
    else
    {
        checkMandatoryCard = YES;
        checkMandatoryCamera = YES;
    }
}

-(void)skipButtonClicked
{
    [self.view endEditing:YES];
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
    ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
    self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
}

- (IBAction)doneButtonClicked:(id)sender
{
    [self.view endEditing:YES];
    if((_isComingFromPayment == 1) || (_isComingFromPayment == 2))
    {
        [self checkMandatoryFields];
        if(checkMandatoryCard)
        {
            [self callStripeForCreatingToken];
        }
    }
    else
    {
        [self checkMandatoryFields];
        if(checkMandatoryCard)
        {
            [self callStripeForCreatingToken];
        }
    }
}

-(void)callStripeForCreatingToken
{
    [[ProgressIndicator sharedInstance]showPIOnView:self.view withMessage:NSLocalizedString(@"Adding Card...", @"Adding Card...")];
    self.stripeCard = [[STPCard alloc] init];
    self.stripeCard.number = _card.number;
    self.stripeCard.cvc = _card.cvc;
    self.stripeCard.expMonth = _card.expMonth;
    self.stripeCard.expYear = _card.expYear;
    [self performStripeOperation];
}


- (void)performStripeOperation
{
    //1
    self.doneButton.enabled = NO;
    //2
    NSString * stripeKey = flStrForStr([[NSUserDefaults standardUserDefaults]  objectForKey:kNSUStripeKey])
    ;
    if (!stripeKey.length)
    {
        stripeKey = kPMDStripeLiveKey;
    }
    
    [Stripe createTokenWithCard:self.stripeCard publishableKey:stripeKey completion:^(STPToken *token , NSError *error)
    {
        NSString *accesstoken = @"";
        if (error)
        {
            [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:[error localizedDescription]];
            
            ProgressIndicator *pi = [ProgressIndicator sharedInstance];
            [pi hideProgressIndicator];
            
            if(_isComingFromPayment == 2)
            {
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if(_isComingFromPayment == 3)
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            }
            else
            {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else if(token != nil)
        {
            accesstoken = token.tokenId;
            [self sendServiceAddCardsDetails:accesstoken];
        }
    }];
}

-(void)cancelButtonClicked
{
    if(_isComingFromPayment == 2)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - User Actions

- (void)scanCardClicked:(id)sender
{
    CardIOPaymentViewController *scanViewController = [[CardIOPaymentViewController alloc] initWithPaymentDelegate:self];
    scanViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:Roboto_Bold size:14], NSFontAttributeName, [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    [scanViewController.navigationBar setTitleTextAttributes:attributes];
    scanViewController.navigationBar.tintColor = [UIColor whiteColor];
    
    scanViewController.disableManualEntryButtons = YES;
    scanViewController.collectCVV = YES;
    [self presentViewController:scanViewController animated:YES completion:nil];
}

+ (BOOL)canReadCardWithCamera
{
    return YES;
}


#pragma mark - CardIOPaymentViewControllerDelegate

- (void)userDidProvideCreditCardInfo:(CardIOCreditCardInfo *)info inPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.infoLabel.text = [NSString stringWithFormat:@"%@", info.cardNumber]; //info.redactedCardNumber
    
    NSString *str = [NSString stringWithFormat:@"%lu",(unsigned long)info.expiryYear];
    str = [str substringFromIndex:2];
    
    self.expLabel.text = [NSString stringWithFormat:@"%02lu/%@",(unsigned long)info.expiryMonth,str];
    self.cvvLabel.text = [NSString stringWithFormat:@"%@",info.cvv];
    self.postalLabel.text = [NSString stringWithFormat:@"%02lu",(unsigned long)info.postalCode];
    
    [self startWithCardNumber:info.cardNumber];
    [self startWithExpNo:self.expLabel.text];
    [self startWithCVVNumber:self.cvvLabel.text];
    
    UIImageView *cardTypeImageView = nil;
    [cardTypeImageView setImage:[CardIOCreditCardInfo logoForCardType:info.cardType]];
}

- (void)userDidCancelPaymentViewController:(CardIOPaymentViewController *)paymentViewController
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)startWithCardNumber:(NSString*)cardNumberString
{
    PTKCardNumber *cardNumber = [PTKCardNumber cardNumberWithString:cardNumberString];
    
    if ( ![cardNumber isPartiallyValid] )
        return;
    
    self.paymentView.cardNumberField.text = [cardNumber formattedStringWithTrail];
    
    [self.paymentView setPlaceholderToCardType];
    
    if ([cardNumber isValid])
    {
        [self.paymentView textFieldIsValid:self.paymentView.cardNumberField];
        [self.paymentView stateMeta];
        
    } else if ([cardNumber isValidLength] && ![cardNumber isValidLuhn])
    {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardNumberField withErrors:YES];
        
    } else if (![cardNumber isValidLength])
    {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardNumberField withErrors:NO];
    }
}

-(void)startWithExpNo:(NSString*)expDetailsString
{
    PTKCardExpiry *cardExpiry = [PTKCardExpiry cardExpiryWithString:expDetailsString];
    
    if (![cardExpiry isPartiallyValid])
        return ;
    
    // Only support shorthand year
    if ([cardExpiry formattedString].length > 5)
        return ;
    
    self.paymentView.cardExpiryField.text = [cardExpiry formattedStringWithTrail];
    
    if ([cardExpiry isValid]) {
        [self.paymentView textFieldIsValid:self.paymentView.cardExpiryField];
        [self.paymentView stateCardCVC];
        
    } else if ([cardExpiry isValidLength] && ![cardExpiry isValidDate]) {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardExpiryField withErrors:YES];
    } else if (![cardExpiry isValidLength]) {
        [self.paymentView textFieldIsInvalid:self.paymentView.cardExpiryField withErrors:NO];
    }
}

-(void)startWithCVVNumber:(NSString*)CVVNumberString
{
    PTKCardCVC *CVVNumber = [PTKCardCVC cardCVCWithString:CVVNumberString];
    
    if ( ![CVVNumber isPartiallyValid] )
        return;
    
    // Only support shorthand year
    if ([CVVNumber formattedString].length > 3)
        return ;
    
    self.paymentView.cardCVCField.text = [CVVNumber formattedStringWithTrail];
    if ([CVVNumber isValid]) {
        [self.paymentView textFieldIsValid:self.paymentView.cardCVCField];
        [self.paymentView stateMeta];
        [self.paymentView.cardNumberField resignFirstResponder];
        [self.paymentView.cardExpiryField resignFirstResponder];
        [self.paymentView.cardCVCField resignFirstResponder];
        [self doneButtonClicked:self];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WebService call

-(void)sendServiceAddCardsDetails:(NSString *)token
{
    NSString *deviceId;
    if (IS_SIMULATOR) {
        deviceId = kPMDTestDeviceidKey;
    }
    else {
        deviceId = [[NSUserDefaults standardUserDefaults] objectForKey:kPMDDeviceIdKey];
    }
    
    NSDictionary *params = @{
                             @"ent_sess_token":[[NSUserDefaults standardUserDefaults]objectForKey:KDAcheckUserSessionToken],
                             @"ent_dev_id":deviceId,
                             @"ent_token":token,
                             @"ent_date_time":[Helper getCurrentDateTime],
                             };
    
    //setup request
    NetworkHandler *networHandler = [NetworkHandler sharedInstance];
    [networHandler composeRequestWithMethod:@"addCard"
                                    paramas:params
                               onComplition:^(BOOL success, NSDictionary *response){
                                   
                                   if (success)
                                   {
                                       [self addCardDetails:response];
                                   }
                               }];
    

    
}

-(void)addCardDetails:(NSDictionary *)response
{
    
    if (!response)
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error") message:[response objectForKey:@"Message"] delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", @"OK") otherButtonTitles:nil];
        [alertView show];
        
    }
    else if ([response objectForKey:@"Error"])
    {
        ProgressIndicator *pi = [ProgressIndicator sharedInstance];
        [pi hideProgressIndicator];
        [Helper showAlertWithTitle:NSLocalizedString(@"Error", @"Error") Message:[response objectForKey:@"Error"]];
        
    }
    else
    {
        
        NSDictionary *dictResponse= [response mutableCopy];
        if ([[dictResponse objectForKey:@"errFlag"] intValue] == 0)
        {
             _arrayContainingCardInfo = dictResponse[@"cards"];
            if (!_arrayContainingCardInfo || !_arrayContainingCardInfo.count){
                
            }
            else
            {
                [self addInDataBase];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
            }
            if(_isComingFromPayment == 2)
            {
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if(_isComingFromPayment == 3)
            {
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];
            }
            else
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationNewCardAddedNameKey object:nil userInfo:nil];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
        else if ([[dictResponse objectForKey:@"errFlag"] intValue] == 1)
        {
            if(_isComingFromPayment == 2)
            {
                ProgressIndicator *pi = [ProgressIndicator sharedInstance];
                [pi hideProgressIndicator];
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
            else if(_isComingFromPayment == 3)
            {
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
                ViewController *viewcontrolelr = [storyboard instantiateViewControllerWithIdentifier:@"home"];
                self.navigationController.viewControllers = [NSArray arrayWithObjects:viewcontrolelr, nil];

            }
            else {
                
                [Helper showAlertWithTitle:NSLocalizedString(@"Message", @"Message") Message:dictResponse[@"errMsg"]];
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}

-(void)addInDataBase
{
    Database *db = [[Database alloc] init];
    for (int i =0; i<_arrayContainingCardInfo.count; i++)
    {
        NSString *str = _arrayContainingCardInfo[i][@"id"];
        
        [self checkCampaignIdAddedOrNot:str:i];
        if(isPresentInDBalready == 1)
        {
            
        }
        else
        {
            [db makeDataBaseEntry:_arrayContainingCardInfo[i]];
        }
    }
}

- (void)checkCampaignIdAddedOrNot:(NSString *)cardId :(int)arrIndex
{
    isPresentInDBalready = 0;
    NSArray *array = [Database getCardDetails];
    if ([array count]== 0)
    {
        // if(flag)
        //[self AddToFavButtonClicked:nil];
    }
    else
    {
        for(int i=0 ; i<[array count];i++)
        {
            Entity *fav = [array objectAtIndex:i];
            if ([fav.idCard isEqualToString:_arrayContainingCardInfo[arrIndex][@"id"]])
            {
                isPresentInDBalready = 1;
                
            }
        }
    }
}

@end
