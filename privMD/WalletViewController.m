//
//  WalletViewController.m
//  RoadyoDispatch
//
//  Created by 3Embed on 20/08/16.
//  Copyright © 2016 Rahul Sharma. All rights reserved.
//

#import "WalletViewController.h"
#import "WalletHistoryViewController.h"
#import "XDKAirMenuController.h"
#import "CustomNavigationBar.h"

@interface WalletViewController ()<CustomNavigationBarDelegate, XDKAirMenuDelegate>
{
    float initialOffset_Y;
    float keyboardHeight;

}
@property (strong,nonatomic)  UITextField *activeTextField;
@end

@implementation WalletViewController
@synthesize mainScrollView;
@synthesize activeTextField;
@synthesize addMoneyTextField, promoCodeTextField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addCustomNavigationBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated
{
    mainScrollView.backgroundColor = [UIColor clearColor];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    mainScrollView.contentSize = CGSizeMake(screenSize.size.width, screenSize.size.height);
}

-(void)viewDidAppear:(BOOL)animated
{
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // Register notification when the keyboard will be hide
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}


#pragma mark- Custom Methods

- (void) addCustomNavigationBar
{
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    CustomNavigationBar *customNavigationBarView = [[CustomNavigationBar alloc] initWithFrame:CGRectMake(0, 0, screenWidth, 64)];
    customNavigationBarView.tag = 78;
    customNavigationBarView.delegate = self;
    [customNavigationBarView setTitle:NSLocalizedString(@"WALLET", @"WALLET")];
    [self.view addSubview:customNavigationBarView];
}

-(void)leftBarButtonClicked:(UIButton *)sender
{
    [self menuButtonclicked];
}

- (void)menuButtonclicked
{
    XDKAirMenuController *menu = [XDKAirMenuController sharedMenu];
    if (menu.isMenuOpened)
        [menu closeMenuAnimated];
    else
        [menu openMenuAnimated];
}



#pragma mark - TextFields

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeTextField = textField;
    [self moveViewUp:textField andKeyboardHeight:keyboardHeight];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField becomeFirstResponder];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if ([textField.text length] > 0)
    {
        PMDReachabilityWrapper *reachability = [PMDReachabilityWrapper sharedInstance];
        if ([reachability isNetworkAvailable])
        {
            if ([textField isEqual:addMoneyTextField])
            {
                NSLog(@"Call service to add Money into wallet");
            }
            else if ([textField isEqual:promoCodeTextField])
            {
                NSLog(@"Call service to check promo code");
            }
        }
        else
        {
            [[ProgressIndicator sharedInstance] showMessage:@"No Network" On:self.view];
        }
    }
    self.activeTextField = nil;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

/*--------------------------------------------*/
#pragma -marks Keyboard Appear and Hide Methods
/*--------------------------------------------*/
/**
 *  Keyboard will be shown on Notification
 *
 *  @param Notification Notification
 */
- (void)keyboardWillShown:(NSNotification*)notification
{
    // Get the size of the keyboard.
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    //Given size may not account for screen rotation
    keyboardHeight = MIN(keyboardSize.height,keyboardSize.width);
    [self moveViewUp:activeTextField andKeyboardHeight:keyboardHeight];
}

/**
 *  Keyboard will be Hidden on Notification
 *
 *  @param notification notification
 */
- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    [self moveViewDown];
}
/**
 *  Scroll up when keyboard appears
 */
- (void)moveViewUp:(UITextField *)textfield andKeyboardHeight:(float)height
{
    float textfieldMaxY = CGRectGetMinY([[[textfield superview] superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([[textfield superview] superview].frame);
    textfieldMaxY = textfieldMaxY + CGRectGetMinY([textfield superview].frame);
    
    textfieldMaxY = textfieldMaxY + CGRectGetMaxY(textfield.frame);
    
    float remainder = CGRectGetHeight(self.view.frame) - (textfieldMaxY + height) + initialOffset_Y;
    if (remainder >= 0)
    {
        return;
    }
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y - remainder);
                     }];
    [mainScrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)];
}

/**
 *  Scroll down when keyboard dismiss
 */
- (void)moveViewDown
{
    mainScrollView.contentOffset = CGPointMake(0, initialOffset_Y);
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"walletHistoryVC"])
    {
        NSLog(@"Add data to next VC");
    }
}

- (IBAction)currentBalanceBtnAction:(id)sender
{
    [self performSegueWithIdentifier:@"walletHistoryVC" sender:self];
}

- (IBAction)addMoneyBtn1Action:(id)sender
{
    addMoneyTextField.text = @"399";
}

- (IBAction)addMoneyBtn2Action:(id)sender
{
    addMoneyTextField.text = @"999";
}

- (IBAction)addMoneyBtn3Action:(id)sender
{
    addMoneyTextField.text = @"1399";
}

- (IBAction)promoCodeApplyBtnAction:(id)sender
{
    NSLog(@"Call service to check promo code");
}

- (IBAction)addMoneyBtnAction:(id)sender
{
    NSLog(@"Call service to add Money into wallet");
}

@end
