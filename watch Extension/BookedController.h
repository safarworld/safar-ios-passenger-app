//
//  BookedController.h
//  SimpleWeather
//
//  Created by 3Embed on 09/03/17.
//  Copyright © 2017 AppCoda. All rights reserved.
//

#import <WatchKit/WatchKit.h>

@interface BookedController : WKInterfaceController
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceMap *map;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceLabel *statusLabel;
@property (strong, nonatomic) UIImage *image;
@end
