//
//  NetworkStatusShowingView.m
//  UBER
//
//  Created by Rahul Sharma on 03/02/15.
//  Copyright (c) 2015 Rahul Sharma. All rights reserved.
//

#import "NetworkStatusShowingView.h"

@implementation NetworkStatusShowingView

static NetworkStatusShowingView *showNetworkStatusView = nil;

+ (id)sharedInstance {
    
    if (!showNetworkStatusView) {
        
        showNetworkStatusView  = [[self alloc] init];
    }
    
    return showNetworkStatusView;
}

-(id)init
{
    self = [super init];
    if (self) {
        // Initialization code

        [self addViewToShowNetworkStatus];
    }
    return self;
}


-(void)addViewToShowNetworkStatus {
    CGFloat screenWidth = [UIScreen mainScreen].bounds.size.width;
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *showStatus = [[UIView alloc]initWithFrame:CGRectMake(0, 64, screenWidth, 44)];
    showStatus.tag = 1000;
    showStatus.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.7];
    UILabel *msg = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenWidth, 44)];
    msg.backgroundColor = [UIColor clearColor];
    msg.textAlignment = NSTextAlignmentCenter;
    msg.text = NSLocalizedString(@"Could not connect to the Network", @"Could not connect to the Network");
    msg.textColor = [UIColor whiteColor];
    [showStatus addSubview:msg];
    [frontWindow addSubview:showStatus];
}

-(void)hide
{
    [UIView animateWithDuration:1.0
                          delay:0.2
                        options: UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                        
                        
                     }
                     completion:^(BOOL finished){
                         
                         UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
                         UIView *showStatus = [frontWindow viewWithTag:1000];
                         [showStatus removeFromSuperview];
                         showNetworkStatusView = nil;
                     }];
}


+(void)removeViewShowingNetworkStatus
{
    UIWindow *frontWindow = [[UIApplication sharedApplication] keyWindow];
    UIView *showStatus = [frontWindow viewWithTag:1000];
    [showStatus removeFromSuperview];
    showNetworkStatusView = nil;
}


@end
