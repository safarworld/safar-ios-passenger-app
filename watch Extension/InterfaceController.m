//
//  InterfaceController.m
//  SimpleWeather WatchKit Extension
//
//  Created by Simon Ng on 12/8/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

#import "InterfaceController.h"


@interface InterfaceController()

@end


@implementation InterfaceController
@synthesize loggedOutMessageLabel;
//@synthesize onCompletion, onCompletionForLiveBooking;


-(void)session:(WCSession *)session activationDidCompleteWithState:(WCSessionActivationState)activationState error:(NSError *)error {
    NSLog(@"New Session activationState: %ld", (long)activationState);
}
-(void)session:(WCSession *)session didReceiveApplicationContext:(NSDictionary<NSString *,id> *)applicationContext {
    NSLog(@"New Session Context: %@", applicationContext);
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    for (NSString *key in applicationContext.allKeys) {
        [defaults setObject:[applicationContext objectForKey:key] forKey:key];
    }
    [defaults synchronize];
}

- (void)awakeWithContext:(id)context {
    [super awakeWithContext:context];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentLatForWatch"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"CurrentLongForWatch"];
    WCSession* session = [WCSession defaultSession];
    session.delegate = self;
    [session activateSession];
    [self getCurrentLocation];
}

- (void)willActivate {
    // This method is called when watch view controller is about to be visible to user
    [super willActivate];
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    if (!sessionToken.length) {
        [loggedOutMessageLabel setHidden:NO];
        return;
    } else {
        [loggedOutMessageLabel setHidden:YES];
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"] != 0 && [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"] != 0) {
        NSString *lat = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"];
        NSString *log = [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"];
        [self sendServiceToGetCurrentVehiclesAndDriver:lat longitude:log];
    }
}

- (void)didDeactivate {
    // This method is called when watch view controller is no longer visible
    [super didDeactivate];
    NSLog(@"Did Deactivate");
}

-(void) getCurrentLocation {
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestWhenInUseAuthorization];
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    CLLocation *location = [locations lastObject];
    NSString *lat = [NSString stringWithFormat:@"%f",location.coordinate.latitude];
    NSString * log = [NSString stringWithFormat:@"%f",location.coordinate.longitude];
    
    [[NSUserDefaults standardUserDefaults]setObject:lat forKey:@"CurrentLatForWatch"];
    [[NSUserDefaults standardUserDefaults]setObject:log forKey:@"CurrentLongForWatch"];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLatForWatch"] == 0 && [[NSUserDefaults standardUserDefaults] objectForKey:@"CurrentLongForWatch"] == 0) {
        [self getCurrentLocation];
    } else {
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self.locationManager stopUpdatingLocation];
        self.locationManager.delegate = nil;
        [self sendServiceToGetCurrentVehiclesAndDriver:lat longitude:log];
        [self requestForGoogleGeocoding:location];
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
    {
        // The user denied your app access to location information.
    }
    else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorNetwork)
    {
        
    }
    else  if ([[error domain] isEqualToString: kCLErrorDomain] && [error code] == kCLErrorDenied)
    {
        
    }
    NSLog(@"Location Not Found");
}

/*
 Get GoogleGeocoding
 @Params lattitude, longitude
 @Return nil.
 */

-(void)requestForGoogleGeocoding:(CLLocation *)location {
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                      
                       NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                       if (error){
                           NSLog(@"Geocode failed with error: %@", error);
                           return;
                       }
                       NSLog(@"Place Mark = %@", placemarks);
                       CLPlacemark *placemark = [placemarks objectAtIndex:0];
                       NSArray *addrArray = placemark.addressDictionary[@"FormattedAddressLines"];
                       NSString *pickupAddress = [addrArray componentsJoinedByString:@", "];
                       NSLog(@"PICKUP ADDRESS - %@", pickupAddress);
                   }];
}


#pragma mark - WebServiceCall - 

-(void)sendServiceToGetCurrentVehiclesAndDriver:(NSString *)latitude longitude:(NSString *)longitude {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *url = [NSURL URLWithString:@"http://188.166.48.202/safar/services_v2.php/getVehilesAndDriversTest"];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:45.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSUserDefaults *shared = [[NSUserDefaults alloc] initWithSuiteName:@"group.safarsiri"];
    NSString *sessionToken = [shared objectForKey:@"ent_sess_tokenSiri"];
    if(!sessionToken.length) {
        sessionToken = @"";
    }
    NSString *deviceID = [shared objectForKey:@"deviceidSiri"];
    if(!deviceID.length) {
        deviceID = @"";
    }
    NSDictionary *params = @{
                             @"lt":latitude,
                             @"lg":longitude,
                             @"ent_sess_token":sessionToken,
                             @"ent_dev_id":deviceID
                             };
    NSError *error = nil;
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [request setHTTPBody:jsonData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(error == nil) {
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            if(error!=nil) {
                NSLog(@"error = %@",error);
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self sortVehiclesAndDriversData:json];
            });
        }
        else{
            NSLog(@"Error : %@",error.description);
        }
    }];
    [postDataTask resume];
}


-(void)sortVehiclesAndDriversData:(NSDictionary *)response {
    if ([response[@"flag"] integerValue] == 1) {
        
    } else if([response[@"flag"] integerValue] == 0) {
        NSMutableArray *types = [response[@"types"] mutableCopy];
        if([types count]){
            NSMutableArray* controllerNames = [NSMutableArray new];
            [controllerNames addObject:@"showDetails"];
            [WKInterfaceController reloadRootControllersWithNames:controllerNames contexts:[NSArray arrayWithObject:types]];
        }
    }
}

@end



