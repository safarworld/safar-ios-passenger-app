//
//  InterfaceController.h
//  SimpleWeather WatchKit Extension
//
//  Created by Simon Ng on 12/8/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <WatchConnectivity/WatchConnectivity.h>

@interface InterfaceController : WKInterfaceController <CLLocationManagerDelegate, WCSessionDelegate>

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (unsafe_unretained, nonatomic) IBOutlet WKInterfaceLabel *loggedOutMessageLabel;


@end
